-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 24, 2019 at 01:49 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shopping_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `auto_invoice_id`
--

DROP TABLE IF EXISTS `auto_invoice_id`;
CREATE TABLE IF NOT EXISTS `auto_invoice_id` (
  `id` varchar(64) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `auto_order_id`
--

DROP TABLE IF EXISTS `auto_order_id`;
CREATE TABLE IF NOT EXISTS `auto_order_id` (
  `id` char(16) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `auto_order_id`
--

INSERT INTO `auto_order_id` (`id`) VALUES
('201907000001');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
CREATE TABLE IF NOT EXISTS `banks` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `branch` varchar(250) DEFAULT '',
  `accountNumber` varchar(150) DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `type` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`bank_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`bank_id`, `lang`, `title`, `branch`, `accountNumber`, `name`, `active`, `order`, `type`, `file`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, NULL, 'ธนาคารกรุงไทย', 'รามคำแหง', '1234567890', 'นายทดสอบ ทดสอบ', 1, 0, 'บัญชีออมทรัพย์', 'uploads/banks/2019/07/80d869552e3b66fa7a3c44a8b80f8186.jpg', '2019-07-17 15:09:14', '1', '2019-07-17 15:11:43', '1', 0, NULL, NULL),
(2, NULL, 'ธนาคารกสิกรไทย', 'รามคำแหง', '1234567891', 'นายทดสอบ ทดสอบ', 1, 0, 'บัญชีออมทรัพย์', 'uploads/banks/2019/07/8dc614c803d4deba203931467012cf44.jpg', '2019-07-17 15:10:45', '1', '2019-07-17 15:16:46', '1', 0, '2019-07-17 15:16:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `excerpt` text,
  `detail` text,
  `file` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `lang`, `title`, `slug`, `excerpt`, `detail`, `file`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `startDate`, `endDate`, `type`) VALUES
(2, NULL, 'หน้า home', 'หน้า-home', 'หน้า home', '<p>หน้า home<br></p>', 'uploads/banners/2019/07/e88a0286ecaf0642387805553beb2edd.jpg', 1, 0, '2019-07-19 22:59:24', '1', '2019-07-21 17:28:49', '1', 0, NULL, NULL, '2019-07-19', '2019-10-05', 'home'),
(3, NULL, 'หน้า products', 'หน้า-products', 'หน้า products', '<p>หน้า products<br></p>', 'uploads/banners/2019/07/cd1167e39e3042efb5643c0bf384aa6e.jpg', 1, 0, '2019-07-19 23:02:40', '1', '2019-07-21 17:29:00', '1', 0, NULL, NULL, '2019-07-19', '2019-09-07', 'products'),
(4, NULL, 'contact', 'contact', 'contact', '<div xss=removed><br></div>', 'uploads/banners/2019/07/5d10b919458b909f0f4f9149880d14f1.jpg', 1, 0, '2019-07-22 20:45:13', '1', '2019-07-22 20:45:16', '1', 0, NULL, NULL, '2019-07-01', '2019-09-07', 'contact');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `categorie_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text,
  PRIMARY KEY (`categorie_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categorie_id`, `parent_id`, `slug`, `lang`, `title`, `excerpt`, `detail`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 0, 'เสื้อผ้า', NULL, 'เสื้อผ้า', 'เสื้อผ้า', NULL, 1, 0, '2019-07-10 20:10:13', '1', '2019-07-10 23:10:24', '1', 0, NULL, NULL, 'เสื้อผ้า', 'เสื้อผ้า', 'เสื้อผ้า'),
(2, 0, 'กางเกง', NULL, 'กางเกง', 'กางเกง', NULL, 1, 0, '2019-07-10 20:31:21', '1', '2019-07-10 23:10:49', '1', 0, NULL, NULL, 'กางเกง', 'กางเกง', 'กางเกง'),
(3, 0, 'รองเท้า', NULL, 'รองเท้า', 'รองเท้า', NULL, 1, 0, '2019-07-10 20:31:36', '1', '2019-07-10 23:10:41', '1', 0, NULL, NULL, 'รองเท้า', 'รองเท้า', 'รองเท้า'),
(4, 1, 'เสื้อหางคู่', NULL, 'เสื้อหางคู่', 'เสื้อหางคู่', NULL, 1, 0, '2019-07-12 20:59:13', '1', '2019-07-12 20:59:18', '1', 0, NULL, NULL, 'เสื้อหางคู่', 'เสื้อหางคู่', 'เสื้อหางคู่'),
(5, 3, 'รองเท้าของฉัน', NULL, 'รองเท้าของฉัน', 'รองเท้าของฉัน', NULL, 1, 0, '2019-07-12 21:14:21', '1', '2019-07-12 21:14:27', '1', 0, NULL, NULL, 'รองเท้าของฉัน', 'รองเท้าของฉัน', 'รองเท้าของฉัน'),
(6, 2, 'กางเกงหางคู่', NULL, 'กางเกงหางคู่', 'กางเกงหางคู่', NULL, 1, 0, '2019-07-12 21:15:00', '1', '2019-07-12 21:21:22', '1', 0, NULL, NULL, 'กางเกงหางคู่', 'กางเกงหางคู่', 'กางเกงหางคู่');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`id`,`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0bhb13vasncotk8hku8mr1b56rsv7p89', '127.0.0.1', 1563645947, '__ci_last_regenerate|i:1563645947;cart_data|a:1:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}}'),
('0gvea952ksj0ven114jmaksb730ebcd2', '127.0.0.1', 1563651849, '__ci_last_regenerate|i:1563651849;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('0pja1nt706teg6cu00v1nq4phj8mdluu', '127.0.0.1', 1563633431, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563633431;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('0s8o9vfoggclh7r4h9dvp792os6uf1p0', '127.0.0.1', 1563698685, '__ci_last_regenerate|i:1563698685;reserve_data|s:6:\"969632\";cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}}'),
('0uicgh9unv6frn38tvu2ls6eih9m1tts', '127.0.0.1', 1563808439, '__ci_last_regenerate|i:1563808439;'),
('15rk0bu4vka4kkuhuks6lostq63vpgg5', '127.0.0.1', 1563639709, '__ci_last_regenerate|i:1563639709;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('1624p82k0md02t8j8i73mfvmgevoncni', '127.0.0.1', 1563900581, '__ci_last_regenerate|i:1563900581;'),
('163jcd3ssfme9qdj02obi4a48agkfmj8', '127.0.0.1', 1563634248, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563634248;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('16i58ivqa307064nq67gpqfo92451gau', '127.0.0.1', 1563640048, '__ci_last_regenerate|i:1563640048;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('1elb61up22vhci2mf71rnql8e2dbnfek', '127.0.0.1', 1563645234, '__ci_last_regenerate|i:1563645234;cart_data|a:1:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:2:\"11\";}}'),
('1fp8jdn6kt047ilf68i3bb5k85kccubo', '127.0.0.1', 1563895694, '__ci_last_regenerate|i:1563895694;'),
('1nh0g86edu2mba277l57f359g12qg1m1', '127.0.0.1', 1563659827, '__ci_last_regenerate|i:1563659575;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('21blnsu80dq7snkk6tucc3vdlkgcusuu', '127.0.0.1', 1563702435, '__ci_last_regenerate|i:1563702435;'),
('24g8ernrh94tdq2q6fi887d1q53i25as', '127.0.0.1', 1563655369, '__ci_last_regenerate|i:1563655369;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('2dfef5bo9ksndeooaefl45eqfqe5m9h4', '127.0.0.1', 1563644435, '__ci_last_regenerate|i:1563644435;cart_data|a:1:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";i:77;}}'),
('2p1manm22tps4h93o6rsutu23ssoggk2', '127.0.0.1', 1563638672, '__ci_last_regenerate|i:1563638672;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('2qof8ou9ihgrf9qbikae6ktvhbi218vu', '127.0.0.1', 1563648434, '__ci_last_regenerate|i:1563648434;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('2vm8ve0ufslv5j0n8qbo3bueq49kdlps', '127.0.0.1', 1563811152, '__ci_last_regenerate|i:1563811084;'),
('35b9u1fto9obplqo7t3mtk75tisvr115', '127.0.0.1', 1563699386, '__ci_last_regenerate|i:1563699386;reserve_data|s:6:\"969632\";cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}}'),
('35ol7nkb08r13mao9rgn1tok9ior2kb0', '127.0.0.1', 1563899806, '__ci_last_regenerate|i:1563899806;'),
('38smmda6rm4n2uu00vdqqmmbohvsu8q9', '127.0.0.1', 1563722129, '__ci_last_regenerate|i:1563722129;cart_data|a:1:{i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";s:1:\"3\";}}reserve_data|s:6:\"780897\";'),
('3bg8urm6f5uojlimht0akbu4f3aevub9', '127.0.0.1', 1563700069, '__ci_last_regenerate|i:1563700069;reserve_data|s:6:\"969632\";cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}}'),
('3o948773u79q7skfl7kbu4qa5vuseads', '127.0.0.1', 1563898075, '__ci_last_regenerate|i:1563898075;'),
('468aisc25rr5dlt07e7ovvfjgc3r8fop', '127.0.0.1', 1563717347, '__ci_last_regenerate|i:1563717347;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('4c7sdjoda32amf5c118rniiggh88hrd6', '127.0.0.1', 1563710027, '__ci_last_regenerate|i:1563710027;cart_data|a:4:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";i:1;}i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}i:10;a:4:{s:10:\"product_id\";s:2:\"10\";s:3:\"key\";s:2:\"83\";s:6:\"option\";a:3:{i:0;s:1:\"6\";i:1;s:0:\"\";i:2;s:0:\"\";}s:3:\"qty\";i:1;}}'),
('4cbpnrapaq5lhtqlvi3dmq6mr7cqfe03', '127.0.0.1', 1563717991, '__ci_last_regenerate|i:1563717991;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('4lqil04c9m3pol00mvdq88qhn1057h6v', '127.0.0.1', 1563803563, '__ci_last_regenerate|i:1563803563;'),
('4uv51ik9vn4ei6dg52mg50m77i3kejkv', '127.0.0.1', 1563699711, '__ci_last_regenerate|i:1563699711;reserve_data|s:6:\"969632\";cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}}'),
('51rfjkldhtffgkenr3krgu7sfhio5ekl', '127.0.0.1', 1563700372, '__ci_last_regenerate|i:1563700372;reserve_data|s:6:\"969632\";cart_data|a:1:{i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}}'),
('5a5vcjj1ffjruekff4nao3oh23t2ejdu', '127.0.0.1', 1563702743, '__ci_last_regenerate|i:1563702743;'),
('5aromu51ard0j8hj11cvqi7pj3dm7rpd', '127.0.0.1', 1563649828, '__ci_last_regenerate|i:1563649828;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('5fnrdt1cs638q41dngdo4pl31salhu5g', '127.0.0.1', 1563652167, '__ci_last_regenerate|i:1563652167;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"119539\";'),
('5i4lbsaathmakhqqf6lsgavackt3dkb2', '127.0.0.1', 1563811084, '__ci_last_regenerate|i:1563811084;'),
('5kge4fjfol1tjq369soe1qmb0ki5gmcd', '127.0.0.1', 1563643078, '__ci_last_regenerate|i:1563643078;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:13;a:4:{s:10:\"product_id\";s:2:\"13\";s:3:\"key\";s:2:\"86\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}s:3:\"qty\";i:1;}}'),
('5qetlbvcgjia6qk9fguscm74cq01r04c', '127.0.0.1', 1563703052, '__ci_last_regenerate|i:1563703052;'),
('6bjkblht21jvht6rosf77rg38m63nr9k', '127.0.0.1', 1563643487, '__ci_last_regenerate|i:1563643487;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:13;a:4:{s:10:\"product_id\";s:2:\"13\";s:3:\"key\";s:2:\"86\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:1:\"9\";}s:3:\"qty\";i:1;}}'),
('6ic2c0dl7haieg0gsh0i56jgurcdhb0s', '127.0.0.1', 1563644742, '__ci_last_regenerate|i:1563644742;cart_data|a:1:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";i:3;}}'),
('71dd3v8ktudno6e1f16sq2j7417op6u8', '127.0.0.1', 1563638117, '__ci_last_regenerate|i:1563638117;cart_data|a:3:{i:16;a:1:{i:89;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";i:1;}}i:15;a:1:{i:88;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}}i:14;a:1:{i:87;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:3;}}}'),
('7e9tgpv5cajqtdkandkcn1p1lokbd2tb', '127.0.0.1', 1563695106, '__ci_last_regenerate|i:1563695106;'),
('7sjl1rtmfkjjjoiapvcpoirik86uji0f', '127.0.0.1', 1563694172, '__ci_last_regenerate|i:1563694172;'),
('7st8dukk7dq2k54d1dngftrriiicqkk3', '127.0.0.1', 1563657600, '__ci_last_regenerate|i:1563657600;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('7v83katj4uumrbkvb6ghqshaduhn8v29', '127.0.0.1', 1563636877, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563636877;cart_data|a:3:{i:15;a:3:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;i:88;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:5;}}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('875lvj52feprk1ir4lm0rcogp03gr91t', '127.0.0.1', 1563631292, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563631292;cart_data|a:1:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:1;}}'),
('8d1fuij3asr2arbist378rlutrrsd3qe', '127.0.0.1', 1563715699, '__ci_last_regenerate|i:1563715699;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:6:\"orders\";a:8:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:9:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:10:\"order_code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:3:\"tel\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:12:\"payment_type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"status\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:6:\"status\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";}s:7:\"recycle\";i:0;}}'),
('8hrue4slk55n60hgvmv93dqensvplffc', '127.0.0.1', 1563810769, '__ci_last_regenerate|i:1563810769;'),
('91dm97sv0eervkpf2fgllig1nij4lsnj', '127.0.0.1', 1563807017, '__ci_last_regenerate|i:1563807017;'),
('94u7l9d0eivccogcmhhfb5ds697bejdj', '127.0.0.1', 1563653724, '__ci_last_regenerate|i:1563653724;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"738455\";'),
('98untf5ljgpjfcvlqlgk0nkdl4cdqb1s', '127.0.0.1', 1563655843, '__ci_last_regenerate|i:1563655843;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('9euun6f0b0ggvn3mgedj764hpehq9fl8', '127.0.0.1', 1563720839, '__ci_last_regenerate|i:1563720839;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('9f327t3u7un656u80vf8jdhkoobmp79p', '127.0.0.1', 1563657299, '__ci_last_regenerate|i:1563657299;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('9hui6d5vnar4kvfotj9dubeg3daakoa2', '127.0.0.1', 1563802958, '__ci_last_regenerate|i:1563802958;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:5:\"banks\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:8:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:13:\"accountNumber\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('9l0ovk8rd6aklekb2gnc9sqmga0qqi4m', '127.0.0.1', 1563698280, '__ci_last_regenerate|i:1563698280;reserve_data|s:6:\"969632\";cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}}'),
('9pm2mljl3468pn3h0jvalukqpecf2c3n', '127.0.0.1', 1563708126, '__ci_last_regenerate|i:1563708126;'),
('9tq1dv0jb3tt84kjv7srgbeguo2gp1he', '127.0.0.1', 1563718760, '__ci_last_regenerate|i:1563718760;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('9vq1vj4f4vkrfrttpdq6q03io6215a0a', '127.0.0.1', 1563705066, '__ci_last_regenerate|i:1563705066;'),
('a2uk5no52affnjmjj4hsd8br1029b8h5', '127.0.0.1', 1563659198, '__ci_last_regenerate|i:1563659198;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('a6i5gglovbpeq6f205cb53cgqoe6t78s', '127.0.0.1', 1563805805, '__ci_last_regenerate|i:1563805805;'),
('acd8ev0j4482m95m1mt960ba9dfb2lpu', '127.0.0.1', 1563848417, '__ci_last_regenerate|i:1563848301;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:9:\"contactus\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"phone\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:5:\"email\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"5\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('aidaopp52m2klo1382bkpdapghqn3khs', '127.0.0.1', 1563695784, '__ci_last_regenerate|i:1563695784;reserve_data|s:6:\"969632\";cart_data|a:1:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('alv20dp7k3fe3djsd839lqbt9o8q87ea', '127.0.0.1', 1563896039, '__ci_last_regenerate|i:1563896039;'),
('ao983k06f7ac781hrmdonnnei24asoau', '127.0.0.1', 1563805464, '__ci_last_regenerate|i:1563805464;'),
('avth1ceeil9dafnbgrtp96l9uv58cfo0', '127.0.0.1', 1563899406, '__ci_last_regenerate|i:1563899406;'),
('b04ie789h7qmn97vbr7v5n6fcj4f7df1', '127.0.0.1', 1563900274, '__ci_last_regenerate|i:1563900274;'),
('batag74fi2vig8msdu0tjd20m1gruak0', '127.0.0.1', 1563807697, '__ci_last_regenerate|i:1563807697;'),
('bh2us7jecu32srmrued3rn16lnhja7mc', '127.0.0.1', 1563806350, '__ci_last_regenerate|i:1563806350;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:9:\"contactus\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"phone\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:5:\"email\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"5\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('c9a2d65e4a8nf4hrg3p9m22dl4d9m1m3', '127.0.0.1', 1563717674, '__ci_last_regenerate|i:1563717674;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('cab1eo1pmgeh3fc931k41v2r5ca659rj', '127.0.0.1', 1563696427, '__ci_last_regenerate|i:1563696427;reserve_data|s:6:\"969632\";cart_data|a:1:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}}'),
('ceeqe3immdagvkbshilpid4rcoor445h', '127.0.0.1', 1563715682, '__ci_last_regenerate|i:1563715682;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('cgu745qajqj1o0s2ekrp4g6r1qo1utl6', '127.0.0.1', 1563697422, '__ci_last_regenerate|i:1563697422;reserve_data|s:6:\"969632\";cart_data|a:1:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}}'),
('cjqqpq7j342qq2imrm7ge1i06alhm55p', '127.0.0.1', 1563652637, '__ci_last_regenerate|i:1563652637;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"738455\";'),
('ck5sqiftoljp7d68sh5bf0vhd1q8ekhc', '127.0.0.1', 1563806045, '__ci_last_regenerate|i:1563806045;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:7:\"banners\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:8:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('cljkq1rh8use373psgd8n42882gtbveu', '127.0.0.1', 1563634734, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563634734;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('d2r3c9qr5bqhcogjbmjpcqaltn2o1jkr', '127.0.0.1', 1563650590, '__ci_last_regenerate|i:1563650590;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('d5pv47itd7a7o65eoplegelbde51faet', '127.0.0.1', 1563648806, '__ci_last_regenerate|i:1563648806;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('dbppmpst9dqms6e32cal4sqav7a0c90u', '127.0.0.1', 1563709664, '__ci_last_regenerate|i:1563709664;cart_data|a:4:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";i:1;}i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}i:10;a:4:{s:10:\"product_id\";s:2:\"10\";s:3:\"key\";s:2:\"83\";s:6:\"option\";a:3:{i:0;s:1:\"6\";i:1;s:0:\"\";i:2;s:0:\"\";}s:3:\"qty\";i:1;}}'),
('dilh6f43ti4i7vi88ds0gnj36ne85367', '127.0.0.1', 1563711448, '__ci_last_regenerate|i:1563711448;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('dldje6504lf31l59dhbich54bbpv422l', '127.0.0.1', 1563706687, '__ci_last_regenerate|i:1563706687;'),
('dnl9faak5t69d71gonqtmgcrddcf0hru', '127.0.0.1', 1563899073, '__ci_last_regenerate|i:1563899073;'),
('dofmqjndep9iocpislja60rt2bgpmibh', '127.0.0.1', 1563649113, '__ci_last_regenerate|i:1563649113;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('dt1b9msummfa1ubk9j439pjf1ovgph6c', '127.0.0.1', 1563635329, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563635329;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('dtml8q1qbin76chu5atoo1hkfaul0gjo', '127.0.0.1', 1563696818, '__ci_last_regenerate|i:1563696818;reserve_data|s:6:\"969632\";cart_data|a:1:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}}'),
('e5o8g4b295htguemnqfh6o77c5f0hksm', '127.0.0.1', 1563641623, '__ci_last_regenerate|i:1563641623;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('efjd2vimaro9d5bd7ar6qs29lbda7hdm', '127.0.0.1', 1563715701, '__ci_last_regenerate|i:1563715699;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:7:\"banners\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:8:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}');
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('eio3msnjhgna84eff3lkg3v8p79p97ud', '127.0.0.1', 1563705234, '__ci_last_regenerate|i:1563705234;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:7:\"banners\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:8:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('fcs1snljqvvs8j4f49b9llugi576m35f', '127.0.0.1', 1563693822, '__ci_last_regenerate|i:1563693822;'),
('ffd9l6va8pj6jpdh284qglnjhfne1sr5', '127.0.0.1', 1563810030, '__ci_last_regenerate|i:1563810030;'),
('fnemd5rvroj8d5b2n8pfhtnecipcdscm', '127.0.0.1', 1563718313, '__ci_last_regenerate|i:1563718313;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('fpii00suma44i1car86josngduks12r5', '127.0.0.1', 1563635676, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563635676;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('g6v2rr5tmsl2ti111484ruauvgve6s5v', '127.0.0.1', 1563712622, '__ci_last_regenerate|i:1563712622;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('g9bjlanbh5nuv0vg0p6eftb1ia9bq032', '127.0.0.1', 1563639381, '__ci_last_regenerate|i:1563639381;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('gblv9idag3oc62ci3dtpugv2risig91o', '127.0.0.1', 1563720386, '__ci_last_regenerate|i:1563720386;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('gdu6rm0oapn6jnjmds79oq7fuao70s3s', '127.0.0.1', 1563636327, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563636327;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('gfp2e24lendc62laujhq29kmqorhp39p', '127.0.0.1', 1563717004, '__ci_last_regenerate|i:1563717004;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('gh3d6q22oih3ji89jtajs7bmugr9ijg0', '127.0.0.1', 1563656516, '__ci_last_regenerate|i:1563656516;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('gn9bnteqomv621bcag9dadgsvofkiof1', '127.0.0.1', 1563711834, '__ci_last_regenerate|i:1563711834;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('golqa7ip31itt55se3sqjdqsa70agpcj', '127.0.0.1', 1563901312, '__ci_last_regenerate|i:1563901312;'),
('grh7ohir3e3unjdmt67fum729m0ik0m6', '127.0.0.1', 1563686841, '__ci_last_regenerate|i:1563686841;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}reserve_data|s:6:\"288801\";'),
('gtvl06v2pfeu55b959cng1m8oj248bqc', '127.0.0.1', 1563704406, '__ci_last_regenerate|i:1563704406;'),
('h3mvsja0hp9oj0h39nvnov57ib1mn1g4', '127.0.0.1', 1563806677, '__ci_last_regenerate|i:1563806677;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:9:\"contactus\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"phone\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:5:\"email\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"5\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('h70i4hbofvh86rht817nofdtvdjma12i', '127.0.0.1', 1563896073, '__ci_last_regenerate|i:1563896073;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:17:\"sent_successfully\";a:10:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:9:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:10:\"order_code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:3:\"tel\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:12:\"payment_type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"status\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:3:\"100\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:10:\"order_year\";s:4:\"2019\";s:11:\"order_month\";s:1:\"7\";s:6:\"status\";s:1:\"5\";s:7:\"recycle\";i:0;}}'),
('h8kjeu0o6n9lajpm9s1qfij1ngirv2ge', '127.0.0.1', 1563643992, '__ci_last_regenerate|i:1563643992;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:3;}}'),
('hcocfgpojr08hi3rejvr2hgjdjv74lhn', '127.0.0.1', 1563845697, '__ci_last_regenerate|i:1563845694;'),
('hkpiv9bspj4sgr73qdo4544fbmtmvms0', '127.0.0.1', 1563722786, '__ci_last_regenerate|i:1563722758;cart_data|a:0:{}reserve_data|s:6:\"780897\";'),
('hku3rfct3kddb6v2cvkocmhklcjdi3op', '127.0.0.1', 1563633093, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563633093;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('i41mgcubkh1b856o1b87f0i1i50kci26', '127.0.0.1', 1563719476, '__ci_last_regenerate|i:1563719476;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('i65ma9e2pnivvab8igp9ofsnmp0trhl3', '127.0.0.1', 1563721800, '__ci_last_regenerate|i:1563721800;cart_data|a:2:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}i:7;a:4:{s:10:\"product_id\";s:1:\"7\";s:3:\"key\";s:2:\"80\";s:6:\"option\";a:2:{i:0;s:1:\"3\";i:1;s:1:\"9\";}s:3:\"qty\";i:1;}}reserve_data|s:6:\"780897\";'),
('i7358d3cllschieo0oq5he3s17ovq7co', '127.0.0.1', 1563898404, '__ci_last_regenerate|i:1563898404;'),
('ii3dr0ov37ntjr9e2sc7clp0a0k108pg', '127.0.0.1', 1563893889, '__ci_last_regenerate|i:1563893889;'),
('il32c84najinm656o5k9r20s99bnout7', '127.0.0.1', 1563642559, '__ci_last_regenerate|i:1563642559;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:10;a:4:{s:10:\"product_id\";s:2:\"10\";s:3:\"key\";s:2:\"83\";s:6:\"option\";a:1:{i:0;s:1:\"6\";}s:3:\"qty\";i:1;}}'),
('il649r12uj5oc305lrpl37hn9u6of437', '127.0.0.1', 1563807378, '__ci_last_regenerate|i:1563807378;'),
('ipban16ro9vdsl83d43v2hvanoe682pb', '127.0.0.1', 1563655000, '__ci_last_regenerate|i:1563655000;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('iqsfhoeffeqgsojb0hgl7g1f4avuupmm', '127.0.0.1', 1563896387, '__ci_last_regenerate|i:1563896387;'),
('j0dp4csliavrecc426qurja8kecgdkuo', '127.0.0.1', 1563721484, '__ci_last_regenerate|i:1563721484;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('j3c3k2j8malmtcd51fe73ag6qud6l9fm', '127.0.0.1', 1563703360, '__ci_last_regenerate|i:1563703360;'),
('j3se1i7fmaf5fa6v2u50blp1amlm24tl', '127.0.0.1', 1563701170, '__ci_last_regenerate|i:1563701170;reserve_data|s:6:\"204545\";cart_data|a:2:{i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('ju3ldd2ddqukamc7lb0v09d4e3lulu9f', '127.0.0.1', 1563711004, '__ci_last_regenerate|i:1563711004;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}'),
('k1vsccqen8f6bom00pgbdobq0k3u6jde', '127.0.0.1', 1563695422, '__ci_last_regenerate|i:1563695422;'),
('k87ndfsa8mv4amsfik4rnok79hssj6nj', '127.0.0.1', 1563642004, '__ci_last_regenerate|i:1563642004;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('kbdo7v5ficjah9sq7dfsfg8okjql2tkk', '127.0.0.1', 1563804365, '__ci_last_regenerate|i:1563804365;'),
('kceshdckp62thgp67pntovk3quqqoa33', '127.0.0.1', 1563647659, '__ci_last_regenerate|i:1563647659;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('krrrre5nj2ld22iltgl81nm313nvs3bq', '127.0.0.1', 1563807240, '__ci_last_regenerate|i:1563807240;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:9:\"contactus\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"phone\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:5:\"email\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"5\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('l06vebmse552bgr8ffa2hi4l4u8l15h7', '127.0.0.1', 1563656947, '__ci_last_regenerate|i:1563656947;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('l3oval449b8s2qbrgu98p81eq02od0v2', '127.0.0.1', 1563659575, '__ci_last_regenerate|i:1563659575;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('l8qaq8a7jn2pvrdp5ipn97jrs1kgn6uc', '127.0.0.1', 1563637443, '__ci_last_regenerate|i:1563637443;cart_data|a:3:{i:16;a:1:{i:89;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";i:1;}}i:15;a:1:{i:88;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}}i:14;a:1:{i:87;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:3;}}}'),
('lrj5napicg6e3gvsa8ri3cjb8dmjn8ov', '127.0.0.1', 1563901729, '__ci_last_regenerate|i:1563901729;'),
('mbop00qifab89ue8t665ae4302ocg4jr', '127.0.0.1', 1563648000, '__ci_last_regenerate|i:1563648000;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('mdhn0rrvkvrq1qm583rlf6drjn5ivh5l', '127.0.0.1', 1563902379, '__ci_last_regenerate|i:1563902156;'),
('mke2i4thdtn86ilap23cp94o8cpq7fd9', '127.0.0.1', 1563766812, '__ci_last_regenerate|i:1563766757;'),
('mmmd7na2qk86e3logq73fjap77dbua2v', '127.0.0.1', 1563650240, '__ci_last_regenerate|i:1563650240;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('mp0u385trj4n47i1ebe8mj38e2f93nvi', '127.0.0.1', 1563697741, '__ci_last_regenerate|i:1563697741;reserve_data|s:6:\"969632\";cart_data|a:4:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:6;a:4:{s:10:\"product_id\";s:1:\"6\";s:3:\"key\";s:2:\"79\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:0:\"\";}s:3:\"qty\";i:4;}i:1;a:4:{s:10:\"product_id\";s:1:\"1\";s:3:\"key\";N;s:6:\"option\";N;s:3:\"qty\";i:4;}}'),
('ne1sf2q4gqviboc4du1bqo08c2n5koqn', '127.0.0.1', 1563848301, '__ci_last_regenerate|i:1563848301;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:9:\"contactus\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:5:\"phone\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:5:\"email\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"5\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('nfnqgllhcss2vd2kfibeoicesj6rf6ho', '127.0.0.1', 1563656152, '__ci_last_regenerate|i:1563656152;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('nms2ckptln19503oc9r01ilckd0h8cpi', '127.0.0.1', 1563900911, '__ci_last_regenerate|i:1563900911;'),
('o46h9mkp821u17t20o86mtnkt1j78flp', '127.0.0.1', 1563894874, '__ci_last_regenerate|i:1563894874;'),
('o5j7arbtd6evded8oc2bi0q5eipce5oi', '127.0.0.1', 1563654075, '__ci_last_regenerate|i:1563654075;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"851017\";'),
('oa3k9iocvooffo4ra6pq4rk8fl1gh8t3', '127.0.0.1', 1563808759, '__ci_last_regenerate|i:1563808759;'),
('oihfato4nqjqdc472t360hrf198toaq3', '127.0.0.1', 1563719161, '__ci_last_regenerate|i:1563719161;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('p03r67i4kp4tqfn84c56v1m9d140pkm8', '127.0.0.1', 1563658790, '__ci_last_regenerate|i:1563658790;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('p4e2andr5u1v74lqctilr6g5lfp3tf13', '127.0.0.1', 1563657916, '__ci_last_regenerate|i:1563657916;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"999395\";'),
('p5p2b2b6hh1v64ucu34e7mbgdb2neuel', '127.0.0.1', 1563810442, '__ci_last_regenerate|i:1563810442;'),
('p8cpm1baqjj9ci0hiliqu7tpqbct50ia', '127.0.0.1', 1563639020, '__ci_last_regenerate|i:1563639020;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('pe4jpvm82b1tgpif1u0bku18ldnqmcn8', '127.0.0.1', 1563645541, '__ci_last_regenerate|i:1563645541;cart_data|a:1:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:2:\"11\";}}'),
('pj7bcjqp5smenksgmgre76bdrc6vtgae', '127.0.0.1', 1563651475, '__ci_last_regenerate|i:1563651475;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('pocspiics84tafu7nppokmv4bsh52v3e', '127.0.0.1', 1563646712, '__ci_last_regenerate|i:1563646712;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('q5n8da58836103o5hg7r8hernl6kgfo3', '127.0.0.1', 1563633835, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563633835;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:5;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('q78sfnbq73deofou01ck18dqbjv5i780', '127.0.0.1', 1563640361, '__ci_last_regenerate|i:1563640361;cart_data|a:2:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:2;}i:14;a:4:{s:10:\"product_id\";s:2:\"14\";s:3:\"key\";s:2:\"87\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('q7qfd0n4gsf5irrh2pbkectaqu07p83s', '127.0.0.1', 1563710415, '__ci_last_regenerate|i:1563710415;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}'),
('q7vopddeulavojss6ardrgmk49kv8u3s', '127.0.0.1', 1563808002, '__ci_last_regenerate|i:1563808002;'),
('qe6kecjrepq2d3eqbtrj7ckqhbsmtbl5', '127.0.0.1', 1563647325, '__ci_last_regenerate|i:1563647325;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('qfpicmqduevk6tboaatmlduasu2uv65h', '127.0.0.1', 1563898564, '__ci_last_regenerate|i:1563898564;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:10:\"categories\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('qsq523tndjldq3s8k8c3cc2kn9dii5t0', '127.0.0.1', 1563899917, '__ci_last_regenerate|i:1563899917;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:17:\"sent_successfully\";a:10:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:9:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:10:\"order_code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:3:\"tel\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:12:\"payment_type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"status\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:3:\"100\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:10:\"order_year\";s:4:\"2019\";s:11:\"order_month\";s:1:\"7\";s:6:\"status\";s:1:\"5\";s:7:\"recycle\";i:0;}}'),
('qtlu2nfttuhgqlsdm92vk02bgnjkkdql', '127.0.0.1', 1563709257, '__ci_last_regenerate|i:1563709257;cart_data|a:3:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";i:1;}i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}i:4;a:4:{s:10:\"product_id\";s:1:\"4\";s:3:\"key\";s:2:\"77\";s:6:\"option\";a:1:{i:0;s:1:\"3\";}s:3:\"qty\";i:1;}}'),
('que8gqcpfhaog094edujeii5542vpbrb', '127.0.0.1', 1563646285, '__ci_last_regenerate|i:1563646285;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}');
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('rhe2vd09gh8863orcffm5i5563e4r2tl', '127.0.0.1', 1563899923, '__ci_last_regenerate|i:1563899917;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:8:\"products\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('rjdev8l6h4d02np24282uj5cf31avhph', '127.0.0.1', 1563649495, '__ci_last_regenerate|i:1563649495;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}'),
('rs8ntat9o576glbh5pe8511am8c8m25d', '127.0.0.1', 1563902156, '__ci_last_regenerate|i:1563902156;'),
('sajr75anst3i6ode334ju5ul4fcgf4k5', '127.0.0.1', 1563802524, '__ci_last_regenerate|i:1563802524;'),
('sneqd2jnj97eljh4s9ho55t5bg5016po', '127.0.0.1', 1563803148, '__ci_last_regenerate|i:1563803148;'),
('ssupin77g8f7bo1artnfpgr2q8vm36n9', '127.0.0.1', 1563694551, '__ci_last_regenerate|i:1563694551;'),
('t1alg9gl30j3hu9r33gvjtjd9gd31r6q', '127.0.0.1', 1563719838, '__ci_last_regenerate|i:1563719838;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('t2o9tjbbldgrc817gmn3pctc760cm8ej', '127.0.0.1', 1563690082, '__ci_last_regenerate|i:1563690082;cart_data|a:2:{i:10;a:4:{s:10:\"product_id\";s:2:\"10\";s:3:\"key\";s:2:\"83\";s:6:\"option\";a:1:{i:0;s:1:\"6\";}s:3:\"qty\";i:1;}i:9;a:4:{s:10:\"product_id\";s:1:\"9\";s:3:\"key\";s:2:\"82\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}reserve_data|s:6:\"319514\";'),
('t64i2lvck61rgb1t5k9t2101pdqsioqr', '127.0.0.1', 1563806704, '__ci_last_regenerate|i:1563806704;'),
('tat2k4p5ts5ri3b1c8bstqrs2qn6ti6i', '127.0.0.1', 1563704885, '__ci_last_regenerate|i:1563704885;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:17:\"sent_successfully\";a:10:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:9:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:10:\"order_code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:3:\"tel\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:12:\"payment_type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"status\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:4:\"desc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:3:\"100\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:10:\"order_year\";s:4:\"2019\";s:11:\"order_month\";s:1:\"7\";s:6:\"status\";s:1:\"5\";s:7:\"recycle\";i:0;}}'),
('tb3ap8ksauo4b10q0g1nth8fahh67m4s', '127.0.0.1', 1563893559, '__ci_last_regenerate|i:1563893559;'),
('tb7qe7gbi46dbmpam8a6tmdmis4rj3fq', '127.0.0.1', 1563708862, '__ci_last_regenerate|i:1563708862;'),
('tdb20b321ktc1539nmefuinhf526oi95', '127.0.0.1', 1563696093, '__ci_last_regenerate|i:1563696093;reserve_data|s:6:\"969632\";cart_data|a:1:{i:15;a:4:{s:10:\"product_id\";s:2:\"15\";s:3:\"key\";s:2:\"88\";s:6:\"option\";a:2:{i:0;s:1:\"2\";i:1;s:2:\"10\";}s:3:\"qty\";i:1;}}'),
('tdti7qiq6klqsrgjiamc3720hjv3lrjg', '127.0.0.1', 1563802832, '__ci_last_regenerate|i:1563802832;'),
('tuakev24i3ul4t1dbaja2bnqqmqmh97b', '127.0.0.1', 1563684562, '__ci_last_regenerate|i:1563684562;reserve_data|s:6:\"111573\";'),
('tve5l4f8s28lk8nba8afu7t4clqmkinr', '127.0.0.1', 1563704744, '__ci_last_regenerate|i:1563704744;'),
('u3ke68km8hvbte2aru8bsnas9ha7c407', '127.0.0.1', 1563898727, '__ci_last_regenerate|i:1563898727;'),
('ugd9jc1lduitqml980njlmld0rjs48go', '127.0.0.1', 1563650060, '__ci_last_regenerate|i:1563650060;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:47:\"http://st-adminshopping.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}condition|a:1:{s:6:\"orders\";a:8:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:9:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:10:\"order_code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:3:\"tel\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:12:\"payment_type\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:7;a:5:{s:4:\"data\";s:6:\"status\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:8;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:6:\"status\";a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";}s:7:\"recycle\";i:0;}}'),
('uoubraadr9cpggk0sjahl88na9d171kh', '127.0.0.1', 1563632314, 'cart_contents|a:4:{s:10:\"cart_total\";d:199.75;s:11:\"total_items\";d:5;s:32:\"c74d97b01eae257e44aa9d5bade97baf\";a:6:{s:2:\"id\";s:2:\"16\";s:3:\"qty\";d:2;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"c74d97b01eae257e44aa9d5bade97baf\";s:8:\"subtotal\";d:79.9;}s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";a:6:{s:2:\"id\";s:2:\"15\";s:3:\"qty\";d:3;s:5:\"price\";d:39.95;s:4:\"name\";s:7:\"T-Shirt\";s:5:\"rowid\";s:32:\"9bf31c7ff062936a96d3c8bd1f8f2ff3\";s:8:\"subtotal\";d:119.85000000000001;}}__ci_last_regenerate|i:1563632314;cart_data|a:3:{i:15;a:2:{s:10:\"product_id\";s:2:\"15\";s:3:\"qty\";i:4;}i:16;a:2:{s:10:\"product_id\";s:2:\"16\";s:3:\"qty\";i:3;}i:14;a:2:{s:10:\"product_id\";s:2:\"14\";s:3:\"qty\";i:1;}}'),
('urkd6dqd61pogujhrj2ltcej7n4bq80i', '127.0.0.1', 1563721177, '__ci_last_regenerate|i:1563721177;cart_data|a:1:{i:3;a:4:{s:10:\"product_id\";s:1:\"3\";s:3:\"key\";s:2:\"91\";s:6:\"option\";a:2:{i:0;s:0:\"\";i:1;s:1:\"2\";}s:3:\"qty\";s:1:\"2\";}}reserve_data|s:6:\"780897\";'),
('uvvmkm63lbds65en68i20hcpm6k7ok21', '127.0.0.1', 1563703968, '__ci_last_regenerate|i:1563703968;'),
('vbc2qniva79p34c50cmrpdi7fr7durcu', '127.0.0.1', 1563703662, '__ci_last_regenerate|i:1563703662;'),
('vlb2095o04v4ngu30m0fpagb8isfld0m', '127.0.0.1', 1563892919, '__ci_last_regenerate|i:1563892919;'),
('vmd07v09rcjvjoqvds2eu70ir4qb179j', '127.0.0.1', 1563722758, '__ci_last_regenerate|i:1563722758;cart_data|a:0:{}reserve_data|s:6:\"780897\";'),
('vuducuvkiu59f029tshhb1tt9sto60cm', '127.0.0.1', 1563647022, '__ci_last_regenerate|i:1563647022;cart_data|a:2:{i:11;a:4:{s:10:\"product_id\";s:2:\"11\";s:3:\"key\";s:2:\"84\";s:6:\"option\";a:2:{i:0;s:1:\"7\";i:1;s:2:\"10\";}s:3:\"qty\";s:1:\"1\";}i:16;a:4:{s:10:\"product_id\";s:2:\"16\";s:3:\"key\";s:2:\"89\";s:6:\"option\";a:2:{i:0;s:1:\"6\";i:1;s:2:\"11\";}s:3:\"qty\";s:1:\"2\";}}');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `color_code` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`color_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`color_id`, `title`, `color_code`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'สีแดง', '#ff0000', 1, 0, '2019-07-10 21:45:20', '1', '2019-07-10 21:55:56', '1', 0, '2019-07-10 21:55:17', 1),
(2, 'สีฟ้า', '#0000ff', 1, 0, '2019-07-10 21:46:46', '1', '2019-07-10 21:46:49', '1', 0, NULL, NULL),
(3, 'สีส้ม', '#ff8000', 1, 0, '2019-07-10 21:47:01', '1', '2019-07-10 21:47:56', '1', 0, NULL, NULL),
(4, 'สีชมพู้', '#ff00ff', 1, 0, '2019-07-10 21:47:19', '1', '2019-07-10 21:47:55', '1', 0, NULL, NULL),
(5, 'สีเหลือง', '#ffff00', 1, 0, '2019-07-10 21:47:36', '1', '2019-07-10 21:47:57', '1', 0, NULL, NULL),
(6, 'สีดำ', '#000000', 1, 0, '2019-07-10 21:47:48', '1', '2019-07-10 21:47:56', '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `variable` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `lang` varchar(2) NOT NULL DEFAULT 'th',
  `description` varchar(250) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`variable`,`lang`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`variable`, `value`, `lang`, `description`, `updateDate`, `type`) VALUES
('aboutUs', '&lt;h1 style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: THSarabunNew; color: rgb(88, 88, 91);&quot;&gt;Award-Winning Customer Support&lt;/h1&gt;&lt;p style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;Register.com&#039;s award winning support team is here to help you find the online solutions that work best for your business. Our experts are ready to give you the insight you need, so feel free to chat with us if you have any questions.&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;/p&gt;&lt;h1 style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: THSarabunNew; color: rgb(88, 88, 91);&quot;&gt;Award-Winning Customer Support&lt;/h1&gt;&lt;p style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;Register.com&#039;s award winning support team is here to help you find the online solutions that work best for your business. Our experts are ready to give you the insight you need, so feel free to chat with us if you have any questions.&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;/p&gt;&lt;h1 style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: THSarabunNew; color: rgb(88, 88, 91);&quot;&gt;Award-Winning Customer Support&lt;/h1&gt;&lt;p style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;Register.com&#039;s award winning support team is here to help you find the online solutions that work best for your business. Our experts are ready to give you the insight you need, so feel free to chat with us if you have any questions.&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;/p&gt;', 'th', NULL, '2019-05-16 14:35:24', 'about'),
('content', 'Chrn Property Management ถนนประชาอุทิศ ตำบลบางแม่นาง อำเภอบางใหญ่ จังหวัดนนทบุรี 11140\r\nโทรศัพท์: 02-225-3654 โทรสาร: 02-225-3654\r\nอีเมล์: admin@admin.com\r\n', 'th', NULL, '2018-03-15 09:03:35', 'contact'),
('content', 'ข่าวสาร บทความ และกิจกรรมของบริษัทฯ', 'th', NULL, '2018-03-11 14:17:02', 'news'),
('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', NULL, '2018-03-15 08:47:18', 'portfolio'),
('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', NULL, '2018-03-15 09:18:38', 'service'),
('content', 'ส่วนหนึ่งของทีมงานบริหารที่มีประสบการณ์การทำงานที่เชี่ยวชาญ', 'th', NULL, '2018-03-10 16:42:36', 'team'),
('detail', '1234 - Bandit Tringi lAliquam Vitae. New York', 'th', NULL, '2019-07-23 09:19:53', 'contactus'),
('email', 'test@gmail.com', 'th', NULL, '2019-07-23 09:19:53', 'contactus'),
('facebook', 'test@gmail.com', 'th', NULL, '2019-07-23 09:19:53', 'contactus'),
('files', '', 'th', NULL, '2019-05-16 14:35:24', 'about'),
('line', 'test@gmail.com', 'th', NULL, '2019-07-23 09:19:53', 'contactus'),
('mailAuthenticate', '1', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('mailDefault', 'comsci2535@gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('mailMethod', '1', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('metaDescription', 'Admin Shopping', 'th', NULL, '2019-07-10 00:18:27', 'general'),
('metaKeyword', 'Admin Shopping', 'th', NULL, '2019-07-10 00:18:27', 'general'),
('phone', '0807055971', 'th', NULL, '2019-07-23 09:19:53', 'contactus'),
('senderEmail', 'comsci2535@gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('senderName', 'คอร์สเรียนออนไลน์', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('siteTitle', 'Admin Shopping', 'th', NULL, '2019-07-10 00:18:27', 'general'),
('SMTPpassword', '5411425016lee', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('SMTPport', '465', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('SMTPserver', 'smtp.gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('SMTPusername', 'comsci2535@gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail'),
('time', '-', 'th', NULL, '2019-07-23 09:19:53', 'contactus');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE IF NOT EXISTS `contact_us` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(250) DEFAULT '',
  `lname` varchar(250) DEFAULT '',
  `email` varchar(150) DEFAULT '',
  `phone` varchar(25) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`contact_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_id`, `fname`, `lname`, `email`, `phone`, `title`, `detail`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'test1', 'test2', 'test@gmail.com', '08654645546', 'test', 'test', 1, '2019-07-22 21:09:42', '0', '2019-07-22 21:09:42', '0', 0, NULL, NULL),
(2, 'ewffdf', 'fsdfsdf', 'test@gmail.com', '0814254555', 'tfsdf', 'dasdasdasd', 1, '2019-07-22 21:28:30', '0', '2019-07-22 21:28:30', '0', 0, NULL, NULL),
(3, 'sdfsdf', 'fsdfsdf', 'terst@gmail.com', '085345646', 'retgfdfg', 'dfgsgsfgsg', 1, '2019-07-22 21:30:24', '0', '2019-07-22 21:30:24', '0', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

DROP TABLE IF EXISTS `info`;
CREATE TABLE IF NOT EXISTS `info` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `tax_id` int(20) NOT NULL,
  `tel` varchar(12) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `file` text NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`info_id`, `lang`, `title`, `slug`, `excerpt`, `detail`, `tax_id`, `tel`, `active`, `order`, `file`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, NULL, 'บริษัท Shopping จำกัด', 'บริษัท-shopping-จำกัด', '88/43 ชั้นที่ 2 หมู่ที่ 4 หมู่บ้านไทยสมบูรณ์สาม\r\nตำบลคลองสาม อำเภอคลองหลวง จังหวัดปทุมธานี 12120', 'test', 2147483647, '02-297-0360', 0, 0, 'uploads/logo/2019/07/918192d4dda33330c0e27d10a1c2c0cf.png', '2019-03-11 17:32:42', '', '2019-07-17 15:11:17', '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `moduleId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT NULL,
  `directory` varchar(200) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` int(10) DEFAULT '0',
  `method` varchar(50) DEFAULT NULL,
  `param` varchar(150) DEFAULT NULL,
  `isSidebar` int(11) DEFAULT '1' COMMENT '0:hide,1show',
  `isDev` tinyint(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT 'fa fa-angle-double-right',
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`moduleId`)
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`moduleId`, `parentId`, `type`, `active`, `title`, `directory`, `class`, `order`, `method`, `param`, `isSidebar`, `isDev`, `icon`, `createBy`, `updateBy`, `createDate`, `updateDate`, `modify`, `view`, `print`, `import`, `export`, `descript`, `recycle`, `recycleDate`, `recycleBy`) VALUES
(1, 0, 1, 1, 'แผงควบคุม', 'admin', 'dashboard', 5, '', NULL, 1, 0, 'flaticon-line-graph', 0, 3, '2016-07-23 08:54:42', '2019-02-04 16:46:17', 0, 1, 0, 0, 0, '', 0, NULL, NULL),
(2, 0, 1, 1, 'ตั้งค่า', 'admin', '', 6, '', '', 1, 0, 'fa fa-cogs', 0, 7, '2016-07-23 08:54:42', '2016-12-10 10:37:56', 0, 1, 0, 0, 0, '', 0, NULL, NULL),
(8, 2, 1, 1, 'ผู้ใช้งาน', 'admin', 'users', 4, '', '', 1, 0, 'fa fa-user', 0, 1, '2016-07-23 08:54:42', '2019-03-25 22:00:37', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(9, 2, 1, 1, 'พื้นฐาน', 'admin', 'config_general', 6, NULL, '', 1, 0, 'fa fa-circle-o', 0, 1, '2016-07-23 08:54:42', '2019-03-02 15:28:20', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(10, 2, 1, 1, 'อีเมล์', 'admin', 'config_mail', 7, '', '', 1, 0, 'fa fa-circle-o', 0, 1, '2016-07-23 08:54:42', '2019-03-02 15:28:21', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(20, 2, 1, 1, 'กลุ่มผู้ใช้งาน', 'admin', 'roles', 5, '', '', 1, 0, 'fa fa-users', 0, NULL, '2016-07-23 08:54:42', '2019-02-06 22:47:00', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(30, 0, 1, 1, 'โมดูล', 'admin', 'module', 8, '', '', 1, 1, 'fa fa-cog', 0, 7, '2016-07-23 08:54:42', '2016-07-16 13:04:45', 0, 0, 0, 0, 0, '', 0, NULL, NULL),
(166, 0, 1, 1, 'คลังไฟล์', 'admin', 'library', 7, '', '', 0, 0, 'fa fa-briefcase', 7, 1, '2016-07-23 08:54:42', '2018-03-23 14:48:52', 0, 0, 0, 0, 0, '', 0, NULL, NULL),
(193, 0, 1, 0, 'รายงาน', 'admin', '', 8, '', '', 1, 0, 'fa fa-bar-chart', 7, 1, '2017-01-19 13:06:36', '2019-04-15 15:16:08', 0, 0, 0, 0, 0, '', 1, '2019-04-15 15:16:08', 1),
(195, 0, 1, 0, 'ประวัติการใช้งาน', 'admin', 'stat_admin', 14, '', '', 1, 0, 'fa fa-calendar-o', 7, 3, '2017-01-19 13:11:57', '2018-10-13 00:20:58', 0, 0, 0, 0, 0, '', 0, NULL, NULL),
(267, 0, 1, 0, 'Repo', 'admin', 'repo', 21, NULL, NULL, 1, 1, 'fa fa-circle-o', 1, 3, '2018-03-16 21:32:34', '2018-07-03 04:28:33', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับrepo', 0, NULL, NULL),
(268, 0, 1, 1, 'จัดการข้อมูลสินค้า', '', '', 1, NULL, NULL, 1, 0, 'fa fa-box', 1, 1, '2019-07-10 00:25:35', '2019-07-17 15:20:04', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการข้อมูลสินค้า', 0, NULL, NULL),
(269, 268, 1, 1, 'ข้อมูลสินค้า', '', 'products', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-10 00:27:24', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับข้อมูลสินค้า', 0, NULL, NULL),
(270, 268, 1, 1, 'จัดการประเภทสินค้า', '', 'categories', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-10 00:30:09', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการประเภทสินค้า', 0, NULL, NULL),
(271, 268, 1, 0, 'จัดการสี', '', 'colors', 0, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2019-07-10 21:33:47', '2019-07-12 22:06:49', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการสี', 0, NULL, NULL),
(272, 268, 1, 0, 'การจัดการไซต์', '', 'sizes', 0, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2019-07-10 22:06:29', '2019-07-12 22:06:52', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับการจัดการไซต์', 0, NULL, NULL),
(273, 2, 1, 1, 'จัดการสถานะ', '', 'status', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-10 22:29:47', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการสถานะ', 0, NULL, NULL),
(274, 268, 1, 1, 'คุณสมบัติสินค้า', '', 'product_attributes', 3, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-12 22:06:34', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับคุณสมบัติสินค้า', 0, NULL, NULL),
(275, 276, 1, 1, 'จัดการข้อมูลสั่งซื้อสินค้า', '', 'orders', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2019-07-15 21:52:02', '2019-07-15 21:53:52', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการข้อมูลสั่งซื้อสินค้า', 0, NULL, NULL),
(276, 0, 1, 1, 'จัดข้อมูลการสั่งซื้อ', '', '', 2, NULL, NULL, 1, 0, 'fa fa-truck', 1, 1, '2019-07-15 21:53:23', '2019-07-17 15:20:20', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดข้อมูลการสั่งซื้อ', 0, NULL, NULL),
(277, 2, 1, 1, 'เกี่ยวกับข้อมูลทั่วไป', '', 'info', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-16 22:41:36', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับเกี่ยวกับข้อมูลทั่วไป', 0, NULL, NULL),
(278, 276, 1, 0, 'จัดการเลขที่นำส่งสินค้า', '', 'tracking_truck', 0, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2019-07-17 12:05:44', '2019-07-17 12:20:49', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการเลขที่นำส่งสินค้า', 1, '2019-07-17 12:20:49', 1),
(279, 276, 1, 1, 'จัดการเลขที่นำส่งสินค้า', '', 'tracking_trucks', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-17 12:22:12', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการเลขที่นำส่งสินค้า', 0, NULL, NULL),
(280, 276, 1, 1, 'จัดการส่งเรียบร้อยแล้ว', '', 'sent_successfully', 3, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-17 13:17:54', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการส่งเรียบร้อยแล้ว', 0, NULL, NULL),
(281, 2, 1, 1, 'ตั้งค่าบัญชีธนาคาร', '', 'banks', 3, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-17 15:02:19', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าบัญชีธนาคาร', 0, NULL, NULL),
(282, 0, 1, 1, 'จัดการ Banner', '', 'banners', 3, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-19 22:53:35', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการ banner', 0, NULL, NULL),
(283, 0, 1, 1, 'ติดต่อเรา', '', '', 4, NULL, NULL, 1, 0, 'flaticon-envelope', 1, 0, '2019-07-22 21:34:54', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับติดต่อเรา', 0, NULL, NULL),
(284, 283, 1, 1, 'ข้อความติดต่อเรา', '', 'contactus', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-22 21:36:22', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับข้อความติดต่อเรา', 0, NULL, NULL),
(285, 283, 1, 1, 'ตั้งค่าติดต่อเรา', '', 'config_contactus', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-07-22 21:38:15', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าติดต่อเรา', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_code` varchar(150) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `lasname` varchar(255) DEFAULT NULL,
  `address` text,
  `provinces` varchar(255) DEFAULT NULL,
  `amphures` varchar(255) DEFAULT NULL,
  `districts` varchar(255) DEFAULT NULL,
  `zip_code` varchar(15) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tracking_code` varchar(100) DEFAULT NULL,
  `discount` int(15) DEFAULT NULL,
  `promotion` varchar(64) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '0=ยังไม่ชำระเงิน,1=ชำระเงินแล้ว',
  `coupon_id` int(15) DEFAULT NULL,
  `payment_type` int(1) DEFAULT '0',
  `premise` varchar(255) DEFAULT NULL,
  `file` text,
  `reserve_code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_code`, `name`, `lasname`, `address`, `provinces`, `amphures`, `districts`, `zip_code`, `tel`, `email`, `tracking_code`, `discount`, `promotion`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `status`, `coupon_id`, `payment_type`, `premise`, `file`, `reserve_code`) VALUES
(1, '201907000001', 'Ruslee', 'Seebu', 'Test', 'ปัตตานี', 'กะพ้อ', 'กะรุบี', '94230', '0807055971', 'test@gmail.com', '123456789', 1111, '0', '2019-07-23 21:59:23', 0, '2019-07-23 22:01:35', 1, '0', NULL, NULL, 5, NULL, 0, NULL, 'uploads/payment/2019/07/ab9e604c85566ce138a3ba3055174c40.jpg', '603770');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE IF NOT EXISTS `orders_detail` (
  `order_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `attributes_option` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`order_detail_id`) USING BTREE,
  KEY `order_detail_index` (`order_id`,`product_id`,`product_price`,`quantity`,`created_at`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `orders_detail`
--

INSERT INTO `orders_detail` (`order_detail_id`, `order_id`, `product_id`, `product_price`, `quantity`, `attributes_option`, `created_at`) VALUES
(1, 1, 16, 1111, 1, '6,11', '2019-07-23 21:59:23');

-- --------------------------------------------------------

--
-- Table structure for table `orders_status_action`
--

DROP TABLE IF EXISTS `orders_status_action`;
CREATE TABLE IF NOT EXISTS `orders_status_action` (
  `order_status_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `is_process` int(1) DEFAULT '0',
  `process` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`order_status_action_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_status_action`
--

INSERT INTO `orders_status_action` (`order_status_action_id`, `order_id`, `title`, `status`, `is_process`, `process`, `created_at`, `created_by`) VALUES
(1, 1, NULL, 1, 0, NULL, '2019-07-23 21:59:23', '0'),
(2, 1, NULL, 2, 0, NULL, '2019-07-23 22:01:14', '1'),
(3, 1, 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง', 0, 1, 'print', '2019-07-23 22:01:22', '1'),
(4, 1, NULL, 4, 0, NULL, '2019-07-23 22:01:35', '1'),
(5, 1, NULL, 5, 0, NULL, '2019-07-23 22:01:43', '1');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `name_en` varchar(255) DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'เวลาสร้าง',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'เวลาแก้ไข',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `name`, `name_en`, `created_at`, `updated_at`) VALUES
(1, 'เข้าถึง', 'view', '2018-01-21 10:24:49', '2018-01-21 10:24:52'),
(2, 'สร้าง', 'create', '2018-01-21 10:27:13', '2018-01-21 10:27:15'),
(3, 'แก้ไข', 'edit', '2018-01-21 10:27:40', '2018-01-21 10:27:42'),
(4, 'ลบ', 'delete', '2018-01-21 10:28:20', '2018-01-21 10:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `role_id` int(11) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `moduleId`, `permission_id`) VALUES
(21, 1, 1),
(21, 1, 2),
(21, 1, 3),
(21, 1, 4),
(21, 299, 1),
(21, 299, 2),
(21, 299, 3),
(21, 299, 4),
(21, 300, 1),
(21, 300, 2),
(21, 300, 3),
(21, 300, 4),
(21, 296, 1),
(21, 296, 2),
(21, 296, 3),
(21, 296, 4),
(21, 297, 1),
(21, 297, 2),
(21, 297, 3),
(21, 297, 4),
(21, 20, 1),
(21, 20, 2),
(21, 20, 3),
(21, 20, 4),
(16, 1, 1),
(16, 1, 2),
(16, 1, 3),
(16, 1, 4),
(16, 307, 1),
(16, 307, 2),
(16, 307, 3),
(16, 307, 4),
(16, 306, 1),
(16, 306, 2),
(16, 306, 3),
(16, 306, 4),
(16, 305, 1),
(16, 305, 2),
(16, 305, 3),
(16, 305, 4),
(16, 295, 1),
(16, 295, 2),
(16, 295, 3),
(16, 295, 4),
(1, 1, 1),
(1, 1, 2),
(1, 1, 3),
(1, 1, 4),
(1, 307, 1),
(1, 307, 2),
(1, 307, 3),
(1, 307, 4),
(1, 306, 1),
(1, 306, 2),
(1, 306, 3),
(1, 306, 4),
(1, 305, 1),
(1, 305, 2),
(1, 305, 3),
(1, 305, 4),
(1, 295, 1),
(1, 295, 2),
(1, 295, 3),
(1, 295, 4),
(1, 299, 1),
(1, 299, 2),
(1, 299, 3),
(1, 299, 4),
(1, 322, 1),
(1, 322, 2),
(1, 322, 3),
(1, 322, 4),
(1, 308, 1),
(1, 308, 2),
(1, 308, 3),
(1, 308, 4),
(1, 300, 1),
(1, 300, 2),
(1, 300, 3),
(1, 300, 4),
(1, 324, 1),
(1, 324, 2),
(1, 324, 3),
(1, 324, 4),
(1, 325, 1),
(1, 325, 2),
(1, 325, 3),
(1, 325, 4),
(1, 320, 1),
(1, 320, 2),
(1, 320, 3),
(1, 320, 4),
(1, 321, 1),
(1, 321, 2),
(1, 321, 3),
(1, 321, 4),
(1, 312, 1),
(1, 312, 2),
(1, 312, 3),
(1, 312, 4),
(1, 301, 1),
(1, 301, 2),
(1, 301, 3),
(1, 301, 4),
(1, 304, 1),
(1, 304, 2),
(1, 304, 3),
(1, 304, 4),
(1, 313, 1),
(1, 313, 2),
(1, 313, 3),
(1, 313, 4),
(1, 315, 1),
(1, 315, 2),
(1, 315, 3),
(1, 315, 4),
(1, 310, 1),
(1, 310, 2),
(1, 310, 3),
(1, 310, 4),
(1, 318, 1),
(1, 318, 2),
(1, 318, 3),
(1, 318, 4),
(1, 319, 1),
(1, 319, 2),
(1, 319, 3),
(1, 319, 4),
(1, 323, 1),
(1, 323, 2),
(1, 323, 3),
(1, 323, 4),
(1, 316, 1),
(1, 316, 2),
(1, 316, 3),
(1, 316, 4),
(1, 309, 1),
(1, 309, 2),
(1, 309, 3),
(1, 309, 4),
(1, 311, 1),
(1, 311, 2),
(1, 311, 3),
(1, 311, 4),
(1, 8, 1),
(1, 8, 2),
(1, 8, 3),
(1, 8, 4),
(1, 20, 1),
(1, 20, 2),
(1, 20, 3),
(1, 20, 4),
(1, 9, 1),
(1, 9, 2),
(1, 9, 3),
(1, 9, 4),
(1, 10, 1),
(1, 10, 2),
(1, 10, 3),
(1, 10, 4);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `recommend` int(1) DEFAULT '0',
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text,
  `is_limit` int(1) DEFAULT '0',
  `file` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`product_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `recommend`, `slug`, `lang`, `title`, `excerpt`, `detail`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`, `is_limit`, `file`) VALUES
(1, 1, 'รองเท้าช้างดาว', NULL, 'รองเท้าช้างดาว', 'รองเท้าช้างดาว', '<p>รองเท้าช้างดาว<br></p>', 1, 0, '2019-07-14 16:48:27', '1', '2019-07-17 20:56:50', '1', 0, NULL, NULL, 'รองเท้าช้างดาว', 'รองเท้าช้างดาว', 'รองเท้าช้างดาว', 0, 'uploads/products/2019/07/b0698f5eed7bebf98811796d0eda1f45.jpg'),
(2, 1, 'สินค้าทดสอบ-1', NULL, 'สินค้าทดสอบ 1', 'สินค้าทดสอบ 1', '<p>สินค้าทดสอบ 1<br></p>', 1, 0, '2019-07-17 21:05:36', '1', '2019-07-17 21:05:49', '1', 0, NULL, NULL, 'สินค้าทดสอบ 1', 'สินค้าทดสอบ 1', 'สินค้าทดสอบ 1', 0, 'uploads/products/2019/07/f0c86e0e95d3fc57e95a8703d2de13fc.jpg'),
(3, 1, 'สินค้าทดสอบ-2', NULL, 'สินค้าทดสอบ 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmol tempor incidid ut labore et dolore magna aliqua. Ut enim ad minim veni quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in', '<p><span xss=removed>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in</span><br></p>', 1, 0, '2019-07-17 21:06:40', '1', '2019-07-19 22:08:59', '1', 0, NULL, NULL, 'สินค้าทดสอบ 2', 'สินค้าทดสอบ 2', 'สินค้าทดสอบ 2', 0, 'uploads/products/2019/07/37365ab7f8ef7d37fbcfc2c9fcee3c92.jpg'),
(4, 1, 'สินค้าทดสอบ-3', NULL, 'สินค้าทดสอบ 3', 'สินค้าทดสอบ 3', '<p>สินค้าทดสอบ 3<br></p>', 1, 0, '2019-07-17 21:07:29', '1', '2019-07-17 21:07:32', '1', 0, NULL, NULL, 'สินค้าทดสอบ 3', 'สินค้าทดสอบ 3', 'สินค้าทดสอบ 3', 0, 'uploads/products/2019/07/3bbd94246c7cc7c880449887afe94f4c.jpeg'),
(5, 0, 'สินค้าทดสอบ-4', NULL, 'สินค้าทดสอบ 4', 'สินค้าทดสอบ 4', '<p>สินค้าทดสอบ 4<br></p>', 1, 0, '2019-07-17 21:10:02', '1', '2019-07-17 21:10:05', '1', 0, NULL, NULL, 'สินค้าทดสอบ 4', 'สินค้าทดสอบ 4', 'สินค้าทดสอบ 4', 0, 'uploads/products/2019/07/33c4f7cc4d35339215897d8944cd3614.jpg'),
(6, 1, 'สินค้าทดสอบ-5', NULL, 'สินค้าทดสอบ 5', 'สินค้าทดสอบ 5', '<p>สินค้าทดสอบ 5<br></p>', 1, 0, '2019-07-18 23:06:06', '1', '2019-07-19 14:44:07', '1', 0, NULL, NULL, 'สินค้าทดสอบ 5', 'สินค้าทดสอบ 5', 'สินค้าทดสอบ 5', 0, 'uploads/products/2019/07/3793548c1ab1476ee1c84946348a74ee.png'),
(7, 0, 'สินค้าทดสอบ-6', NULL, 'สินค้าทดสอบ 6', 'สินค้าทดสอบ 6', '<p>สินค้าทดสอบ 6<br></p>', 1, 0, '2019-07-19 14:43:23', '1', '2019-07-19 14:44:08', '1', 0, NULL, NULL, 'สินค้าทดสอบ 6', 'สินค้าทดสอบ 6', 'สินค้าทดสอบ 6', 0, 'uploads/products/2019/07/2f74fa321f83bd35db50d057b7a7acfe.jpg'),
(8, 0, 'สินค้าทดสอบ-7', NULL, 'สินค้าทดสอบ 7', 'สินค้าทดสอบ 7', '<p>สินค้าทดสอบ 7<br></p>', 1, 0, '2019-07-19 14:44:04', '1', '2019-07-19 14:44:08', '1', 0, NULL, NULL, 'สินค้าทดสอบ 7', 'สินค้าทดสอบ 7', 'สินค้าทดสอบ 7', 0, 'uploads/products/2019/07/0ffa2b9b4e2253c123fa879b7e624eb9.jpg'),
(9, 0, 'สินค้าทดสอบ-8', NULL, 'สินค้าทดสอบ 8', 'สินค้าทดสอบ 8', '<p>สินค้าทดสอบ 8<br></p>', 1, 0, '2019-07-19 14:44:49', '1', '2019-07-19 14:45:33', '1', 0, NULL, NULL, 'สินค้าทดสอบ 8', 'สินค้าทดสอบ 8', 'สินค้าทดสอบ 8', 0, 'uploads/products/2019/07/686719df6a30043869799d421d2a46ad.jpg'),
(10, 0, 'สินค้าทดสอบ-9', NULL, 'สินค้าทดสอบ 9', 'สินค้าทดสอบ 9', '<p>สินค้าทดสอบ 9<br></p>', 1, 0, '2019-07-19 14:45:28', '1', '2019-07-19 14:45:33', '1', 0, NULL, NULL, 'สินค้าทดสอบ 9', 'สินค้าทดสอบ 9', 'สินค้าทดสอบ 9', 0, 'uploads/products/2019/07/93bbc57eb3261a7872ff59b7746cb1aa.jpg'),
(11, 0, 'สินค้าทดสอบ-10', NULL, 'สินค้าทดสอบ 10', 'สินค้าทดสอบ 10', '<p>สินค้าทดสอบ 10<br></p>', 1, 0, '2019-07-19 14:46:12', '1', '2019-07-19 14:46:16', '1', 0, NULL, NULL, 'สินค้าทดสอบ 10', 'สินค้าทดสอบ 10', 'สินค้าทดสอบ 10', 0, 'uploads/products/2019/07/60ec7cf72cc464ef441cf3baf8638b5b.jpg'),
(12, 0, 'สินค้าทดสอบ-11', NULL, 'สินค้าทดสอบ 11', 'สินค้าทดสอบ 11', '<p>สินค้าทดสอบ 11<br></p>', 1, 0, '2019-07-19 14:51:30', '1', '2019-07-19 14:57:32', '1', 0, NULL, NULL, 'สินค้าทดสอบ 11', 'สินค้าทดสอบ 11', 'สินค้าทดสอบ 11', 0, 'uploads/products/2019/07/f196de4ec8ed26d92c13e1e3f46fd3c0.jpg'),
(13, 0, 'สินค้าทดสอบ-12', NULL, 'สินค้าทดสอบ 12', 'สินค้าทดสอบ 12', '<p>สินค้าทดสอบ 12<br></p>', 1, 0, '2019-07-19 14:56:32', '1', '2019-07-19 14:57:32', '1', 0, NULL, NULL, 'สินค้าทดสอบ 12', 'สินค้าทดสอบ 12', 'สินค้าทดสอบ 12', 0, 'uploads/products/2019/07/4a3f8485d652d8ca4863d0372f9220e5.jpg'),
(14, 0, 'สินค้าทดสอบ-13', NULL, 'สินค้าทดสอบ 13', 'สินค้าทดสอบ 13', '<p>สินค้าทดสอบ 13<br></p>', 1, 0, '2019-07-19 15:11:42', '1', '2019-07-19 15:12:25', '1', 0, NULL, NULL, 'สินค้าทดสอบ 13', 'สินค้าทดสอบ 13', 'สินค้าทดสอบ 13', 0, 'uploads/products/2019/07/07dee5fb85e6887a47b9405f71fb4a8d.jpg'),
(15, 0, 'สินค้าทดสอบ-14', NULL, 'สินค้าทดสอบ 14', 'สินค้าทดสอบ 14', '<p>สินค้าทดสอบ 14<br></p>', 1, 0, '2019-07-19 15:12:21', '1', '2019-07-19 15:12:25', '1', 0, NULL, NULL, 'สินค้าทดสอบ 14', 'สินค้าทดสอบ 14', 'สินค้าทดสอบ 14', 0, 'uploads/products/2019/07/26b7fbb158994fb8a441e39ae9a3b3e5.jpg'),
(16, 0, 'สินค้าทดสอบ-15', NULL, 'สินค้าทดสอบ 15', 'สินค้าทดสอบ 15', '<p>สินค้าทดสอบ 15<br></p>', 1, 0, '2019-07-19 15:13:06', '1', '2019-07-19 15:13:09', '1', 0, NULL, NULL, 'สินค้าทดสอบ 15', 'สินค้าทดสอบ 15', 'สินค้าทดสอบ 15', 0, 'uploads/products/2019/07/afc78a43c835129c97877525b71fa5db.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
CREATE TABLE IF NOT EXISTS `product_attributes` (
  `product_attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text,
  PRIMARY KEY (`product_attribute_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`product_attribute_id`, `slug`, `lang`, `title`, `parent_id`, `excerpt`, `detail`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'สี', NULL, 'สี', 0, 'สี', NULL, 1, 0, '2019-07-12 22:28:27', '1', '2019-07-12 22:29:18', '1', 0, NULL, NULL, 'สี', 'สี', 'สี'),
(2, 'แดง', NULL, 'แดง', 1, 'แดง', NULL, 1, 0, '2019-07-12 22:31:01', '1', '2019-07-12 22:31:04', '1', 0, NULL, NULL, 'แดง', 'แดง', 'แดง'),
(3, 'ฟ้า', NULL, 'ฟ้า', 1, 'ฟ้า', NULL, 1, 0, '2019-07-12 22:31:20', '1', '2019-07-12 22:31:22', '1', 0, NULL, NULL, 'ฟ้า', 'ฟ้า', 'ฟ้า'),
(4, 'เหลือง', NULL, 'เหลือง', 1, 'เหลือง', NULL, 1, 0, '2019-07-12 22:31:45', '1', '2019-07-12 22:32:22', '1', 0, NULL, NULL, 'เหลือง', 'เหลือง', 'เหลือง'),
(5, 'เขี่ยว', NULL, 'เขี่ยว', 1, 'เขี่ยว', NULL, 1, 0, '2019-07-12 22:32:04', '1', '2019-07-12 22:32:22', '1', 0, NULL, NULL, 'เขี่ยว', 'เขี่ยว', 'เขี่ยว'),
(6, 'ส้ม', NULL, 'ส้ม', 1, 'ส้ม', NULL, 1, 0, '2019-07-12 22:32:18', '1', '2019-07-12 22:32:21', '1', 0, NULL, NULL, 'ส้ม', 'ส้ม', 'ส้ม'),
(7, 'ม่วง', NULL, 'ม่วง', 1, 'ม่วง', NULL, 1, 0, '2019-07-12 22:32:56', '1', '2019-07-12 22:34:16', '1', 0, NULL, NULL, 'ม่วง', 'ม่วง', 'ม่วง'),
(8, 'ไซส์-s-l', NULL, 'ไซส์ S-L', 0, 'ไซส์ S-L', NULL, 1, 0, '2019-07-12 22:34:00', '1', '2019-07-12 22:34:39', '1', 0, NULL, NULL, 'ไซส์', 'ไซส์', 'ไซส์'),
(9, 's', NULL, 'S', 8, 'ขนาด 120x120', NULL, 1, 0, '2019-07-12 22:35:17', '1', '2019-07-12 22:38:36', '1', 0, NULL, NULL, 'S', 'ขนาด 120x120', 'S'),
(10, 'ss', NULL, 'SS', 8, 'ขนาด 120x120', NULL, 1, 0, '2019-07-12 22:35:35', '1', '2019-07-12 22:38:36', '1', 0, NULL, NULL, 'SS', 'ขนาด 120x120', 'SS'),
(11, 'm', NULL, 'M', 8, 'ขนาด 120x120', NULL, 1, 0, '2019-07-12 22:35:48', '1', '2019-07-12 22:38:35', '1', 0, NULL, NULL, 'M', 'ขนาด 120x120', 'M'),
(12, 'l', NULL, 'L', 8, 'ขนาด 120x120', NULL, 1, 0, '2019-07-12 22:36:03', '1', '2019-07-12 22:38:34', '1', 0, NULL, NULL, 'L', 'ขนาด 120x120', 'L');

-- --------------------------------------------------------

--
-- Table structure for table `product_categorie_map`
--

DROP TABLE IF EXISTS `product_categorie_map`;
CREATE TABLE IF NOT EXISTS `product_categorie_map` (
  `product_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_categorie_map`
--

INSERT INTO `product_categorie_map` (`product_id`, `categorie_id`) VALUES
(1, 5),
(2, 4),
(3, 4),
(4, 4),
(5, 6),
(6, 6),
(7, 6),
(8, 5),
(9, 4),
(10, 4),
(11, 5),
(12, 4),
(13, 6),
(14, 6),
(15, 5),
(16, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_img_map`
--

DROP TABLE IF EXISTS `product_img_map`;
CREATE TABLE IF NOT EXISTS `product_img_map` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `file_path` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_img_map`
--

INSERT INTO `product_img_map` (`img_id`, `product_id`, `file_path`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(98, 9, 'uploads/products/2019/07/b6a83bb72fafcb2eccdc2d15c53ee215.jpg', '2019-07-19 14:44:49', '1', '2019-07-19 14:44:49', '1'),
(97, 9, 'uploads/products/2019/07/35791faf0c4af1a347ce19e2ced23312.jpg', '2019-07-19 14:44:49', '1', '2019-07-19 14:44:49', '1'),
(96, 8, 'uploads/products/2019/07/f25ce3b83945e1e8ed25fea79e5fc61f.jpeg', '2019-07-19 14:44:04', '1', '2019-07-19 14:44:04', '1'),
(95, 8, 'uploads/products/2019/07/55cded634cde766d254b911ef0122354.jpg', '2019-07-19 14:44:04', '1', '2019-07-19 14:44:04', '1'),
(94, 7, 'uploads/products/2019/07/1391175bce489e211532f4239e327baa.jpg', '2019-07-19 14:43:23', '1', '2019-07-19 14:43:23', '1'),
(93, 7, 'uploads/products/2019/07/81e23cd04bbbb96d0091cbb95d54b9fe.jpg', '2019-07-19 14:43:23', '1', '2019-07-19 14:43:23', '1'),
(92, 6, 'uploads/products/2019/07/4ea6c63dc8d1733bd66a7d5b82b25321.jpg', '2019-07-18 23:06:06', '1', '2019-07-18 23:06:06', '1'),
(91, 5, 'uploads/products/2019/07/367a46f1df72ca004d79a73f48644156.jpg', '2019-07-17 21:10:02', '1', '2019-07-17 21:10:02', '1'),
(90, 4, 'uploads/products/2019/07/28257b8a3079bc09bba4c26a6557830e.jpeg', '2019-07-17 21:07:29', '1', '2019-07-17 21:07:29', '1'),
(110, 3, 'uploads/products/2019/07/4a13d711950c952a3fb62970f583c26c.jpg', '2019-07-19 22:08:59', '1', '2019-07-19 22:08:59', '1'),
(111, 3, 'uploads/products/2019/07/ed6e64cb23653e2631e6aee6ea080b6d.jpg', '2019-07-19 22:08:59', '1', '2019-07-19 22:08:59', '1'),
(88, 2, 'uploads/products/2019/07/1f946550e4425c5b40d743bab7d1ee98.jpg', '2019-07-17 21:05:49', '1', '2019-07-17 21:05:49', '1'),
(84, 1, 'uploads/products/2019/07/93dfc46b4ac3153251bd6798a603a7c8.jpg', '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1'),
(85, 1, 'uploads/products/2019/07/7868b8205eea93a82618f24fe2335a31.jpg', '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1'),
(86, 1, 'uploads/products/2019/07/5bb2c1cb2c58319a6528b423905cc5b3.jpg', '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1'),
(99, 10, 'uploads/products/2019/07/cb406b6b578a318f9b265b97480c7b79.jpg', '2019-07-19 14:45:28', '1', '2019-07-19 14:45:28', '1'),
(100, 11, 'uploads/products/2019/07/7566c98d479985519e339b8de57a1faf.jpeg', '2019-07-19 14:46:12', '1', '2019-07-19 14:46:12', '1'),
(101, 12, 'uploads/products/2019/07/439033523dd45f353f60b4ad2a2d4989.jpg', '2019-07-19 14:51:30', '1', '2019-07-19 14:51:30', '1'),
(102, 13, 'uploads/products/2019/07/44256c708a7a0a3fc777c4914533c4b5.jpg', '2019-07-19 14:56:32', '1', '2019-07-19 14:56:32', '1'),
(103, 14, 'uploads/products/2019/07/36b59f67f886281bf597cce2888f3383.jpg', '2019-07-19 15:11:43', '1', '2019-07-19 15:11:43', '1'),
(104, 15, 'uploads/products/2019/07/7b768fcfd7b547fa0f1d92867275408e.jpeg', '2019-07-19 15:12:21', '1', '2019-07-19 15:12:21', '1'),
(105, 15, 'uploads/products/2019/07/ef971cd3dd315bdf2ade423304f28fd5.jpg', '2019-07-19 15:12:21', '1', '2019-07-19 15:12:21', '1'),
(106, 16, 'uploads/products/2019/07/4414fb292a0c1855bcaef1ab9e042138.jpg', '2019-07-19 15:13:06', '1', '2019-07-19 15:13:06', '1'),
(107, 16, 'uploads/products/2019/07/b8d2cc72af590cbab2dadb71c923008e.jpg', '2019-07-19 15:13:06', '1', '2019-07-19 15:13:06', '1'),
(108, 16, 'uploads/products/2019/07/49f153004439b24540eec9b6a46d5497.jpg', '2019-07-19 15:13:06', '1', '2019-07-19 15:13:06', '1'),
(112, 3, 'uploads/products/2019/07/88e9eb611739444763bbd4ea265a5188.jpg', '2019-07-19 22:08:59', '1', '2019-07-19 22:08:59', '1'),
(113, 3, 'uploads/products/2019/07/b392c6188fb233d193b7585baa68cd6e.jpg', '2019-07-19 22:08:59', '1', '2019-07-19 22:08:59', '1');

-- --------------------------------------------------------

--
-- Table structure for table `repo`
--

DROP TABLE IF EXISTS `repo`;
CREATE TABLE IF NOT EXISTS `repo` (
  `repoId` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text,
  PRIMARY KEY (`repoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reserves`
--

DROP TABLE IF EXISTS `reserves`;
CREATE TABLE IF NOT EXISTS `reserves` (
  `reserve_id` int(11) NOT NULL AUTO_INCREMENT,
  `reserve_code` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  PRIMARY KEY (`reserve_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reserves`
--

INSERT INTO `reserves` (`reserve_id`, `reserve_code`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '959658', '2019-07-22 20:43:53', 0, '2019-07-22 20:43:53', 0),
(2, '857808', '2019-07-22 21:45:10', 0, '2019-07-22 21:45:10', 0),
(4, '261954', '2019-07-23 22:00:03', 0, '2019-07-23 22:00:03', 0),
(5, '708913', '2019-07-23 22:00:06', 0, '2019-07-23 22:00:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `reserves_detail`
--

DROP TABLE IF EXISTS `reserves_detail`;
CREATE TABLE IF NOT EXISTS `reserves_detail` (
  `reserve_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `reserve_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_price` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `attributes_option` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`reserve_detail_id`) USING BTREE,
  KEY `order_detail_index` (`reserve_id`,`product_id`,`product_price`,`quantity`,`created_at`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` text,
  `active` tinyint(1) UNSIGNED DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `create_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) UNSIGNED DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT '1',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `remark`, `active`, `created_at`, `create_by`, `updated_at`, `update_by`, `recycle`, `recycle_at`, `recycle_by`, `visible`) VALUES
(1, 'ผู้ดูแลระบบ', 'บริหารจัดการระบบ', 1, '2016-07-13 10:55:21', NULL, '2019-05-16 07:40:14', 1, 0, NULL, NULL, 0),
(16, 'ผู้อัพเดทข้อมูลเว็บไซต์', 'บริหารจัดการข้อมูลเว็บไซต์', 1, '2018-08-05 00:20:00', 22, '2019-03-29 16:50:32', 1, 0, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `seos`
--

DROP TABLE IF EXISTS `seos`;
CREATE TABLE IF NOT EXISTS `seos` (
  `seo_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `display_page` varchar(255) NOT NULL,
  `file` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`seo_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
CREATE TABLE IF NOT EXISTS `sizes` (
  `size_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`size_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`size_id`, `title`, `excerpt`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'S', 'ขนาดความสู่ง 150x50', 1, 0, '2019-07-10 22:15:47', '1', '2019-07-10 22:15:51', '1', 0, NULL, NULL),
(2, 'M', 'ขนาดความสู่ง 250x150', 1, 0, '2019-07-10 22:16:22', '1', '2019-07-10 22:16:25', '1', 0, NULL, NULL),
(3, 'SS', 'ขนาดความสู่ง 170x55', 1, 0, '2019-07-10 22:16:45', '1', '2019-07-10 22:16:49', '1', 0, NULL, NULL),
(4, 'L', 'ขนาดความสู่ง 250x200', 1, 0, '2019-07-10 22:17:15', '1', '2019-07-10 22:17:35', '1', 0, NULL, NULL),
(5, 'XL', 'ขนาดความสู่ง 350x200', 1, 0, '2019-07-10 22:17:32', '1', '2019-07-10 22:20:25', '1', 0, '2019-07-10 22:18:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`status_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `title`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'รอดำเนินการ', 1, 0, '2019-07-10 22:36:42', '1', '2019-07-10 22:37:13', '1', 0, NULL, NULL),
(2, 'รอการตรวจสอบ', 1, 0, '2019-07-10 22:37:29', '1', '2019-07-10 22:37:31', '1', 0, NULL, NULL),
(3, 'ชำระเงินแล้ว', 1, 0, '2019-07-10 22:37:49', '1', '2019-07-10 22:37:51', '1', 0, NULL, NULL),
(4, 'กำลังจัดส่ง', 1, 0, '2019-07-10 22:38:03', '1', '2019-07-10 22:38:05', '1', 0, NULL, NULL),
(5, 'ส่งเรียบร้อยแล้ว', 1, 0, '2019-07-10 22:38:22', '1', '2019-07-10 22:38:27', '1', 0, NULL, NULL),
(6, 'ยกเลิก', 1, 0, '2019-07-10 22:38:40', '1', '2019-07-10 22:40:01', '1', 0, NULL, NULL),
(7, 'รายการตีกลับ', 1, 0, '2019-07-10 22:38:53', '1', '2019-07-10 22:40:24', '1', 0, '2019-07-10 22:40:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
CREATE TABLE IF NOT EXISTS `stocks` (
  `stock_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT '0',
  `product_price` float(11,2) DEFAULT '0.00',
  `stock_qty` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `is_limit` int(1) DEFAULT '0',
  PRIMARY KEY (`stock_id`)
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`stock_id`, `product_id`, `product_price`, `stock_qty`, `created_at`, `created_by`, `updated_at`, `updated_by`, `is_limit`) VALUES
(75, 2, 20000.00, 0, '2019-07-17 21:05:49', '1', '2019-07-17 21:05:49', '1', 0),
(73, 1, 150.00, 12, '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1', 1),
(72, 1, 150.00, 50, '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1', 1),
(91, 3, 20000.00, -2, '2019-07-19 22:08:59', '1', '2019-07-19 22:08:59', '1', 1),
(77, 4, 45000.00, -2, '2019-07-17 21:07:29', '1', '2019-07-17 21:07:29', '1', 1),
(78, 5, 42000.00, 0, '2019-07-17 21:10:02', '1', '2019-07-17 21:10:02', '1', 1),
(79, 6, 450.00, -1, '2019-07-18 23:06:06', '1', '2019-07-18 23:06:06', '1', 0),
(80, 7, 2333.00, 0, '2019-07-19 14:43:23', '1', '2019-07-19 14:43:23', '1', 1),
(81, 8, 2323.00, 0, '2019-07-19 14:44:04', '1', '2019-07-19 14:44:04', '1', 0),
(82, 9, 324.00, -1, '2019-07-19 14:44:49', '1', '2019-07-19 14:44:49', '1', 0),
(83, 10, 32423.00, -1, '2019-07-19 14:45:28', '1', '2019-07-19 14:45:28', '1', 0),
(84, 11, 32422.00, 44, '2019-07-19 14:46:12', '1', '2019-07-19 14:46:12', '1', 0),
(85, 12, 6765.00, 0, '2019-07-19 14:51:30', '1', '2019-07-19 14:51:30', '1', 0),
(86, 13, 3333.00, 0, '2019-07-19 14:56:32', '1', '2019-07-19 14:56:32', '1', 1),
(87, 14, 6767.00, -2, '2019-07-19 15:11:42', '1', '2019-07-19 15:11:42', '1', 0),
(88, 15, 233.00, -3, '2019-07-19 15:12:21', '1', '2019-07-19 15:12:21', '1', 0),
(89, 16, 1111.00, 14, '2019-07-19 15:13:06', '1', '2019-07-19 15:13:06', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stocks_detail`
--

DROP TABLE IF EXISTS `stocks_detail`;
CREATE TABLE IF NOT EXISTS `stocks_detail` (
  `stock_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT '0',
  `attributes_group` int(11) DEFAULT NULL,
  `attributes_option` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`stock_detail_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocks_detail`
--

INSERT INTO `stocks_detail` (`stock_detail_id`, `stock_id`, `product_id`, `attributes_group`, `attributes_option`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(108, 73, 1, 8, 10, '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1'),
(107, 73, 1, 1, 4, '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1'),
(106, 72, 1, 8, 9, '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1'),
(105, 72, 1, 1, 7, '2019-07-17 20:56:50', '1', '2019-07-17 20:56:50', '1'),
(112, 75, 2, 8, 12, '2019-07-17 21:05:49', '1', '2019-07-17 21:05:49', '1'),
(111, 75, 2, 1, 2, '2019-07-17 21:05:49', '1', '2019-07-17 21:05:49', '1'),
(144, 91, 3, 8, NULL, '2019-07-19 22:08:59', '1', '2019-07-19 22:08:59', '1'),
(115, 77, 4, 1, 3, '2019-07-17 21:07:29', '1', '2019-07-17 21:07:29', '1'),
(116, 77, 4, 8, NULL, '2019-07-17 21:07:29', '1', '2019-07-17 21:07:29', '1'),
(117, 78, 5, 1, 6, '2019-07-17 21:10:02', '1', '2019-07-17 21:10:02', '1'),
(118, 78, 5, 8, NULL, '2019-07-17 21:10:02', '1', '2019-07-17 21:10:02', '1'),
(119, 79, 6, 1, 2, '2019-07-18 23:06:06', '1', '2019-07-18 23:06:06', '1'),
(120, 79, 6, 8, 10, '2019-07-18 23:06:06', '1', '2019-07-18 23:06:06', '1'),
(121, 80, 7, 1, 3, '2019-07-19 14:43:23', '1', '2019-07-19 14:43:23', '1'),
(122, 80, 7, 8, 9, '2019-07-19 14:43:23', '1', '2019-07-19 14:43:23', '1'),
(123, 81, 8, 1, 2, '2019-07-19 14:44:04', '1', '2019-07-19 14:44:04', '1'),
(124, 81, 8, 8, 9, '2019-07-19 14:44:04', '1', '2019-07-19 14:44:04', '1'),
(125, 82, 9, 1, 6, '2019-07-19 14:44:49', '1', '2019-07-19 14:44:49', '1'),
(126, 82, 9, 8, 10, '2019-07-19 14:44:49', '1', '2019-07-19 14:44:49', '1'),
(127, 83, 10, 1, 6, '2019-07-19 14:45:28', '1', '2019-07-19 14:45:28', '1'),
(128, 83, 10, 8, NULL, '2019-07-19 14:45:28', '1', '2019-07-19 14:45:28', '1'),
(129, 84, 11, 1, 7, '2019-07-19 14:46:12', '1', '2019-07-19 14:46:12', '1'),
(130, 84, 11, 8, 10, '2019-07-19 14:46:12', '1', '2019-07-19 14:46:12', '1'),
(131, 85, 12, 1, 2, '2019-07-19 14:51:30', '1', '2019-07-19 14:51:30', '1'),
(132, 85, 12, 8, 11, '2019-07-19 14:51:30', '1', '2019-07-19 14:51:30', '1'),
(133, 86, 13, 1, 6, '2019-07-19 14:56:32', '1', '2019-07-19 14:56:32', '1'),
(134, 86, 13, 8, 9, '2019-07-19 14:56:32', '1', '2019-07-19 14:56:32', '1'),
(135, 87, 14, 1, 2, '2019-07-19 15:11:42', '1', '2019-07-19 15:11:42', '1'),
(136, 87, 14, 8, 10, '2019-07-19 15:11:42', '1', '2019-07-19 15:11:42', '1'),
(137, 88, 15, 1, 2, '2019-07-19 15:12:21', '1', '2019-07-19 15:12:21', '1'),
(138, 88, 15, 8, 10, '2019-07-19 15:12:21', '1', '2019-07-19 15:12:21', '1'),
(139, 89, 16, 1, 6, '2019-07-19 15:13:06', '1', '2019-07-19 15:13:06', '1'),
(140, 89, 16, 8, 11, '2019-07-19 15:13:06', '1', '2019-07-19 15:13:06', '1'),
(143, 91, 3, 1, 2, '2019-07-19 22:08:59', '1', '2019-07-19 22:08:59', '1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `oauth_provider` enum('','facebook','google','twitter') DEFAULT NULL,
  `oauth_uid` varchar(50) DEFAULT NULL,
  `oauth_picture` varchar(500) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `file` varchar(100) DEFAULT NULL,
  `couponCode` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `salt`, `fname`, `lname`, `fullname`, `avatar`, `email`, `phone`, `role_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `type`, `oauth_provider`, `oauth_uid`, `oauth_picture`, `active`, `file`, `couponCode`) VALUES
(1, 'super_super_admin', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'ST', 'โปรแกรมเมอร์', 'ST โปรแกรมเมอร์', '', 'admin@admin.com', '', 1, '2019-02-03 16:25:30', 'ADMIN.S', '2019-03-31 17:45:05', '1', 0, NULL, NULL, 'developer', NULL, NULL, NULL, 1, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `usertracking`
--

DROP TABLE IF EXISTS `usertracking`;
CREATE TABLE IF NOT EXISTS `usertracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(100) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `requestUri` text NOT NULL,
  `timestamp` varchar(20) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `agent` text NOT NULL,
  `referer` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
