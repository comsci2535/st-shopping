/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50723
 Source Host           : localhost:3306
 Source Schema         : admin_db

 Target Server Type    : MySQL
 Target Server Version : 50723
 File Encoding         : 65001

 Date: 10/07/2019 00:06:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `timestamp` int(11) NULL DEFAULT NULL,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`, `ip_address`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('3qdtnc0jo4l8tkqqgnr7dan28h8btg48', '127.0.0.1', 1561657067, '__ci_last_regenerate|i:1561656921;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:42:\"http://admindesign.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('ar6c17fgqptlk63kojc33jhdis9j7e6q', '127.0.0.1', 1559123927, '__ci_last_regenerate|i:1559123927;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:42:\"http://admindesign.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('qtnd9360trrmcp9bmilq8lrle12ovjm2', '127.0.0.1', 1559124077, '__ci_last_regenerate|i:1559123927;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:42:\"http://admindesign.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');
INSERT INTO `ci_sessions` VALUES ('v8o9ttu62q7t73j287mbipg9jp93nks3', '127.0.0.1', 1562691941, '__ci_last_regenerate|i:1562691915;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:42:\"http://admindesign.test:82/images/user.png\";s:5:\"roles\";a:29:{i:1;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:307;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:306;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:305;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:295;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:299;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:322;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:308;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:300;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:324;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:325;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:320;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:321;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:312;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:301;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:304;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:313;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:315;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:310;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:318;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:319;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:323;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:316;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:309;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:311;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:8;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:20;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:9;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}i:10;a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}}}');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config`  (
  `variable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lang` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'th',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updateDate` datetime(0) NULL DEFAULT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`variable`, `lang`, `type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('aboutUs', '&lt;h1 style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: THSarabunNew; color: rgb(88, 88, 91);&quot;&gt;Award-Winning Customer Support&lt;/h1&gt;&lt;p style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;Register.com&#039;s award winning support team is here to help you find the online solutions that work best for your business. Our experts are ready to give you the insight you need, so feel free to chat with us if you have any questions.&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;/p&gt;&lt;h1 style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: THSarabunNew; color: rgb(88, 88, 91);&quot;&gt;Award-Winning Customer Support&lt;/h1&gt;&lt;p style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;Register.com&#039;s award winning support team is here to help you find the online solutions that work best for your business. Our experts are ready to give you the insight you need, so feel free to chat with us if you have any questions.&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;/p&gt;&lt;h1 style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: THSarabunNew; color: rgb(88, 88, 91);&quot;&gt;Award-Winning Customer Support&lt;/h1&gt;&lt;p style=&quot;margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;Register.com&#039;s award winning support team is here to help you find the online solutions that work best for your business. Our experts are ready to give you the insight you need, so feel free to chat with us if you have any questions.&lt;/p&gt;&lt;p&gt;&lt;br style=&quot;color: rgb(88, 88, 91); font-family: THSarabunNew; font-size: 20px;&quot;&gt;&lt;/p&gt;', 'th', NULL, '2019-05-16 14:35:24', 'about');
INSERT INTO `config` VALUES ('content', 'Chrn Property Management ถนนประชาอุทิศ ตำบลบางแม่นาง อำเภอบางใหญ่ จังหวัดนนทบุรี 11140\r\nโทรศัพท์: 02-225-3654 โทรสาร: 02-225-3654\r\nอีเมล์: admin@admin.com\r\n', 'th', NULL, '2018-03-15 09:03:35', 'contact');
INSERT INTO `config` VALUES ('content', 'ข่าวสาร บทความ และกิจกรรมของบริษัทฯ', 'th', NULL, '2018-03-11 14:17:02', 'news');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', NULL, '2018-03-15 08:47:18', 'portfolio');
INSERT INTO `config` VALUES ('content', 'บริหารงานโดยบุคลากรที่มีประสบการณ์ มีความรู้ความชำนาญและนักวิชาการให้คำแนะนำวางแผนและจัดระบบ จึงทำให้การทำงานมีประสิทธิภาพ  โดยคัดสรรบุคลากรที่มีคุณภาพ  รักงานบริการมีการอบรมบุคลากรทั้งภาคทฤษฏีและภาคปฏิบัติ  มีเจ้าหน้าที่ตรวจสอบ  การปฏิบัติงานจากส่วนกลางเข้าทำการตรวจสอบเพื่อตรวจสอบการปฏิบัติงานของพนักงาน ให้ได้คุณภาพมาตรฐาน  มีหน่วยปฏิบัติการเข้า พบลูกค้าเพื่อแก้ไขปัญหาต่าง ๆ ของระบบงาน', 'th', NULL, '2018-03-15 09:18:38', 'service');
INSERT INTO `config` VALUES ('content', 'ส่วนหนึ่งของทีมงานบริหารที่มีประสบการณ์การทำงานที่เชี่ยวชาญ', 'th', NULL, '2018-03-10 16:42:36', 'team');
INSERT INTO `config` VALUES ('files', '', 'th', NULL, '2019-05-16 14:35:24', 'about');
INSERT INTO `config` VALUES ('mailAuthenticate', '1', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('mailDefault', 'comsci2535@gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('mailMethod', '1', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('metaDescription', 'Getup School', 'th', NULL, '2019-05-16 09:39:04', 'general');
INSERT INTO `config` VALUES ('metaKeyword', 'Getup School', 'th', NULL, '2019-05-16 09:39:04', 'general');
INSERT INTO `config` VALUES ('senderEmail', 'comsci2535@gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('senderName', 'ซื้อของออนไลน์', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('siteTitle', 'Getup School', 'th', NULL, '2019-05-16 09:39:04', 'general');
INSERT INTO `config` VALUES ('SMTPpassword', '5411425016lee', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('SMTPport', '465', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('SMTPserver', 'smtp.gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail');
INSERT INTO `config` VALUES ('SMTPusername', 'comsci2535@gmail.com', 'th', NULL, '2019-05-26 00:23:26', 'mail');

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module`  (
  `moduleId` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` smallint(6) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT 1 COMMENT '1menu, 2separate',
  `active` tinyint(1) NULL DEFAULT 0 COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `directory` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `class` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order` int(10) NULL DEFAULT 0,
  `method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `param` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isSidebar` int(11) NULL DEFAULT 1 COMMENT '0:hide,1show',
  `isDev` tinyint(1) NULL DEFAULT 0,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'fa fa-angle-double-right',
  `createBy` int(11) NULL DEFAULT 0,
  `updateBy` int(11) NULL DEFAULT 0,
  `createDate` datetime(0) NULL DEFAULT NULL,
  `updateDate` datetime(0) NULL DEFAULT NULL,
  `modify` int(11) NULL DEFAULT 0,
  `view` int(11) NULL DEFAULT 0,
  `print` int(11) NULL DEFAULT 0,
  `import` int(11) NULL DEFAULT 0,
  `export` int(11) NULL DEFAULT 0,
  `descript` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `recycle` int(11) NULL DEFAULT 0,
  `recycleDate` datetime(0) NULL DEFAULT NULL,
  `recycleBy` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`moduleId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 268 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES (1, 0, 1, 1, 'แผงควบคุม', 'admin', 'dashboard', 1, '', NULL, 1, 0, 'flaticon-line-graph', 0, 3, '2016-07-23 08:54:42', '2019-02-04 16:46:17', 0, 1, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (2, 0, 1, 1, 'ตั้งค่า', 'admin', '', 13, '', '', 1, 0, 'fa fa-cogs', 0, 7, '2016-07-23 08:54:42', '2016-12-10 10:37:56', 0, 1, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (8, 2, 1, 1, 'ผู้ใช้งาน', 'admin', 'users', 4, '', '', 1, 0, 'fa fa-user', 0, 1, '2016-07-23 08:54:42', '2019-03-25 22:00:37', 1, 1, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (9, 2, 1, 1, 'พื้นฐาน', 'admin', 'config_general', 6, NULL, '', 1, 0, 'fa fa-circle-o', 0, 1, '2016-07-23 08:54:42', '2019-03-02 15:28:20', 1, 1, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (10, 2, 1, 1, 'อีเมล์', 'admin', 'config_mail', 7, '', '', 1, 0, 'fa fa-circle-o', 0, 1, '2016-07-23 08:54:42', '2019-03-02 15:28:21', 1, 1, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (20, 2, 1, 1, 'กลุ่มผู้ใช้งาน', 'admin', 'roles', 5, '', '', 1, 0, 'fa fa-users', 0, NULL, '2016-07-23 08:54:42', '2019-02-06 22:47:00', 1, 1, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (30, 0, 1, 1, 'โมดูล', 'admin', 'module', 16, '', '', 1, 1, 'fa fa-cog', 0, 7, '2016-07-23 08:54:42', '2016-07-16 13:04:45', 0, 0, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (166, 0, 1, 1, 'คลังไฟล์', 'admin', 'library', 15, '', '', 0, 0, 'fa fa-briefcase', 7, 1, '2016-07-23 08:54:42', '2018-03-23 14:48:52', 0, 0, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (193, 0, 1, 0, 'รายงาน', 'admin', '', 8, '', '', 1, 0, 'fa fa-bar-chart', 7, 1, '2017-01-19 13:06:36', '2019-04-15 15:16:08', 0, 0, 0, 0, 0, '', 1, '2019-04-15 15:16:08', 1);
INSERT INTO `module` VALUES (195, 0, 1, 0, 'ประวัติการใช้งาน', 'admin', 'stat_admin', 14, '', '', 1, 0, 'fa fa-calendar-o', 7, 3, '2017-01-19 13:11:57', '2018-10-13 00:20:58', 0, 0, 0, 0, 0, '', 0, NULL, NULL);
INSERT INTO `module` VALUES (267, 0, 1, 0, 'Repo', 'admin', 'repo', 21, NULL, NULL, 1, 1, 'fa fa-circle-o', 1, 3, '2018-03-16 21:32:34', '2018-07-03 04:28:33', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับrepo', 0, NULL, NULL);

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role`  (
  `role_id` int(11) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES (21, 1, 1);
INSERT INTO `permission_role` VALUES (21, 1, 2);
INSERT INTO `permission_role` VALUES (21, 1, 3);
INSERT INTO `permission_role` VALUES (21, 1, 4);
INSERT INTO `permission_role` VALUES (21, 299, 1);
INSERT INTO `permission_role` VALUES (21, 299, 2);
INSERT INTO `permission_role` VALUES (21, 299, 3);
INSERT INTO `permission_role` VALUES (21, 299, 4);
INSERT INTO `permission_role` VALUES (21, 300, 1);
INSERT INTO `permission_role` VALUES (21, 300, 2);
INSERT INTO `permission_role` VALUES (21, 300, 3);
INSERT INTO `permission_role` VALUES (21, 300, 4);
INSERT INTO `permission_role` VALUES (21, 296, 1);
INSERT INTO `permission_role` VALUES (21, 296, 2);
INSERT INTO `permission_role` VALUES (21, 296, 3);
INSERT INTO `permission_role` VALUES (21, 296, 4);
INSERT INTO `permission_role` VALUES (21, 297, 1);
INSERT INTO `permission_role` VALUES (21, 297, 2);
INSERT INTO `permission_role` VALUES (21, 297, 3);
INSERT INTO `permission_role` VALUES (21, 297, 4);
INSERT INTO `permission_role` VALUES (21, 20, 1);
INSERT INTO `permission_role` VALUES (21, 20, 2);
INSERT INTO `permission_role` VALUES (21, 20, 3);
INSERT INTO `permission_role` VALUES (21, 20, 4);
INSERT INTO `permission_role` VALUES (16, 1, 1);
INSERT INTO `permission_role` VALUES (16, 1, 2);
INSERT INTO `permission_role` VALUES (16, 1, 3);
INSERT INTO `permission_role` VALUES (16, 1, 4);
INSERT INTO `permission_role` VALUES (16, 307, 1);
INSERT INTO `permission_role` VALUES (16, 307, 2);
INSERT INTO `permission_role` VALUES (16, 307, 3);
INSERT INTO `permission_role` VALUES (16, 307, 4);
INSERT INTO `permission_role` VALUES (16, 306, 1);
INSERT INTO `permission_role` VALUES (16, 306, 2);
INSERT INTO `permission_role` VALUES (16, 306, 3);
INSERT INTO `permission_role` VALUES (16, 306, 4);
INSERT INTO `permission_role` VALUES (16, 305, 1);
INSERT INTO `permission_role` VALUES (16, 305, 2);
INSERT INTO `permission_role` VALUES (16, 305, 3);
INSERT INTO `permission_role` VALUES (16, 305, 4);
INSERT INTO `permission_role` VALUES (16, 295, 1);
INSERT INTO `permission_role` VALUES (16, 295, 2);
INSERT INTO `permission_role` VALUES (16, 295, 3);
INSERT INTO `permission_role` VALUES (16, 295, 4);
INSERT INTO `permission_role` VALUES (1, 1, 1);
INSERT INTO `permission_role` VALUES (1, 1, 2);
INSERT INTO `permission_role` VALUES (1, 1, 3);
INSERT INTO `permission_role` VALUES (1, 1, 4);
INSERT INTO `permission_role` VALUES (1, 307, 1);
INSERT INTO `permission_role` VALUES (1, 307, 2);
INSERT INTO `permission_role` VALUES (1, 307, 3);
INSERT INTO `permission_role` VALUES (1, 307, 4);
INSERT INTO `permission_role` VALUES (1, 306, 1);
INSERT INTO `permission_role` VALUES (1, 306, 2);
INSERT INTO `permission_role` VALUES (1, 306, 3);
INSERT INTO `permission_role` VALUES (1, 306, 4);
INSERT INTO `permission_role` VALUES (1, 305, 1);
INSERT INTO `permission_role` VALUES (1, 305, 2);
INSERT INTO `permission_role` VALUES (1, 305, 3);
INSERT INTO `permission_role` VALUES (1, 305, 4);
INSERT INTO `permission_role` VALUES (1, 295, 1);
INSERT INTO `permission_role` VALUES (1, 295, 2);
INSERT INTO `permission_role` VALUES (1, 295, 3);
INSERT INTO `permission_role` VALUES (1, 295, 4);
INSERT INTO `permission_role` VALUES (1, 299, 1);
INSERT INTO `permission_role` VALUES (1, 299, 2);
INSERT INTO `permission_role` VALUES (1, 299, 3);
INSERT INTO `permission_role` VALUES (1, 299, 4);
INSERT INTO `permission_role` VALUES (1, 322, 1);
INSERT INTO `permission_role` VALUES (1, 322, 2);
INSERT INTO `permission_role` VALUES (1, 322, 3);
INSERT INTO `permission_role` VALUES (1, 322, 4);
INSERT INTO `permission_role` VALUES (1, 308, 1);
INSERT INTO `permission_role` VALUES (1, 308, 2);
INSERT INTO `permission_role` VALUES (1, 308, 3);
INSERT INTO `permission_role` VALUES (1, 308, 4);
INSERT INTO `permission_role` VALUES (1, 300, 1);
INSERT INTO `permission_role` VALUES (1, 300, 2);
INSERT INTO `permission_role` VALUES (1, 300, 3);
INSERT INTO `permission_role` VALUES (1, 300, 4);
INSERT INTO `permission_role` VALUES (1, 324, 1);
INSERT INTO `permission_role` VALUES (1, 324, 2);
INSERT INTO `permission_role` VALUES (1, 324, 3);
INSERT INTO `permission_role` VALUES (1, 324, 4);
INSERT INTO `permission_role` VALUES (1, 325, 1);
INSERT INTO `permission_role` VALUES (1, 325, 2);
INSERT INTO `permission_role` VALUES (1, 325, 3);
INSERT INTO `permission_role` VALUES (1, 325, 4);
INSERT INTO `permission_role` VALUES (1, 320, 1);
INSERT INTO `permission_role` VALUES (1, 320, 2);
INSERT INTO `permission_role` VALUES (1, 320, 3);
INSERT INTO `permission_role` VALUES (1, 320, 4);
INSERT INTO `permission_role` VALUES (1, 321, 1);
INSERT INTO `permission_role` VALUES (1, 321, 2);
INSERT INTO `permission_role` VALUES (1, 321, 3);
INSERT INTO `permission_role` VALUES (1, 321, 4);
INSERT INTO `permission_role` VALUES (1, 312, 1);
INSERT INTO `permission_role` VALUES (1, 312, 2);
INSERT INTO `permission_role` VALUES (1, 312, 3);
INSERT INTO `permission_role` VALUES (1, 312, 4);
INSERT INTO `permission_role` VALUES (1, 301, 1);
INSERT INTO `permission_role` VALUES (1, 301, 2);
INSERT INTO `permission_role` VALUES (1, 301, 3);
INSERT INTO `permission_role` VALUES (1, 301, 4);
INSERT INTO `permission_role` VALUES (1, 304, 1);
INSERT INTO `permission_role` VALUES (1, 304, 2);
INSERT INTO `permission_role` VALUES (1, 304, 3);
INSERT INTO `permission_role` VALUES (1, 304, 4);
INSERT INTO `permission_role` VALUES (1, 313, 1);
INSERT INTO `permission_role` VALUES (1, 313, 2);
INSERT INTO `permission_role` VALUES (1, 313, 3);
INSERT INTO `permission_role` VALUES (1, 313, 4);
INSERT INTO `permission_role` VALUES (1, 315, 1);
INSERT INTO `permission_role` VALUES (1, 315, 2);
INSERT INTO `permission_role` VALUES (1, 315, 3);
INSERT INTO `permission_role` VALUES (1, 315, 4);
INSERT INTO `permission_role` VALUES (1, 310, 1);
INSERT INTO `permission_role` VALUES (1, 310, 2);
INSERT INTO `permission_role` VALUES (1, 310, 3);
INSERT INTO `permission_role` VALUES (1, 310, 4);
INSERT INTO `permission_role` VALUES (1, 318, 1);
INSERT INTO `permission_role` VALUES (1, 318, 2);
INSERT INTO `permission_role` VALUES (1, 318, 3);
INSERT INTO `permission_role` VALUES (1, 318, 4);
INSERT INTO `permission_role` VALUES (1, 319, 1);
INSERT INTO `permission_role` VALUES (1, 319, 2);
INSERT INTO `permission_role` VALUES (1, 319, 3);
INSERT INTO `permission_role` VALUES (1, 319, 4);
INSERT INTO `permission_role` VALUES (1, 323, 1);
INSERT INTO `permission_role` VALUES (1, 323, 2);
INSERT INTO `permission_role` VALUES (1, 323, 3);
INSERT INTO `permission_role` VALUES (1, 323, 4);
INSERT INTO `permission_role` VALUES (1, 316, 1);
INSERT INTO `permission_role` VALUES (1, 316, 2);
INSERT INTO `permission_role` VALUES (1, 316, 3);
INSERT INTO `permission_role` VALUES (1, 316, 4);
INSERT INTO `permission_role` VALUES (1, 309, 1);
INSERT INTO `permission_role` VALUES (1, 309, 2);
INSERT INTO `permission_role` VALUES (1, 309, 3);
INSERT INTO `permission_role` VALUES (1, 309, 4);
INSERT INTO `permission_role` VALUES (1, 311, 1);
INSERT INTO `permission_role` VALUES (1, 311, 2);
INSERT INTO `permission_role` VALUES (1, 311, 3);
INSERT INTO `permission_role` VALUES (1, 311, 4);
INSERT INTO `permission_role` VALUES (1, 8, 1);
INSERT INTO `permission_role` VALUES (1, 8, 2);
INSERT INTO `permission_role` VALUES (1, 8, 3);
INSERT INTO `permission_role` VALUES (1, 8, 4);
INSERT INTO `permission_role` VALUES (1, 20, 1);
INSERT INTO `permission_role` VALUES (1, 20, 2);
INSERT INTO `permission_role` VALUES (1, 20, 3);
INSERT INTO `permission_role` VALUES (1, 20, 4);
INSERT INTO `permission_role` VALUES (1, 9, 1);
INSERT INTO `permission_role` VALUES (1, 9, 2);
INSERT INTO `permission_role` VALUES (1, 9, 3);
INSERT INTO `permission_role` VALUES (1, 9, 4);
INSERT INTO `permission_role` VALUES (1, 10, 1);
INSERT INTO `permission_role` VALUES (1, 10, 2);
INSERT INTO `permission_role` VALUES (1, 10, 3);
INSERT INTO `permission_role` VALUES (1, 10, 4);

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `name_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `created_at` timestamp(0) NULL DEFAULT NULL COMMENT 'เวลาสร้าง',
  `updated_at` timestamp(0) NULL DEFAULT NULL COMMENT 'เวลาแก้ไข',
  PRIMARY KEY (`permission_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'เข้าถึง', 'view', '2018-01-21 17:24:49', '2018-01-21 17:24:52');
INSERT INTO `permissions` VALUES (2, 'สร้าง', 'create', '2018-01-21 17:27:13', '2018-01-21 17:27:15');
INSERT INTO `permissions` VALUES (3, 'แก้ไข', 'edit', '2018-01-21 17:27:40', '2018-01-21 17:27:42');
INSERT INTO `permissions` VALUES (4, 'ลบ', 'delete', '2018-01-21 17:28:20', '2018-01-21 17:28:22');

-- ----------------------------
-- Table structure for repo
-- ----------------------------
DROP TABLE IF EXISTS `repo`;
CREATE TABLE `repo`  (
  `repoId` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lang` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `excerpt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` tinyint(1) NULL DEFAULT 0 COMMENT '0=Disabled,1=Enabled',
  `order` int(10) NULL DEFAULT 0,
  `created_at` datetime(0) NULL,
  `created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `updated_at` datetime(0) NULL,
  `updated_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `recycle` int(11) NULL DEFAULT 0,
  `recycle_at` datetime(0) NULL DEFAULT NULL,
  `recycle_by` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`repoId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `role_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT 0,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `create_by` int(11) UNSIGNED NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `update_by` int(11) UNSIGNED NULL DEFAULT NULL,
  `recycle` int(11) NULL DEFAULT 0,
  `recycle_at` datetime(0) NULL DEFAULT NULL,
  `recycle_by` int(11) NULL DEFAULT NULL,
  `visible` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'ผู้ดูแลระบบ', 'บริหารจัดการระบบ', 1, '2016-07-13 10:55:21', NULL, '2019-05-16 14:40:14', 1, 0, NULL, NULL, 0);
INSERT INTO `roles` VALUES (16, 'ผู้อัพเดทข้อมูลเว็บไซต์', 'บริหารจัดการข้อมูลเว็บไซต์', 1, '2018-08-05 00:20:00', 22, '2019-03-29 23:50:32', 1, 0, NULL, NULL, 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fullname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `recycle` int(11) NULL DEFAULT 0,
  `recycle_at` datetime(0) NULL DEFAULT NULL,
  `recycle_by` int(11) NULL DEFAULT NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oauth_provider` enum('','facebook','google','twitter') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oauth_uid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `oauth_picture` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` int(1) NULL DEFAULT 0,
  `file` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `couponCode` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'super_super_admin', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'ST', 'โปรแกรมเมอร์', 'ST โปรแกรมเมอร์', '', 'admin@admin.com', '', 1, '2019-02-03 16:25:30', 'ADMIN.S', '2019-03-31 17:45:05', '1', 0, NULL, NULL, 'developer', NULL, NULL, NULL, 1, NULL, '');

-- ----------------------------
-- Table structure for usertracking
-- ----------------------------
DROP TABLE IF EXISTS `usertracking`;
CREATE TABLE `usertracking`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `requestUri` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `timestamp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `agent` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `referer` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
