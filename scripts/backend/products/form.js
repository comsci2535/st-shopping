"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true
        }
    });

    $(document).on('click', '.btn-product-attributes-add', function(){
        var html  = '<div id="" class="form-group m-form__group bok-attributes">';
            html += $('#display-attributes div').filter(function(index){ return index === 0 }).html();
            html += '</div>';
        $('#display-attributes').append(html);
        var n = 1;
        $("#display-attributes .bok-attributes").each(function(){
            $(this).attr('id','display-bok-attributes-'+n);
            $(this).find('.btn-product-attributes-add').attr('data-box','display-bok-attributes-'+n);
            $(this).find('.btn-product-attributes-delete').attr('data-box','display-bok-attributes-'+n).removeClass('d-none');
            $(this).find('.text-attributes-line-checkbox').attr('name','attributes_line['+(n-1)+'][]').attr('data-row', n);
            $(this).find('.text-attributes-checkbox').attr('name','attributes_h['+(n-1)+'][]');
            $(this).find('.check-product-unlimit').attr('data-row', 'display-bok-attributes-'+n);
            $(this).find('.m-checkbox-inline').attr('data-row', n);
            $(this).find('img.img-thumbnail').attr('id', 'text-img-file-'+n);
            $(this).find('.custom-outfile-input').attr('name', 'outfile_'+n);
            $(this).find('.custom-file-input').attr('name', 'file_'+n).attr('data-img', 'text-img-file-'+n);
            n++;
        });

        var line = $('#display-attributes .bok-attributes').length;
        
        $('#display-bok-attributes-'+line).find('.text-attributes-line-checkbox').prop('checked', false);
        $('#display-bok-attributes-'+line).find('img').attr('src','');
        $('#display-bok-attributes-'+line).find('.custom-file-input').val('');
        $('#display-bok-attributes-'+line).find('.custom-file-label').html('Choose file');
        console.log(line);
    });

    $(document).on('click', '.btn-product-attributes-delete', function(){
        
        var file_delete = $('#display-attributes #'+$(this).attr('data-box')).find('.custom-outfile-input').val();
        if(file_delete !=''){
            $('#display-attributes').append('<input type="hidden" name="file_delete[]" value="'+file_delete+'">');
        }
        $('#display-attributes #'+$(this).attr('data-box')).remove();
        var n = 1;
        $("#display-attributes .bok-attributes").each(function(){
            $(this).attr('id','display-bok-attributes-'+n);
            $(this).find('.btn-product-attributes-add').attr('data-box','display-bok-attributes-'+n);
            $(this).find('.btn-product-attributes-delete').attr('data-box','display-bok-attributes-'+n);
            $(this).find('.text-attributes-line-checkbox').attr('name','attributes_line['+(n-1)+'][]').attr('data-row', n);
            $(this).find('.text-attributes-checkbox').attr('name','attributes_h['+(n-1)+'][]');
            $(this).find('.m-checkbox-inline').attr('data-row', n);
            $(this).find('.check-product-unlimit').attr('data-row', 'display-bok-attributes-'+n);
            $(this).find('img.img-thumbnail').attr('id', 'text-img-file-'+n);
            $(this).find('.custom-outfile-input').attr('name', 'outfile_'+n);
            $(this).find('.custom-file-input').attr('name', 'file_'+n).attr('data-img', 'text-img-file-'+n);
            n++;
        });
        var line = $('#display-attributes .bok-attributes .btn-product-attributes-delete').length;
        if(line == 1){
            $('#display-attributes .bok-attributes .btn-product-attributes-delete').addClass('d-none');
        }
        
    })
    $(document).on('click', '.btn-product-img-add', function(){
        var html = '';
        var n = $('#table-product-img tbody tr').length +1;
        if(n <= 6){
            html+= '<tr id="tr-img-'+n+'">';
                html+= '<td class="text-center">';
                    html+= '<img src="'+baseUrl+'images/no-image.png'+'" id="text-img-file-'+n+'" class="img-thumbnail" alt="Cinque Terre" style="width:150px">';
                html+= '</td>';
                html+= '<td>';
                    html+= '<div class="custom-file">';
                        html+= '<input type="file" class="custom-file-input" name="file_'+n+'" data-img="text-img-file-'+n+'">';
                        html+= '<label class="custom-file-label" for="customFile">Choose file</label>';
                        html+= '<small class="m--font-danger" >ขนาดรูปต้องกว้าง ยาว 800 x 800 px ไซต์ไม่เกิน 300 Kb นามสกุลไฟล์ jpg jpeg png เท่านั้น</small>';
                        html+= '<input type="hidden" name="img_id[]" valus="">';
                        html+= '<input type="hidden" name="loop_id[]" valus="1">';
                        html+= '<input type="hidden" name="outfile[]" valus="">';
                        html+= '<p style="margin-top:10px">';
                            html+= '<button type="button" class="btn btn-success btn-sm btn-product-img-add">';
                            html+= '<i class="fa fa-plus" aria-hidden="true"></i> </button>';
                            html+= '<button type="button" class="btn btn-danger btn-sm btn-product-img-delete" data-tr="tr-img-'+n+'">';
                            html+= '<i class="fa fa-times" aria-hidden="true"></i> </button>&nbsp';
                        html+= '</p>';
                    html+= '</div>';
                html+= '</td>';
            html+= '</tr>';

            $('#table-product-img tbody').append(html);
        }else{
            alert('ไม่สามารถเพิ่มรูปภาพ เนื่องจากเกินจำนวนที่กำหนด');
        }
    })

    $(document).on('click', '.btn-product-img-delete', function(){
        $('#table-product-img #'+$(this).attr('data-tr')).remove();
        var n = 1;
        $("#table-product-img tbody tr").each(function(){
            $(this).attr('id','tr-img-'+n);
            $(this).find('.btn-product-img-delete').attr('data-tr','tr-img-'+n);
            $(this).find('.custom-file-input').attr('data-img','text-img-file-'+n);
            n++;
        });
    })

    $(document).on('change', 'input[type="file"]', function(ee){ 
        if(ee.target.files[0].type =="image/png" 
            || ee.target.files[0].type =="image/jpg" 
            || ee.target.files[0].type =="image/jpeg"
        ){

            console.log(ee.target.files[0].size);
            var $this = $(this);
            var img     = $(this).attr('data-img');
            var reader  = new FileReader();
            reader.onload = function (e) { 
                var image = new Image();  
                image.src = e.target.result;  
                image.onload = function () { 
                    var height = this.height;
                    var width = this.width;
                    if (height != 800 || width != 800 || ee.target.files[0].size > 307200) { 
                        toastr["error"]("กรุณาตรวจสอบขนาดรูปภาพ", "");
                        $this.parents('div').find('.custom-file-label').html('Choose file');
                        return false;
                    }else{
                        $('#'+img).attr('src', e.target.result); 
                        return true;
                    } 
                }; 
            };
            reader.readAsDataURL(ee.target.files[0]);
        }else{
            toastr["error"]("กรุณาตรวจสอบประเภทรูปภาพ", "");
        }
    });

    $(document).on('change', '.bok-attributes .check-product-unlimit', function(){
        var $chk = $(this);
        if(this.checked){
            $('#'+$chk.attr('data-row')).find('.text-product-unlimit').val(1);
            $('#'+$chk.attr('data-row')).find('.text-input-stock-qty').hide();
        }else{
            $('#'+$chk.attr('data-row')).find('.text-product-unlimit').val(0);
            $('#'+$chk.attr('data-row')).find('.text-input-stock-qty').show();
        }
    });

    $(document).on('change', '.m-checkbox-inline [type="checkbox"]', function(){
        if (this.checked) {
            $('[data-row="'+$(this).attr('data-row')+'"].m-checkbox-inline [data-id="'+$(this).data('id')+'"]').attr("disabled", true);
            $(this).removeAttr("disabled");
        } else {
            $('[data-row="'+$(this).attr('data-row')+'"].m-checkbox-inline [data-id="'+$(this).data('id')+'"]').attr("disabled", false);
        }
    });

})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
