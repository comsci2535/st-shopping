"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        //language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie')},
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "no", width: "20px", className: "", orderable: false},
            {data: "title", className: "", orderable: true},
            {data: "order_qty",  width: "50px", className: "text-center", orderable: true},
            {data: "view_qty", width: "50px", className: "text-center", orderable: true},
            {data: "input_order_qty", width: "150px", className: "text-left", orderable: false},
            {data: "input_view_qty", width: "150px", className: "text-center", orderable: false},
            {data: "updated_at", width: "70px", className: "", orderable: true},
            // {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })

    $(document).on('click', '.btn-add-view-qty', function () {
        var id              = $(this).attr('data-id');
        var row             = $(this).attr('data-row-id');
        var qty             = $('#'+row).find('.text-view-qty').val();
        var checked         = $('#'+row).find('.check-view-qty:checked').val();
        
        if(qty == undefined || qty == ''){
            swal({
                title                : 'กรุณากรอกข้อมูลเลขเพิ่มจำนวนคนดู',
                text                 : "",
                type                 : 'warning',
                showCancelButton     : false,
                confirmButtonText    : 'ตกลง',
             }).then(function(result){
                 
                if (result.value){
                    $('#'+row).find('.text-view-qty').focus();
                }
            });

            return false;
        }
        
        $.ajax({
            url: controller+"/action/view_qty",
            type: 'POST',
            data: {
                id              : id,
                view_qty        : qty,
                checked         : checked,
                mode            : 'view_qty',
                csrfToken       : get_cookie('csrfCookie')
            },
            success: function(result){
                if (result.success === true) {
                    toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
                    $('#'+row).find('.btn-add-view-qty i').removeClass('fa-save').addClass('fa-check');
                    $('#'+row).find('.btn-add-view-qty').removeClass('btn-primary').addClass('btn-success');
                    setTimeout(() => {
                        dataList.DataTable().draw();
                    }, 2000);
                    
                }
                
            }
        }).fail(function () {
            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
        });

    })

    $(document).on('click', '.btn-add-order-qty', function () {
        var id              = $(this).attr('data-id');
        var row             = $(this).attr('data-row-id');
        var qty             = $('#'+row).find('.text-order-qty').val();
        var checked         = $('#'+row).find('.check-order-qty:checked').val();
        console.log(checked);
        if(qty == undefined || qty == ''){
            swal({
                title                : 'กรุณากรอกข้อมูลเลขเพิ่มจำนวนที่ขาย',
                text                 : "",
                type                 : 'warning',
                showCancelButton     : false,
                confirmButtonText    : 'ตกลง',
             }).then(function(result){
                 
                if (result.value){
                    $('#'+row).find('.text-order-qty').focus();
                }
            });

            return false;
        }
        
        $.ajax({
            url: controller+"/action/order_qty",
            type: 'POST',
            data: {
                id              : id,
                order_qty       : qty,
                checked         : checked,
                mode            : 'order_qty',
                csrfToken       : get_cookie('csrfCookie')
            },
            success: function(result){
                if (result.success === true) {
                    toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
                    $('#'+row).find('.btn-add-order-qty i').removeClass('fa-save').addClass('fa-check');
                    $('#'+row).find('.btn-add-order-qty').removeClass('btn-primary').addClass('btn-success');
                    setTimeout(() => {
                        dataList.DataTable().draw();
                    }, 2000);
                    
                }
                
            }
        }).fail(function () {
            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
        });

    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
