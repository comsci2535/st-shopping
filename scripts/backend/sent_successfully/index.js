
"use strict";
$(document).ready(function () {

    $('#form-submit-transport').validate({
        rules: {
            transportID: true,
            trackingCode:true
        }
    });

    $('#transport_id').select2({
        placeholder: 'เลือกการขนส่ง'
    });

    $(document).on('click','.btn-edit-send', function(){
        var $this = $(this); 
        var tracking_code   = $this.parents('tr').find('.text-tracking-code').attr('data-tracking');
        var tracking_id     = $this.parents('tr').find('.text-tracking-id').attr('data-transport'); 
        $("#transportID").val(tracking_id).trigger('change');
        $('#textOrderID').val($this.attr('data-id')); 
        $('#trackingCode').val(tracking_code);
        $('#m_modal-transport').modal('show');
    });   

    //ย้ายรายการ order ไปยังกำลังจัดส่งสินค้า
    $("#form-submit-transport").submit(function(e) {
        e.preventDefault();  
        //BOOTBOX
        var url             = "tracking_trucks/action/tracking"; 
        var transport_id    = $("#transportID option:selected").val();
        var tracking        = $("#trackingCode").val(); 
        var id              = $('#textOrderID').val(); 
        swal({
            title                : 'กรุณายืนยันการทำรายการ',
            text                 : "",
            type                 : 'warning',
            showCancelButton     : true,
            confirmButtonText    : 'ตกลง',
            cancelButtonText    : 'ยกเลิก'
        }).then(function(result){
            if (result.value){
                $.ajax({
                    url        : url,
                    type       : 'POST',
                    dataType   : 'json',
                    headers    : {
                        'X-CSRF-TOKEN': csrfToken
                    },
                    data: {
                        id              : id,
                        tracking_code   : tracking,
                        transport_id    : transport_id,
                        mode            : 'tracking',
                        csrfToken       : get_cookie('csrfCookie')
                    },
                })
                    .done(function (data) {
                    if (data.success === true) {
                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne) 
                        $('#m_modal-transport').modal('hide');
                        dataList.DataTable().draw()
                    } else if (data.success === false) {
                        $('#overlay-box').addClass('hidden');
                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                    }
                    arrayId = []
                })
                .fail(function () {
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
                    arrayId = []
                })
            }
        }); 
    })

    load_datatable(); // load datable
    
    $(document).on('click','#btn-search-order', function(){
        load_datatable();
    });

    $(document).on('click','#btn-search-cancel', function(){
        
        $("#order_month").val(null).trigger('change');
        $("#order_status").val(null).trigger('change');
        $("#transport_id").val(null).trigger('change');
        load_datatable();
    });

    // create Daterange 
    $('input[name="dateRange"]').daterangepicker({
        locale: { 
            // cancelLabel: 'ยกเลิก',
            //  applyLabel:'ตกลง', 
            format: 'DD/MM/YYYY',
            daysOfWeek: [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
               ],
               "monthNames": [
               "มกราคม",
               "กุมภาพันธ์",
               "มีนาคม",
               "เมษายน",
               "พฤษภาคม",
               "มิถุนายน",
               "กรกฎาคม",
               "สิงหาคม",
               "กันยายน",
               "ตุลาคม",
               "พฤศจิกายน",
               "ธันวาคม"
              ],
        },
        autoApply : true,
        autoUpdateInput: false,
        showDropdowns: true,
    })   
    
})

function load_datatable(){
    var order_year      = $('#order_year option:selected').val();
    var order_month     = $('#order_month option:selected').val();
    var order_day       = $('#order_day option:selected').val();
    var order_status    = $('#order_status option:selected').val();
    var transport_id    = $('#transport_id').val();
    var dataList        = $('#data-list').DataTable();
    dataList.destroy();
    dataList = $('#data-list').DataTable({
        serverSide: true,
        processing: true,
        "scrollX": true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {
                csrfToken    : get_cookie('csrfCookie'),
                order_year   : order_year,
                order_month  : order_month,
                order_day    : order_day,
                status       : order_status,
                transport_id : transport_id
            },
        },
        order: [[2, "desc"]],
        pageLength: 100,
        columns: [
            {data: "name", width: "20px", className: "", orderable: false},
            {data: "tracking_code", width: "10px", className: "text-center", orderable: false}, 
            {data: "order_code", width: "30px", className: "", orderable: true},
            {data: "tracking_title", width: "10px", className: "text-center", orderable: false}, 
            {data: "invoice", width: "20px", className: "text-center", orderable: false},
            {data: "tel", className: "", orderable: false},
            {data: "payment_type", width: "10px", className: "", orderable: false},
            {data: "sales", width: "10px", className: "text-center", orderable: false},
            {data: "money", width: "10px", className: "text-center", orderable: false},
            {data: "vat", width: "10px", className: "text-center", orderable: false},
            {data: "product_price", width: "10px", className: "text-center", orderable: false},
            {data: "cost", width: "10px", className: "text-center", orderable: false}, 
            {data: "profit", className: "", orderable: false}, 
            {data: "created_at", width: "10px", className: "", orderable: false},
            {data: "status", width: "10px", className: "text-center", orderable: false},
            {data: "action", width: "10px", className: "text-center", orderable: false},
        ]
        }).on('draw', function () {
            $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
            $('.tb-check-single').iCheck(iCheckboxOpt);
        }).on('processing', function(e, settings, processing) {
            if ( processing ) {
                $('#overlay-box').removeClass('hidden');
            } else {
                $('#overlay-box').addClass('hidden');
                $('.statusPicker').selectpicker({width: '100%'});
            }
        })
}

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
