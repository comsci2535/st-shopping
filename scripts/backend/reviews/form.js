"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true,
            excerpt: {
                required:true
            },
            file: {
                required:false
            }
        }
    });

    // form validate editor
    $('form').each(function () {
        if ($(this).data('validator'))
            $(this).data('validator').settings.ignore = ".note-editor *";
    });

    /* fileinput */
    if(method == 'create'){
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: false,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Picture",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });

        $('#review_file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: false,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Picture",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop picture here …",
        });
    }
    
    if(method == 'edit'){
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewAsData: false,
            initialPreviewConfig: [
            { downloadUrl: file_image  ,extra: {id: file_id, csrfToken: get_cookie('csrfCookie')} },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });

        $('#review_file').fileinput({
            initialPreview: [file_review_file],
            initialPreviewAsData: false,
            initialPreviewConfig: [
            { downloadUrl: file_review_file  ,extra: {id: file_id, csrfToken: get_cookie('csrfCookie')} },
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 0,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: false,
            initialPreviewAsData: true,
            maxFileSize:2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
            browseLabel: "Select Image",
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle :"Drag & drop icon here …",
        });
    }
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
