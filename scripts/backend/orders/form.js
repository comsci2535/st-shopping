"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            title: true,
            slug:true
        }
    });

    // $(document).on('change','.text-attributes-line-checkbox', function(){
    //     $('.text-attributes-line-checkbox').prop('checked', false);
    //     $(this).prop('checked', true); 
    // });

    //zun
    $.Thailand({
        database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#districts'), // input ของตำบล
        $amphoe: $('#amphures'), // input ของอำเภอ
        $province: $('#provinces'), // input ของจังหวัด
        $zipcode: $('#zip_code'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
            console.log('Autocomplete is ready!')
        },
        onDataFill: function(data){
            console.log(data);
        }
    });

    $.Thailand({
        // database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#invoice_districts_id'), // input ของตำบล
        $amphoe: $('#invoice_amphures_id'), // input ของอำเภอ
        $province: $('#invoice_provinces_id'), // input ของจังหวัด
        $zipcode: $('#invoice_zip_code'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
            console.log('Autocomplete is ready!')
        },
        onDataFill: function(data){
            console.log(data);
        }
    });

    if($('#is_invoice').is(':checked')){
        $('.display-vat').show();
        if($('#invoice_type').is(':checked')){
            $('.m-view-vat').hide();
            $('.vat-all').show();
            $('.invoice_no').show();
        }
    }

    $(document).on('click','#is_invoice', function(){
    
        if($(this).is(':checked')){
            $('.display-vat').show();
            $('.m-view-vat').show();
        }else{
            $('#invoice_type').prop('checked', false);
            $('.display-vat').hide();
        }
    });
    
    $(document).on('click','#invoice_type', function(){
        $('#is_invoice').prop('checked', true);
        if($(this).is(':checked')){
            if($('#is_invoice').is(':checked')){
                 $('.display-vat').show();
                 $('.m-view-vat').hide();
            }else{
                $('.display-vat').hide();
            }
        }else{
            $('.m-view-vat').show();
            $('.display-vat').show();
        }
    });
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
