"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        //language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url     : controller+"/data_index",
            type    : 'POST',
            data    : {
                csrfToken   :get_cookie('csrfCookie'),
                status      : [1,2,3]
            },
        },
        order: [[1, "DESC"]],
        pageLength: 10,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "created_at", width: "100px", className: "", orderable: true},
            {data: "order_code", className: "", orderable: true},
            {data: "name", className: "", orderable: true},
            {data: "tel", className: "", orderable: false},
            {data: "discount", className: "", orderable: false},
            {data: "payment_type", width: "110px", className: "", orderable: false},
            {data: "status", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })

    //ย้ายรายการ order ไปยังกำลังจัดส่งสินค้า
    $(document).on('click', '.multi-send-order', function () {
        //BOOTBOX
        var url = controller+'/send_order';
        var set = $('#data-list .tb-check-single');
        $('#overlay-box').removeClass('hidden');  
       
        $(set).each(function () {
            if ($(this).is(":checked")) {
                arrayId.push($(this).closest('tr').attr('id'));
            }
        });
        if (arrayId.length > 0) {
            swal({
               title                : 'กรุณายืนยันการทำรายการ',
               text                 : "",
               type                 : 'warning',
               showCancelButton     : true,
               confirmButtonText    : 'ตกลง',
                cancelButtonText    : 'ยกเลิก'
            }).then(function(result){
               if (result.value){
                   $.ajax({
                        url        : url,
                        type       : 'POST',
                        dataType   : 'json',
                        headers    : {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        data: {
                            id          : arrayId,
                            csrfToken   : csrfToken},
                    })
                     .done(function (data) {
                        if (data.success === true) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                            $('#data-list .tb-check-all').iCheck('uncheck');
                            dataList.DataTable().draw()
                        } else if (data.success === false) {
                            $('#overlay-box').addClass('hidden');
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        }
                        arrayId = []
                    })
                    .fail(function () {
                        toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
                        arrayId = []
                    })
                }
            });
        } else {
            alert_box('กรุณาเลือกรายการสั่งซื้อที่ต้องการ');
        }
   })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
