"use strict";
$(document).ready(function () {
    load_datatable();

    $('#form-submit-transport').validate({
        rules: {
            transportID: true,
            trackingCode:true
        }
    });

    $('#transport_id').select2({
        placeholder: 'เลือกการขนส่ง'
    });

    $(document).on('click','.btn-edit-send', function(){
        var $this = $(this); 
        var tracking_code   = $this.parents('tr').find('.text-tracking-code').attr('data-tracking');
        var tracking_id     = $this.parents('tr').find('.text-tracking-id').attr('data-transport'); 
        $("#transportID").val(tracking_id).trigger('change');
        $('#textOrderID').val($this.attr('data-id')); 
        $('#trackingCode').val(tracking_code);
        $('#m_modal-transport').modal('show');
    });  

     $(document).on('click','#btn-search-order', function(){
        load_datatable();
    });

    $(document).on('click','#btn-search-cancel', function(){
        
        $("#order_month").val(null).trigger('change');
        $("#order_status").val(null).trigger('change');
        load_datatable();
    });

});

$(document).on('click', '.multi-print-checkandbox', function () {
    var set = $('#data-list .tb-check-single');
    var html_input = '';
    $(set).each(function () {
        if ($(this).is(":checked")) {
            arrayId.push($(this).closest('tr').attr('id'));
            html_input+='<input type="hidden" name="arr_code[]" value="'+$(this).closest('tr').attr('id')+'">';
        }
    });
    if (arrayId.length > 0) {
        var html = '<form id="form-submit-orderprint-check" action="'+baseUrlFull+''+controller+'/printvat" method="post">';
        html+= html_input;
        html+= '<input type="hidden" name="csrfToken" value="'+get_cookie('csrfCookie')+'">';
        html+= '</form>';
        $('.m-portlet__body').append(html);
        $('#form-submit-orderprint-check').submit();
    } else {
        alert_box('กรุณาเลือกรายการที่ต้องการปริ้น');
    }
})

function load_datatable(){
    var order_year      = $('#order_year option:selected').val();
    var order_month     = $('#order_month option:selected').val();
    var order_day       = $('#order_day option:selected').val();
    var order_status    = $('#order_status option:selected').val();
    var transport_id    = $('#transport_id').val();
    var dataList        = $('#data-list').DataTable();
    dataList.destroy();
    dataList = $('#data-list').DataTable({
        serverSide: true,
        processing: true,
        ajax: {
            url: controller+"/data_index",
            type: 'POST',
            data: {
                csrfToken    : get_cookie('csrfCookie'),
                order_year   : order_year,
                order_month  : order_month,
                order_day    : order_day,
                status       : order_status,
                transport_id : transport_id
            },
        },
        order: [[1, "desc"]],
        pageLength: 100,
        columns: [
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "created_at", width: "100px", className: "", orderable: true},
            {data: "order_code", className: "", orderable: true},
            {data: "name", className: "", orderable: true},
            {data: "tel", className: "", orderable: false},
            {data: "discount", className: "", orderable: false},
            {data: "payment_type", width: "150px", className: "", orderable: false},
            {data: "status", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
        ]
        }).on('draw', function () {
            $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
            $('.tb-check-single').iCheck(iCheckboxOpt);
        }).on('processing', function(e, settings, processing) {
            if ( processing ) {
                $('#overlay-box').removeClass('hidden');
            } else {
                $('#overlay-box').addClass('hidden');
                $('.statusPicker').selectpicker({width: '100%'});
            }
        }).on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
            $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
            $('.tb-check-single').iCheck(iCheckboxOpt);
        });
}
    
