"use strict";
$(document).ready(function () {
    
    dataList.DataTable({
        //language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        ajax: {
            url     : controller+"/data_index",
            type    : 'POST',
            data    : {
                csrfToken   :get_cookie('csrfCookie'),
                status      : [4]
            },
        },
        order: [[1, "asc"]],
        pageLength: 10,
        columns: [
            {data: "no", width: "20px", className: "text-center", orderable: false},
            {data: "created_at", width: "100px", className: "", orderable: true},
            {data: "order_code", className: "", orderable: true},
            {data: "name", className: "", orderable: true},
            {data: "tel", className: "", orderable: false},
            {data: "discount", className: "", orderable: false},
            {data: "status", width: "50px", className: "text-center", orderable: false},
            {data: "input", width: "20%", className: "", orderable: false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt);
        $('.tb-check-single').iCheck(iCheckboxOpt);
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })

    $(document).on('click', '.btn-add-tracking', function () {
        var id              = $(this).attr('data-id');
        var row             = $(this).attr('data-row-id');
        var tracking        = $('#'+row).find('.text-tracking').val();
        var transport_id    = $('#'+row).find('.transport_id:checked').val(); 
        if(tracking == ''){
            swal({
                title                : 'กรุณากรอกข้อมูลเลข Tracking',
                text                 : "",
                type                 : 'warning',
                showCancelButton     : false,
                confirmButtonText    : 'ตกลง',
             }).then(function(result){
                console.log(result.value);
                if (result.value){
                    $('#'+row).find('.text-tracking').focus();
                }
            });

            return false;
        }
        if(transport_id == undefined){
            swal({
                title                : 'กรุณาเลือกบริษัทจัดส่ง',
                text                 : "",
                type                 : 'warning',
                showCancelButton     : false,
                confirmButtonText    : 'ตกลง',
             }).then(function(result){
                if (result.value){
                    $('#'+row).find('.text-tracking').focus();
                }
            });

            return false;
        }
        
        $.ajax({
            url: controller+"/action/tracking",
            type: 'POST',
            data: {
                id              : id,
                tracking_code   : tracking,
                transport_id    : transport_id,
                mode            : 'tracking',
                csrfToken       : get_cookie('csrfCookie')
            },
            success: function(result){
                if (result.success === true) {
                    toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "")
                    $('#'+row).find('.btn-add-tracking i').removeClass('fa-save').addClass('fa-check');
                    $('#'+row).find('.btn-add-tracking').removeClass('btn-primary').addClass('btn-success');
                    setTimeout(() => {
                        dataList.DataTable().draw();
                    }, 2000);
                    
                }
                
            }
        }).fail(function () {
            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
        });

    })
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
