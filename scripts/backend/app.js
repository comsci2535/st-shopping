"use strict";

var bootstrapToggleOpt = {
    on: '<i class="fa fa-check"></i>',
    off: '<i class="fa fa-times"></i>',
    style: 'android',
    size: 'small'
}

var iCheckboxOpt = {
    checkboxClass: 'icheckbox_square-grey',
    radioClass: 'iradio_square-grey'
}

var tbCheckAll = 0;
var csrfToken = get_cookie('csrfCookie')
var dataList = $('#data-list')
var arrayId = []

$(document).ready(function () {


    //Initialize Select2 Elements
    $(".m-select2").select2();

    $(document).on('keyup', '.amount', function(){
        $(this ).val( formatAmount( $( this ).val() ) );  
    })

    // $('.amount').keyup( function() {
    //     $(this ).val( formatAmount( $( this ).val() ) );  
    // });

    $.validator.setDefaults({
        highlight: function (element) {
            $(element).parent('div').addClass('has-error')
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('has-error')
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ( $(element).hasClass('select2') ) {
                error.insertAfter(element.next('span'))
            } else {
                error.insertAfter(element)
            }
        }
    });

    $.validator.addMethod('isEmail', function (value, element) {
        return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
    }, 'โปรดระบุรูปแบบอีเมล์ที่ถูกต้อง');

    $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt)
    $('input.icheck').iCheck(iCheckboxOpt);
    toastr.options = {closeButton: true, progressBar: true, showMethod: 'slideDown', timeOut: 2000};

    // กำหนดสถานะจากตาราง
    var errorToggle = false
    $('#data-list').on('change', '.bootstrapToggle', function () {
        var bootstrapToggle = $(this)
        var id = $(this).closest('tr').attr('id')
        var status = $(this).prop('checked')
        if (!errorToggle) {
            arrayId.push(id);
            $('#overlay-box').removeClass('hidden');
            $.post($(this).data('method'), {id: arrayId, status: status, csrfToken: csrfToken})
            .done(function (data) {
                $('#overlay-box').addClass('hidden');
                if (data.success === true) {
                    toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                } else if (data.success === false) {
                    toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                    errorToggle = true
                    bootstrapToggle.bootstrapToggle('toggle')
                    errorToggle = false
                }
                arrayId = []
            })
            .fail(function () {
                $('#overlay-box').addClass('hidden');
                toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
                errorToggle = true
                bootstrapToggle.bootstrapToggle('toggle')
                errorToggle = false
                arrayId = []
            })
        }
    });

    //ย้ายรายการลงถังขยะ กู้รายการจากถังขยะ ลบรายการถาวร 
    $('#data-list').on('click', '.tb-action', function () {
         //BOOTBOX
         var id = $(this).closest('tr').attr('id')
         var url = $(this).data('method');

         $('#overlay-box').removeClass('hidden');        
         arrayId.push(id)
         swal({
            title: 'กรุณายืนยันการทำรายการ',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(function(result) {

            if (result.value) {
                $.ajax({
                  url: url,
                  type: 'POST',
                  dataType: 'json',
                  headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                data: {id: arrayId, csrfToken: csrfToken},
            })
                .done(function (data) {
                    if (data.success === true) {
                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        $('#data-list .tb-check-all').iCheck('uncheck');
                        dataList.DataTable().draw()
                    } else if (data.success === false) {
                        $('#overlay-box').addClass('hidden');
                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                    }
                    arrayId = []
                })
                .fail(function () {
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "")
                    arrayId = []
                })

            }
        });
    })

    // เลือกทั้งหมดจากตาราง
    $('#data-list').on('ifChecked', '.tb-check-all', function () {
        $('#data-list .tb-check-single').iCheck('check');
        $(this).parents('tbody tr').addClass('selected')
        tbCheckAll = 1;
    })
    $('#data-list').on('ifUnchecked', '.tb-check-all', function () {
        if (tbCheckAll == 1) {
            $(this).parents('tbody tr').removeClass('selected')
            $('#data-list .tb-check-single').iCheck('uncheck');
        }
    })
    $('#data-list').on('ifChecked', '.tb-check-single', function () {
        tbCheckAll = 0;
        $(this).parents('tr').addClass('selected')
    });
    $('#data-list').on('ifUnchecked', '.tb-check-single', function () {
        tbCheckAll = 0;
        $(this).parents('tr').removeClass('selected')
        $('#data-list .tb-check-all').iCheck('uncheck');
    });


    //ลบทีละหลายๆรายการจากตาราง
    $('.box-tools').on('click', '.trash-multi', function () {
        var set = $('#data-list .tb-check-single')
        $(set).each(function () {
            if ($(this).is(":checked")) {
                arrayId.push($(this).closest('tr').attr('id'))
            }
        })
        if (arrayId.length > 0) {
            //if (confirm("กรุณายืนยันการทำรายการ")) {
                var id = $(this).closest('tr').attr('id')
                var url = $(this).data('method');

                swal({
                    title: 'กรุณายืนยันการทำรายการ',
                    text: "",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'ตกลง',
                    cancelButtonText: 'ยกเลิก'
                }).then(function(result) {

                    if (result.value) {
                        $.post(url, {id: arrayId, csrfToken: csrfToken})
                        .done(function (data) {
                            if (data.success === true) {
                                toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", appName)
                                dataList.DataTable().draw()
                                arrayId = []
                            }
                        })
                        .fail(function () {
                            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                        })
                    }
                });
            } else {
            //alert('กรุณาเลือกรายการที่ต้องการลบ')
            alert_box('กรุณาเลือกรายการที่ต้องการลบ');
        }
    })
    
    // ฟิตเตอร์ตารางใน
    $('.box-filter').click(function(e){
        e.preventDefault();
        $('div.filter').slideToggle()
    })
    $('.btn-filter').click(function(){
        dataList.DataTable().draw()
    })
    
    // create Daterange ใช้ที่ filter ที่หน้าตาราง
    $('input[name="createDateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="createDateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="createStartDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="createEndDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="createDateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="createStartDate"]').val('')
        $('input[name="createEndDate"]').val('')        
    })  
    
    // update Daterange ใช้ที่ filter ที่หน้าตาราง
    $('input[name="updateDateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="updateDateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="updateStartDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="updateEndDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="updateDateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="updateStartDate"]').val('')
        $('input[name="updateEndDate"]').val('')        
    })
    
    $(".format-phone").inputmask({
        "placeholder": "",
        "mask" : "999 999 9999",
    });

})


function SetLocalStorage(name, value)
{
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem(name, value);
    } else {
        alert_box('Web browser ของท่านไม่สนับสนุนการใช้งานฟังก์ชั้นนี้!');
    }
}

function GetLocalStorage(name)
{
    if (typeof (Storage) !== "undefined") {
        var data;
        data = localStorage.getItem(name);
        if (data === null && name === "sa-theme") {
            data = 5;
        }
        return data;
    } else {
        alert_box('Web browser ของท่านไม่สนับสนุนการใช้งานฟังก์ชั้นนี้!');
    }
}

function get_cookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function alert_box(text){

 swal({
    title:text, 
    text:"", 
    type:"warning",
    confirmButtonText: 'ตกลง'
});
}


function formatAmount( number ) {

  var number = number.replace( /[^0-9]/g, '' );

  var x = number.split( ',' );
  var x1 = x[0];
  var x2 = x.length > 1 ? ',' + x[1] : '';

  return formatAmountNoDecimals( x1 ) + x2;
}


function formatAmountNoDecimals( number ) {
  var rgx = /(\d+)(\d{3})/;
  while( rgx.test( number ) ) {
      var number = number.replace( rgx, '$1' + ',' + '$2' );
  }
  return number;
}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
};

$(document).on("keyup blur","input[data-type='currency']", function(){
    formatCurrency($(this), "blur");
})

// $("input[data-type='currency']").on({
//     keyup: function() {
//       formatCurrency($(this));
//     },
//     blur: function() { 
//       formatCurrency($(this), "blur");
//     }
// });


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;

  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    //input_val = "$" + left_side + "." + right_side;
    // input_val = "" + left_side + "." + right_side;
    input_val = "" + left_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    //input_val = "$" + input_val;
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += "";
    }
  }
  
  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}




$(window).on("load", function () {
})

$(window).on("scroll", function () {
})