"use strict";

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            delivery_id : true,
            start_qty   :true,
            end_qty     :true,
            price       :true,
        }
    });
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
