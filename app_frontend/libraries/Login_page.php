<?php 

/**
 * Library users
 */
class Login_page 
{
	public $CI;

	public $class;
	public $method;


	public function __construct()
	{
		$this->CI =& get_instance();

		$this->class    = $this->CI->router->fetch_class();
		$this->method   = $this->CI->router->fetch_method();

        $this->CI->load->model('login/users_model');
        $this->CI->load->helper('cookie');
        // $this->CI->load->library('logs_library');
    }

    /**
     * [check_login Check Login]
     * @return redirect
     */
    public function check_login()
    {
        /**
         * [Check cookie]
         */
        $cookie_username = get_cookie('username');
        $cookie_salt = get_cookie('salt');
        if(!$this->_is_logged_in() && $cookie_username && $cookie_salt){

            // $this->CI->logs_library->save_log($cookie_username,$this->class,$this->method,"Success : LoginRemember");
            //Login remme
            $this->login_remember($cookie_username,$cookie_salt);
        }
         //END Check cookie
    }

    /**
     * [_is_logged_in login is]
     * @return boolean [session] = userUID
     */
    public function _is_logged_in()
    {
        
    	return (bool) $this->CI->session->userdata('users'); 
    }


    private function _set_session($users){
        //Login Success
        $set_session =  array(
            'UID' => $users->user_id,
            'Username' => $users->username,
            'Name' => $users->fname.' '.$users->lname, 
            'fullname' => $users->fullname, 
            // 'RoleId' => $users->role_id,
        );

        $this->CI->session->set_userdata('users',$set_session);
    }

    private function _set_remember($users){
        $expire = (60 * 60 * 24 * 365);
        set_cookie([
            'name' => 'username',
            'value' => $users->username,
            'expire' => $expire,
        ]);

        set_cookie([
            'name' => 'salt',
            'value' => $users->salt,
            'expire' => $expire,
        ]);

    }
    

    private function login_remember($users,$salt){

        $users = $this->CI->users_model->find_users_by_user_and_salt($users,$salt);
        if($users){
            //Set Session
            $this->_set_session($users);
            //Set Remember
            $this->_set_remember($users);

            return true;
        }else{
            return false;
        }
    }



}

