<?php

/**
 *
 */
class Send_email {
	public $CI;

	function __construct() {
		$this->CI = &get_instance();

		$this->CI->load->model('send_email_model');
	}

	public function send($param) {

		$value['subject'] = isset($param['subject']) ? $param['subject'] : '';
		$value['detail'] = isset($param['detail']) ? $param['detail'] : '';
		$value['email_send'] = isset($param['email_send']) ? $param['email_send'] : '';
		$value['email_to'] = isset($param['email_to']) ? $param['email_to'] : '';
		$value['email_cc'] = isset($param['email_cc']) ? $param['email_cc'] : '';
		$value['send_date'] = isset($param['send_date']) ? $param['send_date'] : date('Y-m-d H:i:s');
		$value['file'] = isset($param['file']) ? $param['file'] : '';
		$data = $this->CI->send_email_model->save_send($value);
		if ($data) {
			return true;
		} else {
			return false;
		}
	}

	

}

?>