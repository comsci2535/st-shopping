<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkorder extends MX_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('seos/seos_m');
        $this->load->model('checkorder_m');
        $this->load->model('orders/orders_m');
        $this->load->model('product_attributes/product_attributes_m');
        $this->load->model('products/products_m');
        $this->load->model('reviews/reviews_m');
        $this->load->model('profile/profile_m');
        $this->load->library('uploadfile_library');
        $this->payment = array('ชำระผ่านการโอน', 'จ่ายผ่าน PayPal', 'เก็บเงินปลายทาง');
    }
    
    private function seo()
    {

        $obj_seo = $this->seos_m->get_seos_by_display_page('home')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | ซื้อของออนไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {
		
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'checkorder',
            'footer'  => 'footer',
            'function'=>  array('checkorder'),
        );
       
        $this->load->view('template/body', $data);
    }
    
    public function ajax_order()
    {
        $param                  = array();
        $input                  = $this->input->post(null, true);
        $param['lasname']       = $input['lasname'];
        $param['name']          = $input['name'];
        $param['user_id']       = $input['user_id'];
        $result                 = '';
        $info                   = $this->orders_m->get_orders($param)->result();
        if(!empty($info)):
            foreach($info as $item):
                $item->payment  = $this->payment[$item->payment_type];
                $item->products = $this->orders_m->get_orders_detail_by_order_id($item->order_id)->result();
                $status         = array(1,2,3,4,5,6);
                $item->status   =  $this->orders_m->get_orders_status_action_by_order_id($item->order_id, $status)->result();
                if(!empty($item->products)):
                    foreach($item->products as $product):
                        $inputs['product_id'] = $product->product_id;
                        $product->img           = $this->products_m->get_product_img_map($inputs)->row();
                        $input_['product_attribute_id'] = explode(',',$product->attributes_option);
                        $product->attributes    = $this->product_attributes_m->get_product_attributes_by_id($input_)->result();

                        $inputs_['product_id']  = $product->product_id;
                        $inputs_['user_id']     = $input['user_id'];
                        $infoReviews            = $this->reviews_m->count_reviews($inputs_);
                        $product->qty_reviews   = $infoReviews;

                    endforeach;
                endif;

                $transport = $this->orders_m->get_transport_by_id($item->transport_id)->row(); 
                if(!empty($transport->title)):
                    if($transport->title == 'DHL'):  
                        $transport->url = base_url('carts/pdf?code='.urlencode(base64_encode($item->order_id))); 
                    endif;
                endif; 
                $item->transport = $transport;
            endforeach;
        endif;
        $datas['info']  = $info;
        $result         = $this->load->view('checkorder_data', $datas);

        json_encode($result);
    }

    public function reviews_save()
    {

        $toastr['type']         = 'error';
        $toastr['lineOne']      = config_item('appName');
        $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']        = false;
        $data['toastr']         = $toastr;

        $input  = $this->input->post();

        $value  = $this->_build_data($input);
        $result = $this->reviews_m->insert($value);
        if ( $result ) {
            
            $toastr['type']     = 'success';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'บันทึกข้อมูลเรียบร้อย'; 

        } else {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }

        $data['success']    = $result;
        $data['toastr']     = $toastr;

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    private function _build_data($input)
    {
        $user_id                = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        $info                   = $this->profile_m->get_user_by_id($user_id)->row();

        $fname                  = !empty($info->fname) ? $info->fname : '';
        $lname                  = !empty($info->lname) ? $info->lname : '';

        $value['title']         = $fname.' '.$lname;
        $value['slug']          = str_replace(" ","-",$value['title']);
        if(!empty($info->oauth_picture)):
            $value['file']          = $info->oauth_picture;
        else:
            $value['file']          = !empty($info->file) ? $info->file : '';
        endif;
        
        $value['excerpt']       = $input['comment']; 
        $value['product_id']    = $input['product_id'];

        $path   = 'reviews';
        $upload = $this->uploadfile_library->do_upload('comment_file',TRUE,$path);
         
		if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name']; 
            $value['review_file'] = $file;
        }
        
        $value['user_id']    = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
         
        return $value;
    }

    
}
