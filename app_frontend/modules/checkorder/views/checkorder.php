
<?php echo Modules::run('banners/banners_home', 'contact');?>
<div class="product-details ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-12">
                <h3 class="sidebar-title">ค้นหารายการสั่งซื้อ</h3>
                <div class="sidebar-search">
                    <form action="javascript:void(0)">
                        <div class="row">
                            <div class="col-md-6">
                                <input id="text-name-search" placeholder="ค้นหาตามชื่อ..." 
                                type="text" autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <input id="text-lasname-search" placeholder="ค้นหาตามนามสกุล" 
                                type="text" autocomplete="off">
                            </div>
                        </div>
                        <button id="btn-check-order"><i class="ti-search"></i></button>
                    </form>
                    <input type="hidden" id="user_id" name="user_id" value="<?php echo !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : "";?>">
                </div>
                <hr>
            </div>
        </div>
        <div id="order-check-data" class="row">
        </div>
    </div>
</div>