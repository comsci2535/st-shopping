<?php
if(!empty($info)):
    foreach($info as $key => $item):
?>
<div class="col-md-12 col-lg-12 col-12">
    <div class="sidebar-active product-details-content">
       
        <h3>Order Ref : <?php echo !empty($item->reserve_code) ? $item->reserve_code : '';?></h3>
        
        <div class="details-price">
            <span>฿ <?php echo !empty($item->discount) ? number_format($item->discount,2) : 0;?></span>
        </div>
        <h5>ที่อยู่ในการจัดส่ง : </h5> 
        <div><b>ชื่อ-นามสกุล</b> : <?php echo !empty($item->name) ? $item->name : '';?> <?php echo !empty($item->lasname) ? $item->lasname : '';?>
        <br><b>ที่อยู่</b> : <?php echo !empty($item->address) ? $item->address : '';?> - ตำบล 
        <?php echo !empty($item->districts) ? $item->districts : '';?> - อำเภอ 
        <?php echo !empty($item->amphures) ? $item->amphures : '';?> - จังหวัด
        <?php echo !empty($item->provinces) ? $item->provinces : '';?> 
        <?php echo !empty($item->zip_code) ? $item->zip_code : '';?>
        <br><b>เบอร์โทร</b> : <?php echo !empty($item->tel) ? $item->tel : '';?>
        <br><b>ชำระเงินผ่าน : </b> <?php echo !empty($item->payment) ? $item->payment : 'ไม่มีข้อมูล';?>
        <br><b>บริษัทจัดส่ง</b> : <?php echo !empty($item->transport->title) ? $item->transport->title : 'ไม่มีข้อมูล';?>  
        <b>เลขพัสดุ</b> : <?php echo !empty($item->tracking_code) ? $item->tracking_code : 'ไม่มีข้อมูล';?> 
        <?php if (!empty($item->tracking_code)) { ?>
        <div class="input-group" style="width: 110px;display: inline-flex;">
            <input type="text" id="Clipboard" class="form-control" value="<?php echo $item->tracking_code;?>">
            <div onclick="Clipboard()" class="input-group-append" style="padding: 7px;background: #edeff8;border: 1px solid #ced4da;cursor: pointer;" data-toggle="tooltip" title="Copy !">
                <span class="input-group-text"><i class="ti-files" style="font-size: 22px;"></i></span>
            </div>
        </div>
        <?php } ?>
        <?php
        if(!empty($item->transport->url)):
        ?>
        <br>
            <p>
                <a href="<?php echo !empty($item->transport->url) ? $item->transport->url : '';?>" target="_blank" rel="" style="color: blue;">
                <i class="ti-printer" style="font-size: 22px;"></i> ปริ้มเอกสารอ้างอิง</a>
            </p>
        <?php
        endif;
        ?>
        <br><br>
        <h5>สถานะการจัดส่ง</h5>
        <ul> 
            <?php
            if(!empty($item->status)):
                foreach($item->status as $status):
            ?>
                <li style="color: blue;"><i class="ti-check-box"></i> <?php echo !empty($status->status_title) ? $status->status_title : '';?></li>
            <?php
                endforeach;
            endif;
            ?>
        </ul>
        </div>
        <!-- <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'"> -->
        <?php
        if(!empty($item->products)):
            foreach($item->products as $keyspro => $product): 
        ?>
        <div class="col-lg-12 mt-3 mb-30">
            <div class="product-wrapper mb-30 single-product-list product-list-right-pr mb-60">
                <div class="product-img list-img-width">
                    <a href="avascript:void(0)">
                        <img src="<?php echo !empty($product->img->file_path) ? base_url($product->img->file_path) : '';?>" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'">
                    </a>
                    <!-- <div class="product-action-list-style">
                        <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal" href="#">
                            <i class="pe-7s-look"></i>
                        </a>
                    </div> -->
                </div>
                <div class="product-content-list">
                    <div class="product-list-info">
                        <h4><a href="#"><?php echo !empty($product->title) ? $product->title : 'ไม่มีข้อมูล';?></a></h4>
                        <span>฿ <?php echo !empty($product->product_price) ? number_format($product->product_price) : 0;?> X <?php echo !empty($product->quantity) ? $product->quantity : 0;?></span>
                        <p><?php echo !empty($product->excerpt) ? $product->excerpt : 'ไม่มีข้อมูล';?></p>
                        <p><?php echo !empty($product->excerpt) ? $product->excerpt : 'ไม่มีข้อมูล';?></p>
                        <p>ลักษณะ : 
                            <?php 
                            if(!empty($product->attributes)):
                                $attributes = '';
                                foreach($product->attributes as $option):
                                    $attributes.= !empty($option->title) ? '('.$option->title.') ' : '';
                                endforeach;
                                echo !empty($attributes) ? $attributes : 'ไม่มีข้อมูล';;
                            endif;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if(empty($product->qty_reviews) && !empty($this->session->userData['UID'])):
        ?>
        <h3>รีวิวสินค้า : </h3>
        <form action="#" id="comment-key-<?=$key?>-<?=$keyspro?>" method="POST" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group">
                <textarea class="form-control text-comment-value-<?=$key?>-<?=$keyspro?>" name="comment" rows="5" placeholder="กรุณาระบุ" required></textarea>
            </div>
            <div class="form-group">
                <input type="file" class="form-control" name="comment_file">
                <input type="hidden" name="product_id" value="<?php echo !empty($product->product_id) ? $product->product_id : '';?>">
            </div> 
            <button type="submit" class="btn btn-primary">บันทึกข้อมูล</button>
        </form>

        <script>

            $("form#<?php echo 'comment-key-'.$key.'-'.$keyspro?>").validate({
                rules: {
                    comment: {
                        required: true
                    },
                    messages: {
                        comment: {
                            required: "กรุณาระบุข้อความรีวิวสินค้า"
                        },
                    },
                }
            });

            $("form#<?php echo 'comment-key-'.$key.'-'.$keyspro?>").submit(function(e) {
                console.log();

                $("form#<?php echo 'comment-key-'.$key.'-'.$keyspro?>").validate().element('.text-comment-value-<?=$key?>-<?=$keyspro?>')

                if($("form#<?php echo 'comment-key-'.$key.'-'.$keyspro?>").validate().numberOfInvalids() > 0){
                    return false;
                }

                e.preventDefault(); 
                $.ajax({
                    type: "POST",
                    url: '<?=base_url('checkorder/reviews_save')?>',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data){
    
                        swal({
                            title: 'แจ้งเตือน !',
                            text: data.toastr.lineTwo,
                            type: data.toastr.type,
                            confirmButtonText: 'ตกลง'
                        }).then(function(result) { 
                            get_load_order_data('', '', $('#user_id').val())
                        });

                    },
                    error: function (data) {
                        console.log('An error occurred.');
                        toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                    },
                });
            });
        </script>
        <hr>
        <?php
        endif;
        ?>
        <?php
            endforeach;
        endif;
        ?>
    </div>
</div>
<?php
    endforeach;
endif;
?>     