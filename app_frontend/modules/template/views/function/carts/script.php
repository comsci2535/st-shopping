<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

<link rel="stylesheet" href="<?=base_url('template/metronic/assets/plugins/jquery.Thailand.js/jquery.Thailand.min.css');?>">
<script type="text/javascript" src="<?=base_url('template/metronic/assets/plugins/jquery.Thailand.js/jquery.Thailand.min.js');?>"></script>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<!-- daterangepicker -->
<script type="text/javascript" src="<?=base_url('template/frontend/bootstrap-daterangepicker/moment.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('template/frontend/bootstrap-daterangepicker/daterangepicker.min.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('template/frontend/bootstrap-daterangepicker/daterangepicker.css');?>" />

<script>
$( document ).ready(function() {
    //Lee
    $.Thailand({
        // database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#districts'), // input ของตำบล
        $amphoe: $('#amphures'), // input ของอำเภอ
        $province: $('#provinces'), // input ของจังหวัด
        $zipcode: $('#zip_code'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
            console.log('Autocomplete is ready!')
        },
        onDataFill: function(data){
            console.log(data);
        }
    });

    $.Thailand({
        // database: $('#datajson').val(),
        autocomplete_size: 10, // ถ้าไม่ระบุ ค่า default คือ 20
        $district: $('#invoice_districts_id'), // input ของตำบล
        $amphoe: $('#invoice_amphures_id'), // input ของอำเภอ
        $province: $('#invoice_provinces_id'), // input ของจังหวัด
        $zipcode: $('#invoice_zip_code'), // input ของรหัสไปรษณีย์
        onComplete: function(data) {
            console.log('Autocomplete is ready!')
        },
        onDataFill: function(data){
            console.log(data);
        }
    });
});

$('input[name="transfer_datetime"]').daterangepicker({
    locale: { 
        cancelLabel :'ยกเลิก', 
        applyLabel  :'ตกลง', 
        format      :'DD-MM-YYYY' 
    },
    drops: 'up',

    timePicker      : true,
    timePicker24Hour: true,
    singleDatePicker: true,
    showDropdowns   : true,
    minYear         : 1901,
    maxYear         : parseInt(moment().format('YYYY'),10),
    autoUpdateInput : false,
})  

$('input[name="transfer_datetime"]').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('YYYY-MM-DD h:m'))
})


$(document).on('click','#click-checkout-page', function(){
    var chk     = 0; 
    var vals    = 0;
    $("input.btn-product-qty").each(function(){
        var $this = $(this);
        var limit = $this.attr('data-limit');
        var stock = parseInt($(this).attr('data-stock'));
        var valus = parseInt($(this).val());

        if($(this).val() == 0 || $(this).val() == ''){
            vals++;
        }else{
            var total = stock - valus;
            if(limit == 0){ 
                if(total < 0){ 
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) { 
                        $this.parent().css({"color": "red", "border": "1px solid red"}); 
                    });

                    chk = 1;

                }else{
                    $this.parent().css({"color": "", "border": ""});
                }
            }
        }
       
    });

    if(vals > 0){
        swal({
            // title: 'แจ้งเตือน !',
            text: "ระบุจำนวนสินค้ากรุณาตรวจสอบใหม่อีกครั้ง",
            // type: 'error',
            confirmButtonText: 'ตกลง'
        }).then(function(result) {

        });
    }else{
        setTimeout(() => {
            if(chk == 0){
            window.location.href = '<?=base_url('carts/checkout')?>';
            }
        }, 500);
    }
   
    
}); 

$(document).on('click','#btn-payment-order', function(){

    $('#form-submit-cart').validate().element('#name')
    $('#form-submit-cart').validate().element('#lasname')
    $('#form-submit-cart').validate().element('#address')
    $('#form-submit-cart').validate().element('#provinces')
    $('#form-submit-cart').validate().element('#amphures')
    $('#form-submit-cart').validate().element('#districts')
    $('#form-submit-cart').validate().element('#zip_code')
    $('#form-submit-cart').validate().element('#tel')

    if($('#is_invoice').is(':checked') && $('#invoice_type').is(':checked') === false){
        $('#form-submit-cart').validate().element('#invoice_no')
        $('#form-submit-cart').validate().element('#invoice_address')
        $('#form-submit-cart').validate().element('#invoice_provinces_id')
        $('#form-submit-cart').validate().element('#invoice_amphures_id')
        $('#form-submit-cart').validate().element('#invoice_districts_id')
        $('#form-submit-cart').validate().element('#invoice_tel')
        $('#form-submit-cart').validate().element('#invoice_zip_code')
    }else if($('#is_invoice').is(':checked') && $('#invoice_type').is(':checked')){
        $('#form-submit-cart').validate().element('#invoice_no')
    }

    if($('#invoice_type').is(':checked')){
        $('#form-submit-cart').validate().element('#invoice_no')
    }

    if($('#form-submit-cart').validate().numberOfInvalids() > 0){
        toastr["error"]("กรุณากรอกข้อมูลให้ครบถ้วน", appName);
    }else{
        $('#transfer_amount').val($('#price_paypal').val())
        $('#modal-order-cart').modal('show');
    }
});

//$('#modal-order-cart').modal('show');

$(document).on('click','#btn-payment-delivery', function(){

$('#form-submit-cart').validate().element('#name')
$('#form-submit-cart').validate().element('#lasname')
$('#form-submit-cart').validate().element('#address')
$('#form-submit-cart').validate().element('#provinces')
$('#form-submit-cart').validate().element('#amphures')
$('#form-submit-cart').validate().element('#districts')
$('#form-submit-cart').validate().element('#zip_code')
$('#form-submit-cart').validate().element('#tel')

if($('#is_invoice').is(':checked') && $('#invoice_type').is(':checked') === false){
    $('#form-submit-cart').validate().element('#invoice_no')
    $('#form-submit-cart').validate().element('#invoice_address')
    $('#form-submit-cart').validate().element('#invoice_provinces_id')
    $('#form-submit-cart').validate().element('#invoice_amphures_id')
    $('#form-submit-cart').validate().element('#invoice_districts_id')
    $('#form-submit-cart').validate().element('#invoice_tel')
    $('#form-submit-cart').validate().element('#invoice_zip_code')
}else if($('#is_invoice').is(':checked') && $('#invoice_type').is(':checked')){
    $('#form-submit-cart').validate().element('#invoice_no')
}

    if($('#form-submit-cart').validate().numberOfInvalids() > 0){
        toastr["error"]("กรุณากรอกข้อมูลให้ครบถ้วน", appName);
    }else{
        $('#transfer_amount').val($('#price_paypal').val())
        $('#payment_type').val(2); 
        swal({
            title: 'กรุณายืนยันการทำรายการ',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(function(result) {
            
            if (result.value) {
                $('#form-submit-cart').submit();
            }
        })  
    }
});

$(document).on('click','#btn-transfer', function(){
    
    $('#form-submit-cart').validate().element('#transfer_amount')
    $('#form-submit-cart').validate().element('#transfer_datetime')
    $('#form-submit-cart').validate().element('#files') 
    if($('#form-submit-cart').validate().numberOfInvalids() > 0){
        toastr["error"]("กรุณากรอกข้อมูลให้ครบถ้วน", appName);
    }else{
        $('#form-submit-cart').submit();
    }
});

$('.display-vat').hide();

$(document).on('click','#is_invoice', function(){
    
    if($(this).is(':checked')){
        if($('#invoice_type').is(':checked')){
            $('.invoice_no').show();
        }else{
            $('.display-vat').show();
        }
    }else{
        $('.display-vat').hide();
    }
});

$(document).on('change','.text-input-transport', function(){
    $('.text-input-transport').prop('checked', false);
    $(this).prop('checked', true); 
});




$(document).on('click','#invoice_type', function(){
    
    if($(this).is(':checked')){
        $('.display-vat').hide();
        $('.invoice_no').show();
    }else{
        $('.display-vat').show();
    }
});

// this is the id of the form
$("#form-submit-cart").submit(function(e) {
    var post_url = "<?php echo base_url() ?>";
    e.preventDefault();
    // set funtion loading button > class or id
    $.ajax({
        type: "POST",
        url: post_url+'carts/ajax_carts_payment',
        data:new FormData(this),
        dataType:"json",
        mimeType:"multipart/form-data",
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){

            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
            $('#modal-order-cart').modal('hide');
            if(data.toastr.type != 'error'){ 
                setTimeout(function(){
                    var reserve_ref = $('#text-reserve-code').val();
                    swal({
                        title: 'คุณทำรายการเรียบร้อยแล้ว',
                        text: 'เลขคำสั่งซื้อ : '+reserve_ref+' สามารถตรวจสอบข้อมูลการสั่งซื้อได้ที่เมนู "ตรวจสอบการสั่งซื้อ"',
                        type: 'success',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {
                        
                        if (result.value) {
                            window.location.href = post_url;
                        }
                    });
                    
                }, 2000);
            }else{
                setTimeout(function(){ 
                    swal({
                        title: 'แจ้งเตือน',
                        text: data.toastr.lineTwo,
                        type: data.toastr.type,
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {
                        
                        if (result.value) {
                            location.reload();
                        }
                    });
                    
                }, 2000);
            }
            
        },
        error: function (data) {
            console.log('An error occurred.');
            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
            // set funtion reset button > class or id and time
        },
    }); 
});

var paypal_process = 0;
var process        = 0;
paypal.Button.render({
  // Set your environment
  env: 'production', // sandbox | production
  // Specify the style of the button
  style: {
    label: 'checkout',
      size:  'responsive',    // small | medium | large | responsive
      shape: 'pill',     // pill | rect
      color: 'gold',      // gold | blue | silver | black
      layout: 'horizontal',
      fundingicons: 'true',
    },
    funding: {
      allowed: [ paypal.FUNDING.CARD ],
      disallowed: [ paypal.FUNDING.CREDIT ]
    },
  // PayPal Client IDs - replace with your own
  // Create a PayPal app: https://developer.paypal.com/developer/applications/create
  client: {
      sandbox: 'AX4NCFLLGwrdfJlkuDU8lBY5ALdfO42TXjKdCHAsqGisC-GT3TIUHPrUm3Bv24lpDpbgmGHUjTxtIg0-',
      production: 'Aa5px02xvTASQFob95wHkjmE5o0FwOrbpkZHQgoWQ12LsyYEe7XbRY3STr3U955EVLlZIpTixy_pv-EJ'
    },
    commit:true,

    payment: function(data, actions) {
       
        $('#form-submit-cart').validate().element('#name')
        $('#form-submit-cart').validate().element('#lasname')
        $('#form-submit-cart').validate().element('#address')
        $('#form-submit-cart').validate().element('#provinces')
        $('#form-submit-cart').validate().element('#amphures')
        $('#form-submit-cart').validate().element('#districts')
        $('#form-submit-cart').validate().element('#zip_code')
        $('#form-submit-cart').validate().element('#tel')

        if($('#is_invoice').is(':checked') && $('#invoice_type').is(':checked') === false){
            $('#form-submit-cart').validate().element('#invoice_no')
            $('#form-submit-cart').validate().element('#invoice_address')
            $('#form-submit-cart').validate().element('#invoice_provinces_id')
            $('#form-submit-cart').validate().element('#invoice_amphures_id')
            $('#form-submit-cart').validate().element('#invoice_districts_id')
            $('#form-submit-cart').validate().element('#invoice_tel')
            $('#form-submit-cart').validate().element('#invoice_zip_code')
        }else if($('#is_invoice').is(':checked') && $('#invoice_type').is(':checked')){
            $('#form-submit-cart').validate().element('#invoice_no')
        }
        
        if($('#form-submit-cart').validate().numberOfInvalids() > 0){
            toastr["error"]("กรุณากรอกข้อมูลให้ครบถ้วน", appName);
            return false;
        }else{

            var price_paypal    = $("#price_paypal").val();
                process         = (price_paypal * 4)/100;
                paypal_process  = (parseFloat(price_paypal)+parseFloat(process));
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: paypal_process , currency: 'THB' }
                        }
                    ]
                }
            });
        }
    },
    onAuthorize: function(data, actions) {

        var paymentID = data.paymentID;
        $('#payment_type').val(1);
        $('#premise').val(paymentID);
        $('#paypal_process').val(process);
        $('#status').val(3);
        
        return actions.payment.execute().then(function() { 

            $('#form-submit-cart').submit();
        
        });
    }

}, '#paypal-button-container');
</script>