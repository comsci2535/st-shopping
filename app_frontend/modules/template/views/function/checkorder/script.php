
<script>

    $(document).on('click', '#btn-check-order', function(){
        var lasname = $('#text-lasname-search').val();
        var name    = $('#text-name-search').val();
        var user_id = $('#user_id').val();
        if(name == ''){ 
            swal({
                title: 'แจ้งเตือน',
                text: "กรุณากรอกข้อมูลชื่อ",
                // type: 'error',
                confirmButtonText: 'ตกลง'
            }).then(function(result) { 
                
            });
            return false;
        }
        
        if(lasname == ''){ 
            swal({
                title: 'แจ้งเตือน',
                text: "กรุณากรอกข้อมูลนามสกุล",
                // type: 'error',
                confirmButtonText: 'ตกลง'
            }).then(function(result) { 
                
            });
            return false;
        }

        get_load_order_data(lasname, name, user_id);

    }); 

    get_load_order_data('', '', $('#user_id').val());
    
    function get_load_order_data(lasname, name, user_id)
    {
        $.ajax({
            url: '<?=base_url('checkorder/ajax_order')?>',
            type: "POST",
            data:{
                lasname  : lasname,
                name     : name,
                user_id  : user_id
            },
            beforeSend: function()
            {
                // $('.ajax-load').show();
            }
        })
        .done(function(data){
            if(data == " "){
                console.log("No more records found");
                return;
            }
            setTimeout(function(){
                // $('.ajax-load').hide();
                $("#order-check-data.row div").remove();
                $("#order-check-data.row").append(data);
                $('[data-toggle="tooltip"]').tooltip(); 
            }, 1000);
            
        })
        .fail(function(jqXHR, ajaxOptions, thrownError)
        {
            console.log("server not responding...");
        });
    }
    

</script>