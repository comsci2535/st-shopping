<!-- <script src="<?=base_url('template/frontend');?>/datatables_respon/datatables.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/datatables_respon/dataTables.bootstrap4.min.js" type="text/javascript"></script>
<script src="<?=base_url('template/frontend');?>/datatables_respon/dataTables.responsive.min.js" type="text/javascript"></script> -->

<script>
    $(document).ready(function () {
        var please = 'กรุณากรอก';
        var Tmin = 'ต้องมีอย่างน้อย';
        var Tmax = 'ต้องมีมากสุด';
        var truee = 'กรอกให้ถูก';

        var iCheckboxOpt = {
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        }

        $("#ProfileForm").validate({
            rules: {
                fname: {
                    required: true,
                    minlength: 2
                },
                lname: {
                    required: true,
                    minlength: 2
                },
                phone: {
                    required: false,
                    number: true,
                    minlength: 9,
                    maxlength: 10,
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "login/check_username",
                        type: "post",
                        data: {id: function () {
                                return $('#input-id').val();
                            }, mode: function () {
                                return $('#input-mode').val();
                            }}
                    }
                }, 
            },
            messages: {
                fname: {
                    required: ""+please+" ชื่อ",
                    minlength: ""+Tmin+" 2 คำ"
                },
                lname: {
                    required: ""+please+" นามสกุล",
                    minlength: ""+Tmin+" 2 คำ"
                },
                phone: {
                    number: ""+truee+" เบอร์โทร",
                    minlength: ""+Tmin+" 9 ตัว",
                    maxlength: ""+Tmax+" 10 ตัว",
                },
                email: {
                    required: ""+please+" email",
                    email:  ""+please+" email ให้ถูกต้อง",
                    remote: 'พบ email ซ้ำในระบบ',
                },
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
        });

        $("#ProfileResetPasswordForm").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#user_password_"
                },
            },
            messages: {
                password: {
                    required: ""+please+" รหัสผ่าน",
                    minlength: ""+Tmin+" 5 คำ"
                },
                confirm_password: {
                    required: ""+please+" ยืนยันรหัสผ่าน",
                    minlength: ""+Tmin+" 5 คำ",
                    equalTo: "รหัสผ่านไม่ตรงกัน"
                },
            },
            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
        });

        // this is the id of the form
        $("form#ProfileForm").submit(function(e) {
            
            var post_url = "<?php echo base_url() ?>";
            e.preventDefault();
            // set funtion loading button > class or id
            $.ajax({
                type: "POST",
                url: post_url+'profile/editprofile',
                data:new FormData(this),
                dataType:"json",
                mimeType:"multipart/form-data",
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    
                    setTimeout(function(){
                        swal({
                            title: 'คุณทำรายการเรียบร้อย',
                            text: data.toastr.lineTwo,
                            type: data.toastr.type,
                            confirmButtonText: 'ตกลง'
                        }).then(function(result) {
                            

                        });
                        
                    }, 2000);
                    
                },
                error: function (data) {
                    console.log('An error occurred.');
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                    // set funtion reset button > class or id and time
                },
            });
        });

        // this is the id of the form
        $("form#ProfileResetPasswordForm").submit(function(e) {
            
            var post_url = "<?php echo base_url() ?>";
            e.preventDefault();
            // set funtion loading button > class or id
            $.ajax({
                type: "POST",
                url: post_url+'profile/resetpassword',
                data:new FormData(this),
                dataType:"json",
                mimeType:"multipart/form-data",
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){
                    
                    setTimeout(function(){
                        swal({
                            title: 'คุณทำรายการเรียบร้อย',
                            text: data.toastr.lineTwo,
                            type: data.toastr.type,
                            confirmButtonText: 'ตกลง'
                        }).then(function(result) {
                            

                        });
                        
                    }, 2000);
                    
                },
                error: function (data) {
                    console.log('An error occurred.');
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                    // set funtion reset button > class or id and time
                },
            });
        });
    });
</script>
