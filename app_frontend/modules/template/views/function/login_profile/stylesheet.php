<!-- fileinput -->
<link href="<?=base_url('template/frontend');?>/bootstrap-fileinput-master/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?=base_url('template/frontend'); ?>/icheck/skins/square/grey.css" rel="stylesheet" type="text/css"/>

<link href="<?=base_url('template/frontend/metronic');?>/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

<style>
    .LiveChat {
        height: 0 !important;
    }

    .background-image {
        background: #fff0 !important;
        margin: 60px 0 !important;
        padding: 15px !important;
        border-radius: 8px !important;
        box-shadow: 0px 0px 0px !important;
    }

    .m-card-profile__name {
        font-size: 32px !important;
        font-weight: 600 !important;
    }

    .m-card-profile__email{
        padding: 0 !important;
        font-size: 18px !important;
    }
    .line-buttom{
        border-bottom: 1px solid;
        width: 150px;
    }

    .m-tabs__link{
        font-size: 28px !important;
    }

    label, .btn.m-btn--custom {
        font-size: 24px !important;
    }
</style>