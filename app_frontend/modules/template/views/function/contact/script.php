<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlZPf84AAVt8_FFN7rwQY5nPgB02SlTKs "></script> -->
<script>
    // var myCenter=new google.maps.LatLng(latitude, longitude);
    // function initialize()
    // {
    //     var mapProp = {
    //         center:myCenter,
    //         scrollwheel: false,
    //         zoom:17,
    //         mapTypeId:google.maps.MapTypeId.ROADMAP
    //         };
    //     var map=new google.maps.Map(document.getElementById("hastech2"),mapProp);
    //     var marker=new google.maps.Marker({
    //         position:myCenter,
    //         animation:google.maps.Animation.BOUNCE,
    //         icon:'',
    //         map: map,
    //         });
    //     var styles = [
    //         {
    //         stylers: [
    //             { hue: "#c5c5c5" },
    //             { saturation: -100 }
    //         ]
    //         },
    //     ];
    //     map.setOptions({styles: styles});
    //     marker.setMap(map);
    // }
    // google.maps.event.addDomListener(window, 'load', initialize);


    $('#form-submit-contact').validate({
        rules: {
            fname: true,
            lname:true,
            email:true,
            title:true,
            detail:true,
        }
    });
    // this is the id of the form
    $("#form-submit-contact").submit(function(e) {

        if($('#form-submit-contact').validate().numberOfInvalids() > 0){
            toastr["error"]("กรุณากรอกข้อมูลให้ครบถ้วน", appName);
        }else{
            var post_url = "<?php echo base_url() ?>";
            e.preventDefault();
            // set funtion loading button > class or id
            $.ajax({
                type: "POST",
                url: post_url+'contact/ajax_contact_save',
                data:new FormData(this),
                dataType:"json",
                mimeType:"multipart/form-data",
                contentType: false,
                cache: false,
                processData:false,
                success: function(data){

                    if(data.status > 0){
                        
                        $('#form-submit-contact input').val('');
                        $('#form-submit-contact textarea').val('');

                        setTimeout(function(){
                            swal({
                                title: 'บันทึกข้อมูล',
                                text: "ขอบคุณที่ให้ข้อมูล",
                                type: 'success',
                                confirmButtonText: 'ตกลง'
                            }).then(function(result) {
                                
                                if (result.value) {
                                    window.location.href = post_url+'contact';
                                }
                            });
                        }, 1000);
                    }
                    console.log(data);
                    //window.location.href = post_url+'contact';
                },
                error: function (data) {
                    console.log('An error occurred.');
                    // set funtion reset button > class or id and time
                },
            });
        }
    });

</script>