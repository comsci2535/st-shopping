 <link rel="stylesheet" href="<?=base_url('template/frontend/lightslider/lightslider.css');?>">
 <script src="<?=base_url('template/frontend/lightslider/lightslider.js');?>"></script>
 <script src="<?=base_url('template/frontend/gallery-zoom-zoomy/dist/zoomsl.js');?>"></script>
 <link rel="stylesheet" type="text/css" href="https://sularome.github.io/Zoomple/styles/zoomple.css">
 <script type="text/javascript" src="https://sularome.github.io/Zoomple/zoomple.js"></script>
 <script>
     var page        = 1;
     var categories  = 0;
     var attribute   = 0;
     var fiter       = 0;
     var categoriess = '';
     var search      = '';
     var slug        = '';
     var order_by    = $('.text-order-by option:selected').val();

     $( document ).ready(function() {

        $(document).on('click', '.click-tab-menu-up-down', function(){
            var $this = $(this);
            var textclass = $this.parents('.tab-menu-categories').attr('data-id')
            if($this.attr('data-up') == 0){
                $this.removeClass('ti-angle-down').addClass('ti-angle-up');
                $this.attr('data-up', 1);
                $('.'+textclass).hide(500);
            }else{
                $this.attr('data-up', 0);
                $this.removeClass('ti-angle-up').addClass('ti-angle-down');
                $('.'+textclass).show(500);
            } 
        });

        $(document).on('change', '.text-select-detail-option', function(){
            var id = $(this).val();
            $('.product-details-small .text-loop-'+id).click();
            var price = $('.product-details-small .text-loop-'+id).attr('data-price');
            var persent = $('.product-details-small .text-loop-'+id).attr('data-persent');
            var price_true = $('.product-details-small .text-loop-'+id).attr('data-price-true');
            $('.details-price span.new').html('฿'+price);
            $('.details-price span.persent').html(persent+'%');
            $('.details-price span.cut').html('฿'+price_true);
        });

        $(document).on('click', '.btn-fiter-categories', function(){
            categories = $(this).attr('data-id');
            console.log('categories > '+categories);
            $("#grid-sidebar1 .row div").remove();
            fiter   = 1;
            loadMoreDataFiter(1,url=''); 
        });



        $(document).on('click', '.tab-minus-click', function(){ 
            var $this = $(this);
            if($this.attr('data-event') == 0){
                $this.parents('.product-tags').find('.text-display-li').css({"display":"flow-root"});
                $this.html('<i class="fa fa-minus" aria-hidden="true"></i> แสดงย่อ');
                $this.attr('data-event', 1);
            }else{
                $this.parents('.product-tags').find('.text-display-li').css({"display":"none"});
                $this.html('<i class="fa fa-plus" aria-hidden="true"></i> แสดงทั้งหมด');
                $this.attr('data-event', 0);
            }

        });

        $(document).on('change', '.text-order-by', function(){
            order_by = $(this).val();
            $("#grid-sidebar1 .row div").remove();
            fiter   = 1;
            loadMoreData(1,url='');
        });

        $(document).on('click', '#btn-product-search', function(){

            search = $('#text-product-search').val();
            var text_cat  = $('#text-cat').val();
            if(text_cat != ''){
                text_cat = '&cat='+text_cat;
            }

            if(search != ''){
                search = 'search='+search;
            }
            var slug        = '';
            var attributes  = '';
            var n           = 0; 
            if($('.text-attribute').length > 0){
                $(".text-attribute").each(function(){ 
                    if(n == 0){
                        slug += $(this).val(); 
                    }else{
                        slug += ','+$(this).val(); 
                    }
                    n++;
                }); 
            }

            if(slug != ''){
                attributes = '&attribute='+slug;
            }

            window.location.href = '<?=base_url('products?')?>'+search+text_cat+attributes;
        });


        $(document).on('click', '.btn-attribute-dele', function(){
            var slug                = '';
            var groug               = '';
            var n                   = 0;
            var attribute           = $(this).attr('data-slug');
            var type_attribute      = $(this).attr('data-slug-type');
            if($('.text-attribute').length > 0){
                $(".text-attribute").each(function(){ 
                    console.log('v = '+$(this).val().toString());
                    console.log('a = '+attribute.toString());
                    if($(this).val().toString() == attribute.toString() ){ 
                    }else{
                        if(n == 0){
                            slug += $(this).val(); 
                        }else{
                            slug += ','+$(this).val(); 
                        }
                    } 
                    n++;
                });

            }else{
                slug = attribute;
            } 

            var g                   = 0;
            if($('.text-group-attribute').length > 0){
                $(".text-group-attribute").each(function(){ 
                    if($(this).val() == type_attribute ){
                        if(g == 0){
                            groug += $(this).val(); 
                        }else{
                            groug += ','+$(this).val(); 
                        }
                    }
                    g++;
                }); 
            }else{
                groug = type_attribute;
            }

        // alert('slug = '+slug);
        // alert('type = '+groug);
        var link_attribute = '';
        if(slug.length > 0){
            link_attribute = '&attribute='+slug;
        }

        var link_groug_attribute = '';
        if(groug.length > 0){
            link_groug_attribute = '&type='+groug;
        }

        var text_cat  = $('#text-cat').val();
        if(text_cat != ''){
            text_cat = 'cat='+text_cat+'&';
        } 
        window.location.href = '<?=base_url('products?')?>'+text_cat+link_attribute+link_groug_attribute; 
    });

        function set_input_attribute(){
            var link = '';
            if($('#attribute').val() == ''){
                link = '';
            }else{
                link = 'attribute='+$('#attribute').val();
            }
            return link;
        }

        $(document).on('click', '.btn-fiter-attribute', function(){

            var slug                = '';
            var groug               = '';
            var n                   = 0;
            var attribute           = $(this).attr('data-slug');
            var type_attribute      = $(this).attr('data-slug-type');
            if($('.text-attribute').length > 0){
                $(".text-attribute").each(function(){ 
                    if($(this).val() != attribute ){
                        if(n == 0){
                            slug += $(this).val(); 
                        }else{
                            slug += ','+$(this).val(); 
                        }
                    } 

                    if(attribute == $(this).val()){
                        slug += attribute; 
                    }else{
                        slug += ','+attribute; 
                    }
                    n++;
                }); 
            }else{
                slug = attribute;
            }
            var g                   = 0;
            if($('.text-group-attribute').length > 0){
                $(".text-group-attribute").each(function(){ 
                    if($(this).val() != type_attribute ){
                        if(g == 0){
                            groug += $(this).val(); 
                        }else{
                            groug += ','+$(this).val(); 
                        }
                    }

                    if(type_attribute == $(this).val()){
                        groug += type_attribute; 
                    }else{
                        groug += ','+type_attribute; 
                    }

                    g++;
                }); 
            }else{
                groug = type_attribute;
            }  

            var text_cat  = $('#text-cat').val();
            if(text_cat != ''){
                text_cat = 'cat='+text_cat+'&';
            }

            window.location.href = '<?=base_url('products?')?>'+text_cat+'attribute='+slug+'&type='+groug;

        }); 

        $(window).scroll(function() {
            scrollDistance = $(window).scrollTop() + $(window).height();
            footerDistance = $('footer').offset().top;

            if (scrollDistance >= footerDistance) {

                var num_row    = $("#num_row").val();
                var prepage    = $("#prepage").val();
            // var category = getUrlParameter('category');
            var url = '?page=';
            page++;

            if(page <= Math.ceil(num_row/prepage) ){

                url = url + page;	

                loadMoreData(page,url);
            }   
        }
    }); 

        function loadMoreData(page,url){

            var attribute = '';
            var n         = 0;
            $(".text-attribute").each(function(){  
                if(n == 0){
                    attribute += $(this).val(); 
                }else{
                    attribute += ','+$(this).val(); 
                }
                n++;
            });
            var g                   = 0;
            var type_attribute      = '';
            $(".text-group-attribute").each(function(){  
                if(g == 0){
                    type_attribute += $(this).val(); 
                }else{
                    type_attribute += ','+$(this).val(); 
                }
                g++;
            });

            $.ajax({
                url: '<?=base_url('products/ajax_product')?>' + url,
                type: "get",
                data:{
                    page        : page,
                    cat         : $('#text-cat').val(),
                    groug       : $('#text-groug').val(),
                    attribute   : attribute,
                    type        : type_attribute,
                    search      : $('#text-product-search').val(),
                    p           : $('#text-fiter-p').val(),
                    order_by    : order_by
                },beforeSend: function(){
                $(".product-loader").show();
        }
    })
            .done(function(data){
                if(data == " "){
                    $(".product-loader").hide();
                    console.log("No more records found");
                    return;
                }
                setTimeout(function(){ 
                    $(".product-loader").hide();
                    $("#grid-sidebar1 .row").append(data);
                    $('[data-toggle="tooltip"]').tooltip(); 
                }, 1000);

            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                console.log("server not responding...");
            });
        }

            /*$(".block__pic").imagezoomsl({
                zoomrange: [3, 3]
            });*/
             // $(".block__pic").blowup();
             $('.zoomple2').zoomple({ 
                blankURL : './template/frontend/assets/img/product/fashion-colorful/1.jpg',
                offset : {x:5,y:0},
                showOverlay : true , 
                roundedCorners: false, 
                windowPosition : {x:'right',y:'top'}, 
                zoomWidth : 350,  
                zoomHeight : 350, 
                attachWindowToMouse : false       
            });

             $(".product-details-small").lightSlider({
                item:4,
                loop:true,
                autoWidth:true,
                slideMove:2,
                easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                speed:600,
                responsive : [
                {
                    breakpoint:800,
                    settings: {
                        item:3,
                        slideMove:1,
                        slideMargin:6,
                    }
                },{
                    breakpoint:480,
                    settings: {
                        item:2,
                        slideMove:1
                    }
                }]
            });  
         });

     </script>