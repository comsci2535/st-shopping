<!--Notification Section-->
<!-- <div class="notification-section notification-section-padding-2 notification-img-3 ptb-10">
    <div class="container-fluid">
        <div class="notification-wrapper">
            <div class="notification-pera-graph-3">
                <p>เริ่มซื้อของออนไลน์กับเรา </p>
            </div>
            <div class="notification-btn-close">
                <div class="notification-btn-3">
                    <a href="<?=base_url('products')?>">ซื้อของกับเรา</a>
                </div>
                <div class="notification-close notification-icon-3">
                    <button><i class="pe-7s-close"></i></button>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- header start -->
<header>
    <div class="sidebar-search" style="display: none;">
		<form method="GET" action="<?=base_url('products')?>">
			<input id="text-product-search-all" name="search" placeholder="ค้นหาสินค้า..." type="text" autocomplete="off">
			<button type="submit" id="btn-product-search-all"><i class="ti-search"></i></button>
		</form>
	</div>

    <div class="header-top-furniture wrapper-padding-2 res-header-sm">
        <div class="container-fluid">
            <div class="header-bottom-wrapper">
                <div class="logo-2 furniture-logo ptb-30">
                    <a href="<?=base_url();?>">
                        <img class="img-logo" src="<?=base_url('template/frontend/assets/img/logo/2.png');?>" alt="">
                    </a>
                </div>
                <div class="menu-style-2 furniture-menu menu-hover">
                    <nav>
                        <ul>
                            <li>
                                <a href="<?=base_url()?>">หน้าหลัก</a>
                            </li>
                            <li>
                                <a href="<?=base_url('products')?>">สินค้า</a>
                                <?php echo Modules::run('categories/categories_home');?>
                            </li>
                            <li>
                                <a href="javascript:void(0)">สั่งซื้อสินค้า
                                    <ul class="single-dropdown">
                                        <li><a href="<?=base_url('carts')?>">สินค้าในตะกร้า</a></li>
                                        <li><a href="<?=base_url('carts/checkout')?>">ชำระเงิน</a></li>
                                    </ul>
                                </a>
                            </li>
                            <!-- <li><a href="<?=base_url('abouts')?>">Abouts</a></li> -->
                            <li><a href="<?=base_url('contact')?>">ติดต่อเรา</a></li>
                            <li><a href="<?=base_url('checkorder')?>">ตรวจสอบการสั่งซื้อ</a></li>
                        </ul>
                    </nav>
                </div>
                <div id="text-basket-display">
                    <?php echo  Modules::run('carts/basket');?>
                </div>
            </div>
            <div class="row">
                <div class="mobile-menu-area d-md-block col-md-12 col-lg-12 col-12 d-lg-none d-xl-none">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul class="menu-overflow">
                                <li>
                                    <a href="<?=base_url()?>">หน้าหลัก</a>
                                </li>
                                <li><a href="javascript:void(0)">สินค้า</a>
                                    <?php echo Modules::run('categories/categories_home_mobile');?>
                                </li>
                                <li><a href="javascript:void(0)">สั่งซื้อสินค้า</a>
                                    <ul>
                                        <li><a href="<?=base_url('carts')?>">สินค้าในตะกร้า</a></li>
                                        <li><a href="<?=base_url('carts/checkout')?>">ชำระเงิน</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?=base_url('contact')?>">ติดต่อเรา</a></li>
                                <li><a href="<?=base_url('checkorder')?>">ตรวจสอบการสั่งซื้อ</a></li>
                            </ul>
                        </nav>							
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="header-bottom-furniture wrapper-padding-2 border-top-3">
        <div class="container-fluid">
            <div class="furniture-bottom-wrapper">
                <div class="furniture-login">
                    <ul>
                        <li>Get Access</li>
                    </ul>
                </div>
                <div class="furniture-search">
                    <form action="#">
                        <input placeholder="I am Searching for . . ." type="text">
                        <button>
                            <i class="ti-search"></i>
                        </button>
                    </form>
                </div>
                <div class="furniture-wishlist">
                    <ul>
                        <li><a data-toggle="modal" data-target="#exampleCompare" href="#"><i class="ti-reload"></i> ตรวจสอบการสั่งซื้อ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> -->
</header>