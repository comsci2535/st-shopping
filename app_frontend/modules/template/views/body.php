<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?= $seo; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('template/frontend/assets/img/favicon.png') ?>">

    <!-- all css here -->

    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/magnific-popup.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/animate.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/themify-icons.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/pe-icon-7-stroke.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/meanmenu.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/bundle.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/style.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/custom_style.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('template/frontend/assets/css/responsive.css'); ?>">
    <link href="https://fonts.googleapis.com/css?family=Prompt:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap&subset=latin-ext,thai" rel="stylesheet">
    <script src="<?= base_url('template/frontend/assets/js/vendor/modernizr-2.8.3.min.js'); ?>"></script>

    <link rel="stylesheet" href="<?= base_url("template/metronic/assets/plugins/bootstrap-toggle-master/css/bootstrap-toggle.css") ?>">
    <link href="<?= base_url("template/metronic/assets/plugins/icheck/skins/square/grey.css") ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("template/metronic/assets/plugins/toastr/toastr.css") ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("template/metronic/assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.css") ?>" rel="stylesheet" type="text/css" />
    <script src="https://kit.fontawesome.com/b93cf240f0.js"></script>
    <style>
        label.error {
            color: #ff5050 !important;
            font-size: 13px;
        }

        img.img-logo {
            width: 100px;
        }

        /* Large devices (desktops, 992px and up) */
        @media (min-width: 992px) {
            img.img-logo {
                width: 160px;
            }
        }
    </style>
    <?php
    if (!empty($function)) {
        $length1 = count($function);
        for ($x = 0; $x < $length1; $x++) {
            $this->load->view('function/' . $function[$x] . '/stylesheet');
        }
    }
    ?>
    <!-- zun -->
    <!-- <script src="//code.tidio.co/ypmgkkuvjt06ev1mlz1lzm9bpeepad3k.js" async></script> -->

    Global site tag (gtag.js) - Google Analytics
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126023374-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-126023374-2');
    </script>
    
    <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '972930166493122');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=972930166493122&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <?php $this->load->view('template/' . $header); ?>
    <?php $this->load->view($content); ?>
    <?php $this->load->view('template/' . $footer); ?>


    <div class="modal fade" id="modal-product-detail" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: fixed;right: 0;">
            <span class="pe-7s-close" aria-hidden="true"></span>
        </button>
        <div class="modal-dialog modal-quickview-width" role="document">
            <div class="modal-content">
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-product-option" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: fixed;right: 0;">
            <span class="pe-7s-close" aria-hidden="true"></span>
        </button>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>

    <!-- all js here -->
    <script src="<?= base_url('template/frontend/assets/js/vendor/jquery-1.12.0.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/popper.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/jquery.magnific-popup.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/isotope.pkgd.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/imagesloaded.pkgd.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/jquery.counterup.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/waypoints.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/ajax-mail.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/plugins.js'); ?>"></script>
    <script src="<?= base_url('template/frontend/assets/js/main.js'); ?>"></script>
    <script src="<?= base_url("template/metronic/assets/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js") ?>"></script>
    <script src="<?= base_url("template/metronic/assets/plugins/icheck/icheck.min.js") ?>" type="text/javascript"></script>
    <script src="<?= base_url("template/metronic/assets/plugins/toastr/toastr.min.js") ?>" type="text/javascript"></script>

    <script src="<?= base_url("template/metronic/assets/plugins/jquery-validation/dist/jquery.validate.min.js") ?>"></script>
    <script src="<?= base_url("template/metronic/assets/vendors/vendors/sweetalert2/dist/sweetalert2.min.js") ?>" type="text/javascript"></script>

    <script>
        var appName = "<?= config_item('appName'); ?>";
        var csrfToken = get_cookie('csrfCookie');
        var siteUrl = "<?php echo site_url(); ?>";
        var baseUrl = "<?php echo base_url() ?>";
        var controller = "<?php echo $this->router->class ?>";
        var method = "<?php echo $this->router->method ?>";

        function get_cookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0)
                    return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
    </script>
    <?php
    if (!empty($function)) {
        $length2 = count($function);
        for ($x = 0; $x < $length2; $x++) {
            $this->load->view('function/' . $function[$x] . '/script');
        }
    }
    ?>
    <script>
        $(document).ready(function() {
            <?php if ($this->session->toastr) : ?>
                setTimeout(function() {
                    toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
                }, 500);
                <?php $this->session->unset_userdata('toastr'); ?>
            <?php endif; ?>
        });
    </script>

    <script type="text/javascript">
        $('[data-toggle="tooltip"]').tooltip();

        $(document).on('click', '.btn-product-detail-add-cart', function() {
            var product_id = $(this).attr('data-id');
            var stock = $('#text-detail-qty').attr('data-stock');
            var limit = $('#text-detail-qty').attr('data-limit');
            if (limit == 0 && stock == 0) {
                swal({
                    // title: 'แจ้งเตือน !',
                    text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                    // type: 'error',
                    confirmButtonText: 'ตกลง'
                }).then(function(result) {

                });
            } else {
                get_product_detail_carts(product_id, 0);
            } 
        });

        $(document).on('click', '.btn-product-add-cart', function() {
            var product_id = $(this).attr('data-id');
            var count = $(this).attr('data-count');
            var stock = $('#text-qty').attr('data-stock');
            var limit = $('#text-qty').attr('data-limit');
            if (limit == 0 && stock == 0) {
                swal({
                    // title: 'แจ้งเตือน !',
                    text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                    // type: 'error',
                    confirmButtonText: 'ตกลง'
                }).then(function(result) {

                });
            } else {
                if (count > 0) {
                    get_product_option(product_id);
                } else {
                    get_product_carts(product_id, 0, 0);
                }
            }

        });

        /// check stock product detail
        $(document).on('change', '.text-select-detail-option', function() {
            var obj_select = []
            $(".text-select-detail-option option:selected").each(function() {
                obj_select.push($(this).val());
            });;

            var product_id = $('.product-details-content a.btn-product-detail-add-cart').attr('data-id');
            $.ajax({
                url: baseUrl + 'products/ajax_chestock',
                type: "post",
                data: {
                    product_id: product_id,
                    option: obj_select
                }
            }).done(function(data) {
                $('#text-detail-qty').attr('data-limit', data.is_limit)
                $('#text-detail-qty').attr('data-stock', data.stock_qty);
            }).fail(function(jqXHR, ajaxOptions, thrownError) {

            });

        });

        get_stock();

        function get_stock() {
            var obj_select = []
            $(".text-select-detail-option option:selected").each(function() {
                obj_select.push($(this).val());
            });;

            var product_id = $('.product-details-content a.btn-product-detail-add-cart').attr('data-id');
            $.ajax({
                url: baseUrl + 'products/ajax_chestock',
                type: "post",
                data: {
                    product_id: product_id,
                    option: obj_select
                }
            }).done(function(data) {
                $('#text-detail-qty').attr('data-limit', data.is_limit)
                $('#text-detail-qty').attr('data-stock', data.stock_qty);
            }).fail(function(jqXHR, ajaxOptions, thrownError) {

            });
        }

        $(document).on('click', '.product-details-content .qtybutton', function() {
            var limit = parseInt($(this).parent().find('#text-detail-qty').attr('data-limit'));
            if (limit == 0) {
                var stock = parseInt($(this).parent().find('#text-detail-qty').attr('data-stock'));
                var valus = parseInt($(this).parent().find('#text-detail-qty').val());
                var total = stock - valus;
                if (total < 0) {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {
                        $('#text-detail-qty').val(valus - 1);
                    });
                }
            }
        });

        /*$(document).on('change', '#text-detail-qty', function() {
            var $this = $(this);
            if ($(this).val() != '') {
                var stock = parseInt($(this).attr('data-stock'));
                var valus = parseInt($(this).val());
                var total = stock - valus;
                if (total < 0) {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง BBB",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });
                }
            } else {
                swal({
                    // title: 'แจ้งเตือน !',
                    text: "กรุณาระบุจำนวน กรุณาตรวจสอบใหม่อีกครั้ง",
                    // type: 'error',
                    confirmButtonText: 'ตกลง'
                }).then(function(result) {
                    $this.val(1);
                });
            }

        });*/
        /// check stock product detail

        /// check stock product modal
        function get_modal_stock() {
            var obj_select = []
            $(".text-select-option option:selected").each(function() {
                obj_select.push($(this).val());
            });;

            var product_id = $('#modal-product-detail a.btn-product-add-cart').attr('data-id');
            $.ajax({
                url: baseUrl + 'products/ajax_chestock',
                type: "post",
                data: {
                    product_id: product_id,
                    option: obj_select
                }
            }).done(function(data) {
                $('#text-qty').attr('data-limit', data.is_limit);
                $('#text-qty').attr('data-stock', data.stock_qty);
            }).fail(function(jqXHR, ajaxOptions, thrownError) {

            });
        }

        $(document).on('change', '.text-select-option', function() {
            var obj_select = []
            $(".text-select-option option:selected").each(function() {
                obj_select.push($(this).val());
            });;

            var product_id = $('#modal-product-detail a.btn-product-add-cart').attr('data-id');

            $.ajax({
                url: baseUrl + 'products/ajax_chestock',
                type: "post",
                data: {
                    product_id: product_id,
                    option: obj_select
                }
            }).done(function(data) {
                $('#text-qty').attr('data-limit', data.is_limit);
                $('#text-qty').attr('data-stock', data.stock_qty);
            }).fail(function(jqXHR, ajaxOptions, thrownError) {

            });

        });

        $(document).on('click', '#modal-product-detail .qtybutton', function() {
            var limit = parseInt($(this).parent().find('#text-qty').attr('data-limit'));
            if (limit == 0) {
                var stock = parseInt($(this).parent().find('#text-qty').attr('data-stock'));
                var valus = parseInt($(this).parent().find('#text-qty').val());
                var total = stock - valus;
                if (total < 0) {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {
                        $('#text-qty').val(valus - 1);
                    });
                }
            }
        });

        $(document).on('change', '#text-qty', function() {
            var $this = $(this);
            if ($(this).val() != '') {
                var stock = parseInt($(this).attr('data-stock'));
                var valus = parseInt($(this).val());
                var total = stock - valus;
                if (total < 0) {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });
                }
            } else {
                swal({
                    // title: 'แจ้งเตือน !',
                    text: "กรุณาระบุจำนวน กรุณาตรวจสอบใหม่อีกครั้ง",
                    // type: 'error',
                    confirmButtonText: 'ตกลง'
                }).then(function(result) {
                    $this.val(1);
                });
            }

        });

        /// check stock product modal

        $(document).on('click', '.btn-product-detail', function() {

            var product_id = $(this).attr('data-id');
            $.ajax({
                    url: baseUrl + 'products/ajax_detail',
                    type: "post",
                    data: {
                        product_id: product_id
                    },
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    if (data == " ") {
                        console.log("No more records found");
                        return;
                    }
                    setTimeout(function() {
                        // $('.ajax-load').hide();
                        $("#modal-product-detail .modal-body").html(data);
                        $("#modal-product-detail").modal('show');
                        get_modal_stock();
                    }, 1000);

                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log("server not responding...");
                });
        });

        $(document).on('change', '.btn-product-qty', function() {
            var $this = $(this);
            var total = 0;
            var row = $(this).attr('data-tr');
            var qty = $(this).val();
            if (qty == '') {
                $(this).val(0);
            }
            var limit = $this.attr('data-limit');
            var stock = parseInt($(this).attr('data-stock'));
            var totalstock = stock - qty;
            if (limit == 0) {
                if (totalstock < 0) {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {
                        $this.parent().css({
                            "color": "red",
                            "border": "1px solid red"
                        });
                    });
                } else {
                    var price = $("#table-cart #" + row + ' .product-price-cart .amount').attr('data-price');
                    var product_id = $(this).attr('data-id');
                    total = parseInt($(this).val()) * parseInt(price);
                    $("#table-cart #" + row + ' .product-subtotal .amount').html(total.toLocaleString());
                    var sunTotal = 0;
                    $("#table-cart .product-subtotal .amount").each(function() {
                        sunTotal = sunTotal + parseInt($(this).text().replace(',', ''));
                    });

                    $(".text-sumtotal").html(sunTotal.toLocaleString());
                    get_product_carts_key(product_id, $(this).val(), 1, row);

                    $this.parent().css({
                        "color": "",
                        "border": ""
                    });
                }
            } else {
                var price = $("#table-cart #" + row + ' .product-price-cart .amount').attr('data-price');
                var product_id = $(this).attr('data-id');
                total = parseInt($(this).val()) * parseInt(price);
                $("#table-cart #" + row + ' .product-subtotal .amount').html(total.toLocaleString());
                var sunTotal = 0;
                $("#table-cart .product-subtotal .amount").each(function() {
                    sunTotal = sunTotal + parseInt($(this).text().replace(',', ''));
                });

                $(".text-sumtotal").html(sunTotal.toLocaleString());
                get_product_carts_key(product_id, $(this).val(), 1, row);

                $this.parent().css({
                    "color": "",
                    "border": ""
                });
            }

        });

        $(document).on('click', '.text-product-delete', function() {

            var product_id = $(this).attr('data-id');
            var row = $(this).attr('data-tr');

            $.ajax({
                    url: baseUrl + 'carts/ajax_carts_delete',
                    type: "post",
                    data: {
                        product_id: product_id
                    },
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    if (data.status > 0) {
                        $('#table-cart #' + row).remove();
                        set_qty_cart_delete();
                        set_cart_table();
                        set_cart_tab();

                        swal({
                            title: 'แจ้งเตือน !',
                            text: data.toastr.lineTwo,
                            type: data.toastr.type,
                            confirmButtonText: 'ตกลง'
                        }).then(function(result) {

                            return false;
                        });
                        //toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                    }
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                });
        });

        $(document).on('click', '.text-tab-product-delete', function() {

            var product_id = $(this).attr('data-id');
            var row = $(this).attr('data-tr');

            $.ajax({
                    url: baseUrl + 'carts/ajax_carts_delete',
                    type: "post",
                    data: {
                        product_id: product_id
                    },
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    if (data.status > 0) {
                        $('.header-cart .cart-dropdown li#' + product_id).remove();
                        set_qty_cart_delete();
                        set_cart_tab();
                        swal({
                            title: 'แจ้งเตือน !',
                            text: data.toastr.lineTwo,
                            type: data.toastr.type,
                            confirmButtonText: 'ตกลง'
                        }).then(function(result) {

                            return false;
                        });
                        //toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                    }
                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                });
        });

        function get_product_onstock(product_id, qty, obj_option) {
            var key =
                $.ajax({
                    url: baseUrl + 'products/ajax_prodcut_onstock',
                    type: "post",
                    data: {
                        product_id: product_id,
                        qty: qty,
                        option: obj_option
                    },
                    beforeSend: function() {}
                })
                .done(function(data) {
                    if (data.is_limit != 1) {
                        if (data.stock_qty == " " || data.stock_qty == 0) {
                            // $('.text-qty-onoff-stock').show();
                            // $('.text-qty-onoff-stock').html('สินค้าหมด');
                            $('.text-stock-id').val('');
                        } else {
                            // $('.text-qty-onoff-stock').hide();
                            // $('.text-qty-onoff-stock').html('');
                        }
                    } else {
                        // $('.text-qty-onoff-stock').hide();
                        // $('.text-qty-onoff-stock').html('');
                    }

                    $('.text-stock-qty').val(data.stock_qty)
                    $('.text-stock-id').val(data.stock_id);
                    $('.text-is-limit').val(data.is_limit);

                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    //toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                });

        }

        function get_product_onstock_key(product_id, qty, obj_option, row) {
            var key =
                $.ajax({
                    url: baseUrl + 'products/ajax_prodcut_onstock',
                    type: "post",
                    data: {
                        product_id: product_id,
                        qty: qty,
                        option: obj_option
                    },
                    beforeSend: function() {}
                })
                .done(function(data) {
                    $('#' + row + ' .text-stock-key-qty').val(data.stock_qty)
                    $('#' + row + ' .text-stock-key-id').val(data.stock_id);
                    $('#' + row + ' .text-is-limit-key').val(data.is_limit);

                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    //toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                });

        }

        function get_product_carts(product_id, num, typepage) {

            var option = $('.text-select-option').val();
            var qty = $('#text-qty').val();
            var obj_option = [];
            var n = 0;
            $(".text-select-option option:selected").each(function() {
                if ($(this).val() == '') {
                    alert('กรุณาเลือกลักษณะ');
                    n++;
                    return false;
                } else {
                    obj_option.push($(this).val());
                }

            });

            if (n > 0) {
                return false;
            }


            get_product_onstock(product_id, qty, obj_option);

            setTimeout(function() {

                var key = $('.text-stock-id').val();
                var limit = $('.text-is-limit').val();
                var stock_qty = $('.text-stock-qty').val();
                // console.log(key);

                if (key == '') {

                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });

                    return false;
                }

                if (limit == 0 && stock_qty == 0) {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });
                    return false;
                }

                if (qty == 0 || qty == '') {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "ระบุจำนวนสินค้ากรุณาตรวจสอบใหม่อีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });
                    return false;
                }


                $.ajax({
                        url: baseUrl + 'carts/ajax_carts',
                        type: "post",
                        data: {
                            product_id: product_id,
                            key: key,
                            qty: qty,
                            option: obj_option,
                            num: num
                        },
                        beforeSend: function() {}
                    })
                    .done(function(data) {
                        if (data == " ") {
                            console.log("No more records found");
                            return;
                        }
                        // set qty cart
                        set_qty_cart(data.cart_qty);
                        set_cart_tab();
                        $('#modal-product-option').modal('hide');
                        $('#modal-product-detail').modal('hide');

                        if (typepage == 0) {

                            swal({
                                title: 'แจ้งเตือน !',
                                text: data.toastr.lineTwo,
                                type: data.toastr.type,
                                confirmButtonText: 'ตกลง'
                            }).then(function(result) {
                                $('.modal .modal-body div').remove();
                                return false;
                            });
                            //toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        }

                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError) {
                        //toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                    });

            }, 1000);
        }

        function get_product_carts_key(product_id, num, typepage, row) {

            var qty = num;
            var obj_option = [];
            var n = 0;
            $("#" + row + " .text-attribute-id").each(function() {
                obj_option.push($(this).val());
            });

            get_product_onstock_key(product_id, qty, obj_option, row);

            setTimeout(function() {

                var key = $('#' + row + ' .text-stock-key-id').val();
                var limit = $('#' + row + ' .text-is-limit-key').val();
                var stock_qty = $('#' + row + ' .text-stock-key-qty').val();
                //console.log(key);

                if (key == '') {

                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });

                    return false;
                }

                if (limit == 0 && stock_qty == 0) {

                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                        return false;
                    });

                }

                if (qty == 0 || qty == '') {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "ระบุจำนวนสินค้ากรุณาตรวจสอบใหม่อีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });
                    return false;
                }

                $.ajax({
                        url: baseUrl + 'carts/ajax_carts',
                        type: "post",
                        data: {
                            product_id: product_id,
                            key: key,
                            qty: qty,
                            option: obj_option,
                            num: num
                        },
                        beforeSend: function() {}
                    })
                    .done(function(data) {
                        if (data == " ") {
                            console.log("No more records found");
                            return;
                        }
                        // set qty cart
                        set_qty_cart(data.cart_qty);
                        set_cart_tab();

                        if (typepage == 0) {

                            swal({
                                title: 'แจ้งเตือน !',
                                text: data.toastr.lineTwo,
                                type: data.toastr.type,
                                confirmButtonText: 'ตกลง'
                            }).then(function(result) {

                                return false;
                            });
                            //toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        }

                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError) {
                        //toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                    });

            }, 1000);
        }

        function get_product_detail_carts(product_id, num) {
            var qty = 1;
            var key = $('.text-stock-detail-id').val();
            var option = $('.text-select-detail-option').val();
            var qty = $('#text-detail-qty').val();

            var obj_option = [];
            var n = 0;
            $(".text-select-detail-option option:selected").each(function() {
                //console.log($(this).val());
                if ($(this).val() == '') {
                    alert('กรุณาเลือกลักษณะ');
                    n++;
                    return false;
                } else {
                    obj_option.push($(this).val());
                }
            });

            if (n > 0) {
                return false;
            }

            get_product_onstock(product_id, qty, obj_option);

            setTimeout(function() {

                var key = $('.text-stock-id').val();
                var limit = $('.text-is-limit').val();
                var stock_qty = $('.text-stock-qty').val();
                //console.log(key);

                if (key == '') {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {


                    });
                    return false;
                }

                if (limit == 0 && stock_qty == 0) {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "สินค้ามีไม่เพียงพอ กรุณาใส่จำนวนสินค้าอีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {


                    });
                    return false;
                }

                if (qty == 0 || qty == '') {
                    swal({
                        // title: 'แจ้งเตือน !',
                        text: "ระบุจำนวนสินค้ากรุณาตรวจสอบใหม่อีกครั้ง",
                        // type: 'error',
                        confirmButtonText: 'ตกลง'
                    }).then(function(result) {

                    });
                    return false;
                }

                $.ajax({
                        url: baseUrl + 'carts/ajax_carts',
                        type: "post",
                        data: {
                            product_id: product_id,
                            key: key,
                            qty: qty,
                            option: obj_option,
                            num: num
                        },
                        beforeSend: function() {}
                    })
                    .done(function(data) {
                        if (data == " ") {
                            console.log("No more records found");
                            return;
                        }

                        set_qty_cart(data.cart_qty);
                        set_cart_tab();

                        swal({
                            title: 'แจ้งเตือน !',
                            text: data.toastr.lineTwo,
                            type: data.toastr.type,
                            confirmButtonText: 'ตกลง'
                        }).then(function(result) {});
                        // toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                    })
                    .fail(function(jqXHR, ajaxOptions, thrownError) {
                        //toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", appName)
                    });

            }, 1000);
        }

        function get_product_option(product_id) {

            $.ajax({
                    url: baseUrl + 'products/ajax_detail_option',
                    type: "post",
                    data: {
                        product_id: product_id
                    },
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    if (data == " ") {
                        console.log("No more records found");
                        return;
                    }
                    setTimeout(function() {
                        // $('.ajax-load').hide();
                        $("#modal-product-option .modal-body").html(data);
                        $("#modal-product-option").modal('show');
                    }, 1000);

                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log("server not responding...");
                });
        }

        function set_qty_cart(qty) {
            $('.header-cart .shop-count-furniture').html(qty);
        }

        function set_qty_cart_delete() {
            var qty = $('.header-cart .shop-count-furniture').text();
            var sumQty = parseInt(qty) - 1;
            if (sumQty < 0) {
                $('.header-cart .shop-count-furniture').html(0);
            } else {
                $('.header-cart .shop-count-furniture').html(sumQty);
            }

        }

        function set_cart_table() {
            var sunTotal = 0;
            $("#table-cart .product-subtotal .amount").each(function() {
                sunTotal = sunTotal + parseInt($(this).text().replace(',', ''));
            });

            $(".text-sumtotal").html(sunTotal.toLocaleString());
        }

        function set_cart_tab() {
            $.ajax({
                    url: baseUrl + 'carts/ajax_carts_tab',
                    type: "post",
                    data: {

                    },
                    beforeSend: function() {
                        // $('.ajax-load').show();
                    }
                })
                .done(function(data) {
                    if (data == " ") {
                        console.log("No more records found");
                        return;
                    }

                    $(".header-bottom-wrapper #text-basket-display").html(data);

                })
                .fail(function(jqXHR, ajaxOptions, thrownError) {
                    console.log("server not responding...");
                });
        }
    </script>

    <script>
        $(".cart-search").click(function() {
            $(".sidebar-search").toggle(500);
        });

        $(document).on("click", "#btn-register", function() {
            $('#loginface').modal('hide');
            $('#modal-register').modal('show');
        });

        $('#icheck-profile').click(function() {
            if ($(this).is(':checked')) {
                $('#file').prop('required', true);
                $('#profile-img').removeClass('d-none');
            } else {
                $('#file').prop('required', false);
                $('#profile-img').addClass('d-none');
            }
        });

        $(document).on('change', '.text-select-option', function() {
            var id = $(this).val();
            $('.quick-view-list .text-loop-' + id).click();
            var price = $('.quick-view-list .text-loop-' + id).attr('data-price');
            var persent = $('.quick-view-list .text-loop-' + id).attr('data-persent');
            var price_true = $('.quick-view-list .text-loop-' + id).attr('data-price-true');
            $('.details-data-price span.new').html('฿' + price);
            $('.details-data-price span.persent').html(persent + '%');
            $('.details-data-price span.cut').html('฿' + price_true);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            var please = 'กรุณากรอก';
            var Tmin = 'ต้องมีอย่างน้อย';
            var Tmax = 'ต้องมีมากสุด';
            var truee = 'กรอกให้ถูก';

            var iCheckboxOpt = {
                checkboxClass: 'icheckbox_square-grey',
                radioClass: 'iradio_square-grey'
            }

            $("#RegisterForm").validate({
                rules: {
                    fname: {
                        required: true,
                        minlength: 2
                    },
                    lname: {
                        required: true,
                        minlength: 2
                    },
                    phone: {
                        required: false,
                        number: true,
                        minlength: 9,
                        maxlength: 10,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "login/check_username",
                            type: "post",
                            data: {
                                id: function() {
                                    return $('#input-id').val();
                                },
                                mode: function() {
                                    return $('#input-mode').val();
                                }
                            }
                        }
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                        required: true,
                        minlength: 5,
                        equalTo: "#user_password"
                    },
                },
                messages: {
                    fname: {
                        required: "" + please + " ชื่อ",
                        minlength: "" + Tmin + " 2 คำ"
                    },
                    lname: {
                        required: "" + please + " นามสกุล",
                        minlength: "" + Tmin + " 2 คำ"
                    },
                    phone: {
                        number: "" + truee + " เบอร์โทร",
                        minlength: "" + Tmin + " 9 ตัว",
                        maxlength: "" + Tmax + " 10 ตัว",
                    },
                    email: {
                        required: "" + please + " email",
                        email: "" + please + " email ให้ถูกต้อง",
                        remote: 'พบ email ซ้ำในระบบ',
                    },
                    password: {
                        required: "" + please + " รหัสผ่าน",
                        minlength: "" + Tmin + " 5 คำ"
                    },
                    confirm_password: {
                        required: "" + please + " ยืนยันรหัสผ่าน",
                        minlength: "" + Tmin + " 5 คำ",
                        equalTo: "รหัสผ่านไม่ตรงกัน"
                    },
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
            });

            $("#EditForm").validate({
                rules: {
                    fname: {
                        required: true,
                        minlength: 2
                    },
                    lname: {
                        required: true,
                        minlength: 2
                    },
                    phone: {
                        required: false,
                        number: true,
                        minlength: 9,
                        maxlength: 10,
                    },
                    confirm_password: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    },
                },
                messages: {
                    fname: {
                        required: "" + please + " ชื่อ",
                        minlength: "" + Tmin + " 2 คำ"
                    },
                    lname: {
                        required: "" + please + " นามสกุล",
                        minlength: "" + Tmin + " 2 คำ"
                    },
                    phone: {
                        number: "" + truee + " เบอร์โทร",
                        minlength: "" + Tmin + " 9 ตัว",
                        maxlength: "" + Tmax + " 10 ตัว",
                    },
                    confirm_password: {
                        required: "" + please + " ยืนยันรหัสผ่าน",
                        minlength: "" + Tmin + " 5 คำ",
                        equalTo: "รหัสผ่านไม่ตรงกัน"
                    },
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
            });

            $('#update_password_check').validate({
                rules: {
                    oldPassword: {
                        remote: {
                            url: '<?= site_url('login/update_password_check') ?>',
                            type: "post",
                            data: {
                                id: function() {
                                    return $('#input-id2').val();
                                },
                                mode: function() {
                                    return $('#input-mode').val();
                                }
                            }
                        }
                    },
                    rePassword: {
                        equalTo: "#input-password"
                    }
                },
                messages: {
                    oldPassword: {
                        remote: 'รหัสผ่านปัจจุบันไม่ถูกต้อง'
                    },
                    rePassword: {
                        equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'
                    }
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `help-block` class to the error element
                    error.addClass("help-block");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            var please = 'กรุณากรอก';
            var Tmin = 'ต้องมีอย่างน้อย';
            var Tmax = 'ต้องมีมากสุด';
            var truee = 'กรอกให้ถูก';

            $("#LoginForm").validate({
                rules: {
                    email: {
                        required: true,
                        // email: true
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                },
                messages: {
                    email: {
                        required: "" + please + " email",
                        email: "" + please + " email ให้ถูกต้อง"
                    },
                    password: {
                        required: "" + please + " รหัสผ่าน",
                        minlength: "" + Tmin + " 5 คำ"
                    },
                },
                errorElement: "em",
            });
            /*
            $('form#LoginForm').submit(function(event) {

                  if($('#email').val()==""){
                    $('#email').focus();
                    return false;
                  }
                  if($('#password').val()==""){
                    $('#password').focus();
                    return false;
                  }


                  $('#form-error-div').html(' ');
                  $('#form-success-div').html(' ');

                 // var conf = archiveFunction();
                  //alert(conf);
                  if(confirm("ยืนยันการแจ้งชำระเงินอีกครั้ง !!")){


                    event.preventDefault();             
                    var formObj = $(this);
                    var formURL = formObj.attr("action");
                    var formData = new FormData(this);
                    $('#form-img-div').html('<img src="'+base_url+'images/loading1.gif" alt="loading" title="loading" style="display:inline">');
                    $.ajax({
                      url: formURL,
                      type: 'POST',
                      data:formData,
                      dataType:"json",
                      mimeType:"multipart/form-data",
                      contentType: false,
                      cache: false,
                      processData:false,
                      success: function(data, textStatus, jqXHR){

                       if(data.info_txt == "error")
                       {
                         $('#form-img-div').html(' ');
                         $("#form-error-div").append("<p><strong><li class='text-red'></li>"+data.msg+"</strong>,"+data.msg2+"</p>");
                         $("#form-error-div").slideDown(400);

                       }
                       if(data.info_txt == "success")
                       {
                            
                              setTimeout(function(){
                                  $('#form-img-div').html(' ');
                                  swal({
                                      title: 'คุณทำรายการเรียบร้อย',
                                      text: "ขอให้สนุกในการเรียนน่ะค่ะ",
                                      type: 'success',
                                      confirmButtonText: 'ตกลง'
                                  }).then(function(result) {
                                      
                                      if (result.value) {
                                          window.location=base_url+'profile/my-course';
                                      }
                                  });
                                },2000);

                            }

                          },
                          error: function(jqXHR, textStatus, errorThrown){
                           $('#form-img-div').html(' ');
                           $("#form-error-div").append("<p><strong>บันทึกข้อมูลไม่สำเร็จ</strong>,กรุณาลองใหม่อีกครั้ง!</p>");
                           $("#form-error-div").slideDown(400);
                         }

                       });
                  }else{
                    return false;
                  }
            });
            */
        });
    </script>
    <script>
        function Clipboard() {
            var copyText = document.getElementById("Clipboard");
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
            //   alert("Copied the text: " + copyText.value);
        }
    </script>
    <style>
        #scrollUp {
            display: none !important;
        }
    </style>

    <script>
    $(".owl-carousel").owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:true
    });
    </script>
</body>

</html>