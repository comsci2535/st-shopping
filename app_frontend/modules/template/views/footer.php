<!-- insta feed end -->
<footer class="footer-area">
    <div class="footer-top-area bg-img pt-105 pb-65" style="background-image: url(<?=base_url('template/frontend/assets/img/bg/1.jpg');?>)" data-overlay="9">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="footer-widget mb-40">
                        <h3 class="footer-widget-title">บริการของเรา</h3>
                        <div class="footer-widget-content">
                            <ul>
                                <li><a href="<?=base_url()?>">หน้าหลัก</a></li>
                                <li><a href="<?=base_url('products')?>">สินค้า</a></li>
                                <li><a href="<?=base_url('carts')?>">สั่งซื้อสินค้า</a></li>
                                <li><a href="<?=base_url('contact')?>">ติดต่อเรา</a></li>
                                <li><a href="<?=base_url('checkorder')?>">ตรวจสอบการสั่งซื้อ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <?php echo Modules::run('contact/contact_footer');?>
            </div>
        </div>
    </div>
    <div class="footer-bottom black-bg ptb-20">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="copyright">
                        <p>
                            Copyright ©
                            <a href="https://hastech.company/">ST-Developer</a> 2019 . All Right Reserved. (v.1.0.6)
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- modal -->