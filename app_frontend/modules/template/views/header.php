<!-- header start -->
<header>
	<div class="header-area">
		<div class="header-left-sidebar">
			<div class="logo">
				<a href="<?=base_url()?>"><img src="<?=base_url('template/frontend/assets/img/logo/logo.png');?>" alt=""></a>
			</div>
			<div class="main-menu menu-hover">
				<nav>
					<ul>
						<li>
							<a href="<?=base_url()?>">Home</a>
						</li>
						<li>
							<a href="<?=base_url('products')?>">PRODUCTS</a>
						</li>
						<li><a href="javascript:void(0)">CATEGORIES</a>
							<?php echo  Modules::run('categories/categories_home');?>
						</li>
						<!-- <li><a href="javascript:void(0)">Discount</a></li>
						<li><a href="javascript:void(0)">Payment</a></li> -->
						
						<li><a href="<?=base_url('contact')?>">Contact</a></li>
						<!-- <li><a href="<?=base_url('abouts')?>">Abouts</a></li> -->
						<li><a href="<?=base_url('checkorder')?>">Check order</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<div class="header-right-sidebar">
			<div class="header-search-cart-login">
				<div class="logo">
					<a href="index.html">
						<img src="<?=base_url('template/frontend/assets/img/logo/logo.png');?>" alt="">
					</a>
				</div>
				<!-- <div class="header-search">
					<form action="#">
						<input placeholder="Search What you want" type="text">
						<button>
							<i class="ti-search"></i>
						</button>
					</form>
				</div> -->
				<?php echo  Modules::run('carts/basket_home');?>
				<!-- <div class="header-cart">
					<a class="icon-cart" href="#">
						<i class="ti-shopping-cart"></i>
						<span class="shop-count pink">02</span>
					</a>
					<ul class="cart-dropdown">
						<li class="single-product-cart">
							<div class="cart-img">
								<a href="#"><img src="<?=base_url('template/frontend/assets/img/cart/1.jpg');?>" alt=""></a>
							</div>
							<div class="cart-title">
								<h5><a href="#"> Bits Headphone</a></h5>
								<h6><a href="#">Black</a></h6>
								<span>$80.00 x 1</span>
							</div>
							<div class="cart-delete">
								<a href="#"><i class="ti-trash"></i></a>
							</div>
						</li>
						<li class="single-product-cart">
							<div class="cart-img">
								<a href="#"><img src="<?=base_url('template/frontend/assets/img/cart/2.jpg');?>" alt=""></a>
							</div>
							<div class="cart-title">
								<h5><a href="#"> Bits Headphone</a></h5>
								<h6><a href="#">Black</a></h6>
								<span>$80.00 x 1</span>
							</div>
							<div class="cart-delete">
								<a href="#"><i class="ti-trash"></i></a>
							</div>
						</li>
						<li class="single-product-cart">
							<div class="cart-img">
								<a href="#"><img src="<?=base_url('template/frontend/assets/img/cart/3.jpg');?>" alt=""></a>
							</div>
							<div class="cart-title">
								<h5><a href="#"> Bits Headphone</a></h5>
								<h6><a href="#">Black</a></h6>
								<span>$80.00 x 1</span>
							</div>
							<div class="cart-delete">
								<a href="#"><i class="ti-trash"></i></a>
							</div>
						</li>
						<li class="cart-space">
							<div class="cart-sub">
								<h4>Subtotal</h4>
							</div>
							<div class="cart-price">
								<h4>$240.00</h4>
							</div>
						</li>
						<li class="cart-btn-wrapper">
							<a class="cart-btn btn-hover" href="#">view cart</a>
							<a class="cart-btn btn-hover" href="#">checkout</a>
						</li>
					</ul>
				</div> -->
			</div>
			<div class="mobile-menu-area clearfix d-md-block col-md-12 col-lg-12 col-12 d-lg-none d-xl-none">
				<div class="mobile-menu">
					<nav id="mobile-menu-active">
						<ul class="menu-overflow">
							<li><a href="#">HOME</a>
								<ul>
									<li><a href="index.html">Fashion</a></li>
									<li><a href="index-fashion-2.html">Fashion style 2</a></li>
									<li><a href="index-fruits.html">Fruits</a></li>
									<li><a href="index-book.html">book</a></li>
									<li><a href="index-electronics.html">electronics</a></li>
									<li><a href="index-electronics-2.html">electronics style 2</a></li>
									<li><a href="index-food.html">food & drink</a></li>
									<li><a href="index-furniture.html">furniture</a></li>
									<li><a href="index-handicraft.html">handicraft</a></li>
									<li><a href="index-smart-watch.html">smart watch</a></li>
									<li><a href="index-sports.html">sports</a></li>
								</ul>
							</li>
							<li><a href="#">pages</a>
								<ul>
									<li><a href="about-us.html">about us</a></li>
									<li><a href="menu-list.html">menu list</a></li>
									<li><a href="login.html">login</a></li>
									<li><a href="register.html">register</a></li>
									<li><a href="cart.html">cart page</a></li>
									<li><a href="checkout.html">checkout</a></li>
									<li><a href="wishlist.html">wishlist</a></li>
									<li><a href="contact.html">contact</a></li>
								</ul>
							</li>
							<li><a href="#">shop</a>
								<ul>
									<li><a href="shop-grid-2-col.html"> grid 2 column</a></li>
									<li><a href="shop-grid-3-col.html"> grid 3 column</a></li>
									<li><a href="shop.html">grid 4 column</a></li>
									<li><a href="shop-grid-box.html">grid box style</a></li>
									<li><a href="shop-list-1-col.html"> list 1 column</a></li>
									<li><a href="shop-list-2-col.html">list 2 column</a></li>
									<li><a href="shop-list-box.html">list box style</a></li>
									<li><a href="product-details.html">tab style 1</a></li>
									<li><a href="product-details-2.html">tab style 2</a></li>
									<li><a href="product-details-3.html"> tab style 3</a></li>
									<li><a href="product-details-4.html">sticky style</a></li>
									<li><a href="product-details-5.html">sticky style 2</a></li>
									<li><a href="product-details-6.html">gallery style</a></li>
									<li><a href="product-details-7.html">gallery style 2</a></li>
									<li><a href="product-details-8.html">fixed image style</a></li>
									<li><a href="product-details-9.html">fixed image style 2</a></li>
								</ul>
							</li>
							<li><a href="#">BLOG</a>
								<ul>
									<li><a href="blog.html">blog 3 colunm</a></li>
									<li><a href="blog-2-col.html">blog 2 colunm</a></li>
									<li><a href="blog-sidebar.html">blog sidebar</a></li>
									<li><a href="blog-details.html">blog details</a></li>
									<li><a href="blog-details-sidebar.html">blog details 2</a></li>
								</ul>
							</li>
							<li><a href="contact.html"> Contact  </a></li>
						</ul>
					</nav>
				</div>
			</div>
			<?php echo Modules::run('banners/banners_home', 'home');?>
		</div>
	</div>
</header>
<!-- header end -->