
<?php
if(!empty($info)):
    $attribute_obj = array();
    if(!empty($this->input->get('attribute'))){
        $attribute_obj = explode(',', $this->input->get('attribute'));
    }

    $attribute_type_obj = array();
    if(!empty($this->input->get('type'))){
        $attribute_type_obj = explode(',', $this->input->get('type'));
    }
    foreach($info as $key => $item):
        ?>
        <div class="sidebar-widget mb-40" style="display: none;">
            <a data-toggle="collapse" href="#collapseExample<?=$key?>" role="button" aria-expanded="true" aria-controls="collapseExample<?=$key?>">
                <h3 class="sidebar-title"><?php echo !empty($item['title']) ? $item['title'] :'';?></h3>
            </a>
            <div class="product-tags collapse show" id="collapseExample<?=$key?>">
                <?php
                if(!empty($item['parent'])):
                    $listCout = count($item['parent']);
                ?> 
                <ul>
                    <?php
                        $attribute_active   = '';
                        $attribute_disabled = '';
                        $attribute_href     = '';
                       
                        foreach($item['parent'] as $keyList => $list):
                            $styleList          = 'flow-root';
                            $styleListDisply    = '';
                            if($keyList > 3 &&  $listCout > 4):
                                $styleList          = 'none'; 
                                $styleListDisply    = 'text-display-li';
                            endif;
                            $attribute_search       = in_array($list['slug'], $attribute_obj);
                            $attribute_type_search  = in_array($item['slug'], $attribute_type_obj);
                            if($attribute_search > 0 && $attribute_type_search > 0):
                                $attribute_active   = 'active';
                                $attribute_disabled = 'style="color: currentColor;cursor: not-allowed;opacity: 0.5;text-decoration: none;"';
                                $attribute_href   = '<a href="javascript:void(0)" 
                                class="active btn-attribute-dele" 
                                data-slug="'.$list['slug'].'" data-slug-type="'.$item['slug'].'">&nbsp;&nbsp;<i class="fas fa-times"></i></a>';
                            else:
                                $attribute_active   = 'btn-fiter-attribute';
                                $attribute_disabled = '';
                                $attribute_href     = '';
                            endif;
                            ?>
                            <li class="<?=$styleListDisply?>" style="display: <?=$styleList?>; margin-bottom: 10px;">
                                <a href="javascript:void(0)" 
                                class="<?=$attribute_active?>" <?=$attribute_disabled?>  
                                data-id="<?php echo !empty($list['product_attribute_id']) ? $list['product_attribute_id'] : '';?>" 
                                data-slug="<?php echo !empty($list['slug']) ? $list['slug'] : '';?>"
                                data-slug-type="<?php echo !empty($item['slug']) ? $item['slug'] :'';?>"
                                >
                                <div class="filter-swatch"><span style="background:<?=$list['color']?>;"></span></div>
                                <?php echo !empty($list['title']) ?  $list['title'] :'';?>
                                <?=$attribute_href?>
                            </a> 
                            </li>
                        <?php
                    endforeach; 
                ?>
            </ul>
            <?php 
                if($listCout > 4):
            ?>
                <p class="tab-minus-click" data-event="0" style="color: #bcbcbc;cursor: pointer;padding: 0px 5px;"><i class="fa fa-plus" aria-hidden="true"></i> แสดงทั้งหมด</p>
            <?php
                endif;
            ?>
            <?php
                endif;
            ?>
        </div>
    </div>
    <?php
    endforeach;
endif;
?>