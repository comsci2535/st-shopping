<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_attributes_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_product_attributes_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['categorie_id'])):
            $this->db->where('a.categorie_id', $param['categorie_id']);
        endif;

        if (!empty($param['product_attribute_id'])):
            $this->db->where_in('a.product_attribute_id', $param['product_attribute_id']);
        endif;

        if (!empty($param['parent_id'])):
            $this->db->where_in('a.parent_id', $param['parent_id']);
        endif;

        if (isset($param['slug'])):
            $this->db->where_in('a.slug', $param['slug']);
        endif;
        $this->db->where('a.active', 1);
        $query = $this->db
                        ->select('a.*')
                        ->from('product_attributes a')
                        ->get();
        return $query;
    }

    public function get_product_join_attributes_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['categorie_id'])):
            $this->db->where('a.categorie_id', $param['categorie_id']);
        endif;

        if (isset($param['categorie_groug_id'])):
            $this->db->where('a.categorie_id IN('.$param['categorie_groug_id'].')');
        endif;

        if (!empty($param['title'])):
            $this->db->like('c.title', $param['title']);
            $this->db->or_like('c.excerpt', $param['title']);
            $this->db->or_like('c.detail', $param['title']);
        endif;

        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('product_categorie_map a')
                        ->join('stocks b ','a.product_id = b.product_id')
                        ->join('products c ','a.product_id = c.product_id')
                        ->get();
        return $query;
    }
    
    public function get_product_attributes_by_in($id)
    {
        $this->db->where_in('product_attribute_id', $id);
        
        $query = $this->db
                        ->from('product_attributes a')
                        ->select('a.product_attribute_id, a.title, a.parent_id, a.order')
                        ->where('a.recycle', 0)
                        ->get();
        return $query;
    }
    
    public function get_product_attribute_by_parent_id($param)
    {
        $this->db
                ->order_by('parent_id', 'asc')
                ->order_by('order', 'asc')
                ->order_by('product_attribute_id', 'asc'); 
        $query = $this->db
                        ->from('product_attributes a')
                        ->select('a.product_attribute_id, a.title, a.parent_id, a.order ,a.color')
                        ->where('a.recycle', $param['recycle'])
                        ->where('a.active', $param['active'])
                        ->where('a.parent_id', 0)
                        ->get()->result();
        
        if(!empty($query)):
            foreach($query as $item):
                $item->parent = $this->db
                                ->from('product_attributes a')
                                ->select('a.product_attribute_id, a.title, a.parent_id, a.order,a.color')
                                ->where('a.recycle', $param['recycle'])
                                ->where('a.active', $param['active'])
                                ->where('a.parent_id', $item->product_attribute_id)
                                ->order_by('parent_id', 'asc')
                                ->order_by('order', 'asc')
                                ->order_by('product_attribute_id', 'asc')
                                ->get()->result();
            endforeach;
        endif;
                        
        return $query;
    }
}
