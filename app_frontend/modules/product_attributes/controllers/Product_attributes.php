<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_attributes extends MX_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('seos/seos_m');
        $this->load->model('product_attributes_m');
        $this->load->model('categories/categories_m');
        
    }
    
    private function seo()
    {

        $obj_seo = $this->seos_m->get_seos_by_display_page('home')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | ซื้อของออนไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {
		
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'products',
            'footer'  => 'footer',
            'function'=>  array('products'),
        );

        $this->load->view('template/body', $data);
    }

    public function product_attributes_home()
    {
        
        $input['recycle']   = 0;
        $input['active']    = 1;

        if(!empty($this->input->get('cat'))){
            $inputcat['slug']    = $this->input->get('cat');
            $categories = $this->categories_m->get_categories_by_param($inputcat)->row();
            $input['categorie_id']    = $categories->categorie_id;
        }

        $groug = $this->input->get('groug');
        if(!empty($groug)){ 
            $groug_arr = '';
            $infoCategories      =  $this->categories_m->get_categorie_by_group_slug($groug)->result();
            if(!empty($infoCategories)){
                foreach($infoCategories as $key => $grougs):
                    if($key == 0){
                        $groug_arr.= $grougs->categorie_id;
                    }else{
                        $groug_arr.= ','.$grougs->categorie_id;
                    }

                endforeach;
            }
            
            $input['categorie_groug_id']     = $groug_arr;
        }
       

        if(!empty($this->input->get('search'))){
            $input['title']     = $this->input->get('search');
        }
        
        $attributes         = $this->product_attributes_m->get_product_join_attributes_by_id($input)->result();

        $arr_o = array();
        foreach($attributes as $item){
            $arr  = explode(',', $item->attributes_option);
            foreach($arr as $l){
                array_push($arr_o, $l);
            }
        }
        
        $inputs_['product_attribute_id']    = $arr_o;
        $info           = $this->product_attributes_m->get_product_attributes_by_id($inputs_)->result();
        $list_arr       = array();
        $list_group_arr = array();
        if(!empty($info)){
            foreach($info as $list){

                $list_group_arr[$list->parent_id] = array('product_attribute_id' => $list->parent_id);
                $list_arr[$list->parent_id][] = array(
                    'product_attribute_id'  => $list->product_attribute_id,
                    'title'                 => $list->title,
                    'slug'                  => $list->slug,
                    'color'                 => $list->color,
                    'parent_id'             => $list->parent_id
                );

            }
        }

        $attribute_group = array();
        if(!empty($list_group_arr)){
            foreach($list_group_arr as $gruop){
                $input_['product_attribute_id']    = $gruop['product_attribute_id'];
                $infoAttribute   = $this->product_attributes_m->get_product_attributes_by_id($input_)->row();
                
                array_push($attribute_group, array(
                    'product_attribute_id'  => $infoAttribute->product_attribute_id,
                    'slug'                  => $infoAttribute->slug,
                    'title'                 => $infoAttribute->title,
                    'color'                 => $infoAttribute->color,
                    'parent'                => $list_arr[$gruop['product_attribute_id']]
                ));
            }
        }

        $data['info']       = $attribute_group;
        $this->load->view('attributes', $data);
    }
}
