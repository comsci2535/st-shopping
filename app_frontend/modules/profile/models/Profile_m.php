<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_user_by_id($user_id)
    {
        
        $this->db->where('a.user_id', $user_id);
        return $this->db->get('users a');
    }
}
