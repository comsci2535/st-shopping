<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller {

    function __construct()
    {
		parent::__construct();
        $this->load->model('seos/seos_m');
        $this->load->model('login/users_model');
        $this->load->library('login/users_library');
        $this->load->library('uploadfile_library');
        $this->load->model('profile_m');
        
	}

    private function seo()
    {

        $obj_seo = $this->seos_m->get_seos_by_display_page('home')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | ซื้อของออนไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {
        $user_id = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        $info = $this->profile_m->get_user_by_id($user_id)->row();
       
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'profile',
            'footer'  => 'footer',
            'function'=>  array('profile'),
        );

        $info->user_id  = decode_id($info->user_id);

        $data['info'] = $info;
        
        // $data['password'] = $this->users_library->hash_password($info->password,$info->salt);
        // arr($data['info']);
        $this->load->view('template/body', $data);
    }

    public function editprofile() 
    {

        $toastr['type']         = 'error';
        $toastr['lineOne']      = config_item('appName');
        $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']        = false;
        $data['toastr']         = $toastr;
        
        $input = $this->input->post(null, true);

        $user_id = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        
        $userData['fname']      = !empty($input['fname'])?$input['fname']:'';
        $userData['lname']      = !empty($input['lname'])?$input['lname']:'';
        $userData['fullname']   = $userData['fname'].' '.$userData['lname'];
        $userData['phone']      = !empty($input['phone'])?$input['phone']:'';
        $userData['username']   = !empty($input['email'])?$input['email']:'';
        $userData['email']      = !empty($input['email'])?$input['email']:'';
        $userData['updated_at'] = db_datetime_now();
       
        $path   = 'users';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
        $file = '';
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            // $outfile = $input['oldfile'];
            // if(isset($outfile)){
            //     $this->load->helper("file");
            //     @unlink($outfile);
            // }
            $userData['file'] = $file;
        }

        $result = $this->users_model->update_user($user_id, $userData);
        
        if ($result) {

            $toastr['type']     = 'success';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';

        } else {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }

        $data['success']    = $result;
        $data['toastr']     = $toastr;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function resetpassword() 
    {

        $toastr['type']         = 'error';
        $toastr['lineOne']      = config_item('appName');
        $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']        = false;
        $data['toastr']         = $toastr;
        
        $input                  = $this->input->post(null, true);

        $user_id                = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        $salt                   = $this->users_library->salt();
        $password               = $this->users_library->hash_password($input['password'], $salt);
        $userData['salt']       = $salt;
        $userData['password']   = $password;
        $userData['updated_at'] = db_datetime_now();
       
        $result = $this->users_model->update_user($user_id, $userData);
        
        if ($result) {

            $toastr['type']     = 'success';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';

        } else {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }

        $data['success']    = $result;
        $data['toastr']     = $toastr;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}
