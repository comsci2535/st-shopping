<div class="cart-main-area pt-95 pb-100">
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8">
                <h4 class="pb-30"><i class="ti-id-badge"></i> ข้อมูลส่วนตัว</h4>
                <form id="ProfileForm" 
                    action="#"
                    method="post" enctype="multipart/form-data" 
                    style="width: 100%;" autocomplete="off">
                    <div id="profile-img-edit" class="form-group text-center">
                        <input type="text" id="input-id" value="<?php echo !empty($info->user_id) ? $info->user_id : '';?>">
                        <input id="file-edit" name="file" type="file" data-preview-file-type="text">
                        <img src="<?php echo !empty($info->file) ? base_url($info->file) : $info->oauth_picture;?>" 
                        style="width: 150px;margin-top: 15px;"
                        alt="">
                        <input type="hidden" name="oldfile" value="<?php echo !empty($info->file) ? $info->file : '';?>">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="fname">ชื่อ <span class="required">*</span></label>
                        <input type="text" id="fname" name="fname" class="form-control" 
                        value="<?php echo !empty($info->fname) ? $info->fname : '';?>" 
                        placeholder="ชื่อ">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="lname">นามสกุล <span class="required">*</span></label>
                        <input type="text" id="lname" name="lname" class="form-control"
                        value="<?php echo !empty($info->lname) ? $info->lname : '';?>"
                         placeholder="นามสกุล">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="phone">เบอร์โทร</label>
                        <input type="text" id="phone" name="phone" class="form-control" 
                        value="<?php echo !empty($info->phone) ? $info->phone : '';?>"
                        placeholder="เบอร์โทร">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="email">Email <span class="required">*</span></label>
                        <input type="email" id="email" name="email" class="form-control" 
                        value="<?php echo !empty($info->email) ? $info->email : '';?>"
                         placeholder="Email">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn menu-btn btn-hover" style="height: 47px;cursor: pointer;">แก้ไขข้อมูล</button>
                    </div>
                </form>
            </div>
        </div>
        <?php
             if(empty($info->oauth_provider)):
        ?>
        <div class="row">
            <div class="offset-md-2 col-md-8">
                <hr>
            </div>
            <div class="offset-md-2 col-md-8">
                <h4 class="pb-30"><i class="ti-key"></i> ตั้งรหัสผ่านใหม่</h4>
                <form id="ProfileResetPasswordForm" 
                    action="#"
                    method="post" enctype="multipart/form-data" 
                    style="width: 100%;" autocomplete="off">
                    <div class="form-group">
                        <label class="Bold" for="password">รหัสผ่าน <span class="required">*</span></label>
                        <input type="password" id="user_password_" name="password" class="form-control"
                        value=""
                            placeholder="รหัสผ่าน">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="confirm_password">ยืนยันรหัสผ่าน <span class="required">*</span></label>
                        <input type="password" id="confirm_password" name="confirm_password"
                            class="form-control"
                            value=""
                             placeholder="ยืนยันรหัสผ่าน">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn menu-btn btn-hover" style="height: 47px;cursor: pointer;">เปลี่ยนรหัสผ่าน</button>
                    </div>
                </form>
            </div>
        </div>
        <?php
            endif;
        ?>
    </div>
</div>
<!-- shopping-cart-area end -->
