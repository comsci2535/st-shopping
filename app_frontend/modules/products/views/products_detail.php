<?php echo Modules::run('banners/banners_home', 'products');?>
<div class="product-details ptb-100 pb-90">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-5 col-12">
                <div class="product-details-img-content">
                    <div class="product-details-tab mr-70">
                        <div class="product-details-large tab-content">
                            <?php

                            if(!empty($info->img)):
                                foreach($info->img as $keys => $imgs):
                                    $tab_select = '';
                                    if($keys == 0):
                                        $tab_select = 'active show';
                                    endif;
                                    ?>
                                    <div class="tab-pane <?=$tab_select?> fade" id="pro-details<?=$keys?>" role="tabpanel">
                                        <div class="">
                                            <a href="<?=base_url($imgs->file_path)?>" class="zoomple2">
                                                 <img src="<?php echo !empty($imgs->file_path) ? base_url($imgs->file_path) : '';?>" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'" class="block__pic" style="width: 100%;" data-magnify-src="<?=base_url($imgs->file_path)?>">
                                            </a>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                            <div class="product-details-small nav mt-12 " role=tablist>
                                <?php 
                                if(!empty($info->img)):
                                    foreach($info->img as $key => $img):
                                        $tab_select = '';
                                        if($key == 0):
                                            $tab_select = 'active';
                                        endif;
                                        $class_loop = '';
                                        $loop_attributes = explode(',', $img->attributes_option);
                                        if(!empty($loop_attributes)):
                                            foreach($loop_attributes as $loop):
                                                $class_loop.=' text-loop-'.$loop;
                                            endforeach;
                                        endif;

                                        $price                = !empty($img->product_price) ? number_format($img->product_price, 2) : 0;
                                        $price_true           = !empty($img->product_price_true) ? number_format($img->product_price_true, 2) : 0;
                                        $persent              = (($img->product_price / $img->product_price_true)) * 100;

                                        ?>

                                        <a class="<?=$tab_select?> <?=$class_loop?> mr-12 item" 
                                            href="#pro-details<?=$key?>" 
                                            data-price="<?=$price?>" 
                                            data-persent="<?=number_format($persent,2)?>" 
                                            data-price-true="<?=$price_true?>" 
                                            data-toggle="tab" 
                                            role="tab" 
                                            aria-selected="true">
                                            <img src="<?php echo !empty($img->file_path) ? base_url($img->file_path) : '';?>" alt="" style="width: 141px;" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'">
                                        </a>

                                        <?php
                                    endforeach;
                                endif;
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-7 col-12">
                    <div class="product-details-content">
                        <h3><?php echo !empty($info->title) ? $info->title : '';?></h3>
                        <div class="price product-content details-price">
                            <?php if(!empty($info->price_true) && ($info->price_true != '0.00')){?>
                                <span class="cut">฿<?php echo !empty($info->price_true) ? $info->price_true : 0;?></span> 
                                <span class="persent"><?php echo !empty($info->persent) ? number_format($info->persent, 2) : 0.00;?>%</span> 
                                <br>
                            <?php }?>
                            <span class="new">฿<?php echo !empty($info->price) ? $info->price : 0;?></span>
                            <p class="cut">
                                <label><i class="far fa-eye"></i> <?php echo !empty($info->view_qty) ? $info->view_qty : 0;?></label>
                                <label><i class="fas fa-shopping-cart"></i> <?php echo !empty($info->order_qty) ? $info->order_qty : 0;?></label>
                            </p>
                        </div>
                        <p><?php echo !empty($info->excerpt) ? $info->excerpt :'';?></p>
                        <div class="quick-view-select">
                            <?php
                           
                            if(!empty($info->attributes_option['group'])):
                                foreach($info->attributes_option['group'] as $group):
                                    ?>
                                    <div class="select-option-part">
                                        <?php 
                                        
                                        if(!empty($info->attributes_option['option'][$group['product_attribute_id']])):

                                            // arr($info->attributes_option['option']);
                                            ?>
                                            <label><?php echo !empty($group['title']) ? $group['title'] :'';?>*</label>
                                            <select data-select="" class="select text-select-detail-option">
                                                <!-- <option value="">- กรุณาเลือก -</option> -->
                                                <?php 
                                                foreach($info->attributes_option['option'][$group['product_attribute_id']] as $option):
                                                    ?>
                                                    <option value="<?php echo !empty($option['product_attribute_id']) ? $option['product_attribute_id'] :'';?>"><?php echo !empty($option['title']) ? $option['title'] :'';?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                            <?php  
                                        endif;
                                        ?>
                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>
                        <p class="text-qty-onoff-stock" style="color: #fb6830;display:none;"></p>
                        <input type="hidden" class="text-stock-id" value="">
                        <input type="hidden" class="text-is-limit" value="">
                        <input type="hidden" class="text-stock-qty" value="">
                        <div class="quickview-plus-minus">
                            <div class="cart-plus-minus">
                                <input type="text" value="1" name="qtybutton" id="text-detail-qty" class="cart-plus-minus-box" min="1" max="9999" readonly maxlength="4" pattern="\d*"
                                oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                >
                            </div>
                            <div class="quickview-btn-cart">
                                <a class="btn-hover-black btn-product-detail-add-cart" 
                                data-id="<?php echo !empty($info->product_id) ? $info->product_id : '';?>" 
                                data-count="<?php echo !empty($info->count) ? $info->count : 0;?>"
                                href="javascript:void(0)">หยิบใส่ตะกร้า</a>
                            </div>
                        <!-- <div class="quickview-btn-wishlist">
                            <a class="btn-hover" href="#"><i class="pe-7s-like"></i></a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-description-review-area pb-90">
    <div class="container">
        <div class="product-description-review text-center">
            <div class="description-review-title nav" role=tablist>
                <a class="active" href="#pro-dec" data-toggle="tab" role="tab" aria-selected="true">
                    รายละเอียดสินค้า
                </a>
                <?php
                if(!empty($reviews_count)):
                    ?>
                    <a href="#pro-review" data-toggle="tab" role="tab" aria-selected="false">
                        ความคิดเห็น (<?php echo !empty($reviews_count) ? $reviews_count : 0;?>)
                    </a>
                    <?php
                endif;
                ?>
            </div>
            <div class="description-review-text tab-content">
                <div class="tab-pane active show fade" id="pro-dec" role="tabpanel">
                    <p><?php echo !empty($info->detail) ? html_entity_decode($info->detail) :'';?></p>
                </div>
                <div class="tab-pane fade" id="pro-review" role="tabpanel">
                    <?php echo Modules::run('reviews/reviews_product', $info->product_id);?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-area pb-95">
    <div class="container">
        <div class="section-title-3 text-center mb-50">
            <h2>สินค้าที่เกี่ยวข้อง</h2>
        </div>
        <div class="product-style">
            <div class="related-product-active owl-carousel">
                <?php
                if(!empty($proCate)):
                    foreach($proCate as $cate):
                        ?>
                        <div class="product-wrapper">
                            <div class="product-img">
                                <a href="<?php echo !empty($cate->slug) ? base_url('products/detail/').$cate->slug : '#';?>">
                                    <img src="<?php echo !empty($cate->file) ? base_url($cate->file) : '';?>" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'">
                                </a>
                                <?php
                                if(!empty($item->recommend) && $item->recommend > 0):
                                    ?>
                                    <!-- <span class="new">new</span> -->
                                    <span class="sell">sell</span>
                                    <?php
                                endif;
                                ?>
                                <div class="hanicraft-action-position">
                                    <div class="hanicraft-action">
                                        <a class="animate-top btn-product-detail" 
                                        title="เพิ่มลงตะกร้า" 
                                        data-id="<?php echo !empty($cate->product_id) ? $cate->product_id : '';?>" 
                                        data-count="<?php echo !empty($cate->count) ? $cate->count : 0;?>" 
                                        data-toggle="tooltip" 
                                        href="javascript:void(0)">
                                        <i class="pe-7s-cart"></i>
                                    </a>
                                    <a class="animate-right btn-product-detail" 
                                    title="ดูรายละเอียดสินค้า"
                                    data-id="<?php echo !empty($cate->product_id) ? $cate->product_id : '';?>" 
                                    data-toggle="tooltip" 
                                    href="javascript:void(0)">
                                    <i class="pe-7s-look"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="product-content">
                        <h4><a href="<?php echo !empty($cate->slug) ? base_url('products/detail/').$cate->slug : '#';?>"> <?php echo !empty($cate->title) ? $cate->title : '';?> </a></h4> 
                        <?php if(!empty($cate->price_true) && ($cate->price_true != '0.00')){?>
                            <span class="cut">฿<?php echo !empty($cate->price_true) ? $cate->price_true : 0;?></span> 
                            <span class="persent"><?php echo !empty($cate->persent) ? number_format($cate->persent, 2) : 0.00;?>%</span> 
                        <?php }?>
                        <h5>฿<?php echo !empty($cate->price) ? $cate->price : 0;?></h5>
                        <p class="cut mt-2">
                            <label><i class="far fa-eye"></i> <?php echo !empty($cate->view_qty) ? $cate->view_qty : 0;?></label>
                            <label><i class="fas fa-shopping-cart"></i> <?php echo !empty($cate->order_qty) ? $cate->order_qty : 0;?></label>
                        </p>
                    </div>
                </div>
                <?php
            endforeach;
        endif;
        ?>
    </div>
</div>
</div>
</div>