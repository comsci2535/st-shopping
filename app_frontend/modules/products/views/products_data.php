<?php

if(!empty($info)):
    
    foreach($info as $item):
        ?>
        <div class="col-lg-6 col-md-6 col-xl-3 col-6">
            <div class="product-wrapper mb-30">
                <div class="product-img">
                    <a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>">
                        <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'">
                    </a>
                    <?php
                    if(!empty($item->recommend) && $item->recommend > 0):
                        ?>
                        <!-- <span class="new">new</span> -->
                        <span class="sell">sell</span>
                        <?php
                    endif;
                    ?>
                    <div class="hanicraft-action-position">
                        <div class="hanicraft-action">
                            <a class="animate-top btn-product-detail"
                            title="เพิ่มลงตะกร้า" 
                            data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>" 
                            data-count="<?php echo !empty($item->count) ? $item->count : 0;?>" 
                            data-toggle="tooltip" 
                            href="javascript:void(0)">
                            <i class="pe-7s-cart"></i>
                        </a>
                        <a class="animate-right btn-product-detail" 
                        title="ดูรายละเอียดสินค้า" data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>" 
                        data-toggle="tooltip" href="javascript:void(0)">
                        <i class="pe-7s-look"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="product-content">
         
            <h4><a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>"> <?php echo !empty($item->title) ? $item->title : '';?> </a></h4> 
            <?php if(!empty($item->price_true) && ($item->price_true != '0.00')){?>
                <span class="cut">฿<?php echo !empty($item->price_true) ? $item->price_true : 0;?></span> 
                <span class="persent"><?php echo !empty($item->persent) ? number_format($item->persent, 2) : 0.00;?>%</span> 
            <?php }?>
            <h5>฿<?php echo !empty($item->price) ? $item->price : 0;?></h5>
            <p class="cut mt-2">
                <label><i class="far fa-eye"></i> <?php echo !empty($item->view_qty) ? $item->view_qty : 0;?></label>
                <label><i class="fas fa-shopping-cart"></i> <?php echo !empty($item->order_qty) ? $item->order_qty : 0;?></label>
            </p>
        </div>
    </div>
</div>
<?php
endforeach;
endif;
?>