
<div class="qwick-view-left">
    <div class="quick-view-learg-img">
        <div class="quick-view-tab-content tab-content">
            <?php
            if(!empty($info->img)):
                foreach($info->img as $keys => $imgs):
                    $tab_select = '';
                    if($keys == 0):
                        $tab_select = 'active show';
                    endif;
            ?>
            <div class="tab-pane <?=$tab_select?> fade" id="modal<?=$keys?>" role="tabpanel">
                <img class="block__pic" src="<?php echo !empty($imgs->file_path) ? base_url($imgs->file_path) : '';?>" style="width: 320px;" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/quick-view/l1.jpg');?>'" >
            </div>
            <?php
                endforeach;
            endif;
            ?>
        </div>
    </div>
    <div class="quick-view-list nav" role="tablist">
        <?php
        if(!empty($info->img)):
            
            foreach($info->img as $key => $img):
                $tab_select = '';
                if($key == 0):
                    $tab_select = 'active';
                endif;
                $class_loops = '';
                $loops_attributes = explode(',', $img->attributes_option);
                if(!empty($loops_attributes)):
                    foreach($loops_attributes as $loop):
                        $class_loops.=' text-loop-'.$loop;
                    endforeach;
                endif;

                $prices                = !empty($img->product_price) ? number_format($img->product_price, 2) : 0;
                $prices_true           = !empty($img->product_price_true) ? number_format($img->product_price_true, 2) : 0;
                $persents              = (($img->product_price / $img->product_price_true)) * 100;
        ?>
        <a class="<?=$tab_select?> <?=$class_loops?> mr-12" 
            href="#modal<?=$key?>" 
            data-price="<?=$prices?>" 
            data-persent="<?=number_format($persents,2)?>" 
            data-price-true="<?=$prices_true?>"
            data-toggle="tab" 
            role="tab" 
            aria-selected="true" 
            aria-controls="home<?=$key?>">
            <img src="<?php echo !empty($img->file_path) ? base_url($img->file_path) : '';?>" style="width: 100px;" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/quick-view/s1.jpg');?>'">
        </a>
        <?php
            endforeach;
        endif;
        ?>
    </div>
</div>
<div class="qwick-view-right">
    <div class="qwick-view-content">
        <h3><?php echo !empty($info->title) ? $info->title : '';?></h3>
        <div class="price product-content details-data-price">
            <?php if(!empty($info->price_true) && ($info->price_true != '0.00')){?>
                <span class="cut">฿<?php echo !empty($info->price_true) ? $info->price_true : 0;?></span> 
                <span class="persent"><?php echo !empty($info->persent) ? number_format($info->persent, 2) : 0.00;?>%</span> 
                <br>
            <?php }?>
            <span class="new">฿<?php echo !empty($info->price) ? $info->price : 0;?></span>
            <!-- <span class="old">฿ <?php echo !empty($info->price) ? $info->price : 0;?>  </span> -->
        </div>
        <p class="cut">
            <label><i class="far fa-eye"></i> <?php echo !empty($info->view_qty) ? $info->view_qty : 0;?></label>
            <label><i class="fas fa-shopping-cart"></i> <?php echo !empty($info->order_qty) ? $info->order_qty : 0;?></label>
        </p>
        <p><?php echo !empty($info->excerpt) ? $info->excerpt :'';?></p>
        <div class="quick-view-select">
        <?php
        if(!empty($info->attributes_option['group'])):
            foreach($info->attributes_option['group'] as $group):
        ?>
        <div class="select-option-part">
            <?php 
            if(!empty($info->attributes_option['option'][$group['product_attribute_id']])):
            ?>
            <label><?php echo !empty($group['title']) ? $group['title'] :'';?>*</label>
            <select data-select="" class="select text-select-option">
                
                <?php 
                    foreach($info->attributes_option['option'][$group['product_attribute_id']] as $option):
                        $selected = '';
                        if(sizeof($info->attributes_option['option'][$group['product_attribute_id']]) ==1):
                            $selected = 'selected';
                        endif;    
                ?>
                    <option <?=$selected?> value="<?php echo !empty($option['product_attribute_id']) ? $option['product_attribute_id'] :'';?>"><?php echo !empty($option['title']) ? $option['title'] :'';?></option>
                <?php
                    endforeach;
                ?>
            </select>
            <?php  
            endif;
            ?>
        </div>
        <?php
            endforeach;
        endif;
        ?>
        </div>
        <p class="text-qty-onoff-stock" style="color: #fb6830;display:none;"></p>
        <input type="hidden" class="text-stock-id" value="">
        <input type="hidden" class="text-is-limit" value="">
        <input type="hidden" class="text-stock-qty" value="">
        
        <div class="quickview-plus-minus">
            <div class="cart-plus-minus">
                <div class="dec qtybutton" data-id="<?php echo !empty($info->product_id) ? $info->product_id : '';?>" data-count="<?php echo !empty($info->count) ? $info->count : 0;?>">-</div>
                <input type="text" value="1" name="qtybutton" id="text-qty" class="cart-plus-minus-box"
                readonly oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                >
                <div class="inc qtybutton" data-id="<?php echo !empty($info->product_id) ? $info->product_id : '';?>" data-count="<?php echo !empty($info->count) ? $info->count : 0;?>">+</div>
            </div>
            <div class="quickview-btn-cart">
                <a class="btn-product-add-cart contact-btn btn-hover" data-id="<?php echo !empty($info->product_id) ? $info->product_id : '';?>" data-count="<?php echo !empty($info->count) ? $info->count : 0;?>" href="javascript:void(0)">เพิ่มลงตะกร้า</a>
            </div>
        </div>
    </div>
</div>