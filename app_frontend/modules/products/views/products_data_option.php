  
    <div class="qwick-view-content" style="width: 100%;">
    
        <h3><?php echo !empty($info->title) ? $info->title : '';?></h3>
        <div class="price product-content">
            <?php if(!empty($info->price_true) && ($info->price_true != '0.00')){?>
                <span class="cut">฿<?php echo !empty($info->price_true) ? $info->price_true : 0;?></span> 
                <span class="persent"><?php echo !empty($info->persent) ? number_format($info->persent, 2) : 0.00;?>%</span> 
                <br>
            <?php }?>
            <span class="new">฿<?php echo !empty($info->price) ? $info->price : 0;?></span>
            <p class="cut mt-2">
                <label><i class="far fa-eye"></i> <?php echo !empty($info->view_qty) ? $info->view_qty : 0;?></label>
                <label><i class="fas fa-shopping-cart"></i> <?php echo !empty($info->order_qty) ? $info->order_qty : 0;?></label>
            </p>
            <!-- <span class="old">฿ <?php echo !empty($info->price) ? $info->price : 0;?>  </span> -->
        </div>
        
        <p><?php echo !empty($info->excerpt) ? $info->excerpt :'';?></p>
        <div class="quick-view-select">
        <?php
        if(!empty($info->attributes_option['group'])):
            foreach($info->attributes_option['group'] as $group):
        ?>
        <div class="select-option-part">
            <?php 
            if(!empty($info->attributes_option['option'][$group['product_attribute_id']])):
            ?>
            <label><?php echo !empty($group['title']) ? $group['title'] :'';?>*</label>
            <select data-select="" class="select text-select-option">
                <option value="">- กรุณาเลือก -</option>
                <?php 
                    foreach($info->attributes_option['option'][$group['product_attribute_id']] as $option):
                ?>
                    <option value="<?php echo !empty($option['product_attribute_id']) ? $option['product_attribute_id'] :'';?>"><?php echo !empty($option['title']) ? $option['title'] :'';?></option>
                <?php
                    endforeach;
                ?>
            </select>
            <?php  
            endif;
            ?>
        </div>
        <?php
            endforeach;
        endif;
        ?>
        </div>
        <p class="text-qty-onoff-stock" style="color: #fb6830;display:none;"></p>
        <input type="hidden" class="text-stock-id" value="">
        <input type="hidden" class="text-is-limit" value="">
        <input type="hidden" class="text-stock-qty" value="">
        <div class="quickview-plus-minus">
            <div class="cart-plus-minus">
                <div class="dec qtybutton">-</div>
                <input type="text" value="1" name="qtybutton" id="text-qty" class="cart-plus-minus-box" readonly>
                <div class="inc qtybutton">+</div>
            </div>
            <div class="quickview-btn-cart">
                <a class="contact-btn btn-hover btn-product-add-cart" data-id="<?php echo !empty($info->product_id) ? $info->product_id : '';?>" data-count="<?php echo !empty($info->count) ? $info->count : 0;?>" href="javascript:void(0)">เพิ่มลงตะกร้า</a>
            </div>
        </div>
    </div>