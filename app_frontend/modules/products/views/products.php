<?php echo Modules::run('banners/banners_home', 'products');?>
<div class="shop-page-wrapper shop-page-padding ptb-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="shop-sidebar mr-50">
                    <div class="sidebar-widget mb-50">
                        <h3 class="sidebar-title">ค้นหาสินค้า</h3>
                        <div class="sidebar-search">
                            <form action="javascript:void(0)">
                                <input id="text-product-search" placeholder="ค้นหาสินค้า..." 
                                type="text" value="<?php echo !empty($search) ? $search : '';?>">
                                <button id="btn-product-search"><i class="ti-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <?php echo Modules::run('categories/categories_products');?>
                    
                    <?php echo Modules::run('product_attributes/product_attributes_home');?>
                    <div class="sidebar-widget mb-50 mb-50-mobile-hide">
                    <?php echo Modules::run('products/products_top');?> 
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="shop-product-wrapper res-xl res-xl-btn">
                    <div class="shop-bar-area">
                        <div class="shop-bar pb-60">
                            <div class="shop-found-selector">
                                <div class="shop-found">
                                    <p><span><?php echo !empty($num_row) ? $num_row : 0;?></span> รายการ</p>
                                </div>
                                <div class="shop-selector">
                                    <label>เรียงตาม : </label>
                                    <select name="select" class="text-order-by">
                                        <option value="ASC">เก่าไปใหม่</option>
                                        <option value="DESC" selected>ใหม่ล่าสุด</option>
                                    </select>
                                </div>
                            </div>
                            <div class="shop-filter-tab">
                                <div class="shop-tab nav" role=tablist> 
                                </div>
                            </div>
                        </div>
                        <div class="shop-product-content tab-content">
                            <div id="grid-sidebar1" class="tab-pane fade active show">
                                <div class="row">
                                    <?php
                                    
                                    if(!empty($info)):
                                        foreach($info as $item):
                                            ?>
                                            <div class="col-lg-6 col-md-6 col-xl-3 col-6">
                                                <div class="product-wrapper mb-10">
                                                    <div class="product-img">
                                                        <a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>">
                                                            <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'">
                                                        </a>
                                                        <?php
                                                        if(!empty($item->recommend) && $item->recommend > 0):
                                                            ?> 
                                                            <span class="sell">sell</span>
                                                            <?php
                                                        endif;
                                                        ?>
                                                        <div class="hanicraft-action-position">
                                                            <div class="hanicraft-action">
                                                                <a class="animate-top btn-product-detail" title="เพิ่มลงตะกร้า" 
                                                                data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>" 
                                                                data-count="<?php echo !empty($item->count) ? $item->count : 0;?>" 
                                                                data-toggle="tooltip" 
                                                                href="javascript:void(0)">
                                                                <i class="pe-7s-cart"></i>
                                                            </a>
                                                            <a class="animate-right btn-product-detail" 
                                                            title="ดูรายละเอียดสินค้า" data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>" 
                                                            data-toggle="tooltip" href="javascript:void(0)">
                                                            <i class="pe-7s-look"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <h4><a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>"> <?php echo !empty($item->title) ? $item->title : '';?> </a></h4>
                                                <?php if(!empty($item->price_true) && ($item->price_true != '0.00')){?>
                                                    <span class="cut">฿<?php echo !empty($item->price_true) ? $item->price_true : 0;?></span> 
                                                    <span class="persent"><?php echo !empty($item->persent) ? number_format($item->persent, 2) : 0.00;?>%</span> 
                                                <?php }?>
                                                <h5>฿<?php echo !empty($item->price) ? $item->price : 0;?></h5>
                                                <p class="cut">
                                                    <label><i class="far fa-eye"></i> <?php echo !empty($item->view_qty) ? $item->view_qty : 0;?></label>
                                                    <label><i class="fas fa-shopping-cart"></i> <?php echo !empty($item->order_qty) ? $item->order_qty : 0;?></label>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>
                    </div>
                            <!-- hoder -->
                           <div class="loader product-loader" style="display: none;">Loading...</div>
                    
                </div>
            </div>
            <div class="col-lg-12">
                <div class="shop-sidebar mr-50">
                    <div class="sidebar-widget mb-50 mb-50-mobile-show">
                        <?php echo Modules::run('products/products_top');?>
                    </div>
                </div>
            </div>
        </div>
                 
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="num_row" id="num_row" value="<?=$num_row?>">
<input type="hidden" name="prepage" id="prepage" value="<?=$per_page?>">
<?php
if(!empty($attribute)):
    $attribute_arr = explode(',', $attribute); 
    foreach($attribute_arr as $atb):
?>
    <input type="hidden" class="text-attribute" value="<?php echo !empty($atb)? $atb : '';?>">
<?php
    endforeach;
endif;
?> 

<?php
if(!empty($type_attribute)):
    $attribute_type_arr = explode(',', $type_attribute); 
    foreach($attribute_type_arr as $types):
?>
    <input type="hidden" class="text-group-attribute" value="<?php echo !empty($types)? $types : '';?>">
<?php
    endforeach;
endif;
?> 
<input type="hidden" id="text-cat" value="<?php echo !empty($cat)? $cat : '';?>">
<input type="hidden" id="text-groug" value="<?php echo !empty($groug)? $groug : '';?>">
<input type="hidden" id="text-fiter-p" value="<?php echo !empty($p)? $p : '';?>">