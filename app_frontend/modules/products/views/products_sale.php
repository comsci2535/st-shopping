<?php if(!empty($products)):?>
<div class="product-style-area wrapper-padding-2">
    <div class="container-fluid">
        <div class="section-title text-center mb-60 mt-60">
            <h2>สินค้าขายดี</h2>
        </div>
        <div class="coustom-row-4">
            <?php
            if(!empty($products)):
                foreach($products as $item):
                    ?>
                    <div class="custom-col-two-5 custom-col-style-4">
                        <div class="product-wrapper mb-10">
                            <div class="product-img-hanicraft">
                                <a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>">
                                    <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'" alt="">
                                </a>
                                <?php
                                if(!empty($item->recommend) && $item->recommend > 0):
                                    ?>
                                    <!-- <span class="new">new</span> -->
                                    <span class="sell">sell</span>
                                    <?php
                                endif;
                                ?>
                                <div class="hanicraft-action-position">
                                    <div class="hanicraft-action">
                                        <a class="animate-top btn-product-detail" title="เพิ่มลงตะกร้าสินค้า" 
                                        data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>" 
                                        data-count="<?php echo !empty($item->count) ? $item->count : 0;?>" 
                                        data-toggle="tooltip" 
                                        href="javascript:void(0)">
                                        <i class="pe-7s-cart"></i>
                                    </a>
                                    <a class="animate-right btn-product-detail" 
                                    title="ดูรายละเอียดสินค้า" data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>" 
                                    data-toggle="tooltip" href="javascript:void(0)">
                                    <i class="pe-7s-look"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="product-content-hanicraft">
                        <h4>
                            <a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>"> 
                                <?php echo !empty($item->title) ? $item->title : '';?> </a>
                            </h4>
                            <?php if(!empty($item->price_true) && ($item->price_true != '0.00')){?>
                                <span class="cut">฿<?php echo !empty($item->price_true) ? $item->price_true : 0;?></span> <span class="persent"><?=number_format($item->persent,2)?>%</span> 
                            <?php }?>
                            <h5>฿<?php echo !empty($item->price) ? $item->price : 0;?></h5>
                            <p class="cut mt-2">
                                <label><i class="far fa-eye"></i> <?php echo !empty($item->view_qty) ? $item->view_qty : 0;?></label>
                                <label><i class="fas fa-shopping-cart"></i> <?php echo !empty($item->order_qty) ? $item->order_qty : 0;?></label>
                            </p>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
        endif;
        ?>
    </div>
    <div class="menu-btn-area text-center mt-40">
        <a class="menu-btn btn-hover" href="<?=base_url('products?p=สินค้าขายดี')?>">แสดงสินค้าขายดีทั้งหมด</a>
    </div>
    <br>
</div>
</div>
<?php
endif;
?>