<?php if(!empty($products)):?> 
    <h3 class="sidebar-title">สินค้าอื่นๆ</h3>
    <div class="sidebar-top-rated-all">
        <?php
        if(!empty($products)):
            foreach($products as $item):
                ?>
                <div class="sidebar-top-rated mb-30">
                    <div class="single-top-rated">
                        <div class="top-rated-img">
                            <a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>">
                                <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" alt="" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'" style="width: 91px;"> 
                            </a>
                        </div>
                        <div class="top-rated-text">
                            <h4>
                                <a href="<?php echo !empty($item->slug) ? base_url('products/detail/').$item->slug : 'javascript:void(0)';?>"> <?php echo !empty($item->title) ? $item->title : '';?> </a>
                            </h4>
                            <?php if(!empty($item->price_true) && ($item->price_true != '0.00')){?>
                                <span class="cut">฿<?php echo !empty($item->price_true) ? $item->price_true : 0;?></span> 
                                <span class="persent"><?=number_format($item->persent,2)?>%</span> 
                            <?php }?>
                            <span class="price">฿<?php echo !empty($item->price) ? $item->price : 0;?></span>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
        endif;
        ?>
    </div>
<?php
endif;
?>