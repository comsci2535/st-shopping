<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_products($param)
    {
         
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('a.recommend', $param['recommend']);
        endif;

        if(@$param['sale'] !=''):
            $this->db->where('a.sale', $param['sale']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(!empty($param['search'])):
            $this->db->like('a.title', $param['search']);
        endif;

        if(!empty($param['recommend_arr'])):
            $this->db->where_in('a.recommend', $param['recommend_arr']);
        endif;

        if(!empty($param['sale_arr'])):
            $this->db->where_in('a.sale', $param['sale_arr']);
        endif;

        if(isset($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('a.*');
        $this->db->order_by('a.updated_at', 'DESC');
        $query = $this->db->get('products a');
        return $query;
    }

    public function get_products_fiter($param)
    {
        
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('a.recommend', $param['recommend']);
        endif;

        if(@$param['sale'] !=''):
            $this->db->where('a.sale', $param['sale']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(!empty($param['categorie_id'])):
            $this->db->where('b.categorie_id', $param['categorie_id']);
        endif;

        if(!empty($param['attributes_option'])):
            $this->db->like('c.attributes_option', $param['attributes_option']);
        endif;

        if(!empty($param['search'])):
            $this->db->like('a.title', $param['search']);
        endif;
        

        if(isset($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
        
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('*');
        $this->db->join('product_categorie_map b', 'a.product_id = b.product_id','LEFT');
        $this->db->join('stocks c', 'a.product_id = c.product_id','LEFT');
        $this->db->order_by('a.created_at', $param['order_by']);
        $this->db->group_by('a.product_id');
        $query = $this->db->get('products a');
        return $query;
    }

    public function get_categories_products_fiter($param)
    {
        
        if(!empty($param['product_id'])):
            $this->db->where('d.product_id', $param['product_id']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('d.recommend', $param['recommend']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('d.recommend', $param['recommend']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(!empty($param['categorie_id'])):
            $this->db->where('b.categorie_id', $param['categorie_id']);
        endif;

        if(!empty($param['attributes_option'])):
            $this->db->where('c.attributes_option', $param['attributes_option']);
        endif;

        if(!empty($param['search'])):
            $this->db->like('a.title', $param['search']);
        endif;
        

        if(isset($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
        
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('*');
        $this->db->join('product_categorie_map b', 'a.categorie_id = b.categorie_id','LEFT');
        $this->db->join('products d', 'd.product_id = b.product_id','LEFT');
        $this->db->join('stocks c', 'd.product_id = c.product_id','LEFT');
        
        $this->db->order_by('a.created_at', $param['order_by']);
        $this->db->group_by('d.product_id');
        $query = $this->db->get('categories a');
        return $query;
    }

    public function update($id, $value)
    {
        $query = $this->db
                        ->where('product_id', $id)
                        ->update('products', $value);
        return $query;
    }

    public function count_products($param) 
    {
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('a.recommend', $param['recommend']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(!empty($param['search'])):
            $this->db->like('a.title', $param['search']);
        endif;

        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        return $this->db->count_all_results('products a');
    }

    public function count_categories_products_fiter($param)
    {
        
        if(!empty($param['product_id'])):
            $this->db->where('d.product_id', $param['product_id']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('d.recommend', $param['recommend']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('d.recommend', $param['recommend']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(!empty($param['categorie_id'])):
            $this->db->where('b.categorie_id', $param['categorie_id']);
        endif;

        if(!empty($param['attributes_option'])):
            $this->db->where('c.attributes_option', $param['attributes_option']);
        endif;

        if(!empty($param['search'])):
            $this->db->like('a.title', $param['search']);
        endif;
        
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('*');
        $this->db->join('product_categorie_map b', 'a.categorie_id = b.categorie_id','LEFT');
        $this->db->join('products d', 'd.product_id = b.product_id','LEFT');
        $this->db->join('stocks c', 'd.product_id = c.product_id','LEFT');

        return $this->db->count_all_results('categories a');;
    }
   
    public function get_product_img_map($param)
    {
        
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        $this->db->select('a.*');
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db->get('stocks a');
        return $query;
    }

    public function get_categories_products_query_fiter($param)
    {
        
        $query_shcm = '';

        if(!empty($param['categorie_id'])):
            $query_shcm.= " AND c.categorie_id IN (".$param['categorie_id'].") ";
        endif;

        if(!empty($param['slug'])):
            $query_shcm.= " AND c.slug = '".$param['slug']."' ";
        endif;

        if(!empty($param['search'])):
            $search_arr = explode(' ', $param['search']);
            if(!empty($search_arr)):
                $in_ser = 0;
                foreach($search_arr as $search):
                    if(!empty($search)): 
                        if($in_ser == 0):
                            $query_shcm.= " AND (a.title LIKE '%".$search."%' OR a.excerpt LIKE '%".$search."%' OR a.detail LIKE '%".$search."%' ) ";  
                        else:
                            $query_shcm.= " OR (a.title LIKE '%".$search."%' OR a.excerpt LIKE '%".$search."%' OR a.detail LIKE '%".$search."%' ) ";  
                        endif; 
                        $in_ser++;
                    endif;
                endforeach; 
            endif;
            //$query_shcm.= " AND (a.title LIKE '%".$param['search']."%' OR a.excerpt LIKE '%".$param['search']."%' OR a.detail LIKE '%".$param['search']."%' ) ";
        endif;

        $query_order_shcm = '';
        if(!empty($param['p'])):
            switch($param['p']):
                case 'สินค้าขายดี':
                    $query_shcm.= " AND a.sale = '1' ";
                break;
                case 'สินค้าอื่นๆ':
                    $query_shcm.= " AND a.recommend = '1' ";
                break; 
                case 'สินค้าใหม่':
                    $query_shcm.= " AND a.recommend = '0' AND a.sale = '0' ";
                    $query_order_shcm.= " ,a.created_at ";
                break;

            endswitch;
        endif;

        if(!empty($param['attribute'])):
            foreach($param['attribute'] as $key => $item):
                if($key == 0): 
                    $query_shcm.= " AND (FIND_IN_SET(".$item->product_attribute_id.",replace(replace(attributes_option, ', ', ','),' ,',',')) ";
                else:
                     $query_shcm.= " OR FIND_IN_SET(".$item->product_attribute_id.",replace(replace(attributes_option, ', ', ','),' ,',',')) ";
                endif;
            endforeach;
            $query_shcm.=' ) ';
        endif;

        if(!empty($param['type_attribute'])):
            foreach($param['type_attribute'] as $keys => $items):
                if($keys == 0): 
                    $query_shcm.= " AND (FIND_IN_SET(".$items->product_attribute_id.",replace(replace(attributes_group, ', ', ','),' ,',',')) ";
                else:
                     $query_shcm.= " OR FIND_IN_SET(".$items->product_attribute_id.",replace(replace(attributes_group, ', ', ','),' ,',',')) ";
                endif;
            endforeach;
            $query_shcm.=' ) ';
        endif;

        $query = $this->db->query("SELECT
        a.title,
        a.slug as pro_slug,
        a.order_qty,
        a.view_qty,
        a.file,
        a.product_id,
        c.slug,
        b.categorie_id,
        d.attributes_option,
        d.attributes_group,
        d.product_price,
        d.product_price_true,
        d.stock_qty
        FROM
        products AS a
        INNER JOIN product_categorie_map AS b ON a.product_id = b.product_id
        INNER JOIN categories AS c ON b.categorie_id = c.categorie_id
        INNER JOIN stocks AS d ON a.product_id = d.product_id
        WHERE
        a.active = 1 AND
        a.recycle = 0
        $query_shcm
        GROUP BY a.product_id
        ORDER BY a.updated_at ".$query_order_shcm." ".$param['order_by']
        ." limit ".$param['start']." ,".$param['length']." ");  
        return $query;
    }

    public function count_categories_products_query_fiter($param)
    {
        
        $query_shcm = '';

        if(!empty($param['categorie_id'])):
            $query_shcm.= " AND c.categorie_id IN (".$param['categorie_id'].") ";
        endif;
        
        if(!empty($param['slug'])):
            $query_shcm.= " AND c.slug = '".$param['slug']."' ";
        endif;

        if(!empty($param['search'])):
            $search_arr = explode(' ', $param['search']);
            if(!empty($search_arr)):
                $in_ser = 0;
                foreach($search_arr as $search):
                    if(!empty($search)): 
                        if($in_ser == 0):
                            $query_shcm.= " AND (a.title LIKE '%".$search."%' OR a.excerpt LIKE '%".$search."%' OR a.detail LIKE '%".$search."%' ) ";  
                        else:
                            $query_shcm.= " OR (a.title LIKE '%".$search."%' OR a.excerpt LIKE '%".$search."%' OR a.detail LIKE '%".$search."%' ) ";  
                        endif; 
                        $in_ser++;
                    endif;
                endforeach; 
            endif;
            //$query_shcm.= " AND (a.title LIKE '%".$param['search']."%' OR a.excerpt LIKE '%".$param['search']."%' OR a.detail LIKE '%".$param['search']."%' ) ";
        endif;
        

        if(!empty($param['attribute'])):
            foreach($param['attribute'] as $key => $item):
                if($key == 0): 
                    $query_shcm.= " AND (FIND_IN_SET(".$item->product_attribute_id.",replace(replace(attributes_option, ', ', ','),' ,',',')) ";
                else:
                     $query_shcm.= " OR FIND_IN_SET(".$item->product_attribute_id.",replace(replace(attributes_option, ', ', ','),' ,',',')) ";
                endif;
            endforeach;
            $query_shcm.=' ) ';
        endif;

        if(!empty($param['type_attribute'])):
            foreach($param['type_attribute'] as $keys => $items):
                if($keys == 0): 
                    $query_shcm.= " AND (FIND_IN_SET(".$items->product_attribute_id.",replace(replace(attributes_group, ', ', ','),' ,',',')) ";
                else:
                     $query_shcm.= " OR FIND_IN_SET(".$items->product_attribute_id.",replace(replace(attributes_group, ', ', ','),' ,',',')) ";
                endif;
            endforeach;
            $query_shcm.=' ) ';
        endif;

        $query_order_shcm = '';
        if(!empty($param['p'])):
            switch($param['p']):
                case 'สินค้าขายดี':
                    $query_shcm.= " AND a.sale = '1' ";
                break;
                case 'สินค้าอื่นๆ':
                    $query_shcm.= " AND a.recommend = '1' ";
                break; 
                case 'สินค้าใหม่':
                    $query_shcm.= " AND a.recommend = '0' AND a.sale = '0' ";
                    $query_order_shcm.= " ,a.created_at ";
                break;

                default:
                    $query_order_shcm.= "a.updated_at ";
                break;

            endswitch;
        endif;

        $query = $this->db->query("SELECT
        a.title,
        a.slug as pro_slug,
        a.order_qty,
        a.view_qty,
        a.file,
        a.product_id,
        a.slug,
        c.slug,
        b.categorie_id,
        d.attributes_option,
        d.attributes_group,
        d.product_price,
        d.product_price_true,
        d.stock_qty
        FROM
        products AS a
        INNER JOIN product_categorie_map AS b ON a.product_id = b.product_id
        INNER JOIN categories AS c ON b.categorie_id = c.categorie_id
        INNER JOIN stocks AS d ON a.product_id = d.product_id
        WHERE
        a.active = 1 AND
        a.recycle = 0
        $query_shcm
        GROUP BY a.product_id
        ORDER BY a.product_id, d.product_price ".$query_order_shcm." ".$param['order_by']."");
        return $query->num_rows();
    }
}
