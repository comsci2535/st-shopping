<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MX_Controller {

    private $perPage = 20;
    function __construct() 
    {
        parent::__construct();
        $this->load->model('seos/seos_m');
        $this->load->model('products/products_m');
        $this->load->model('stocks/stocks_m');
        $this->load->model('categories/categories_m');
        $this->load->model('reviews/reviews_m');
        $this->load->model('orders/orders_m');
        $this->load->model('product_attributes/product_attributes_m');
        
    }

    private function seo($obj_seo = null)
    { 
        $title          = !empty($obj_seo->meta_title)? config_item('siteTitle').' | '.$obj_seo->meta_title : config_item('siteTitle').' | ซื้อของออนไลน์';
        $robots         = !empty($obj_seo->meta_title)? $obj_seo->meta_title : config_item('siteTitle');
        $description    = !empty($obj_seo->meta_description)? $obj_seo->meta_description : config_item('metaDescription');
        $keywords       = !empty($obj_seo->meta_keyword)? $obj_seo->meta_keyword : config_item('metaKeyword');
        $img            = !empty($obj_seo->imgone->file_path)? site_url($obj_seo->imgone->file_path) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {

        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'products',
            'footer'  => 'footer',
            'function'=>  array('products'),
        );
        
        $groug = $this->input->get('groug');
        $param = array();
        if(!empty($groug)){ 
            $groug_arr = '';
            $infoCategories      =  $this->categories_m->get_categorie_by_group_slug($groug)->result();
            if(!empty($infoCategories)){
                foreach($infoCategories as $key => $grougs):
                    if($key == 0){
                        $groug_arr.= $grougs->categorie_id;
                    }else{
                        $groug_arr.= ','.$grougs->categorie_id;
                    }

                endforeach;
            }
            
            $param['categorie_id']     = $groug_arr;
        }
        
        $attribute          = $this->input->get('attribute');
        $type_attribute     = $this->input->get('type');

        $input_type_slug['slug'] = explode(',',$this->input->get('type'));
        $info_attribute_type     =  $this->product_attributes_m->get_product_attributes_by_id($input_type_slug)->result();

        if(!empty($info_attribute_type)):
            $product_attribute_arr = array();
            foreach($info_attribute_type as $groups):
                array_push($product_attribute_arr, $groups->product_attribute_id);
            endforeach;
            $input_slug['parent_id'] = $product_attribute_arr;
        endif;

        $input_slug['slug']      = explode(',',$this->input->get('attribute'));
        $info_attribute          =  $this->product_attributes_m->get_product_attributes_by_id($input_slug)->result(); 
        // arr($input_slug);exit();
        $param['length']         = 20;
        $param['start']          = 0;
        $param['order_by']       = 'DESC';
        $param['slug']           = $this->input->get('cat');
        $param['attribute']      = $info_attribute; 
        $param['type_attribute'] = $info_attribute_type;
        $param['search']         = $this->input->get('search');
        
        $param['p']              = $this->input->get('p');

        $data['attribute']       = $attribute;
        $data['type_attribute']  = $type_attribute;
        $data['cat']             = $this->input->get('cat');
        $data['p']               = $this->input->get('p');
        $data['groug']           = $this->input->get('groug');
        $data['search']          = $this->input->get('search');
        $data['num_row']         = $this->products_m->count_categories_products_query_fiter($param);
        $data['per_page']        = $this->perPage;
        $info                    = $this->products_m->get_categories_products_query_fiter($param)->result();

        // arr($this->db->last_query());
        if(!empty($info)):
            foreach($info as $item):
                $item->slug             = $item->pro_slug;
                $inputs['product_id']   = $item->product_id;
                $infoStocks             = $this->stocks_m->get_stock_by_id($inputs)->row();
                $attributes_option      = 0;
                if(!empty($infoStocks->attributes_option)):
                    $attributes_option = 1;
                endif;
                $item->price            = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                $item->price_true       = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                $item->persent          = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                $item->count            = $attributes_option;

                $infoOrders    = $this->orders_m->get_orders_product_by_id($item->product_id)->row();
                $quantity      = 0;
                if(!empty($infoOrders)):
                    $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
                endif;
                $item->order_qty = $item->order_qty + $quantity;
                
            endforeach;
        endif;
        $data['info']       = $info;

        $this->load->view('template/body', $data);
    }

    public function categories($slug = '')
    {

        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'products',
            'footer'  => 'footer',
            'function'=>  array('products'),
        );

        $param = array();
        $param['length']    = 20;
        $param['start']     = 0;
        $param['order_by']  = 'DESC';
        $param['slug']      = $slug;
        $data['num_row']    = $this->products_m->count_categories_products_fiter($param);
        $data['per_page']   = $this->perPage;
        $info               = $this->products_m->get_categories_products_fiter($param)->result();

        if(!empty($info)):
            foreach($info as $item):
                $inputs['product_id']   = $item->product_id;
                $infoStocks             = $this->stocks_m->get_stock_by_id($inputs)->row();
                $attributes_option      = 0;
                if(!empty($infoStocks->attributes_option)):
                    $attributes_option = 1;
                endif;
                $item->price            = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                $item->price_true       = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                $item->persent          = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                $item->count            = $attributes_option;
            endforeach;
        endif;
        $data['info']       = $info;


        $this->load->view('template/body', $data);
    }

    public function ajax_product()
    {
        $param  = array();
        $result = '';
        $param = array();
        if(!empty($this->input->get("page"))){

            if($this->input->get("page") == 1){
                $param['length']    = 20;
                $param['start']     = 0;
            }else{
                //$param['start']     = ceil($this->input->get("page") * $this->perPage);
                $param['start']     = ceil(($this->input->get("page") - 1) * $this->perPage);
                $param['length']    = $this->perPage;
    
            }
            
            $groug = $this->input->get('groug');
           
            if(!empty($groug)){ 
                $groug_arr = '';
                $infoCategories      =  $this->categories_m->get_categorie_by_group_slug($groug)->result();

                if(!empty($infoCategories)){
                    foreach($infoCategories as $key => $grougs):
                        if($key == 0){
                            $groug_arr.= $grougs->categorie_id;
                        }else{
                            $groug_arr.= ','.$grougs->categorie_id;
                        }
                        
                    endforeach;
                }
                
                $param['categorie_id']     = $groug_arr;
            }

            $param['order_by']  = $this->input->get("order_by");
            
            $attribute          = $this->input->get('attribute');
            $type_attribute     = $this->input->get('type');

            $input_type_slug['slug'] = explode(',',$this->input->get('type'));
            $info_attribute_type     =  $this->product_attributes_m->get_product_attributes_by_id($input_type_slug)->result();

            if(!empty($info_attribute_type)):
                $product_attribute_arr = array();
                foreach($info_attribute_type as $groups):
                    array_push($product_attribute_arr, $groups->product_attribute_id);
                endforeach;
                $input_slug['parent_id'] = $product_attribute_arr;
            endif;

            
            $input_slug['slug'] = explode(',',$this->input->get('attribute'));
            $info_attribute =  $this->product_attributes_m->get_product_attributes_by_id($input_slug)->result();

            $param['slug']      = $this->input->get('cat');
            $param['attribute'] = $info_attribute;
            $param['search']    = $this->input->get('search');
            $param['p']         = $this->input->get('p');
            $data['attribute']  = $attribute;
            $data['type_attribute']  = $type_attribute;
            $data['cat']        = $this->input->get('cat');
            $data['search']     = $this->input->get('search');

            $info               = $this->products_m->get_categories_products_query_fiter($param)->result();
            
            // arr($this->db->last_query());
            if(!empty($info)):
                foreach($info as $item):
                    $item->slug             = $item->pro_slug;
                    $inputs['product_id']   = $item->product_id;
                    $infoStocks             = $this->stocks_m->get_stock_by_id($inputs)->row();
                    $attributes_option      = 0;
                    if(!empty($infoStocks->attributes_option)):
                        $attributes_option = 1;
                    endif;
                    $item->price            = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                    $item->price_true       = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                    $item->persent          = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                    $item->count            = $attributes_option;

                    $infoOrders    = $this->orders_m->get_orders_product_by_id($item->product_id)->row();
                    $quantity      = 0;
                    if(!empty($infoOrders)):
                        $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
                    endif;
                    $item->order_qty = $item->order_qty + $quantity;
                
                endforeach;
            endif;
            $datas['info']   = $info;
            $result             = $this->load->view('products_data', $datas);
        }

        json_encode($result);
    }

    public function ajax_prodcut_onstock()
    {
        $input                  = $this->input->post();
        $param['product_id']    = $input['product_id'];

        if(!empty($input['option'])):
            $attributes_option = '';
            foreach($input['option'] as $key => $option):
                if($key > 0):
                    $attributes_option.= ",".$option;
                else:
                    $attributes_option.= $option;
                endif;
            endforeach;

            $param['attributes_option'] = $attributes_option;

        endif;

        $qty                = !empty($input['qty']) ? $input['qty'] : 0;
        $info               = $this->stocks_m->get_stock_by_id($param)->row();
        $stock_qty          = !empty($info->stock_qty) ? (int)$info->stock_qty : 0;
        if($stock_qty > 0):
            $stock_qty = $stock_qty - (int)$qty;
        else:
            $stock_qty = 0;
        endif;


        $data['is_limit']   = !empty($info->is_limit) ? (int)$info->is_limit : 0;
        $data['stock_qty']  = ($stock_qty < 0 ) ? 0 : ($stock_qty >= 0) ? 1 : 0;
        $data['stock_id']   = !empty($info->stock_id) ? $info->stock_id : '';
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function ajax_detail()
    {
        $input              = $this->input->post();
        $info               = $this->products_m->get_products($input)->row();
        if(!empty($info)):
            $inputs['product_id']       = $info->product_id;
            $infoStocks                 = $this->stocks_m->get_stock_by_id($inputs)->row();
            $info->price                = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
            $info->price_true           = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
            $info->persent              = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
            $info->img                  = $this->products_m->get_product_img_map($inputs)->result();
            $info->attributes_option    = $this->set_attributes($this->stocks_m->get_stock_join_attributes_option($inputs));

            $infoOrders    = $this->orders_m->get_orders_product_by_id($info->product_id)->row();
            $quantity      = 0;
            if(!empty($infoOrders)):
                $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
            endif;
            $info->order_qty = $info->order_qty + $quantity;

        endif;
        
        $data['info']   = $info;
        $result         = $this->load->view('products_data_detail', $data);

        json_encode($result);
    }

    public function set_attributes($attributes)
    {
        $value = array();
        if(!empty($attributes)):
            foreach($attributes as $item):
                if(!empty($item['option'])):
                    foreach($item['option'] as $option):
                        $value['option'][$option['parent_id']][$option['product_attribute_id']] = array(
                            'title'                 => $option['title'],
                            'product_attribute_id'  => $option['product_attribute_id']
                        );
                    endforeach;
                endif;

                if(!empty($item['group'])):
                    foreach($item['group'] as $group):
                        $value['group'][$group['product_attribute_id']] = array(
                            'title'                 => $group['title'],
                            'product_attribute_id'  => $group['product_attribute_id']
                        );
                    endforeach;
                endif;

            endforeach;
        endif;
        return $value;
    }

    public function ajax_detail_option()
    {
        $input              = $this->input->post();
        $info               = $this->products_m->get_products($input)->row();
        if(!empty($info)):
            $inputs['product_id']       = $info->product_id;
            $infoStocks                 = $this->stocks_m->get_stock_by_id($inputs)->row();
            $info->price                = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
            $info->price_true           = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
            $info->persent              = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
            $info->img                  = $this->products_m->get_product_img_map($inputs)->result();
            $info->attributes_option    = $this->set_attributes($this->stocks_m->get_stock_join_attributes_option($inputs));
            $infoOrders    = $this->orders_m->get_orders_product_by_id($info->product_id)->row();
            $quantity      = 0;
            if(!empty($infoOrders)):
                $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
            endif;
            $info->order_qty = $info->order_qty + $quantity;
        endif;
        $data['info']   = $info;
        $result         = $this->load->view('products_data_option', $data);

        json_encode($result);
    }

    public function detail($slug = '')
    { 
        $input['slug']    = $slug;

        $info               = $this->products_m->get_products($input)->row();
    
        if(!empty($info)):
            $inputs['product_id']       = $info->product_id;
            $infoStocks                 = $this->stocks_m->get_stock_by_id($inputs)->row();
            $info->price                = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
            $info->price_true           = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
            $info->persent              = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
            $info->img                  = $this->products_m->get_product_img_map($inputs)->result();
            $info->imgone               = $this->products_m->get_product_img_map($inputs)->row();
            $info->attributes_option    = $this->set_attributes($this->stocks_m->get_stock_join_attributes_option($inputs));

            $infoOrders    = $this->orders_m->get_orders_product_by_id($info->product_id)->row();
            $quantity      = 0;
            if(!empty($infoOrders)):
                $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
            endif;
            $info->order_qty = $info->order_qty + $quantity;

        endif;
        
        $inputs['product_id'] =  $info->product_id;

        $categories = $this->categories_m->get_categories_by_id($inputs)->row();
        if(!empty($categories)):
            $input_['categorie_id'] =  $categories->categorie_id;
            $input_['length']       = 6;
            $input_['start']        = 0;
            $infoCategories = $this->categories_m->get_categories_join_products($input_)->result();
            foreach($infoCategories as $item):
                $inputs_['product_id']      = $item->product_id;
                $infoStocks                 = $this->stocks_m->get_stock_by_id($inputs_)->row();
                $item->price                = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                $item->price_true           = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                $item->persent              = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                $attributes_option      = 0;
                if(!empty($infoStocks->attributes_option)):
                    $attributes_option = 1;
                endif;
                $item->count            = $attributes_option;

                $infoOrders    = $this->orders_m->get_orders_product_by_id($info->product_id)->row();
                $quantity      = 0;
                if(!empty($infoOrders)):
                    $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
                endif;
                $item->order_qty = $item->order_qty + $quantity;

            endforeach;
        endif;

        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo($info),
            'header'  => 'header_product',
            'content' => 'products_detail',
            'footer'  => 'footer',
            'function'=>  array('products'),
        );

        $value['view_qty']  = $info->view_qty + 1;
        $this->products_m->update($info->product_id, $value);
        $input_review['product_id'] = $info->product_id;
        $data['reviews_count']      = $this->reviews_m->count_reviews($input_review);
        $data['info']               = $info;
        $data['proCate']            = $infoCategories;

        $this->load->view('template/body', $data);
    }

    public function products_recommend()
    {
        $input['recommend'] = 1;
        $input['start']     = 0;
        $input['length']    = 5;

        $info               = $this->products_m->get_products($input)->result();
        if(!empty($info)):
            foreach($info as $item):
                $inputs['product_id']   = $item->product_id;
                $infoStocks             = $this->stocks_m->get_stock_by_id($inputs)->row();
                $item->price            = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                $item->price_true       = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                $item->persent          = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                $attributes_option      = 0;
                if(!empty($infoStocks->attributes_option)):
                    $attributes_option = 1;
                endif;
                $item->count            = $attributes_option;
                $infoOrders    = $this->orders_m->get_orders_product_by_id($item->product_id)->row();
                $quantity      = 0;
                if(!empty($infoOrders)):
                    $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
                endif;
                $item->order_qty = $item->order_qty + $quantity;
            endforeach;
        endif;
        $data['products']   = $info;
        $this->load->view('products_recommend', $data);
    }

    public function products_sale()
    {
        $input['sale']      = 1;
        $input['start']     = 0;
        $input['length']    = 5;

        $info               = $this->products_m->get_products($input)->result();
        if(!empty($info)):
            foreach($info as $item):
                $inputs['product_id']   = $item->product_id;
                $infoStocks             = $this->stocks_m->get_stock_by_id($inputs)->row();
                $item->price            = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                $item->price_true       = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                $item->persent          = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                $attributes_option      = 0;
                if(!empty($infoStocks->attributes_option)):
                    $attributes_option = 1;
                endif;
                $item->count            = $attributes_option;
                $infoOrders    = $this->orders_m->get_orders_product_by_id($item->product_id)->row();
                $quantity      = 0;
                if(!empty($infoOrders)):
                    $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
                endif;
                $item->order_qty = $item->order_qty + $quantity;
            endforeach;
        endif;
        $data['products']   = $info;
        $this->load->view('products_sale', $data);
    }

    public function products_home()
    {
        $input['recommend_arr'] = [0];
        $input['sale_arr']      = [0];
        $input['start']     = 0;
        $input['length']    = 10; 
        $info               = $this->products_m->get_products($input)->result(); 
        if(!empty($info)):
            foreach($info as $item):
                $inputs['product_id']   = $item->product_id;
                $infoStocks             = $this->stocks_m->get_stock_by_id($inputs)->row();
                $item->price            = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                $item->price_true       = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                $item->persent          = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                $attributes_option      = 0;
                if(!empty($infoStocks->attributes_option)):
                    $attributes_option = 1;
                endif;
                $item->count            = $attributes_option;
                $infoOrders    = $this->orders_m->get_orders_product_by_id($item->product_id)->row();
                $quantity      = 0;
                if(!empty($infoOrders)):
                    $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
                endif;
                $item->order_qty = $item->order_qty + $quantity;
            endforeach;
        endif;
        $data['products']   = $info;
        $this->load->view('products_home', $data);
    }

    public function products_top()
    {
        $input['start']     = 0;
        $input['length']    = 3;
        $input['recommend'] = 1;
        $info               = $this->products_m->get_products($input)->result();
        if(!empty($info)):
            foreach($info as $item):
                $inputs['product_id']    = $item->product_id;
                $infoStocks             = $this->stocks_m->get_stock_by_id($inputs)->row();
                $item->price            = !empty($infoStocks->product_price) ? number_format($infoStocks->product_price, 2) : 0;
                $item->price_true       = !empty($infoStocks->product_price_true) ? number_format($infoStocks->product_price_true, 2) : 0;
                $item->persent          = (100 - (($infoStocks->product_price / $infoStocks->product_price_true)) * 100);
                $attributes_option      = 0;
                if(!empty($infoStocks->attributes_option)):
                    $attributes_option = 1;
                endif;
                $item->count            = $attributes_option;
            endforeach;
        endif;
        $data['products']   = $info;
        $this->load->view('products_top', $data);
    }

    public function ajax_chestock()
    {
        
        $input                  = $this->input->post();
        
        if(!empty($input['product_id'])){
            $param['product_id']    = $input['product_id'];
            if(!empty($input['option'])):
                $attributes_option = '';
                foreach($input['option'] as $key => $option):
                    if($key > 0):
                        $attributes_option.= ",".$option;
                    else:
                        $attributes_option.= $option;
                    endif;
                endforeach;
    
                $param['attributes_option'] = $attributes_option;
    
            endif;
     
            $info               = $this->stocks_m->get_stock_by_id($param)->row();
            $stock_qty          = !empty($info->stock_qty) ? (int)$info->stock_qty : 0;  
    
            $data['is_limit']   = !empty($info->is_limit) ? (int)$info->is_limit : 0;
            $data['stock_qty']  = $stock_qty; 
        }else{
            $data['status'] = 0; 
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data)); 
        
    }
}
