<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products_deliverys_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_deliverys_product_by_id($product_id = '') 
    {
        
        if(!empty($product_id)):
            $this->db->where('a.product_id', $product_id);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('products_deliverys a')
                        ->get();
        return $query;
    }

    public function get_deliverys_by_id($products_delivery_id = '') 
    {
        
        if(!empty($$products_delivery_id)):
            $this->db->where('a.products_delivery_id', $products_delivery_id);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('products_deliverys a')
                        ->get();
        return $query;
    }

    public function get_deliverys_product_by_id_min_man_qty($product_id = '',$qty=0) 
    {
        
        if(!empty($product_id)):
            $this->db->where('a.product_id', $product_id);
        endif;
        $this->db->group_start();
           $this->db->where('a.start_qty <=', $qty); 
           $this->db->where('a.end_qty >=', $qty); 
        $this->db->group_end();

        $this->db->order_by('a.end_qty','DESC');
        $this->db->limit(1);

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('products_deliverys a')
                        ->get();    
        return $query;
    }

     public function get_deliverys_product_by_id_equal_qty($product_id = '',$qty=0) 
    {
        
        if(!empty($product_id)):
            $this->db->where('a.product_id', $product_id);
        endif;
        $this->db->group_start();
           $this->db->where('a.start_qty', $qty); 
           $this->db->where('a.end_qty', $qty); 
        $this->db->group_end();

        $this->db->order_by('a.end_qty','DESC');
        $this->db->limit(1);

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('products_deliverys a')
                        ->get();
                
        return $query;
    }

    
   
}
