<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Carts_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_products($param)
    {
        
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('a.recommend', $param['recommend']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(isset($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('a.*');
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db->get('products a');
        return $query;
    }

    public function count_products($param) 
    {
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if(@$param['recommend'] !=''):
            $this->db->where('a.recommend', $param['recommend']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        if(!empty($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;

        $this->db->where('a.recycle', 0);
        $this->db->where('a.active', 1);
        return $this->db->count_all_results('products a');
    }
   
    public function get_product_img_map($param)
    {  
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        $this->db->select('a.*');
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db->get('product_img_map a');
        return $query;
    }

    public function get_reserves_join_by_id($param)
    {  
        if(!empty($param['reserve_id'])):
            $this->db->where('a.reserve_id', $param['reserve_id']);
        endif;
        if(!empty($param['reserve_code'])):
            $this->db->where('a.reserve_code', $param['reserve_code']);
        endif;

        $this->db->select('a.*, b.*');
        $this->db->join('reserves_detail b', 'a.reserve_id = b.reserve_id');
        $query = $this->db->get('reserves a');
        return $query;
    }

    public function get_reserves_by_id($param)
    {  
        if(!empty($param['reserve_id'])):
            $this->db->where('a.reserve_id', $param['reserve_id']);
        endif;
        if(!empty($param['reserve_code'])):
            $this->db->where('a.reserve_code', $param['reserve_code']);
        endif;

        $this->db->select('a.*');
        $query = $this->db->get('reserves a');
        return $query;
    }

    public function insert($value) {
        $this->db->insert('reserves', $value);
        return $this->db->insert_id();
    }

    public function insert_reserves_detail($value) {
        return $this->db->insert_batch('reserves_detail', $value);
    }

    public function update_stocks($product_id, $stock_id, $value)
    {
        $query = $this->db
                        ->where('stock_id', $stock_id)
                        ->where('product_id', $product_id)
                        ->update('stocks', $value);
        return $query;
    }

    public function update_stocks_ref($product_id, $attributes_option, $value)
    {
        $query = $this->db
                        ->where('attributes_option', $attributes_option)
                        ->where('product_id', $product_id)
                        ->update('stocks', $value);
        return $query;
    }

    public function delete_reserves($id) 
    {
        $this->db->where('reserve_code', $id);
        return $this->db->delete('reserves');
    }

    public function delete_reserves_detail($id) 
    {
        $this->db->where('reserve_id', $id);
        return $this->db->delete('reserves_detail');
    }

    public function delete_reserves_detail_line($id, $reserve_detail_id) 
    {
        $this->db->where('reserve_id', $id);
        $this->db->where('reserve_detail_id', $reserve_detail_id);
        return $this->db->delete('reserves_detail');
    }

}
