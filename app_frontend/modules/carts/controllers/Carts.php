<?php

use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;

defined('BASEPATH') OR exit('No direct script access allowed'); 

require APPPATH.'third_party/vendor/autoload.php';

require APPPATH.'libraries/barcode/BarcodeGenerator.php'; 
require APPPATH.'libraries/barcode/BarcodeGeneratorHTML.php';
require APPPATH.'libraries/barcode/BarcodeGeneratorPNG.php'; 

class Carts extends MX_Controller {

    function __construct() 
    {
		parent::__construct();
        $this->load->model('seos/seos_m');
        $this->load->model('products/products_m');
        $this->load->model('stocks/stocks_m');
        $this->load->model('categories/categories_m');
        $this->load->model('carts_m');
        $this->load->model('info/info_m');
        $this->load->model('transports/transports_m'); 
        $this->load->model('product_attributes/product_attributes_m');
        $this->load->model('banks/banks_m');
        $this->load->library('uploadfile_library');
        // $this->load->library('m_pdf');
        $this->load->model('orders/orders_m');
        $this->load->model('products_deliverys/products_deliverys_m');
        $this->load->model('config/config_m');
        //$this->session->unset_userdata('reserve_data');
    }
    
    private function seo()
    {

        $obj_seo = $this->seos_m->get_seos_by_display_page('home')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | ซื้อของออนไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {
		
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'carts',
            'footer'  => 'footer',
            'function'=>  array('carts'),
        );

        $cart_data = $this->session->userdata('cart_data');
        $data_arr = array();
        if(!empty($cart_data)):
            foreach($cart_data as $item):
                $input['product_id'] = $item['product_id'];
                $info                = $this->products_m->get_products($input)->row();
                if(!empty($info)):
                    $inputs['product_id']               = $info->product_id;
                    $inputs['stock_id']                 = $item['key'];
                    $infoStocks                         = $this->stocks_m->get_stock_by_id($inputs)->row();
                    $info->qty                          = !empty($item['qty']) ? $item['qty'] : 0;
                    $info->price                        = !empty($infoStocks->product_price) ? $infoStocks->product_price : 0;
                    $inputs_['product_attribute_id']    = $item['option'];
                    $info->attributes_option            = $this->product_attributes_m->get_product_attributes_by_id($inputs_)->result();
                    
                    if(!empty($item['option'])):
                        $attributes_option = '';
                        foreach($item['option'] as $key => $option):
                            if($key > 0):
                                $attributes_option.= ",".$option;
                            else:
                                $attributes_option.= $option;
                            endif;
                        endforeach;
            
                        $param['attributes_option'] = $attributes_option;
            
                    endif;
                    
                    $param['product_id']                = $info->product_id;
                    $param['stock_id']                  = $item['key'];
                    $infostockss                        = $this->stocks_m->get_stock_by_id($param)->row();
                    $info->stock_qty                    = !empty($infostockss->stock_qty) ? (int)$infostockss->stock_qty : 0;
                    $info->is_limit                     = !empty($infostockss->is_limit) ? (int)$infostockss->is_limit : 0;
                    

                endif;
                
                $data_arr[] = $info;
                
            endforeach;
        endif;
        $data['info'] = $data_arr;

        $this->load->view('template/body', $data);
    }

    public function checkout()
    {
		
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'checkout',
            'footer'  => 'footer',
            'function'=>  array('carts'),
        );
        // $this->session->sess_destroy();
        // $this->session->unset_userdata('reserve_data');
        // exit();
        $reserve_data  = $this->session->userdata('reserve_data');
        
        $num_str       = '';
        if(empty($reserve_data)):
            $num_str                    = sprintf("%06d", mt_rand(1, 999999));
            
            $value_arr['reserve_code']  =  $num_str;
            $value_arr['created_at']    = db_datetime_now();
            $value_arr['created_by']    = 0;
            $value_arr['updated_at']    = db_datetime_now();
            $value_arr['updated_by']    = 0;
            $id                         = $this->carts_m->insert($value_arr);
            $this->session->set_userdata('reserve_data', $num_str);
        else:

            $param['reserve_code']  = $reserve_data;
            
            $reserve        = $this->carts_m->get_reserves_by_id($param)->row();
            $infoReserve    = $this->carts_m->get_reserves_join_by_id($param)->result();
            if(!empty($infoReserve)):
                foreach($infoReserve as $stock):

                    $inputsStock['product_id']          = $stock->product_id;
                    $inputsStock['attributes_option']   = $stock->attributes_option;
                    $infoStocksRef                      = $this->stocks_m->get_stock_by_id($inputsStock)->row();
                    $stockQty       = (!empty($infoStocksRef->stock_qty) ? $infoStocksRef->stock_qty : 0) + (!empty($stock->quantity) ? $stock->quantity : 0);
                    $values_stocks  = array(
                        'stock_qty' => $stockQty
                    );
                    
                    $this->carts_m->update_stocks_ref($stock->product_id, $stock->attributes_option, $values_stocks);
                    $this->carts_m->delete_reserves_detail_line($stock->reserve_id, $stock->reserve_detail_id);
                endforeach;
            endif;
            $id = $reserve->reserve_id;
        endif;

        $cart_data      = $this->session->userdata('cart_data');

        if(!empty($cart_data)){
            if (count($cart_data) == 0) {
                redirect_back();
            }
        }
        
        $data_arr = array();
        if(!empty($cart_data)):
            $values_arr = array();
          
            //deliverys
             $deliverys =[];
             $qty = 0;
             foreach($cart_data as $item):
                $qty += $item['qty'];
                $deliverys[$item['product_id']]['qty'] = $qty;
             endforeach;  
            
            $deliverys_arr       = [];
             foreach ($deliverys as $key => $value):
                $info_deliverys     = $this->products_deliverys_m->get_deliverys_product_by_id_equal_qty($key,$value['qty'])->result();
               
                if(empty($info_deliverys)){
                  $info_deliverys     = $this->products_deliverys_m->get_deliverys_product_by_id_min_man_qty($key,$value['qty'])->result();
                }
                //echo $this->db->last_query();
                if(!empty($info_deliverys)):
                    
                    foreach($info_deliverys as $deliverys):
                        $deliverys_arr[$key] = [
                            'products_delivery_id' => $deliverys->products_delivery_id,
                            'delivery_id'          => $deliverys->delivery_id,
                            'start_qty'            => $deliverys->start_qty,
                            'end_qty'              => $deliverys->end_qty,
                            'price'                => $deliverys->price,
                            'is_item'              => $deliverys->is_item
                        ];
                    endforeach;
                endif;
             endforeach;
             
            foreach($cart_data as $item):
                $input['product_id'] = $item['product_id'];
                $info                = $this->products_m->get_products($input)->row();
                if(!empty($info)):

                  
                    //$info->deliverys                    = $deliverys_arr;

                    $inputs['product_id']               = $info->product_id;
                    $inputs['stock_id']                 = $item['key'];
                    $infoStocks                         = $this->stocks_m->get_stock_by_id($inputs)->row();
                    $info->qty                          = !empty($item['qty']) ? $item['qty'] : 0;
                    $is_limit                           = !empty($infoStocks->is_limit) ? $infoStocks->is_limit : 0;
                    $info->price                        = !empty($infoStocks->product_price) ? $infoStocks->product_price : 0;
                    $inputs_['product_attribute_id']    = $item['option'];
                    $info->attributes_option            = $this->product_attributes_m->get_product_attributes_by_id($inputs_)->result();
                    
                    $option = '';
                    if(!empty($item['option'])):
                        foreach($item['option'] as $key => $val):
                            if($key > 0):
                                $option.=','.$val;
                            else:
                                $option.= $val;
                            endif;
                        endforeach;
                    endif;

                    $values_arr[] = array(
                        'reserve_id'        => $id,
                        'product_id'        => $info->product_id,
                        'product_price'     => !empty($infoStocks->product_price) ? $infoStocks->product_price : 0,
                        'quantity'          => !empty($item['qty']) ? $item['qty'] : 0 ,
                        'attributes_option' => $option,
                        'created_at'        => db_datetime_now()
                    );

                    if($is_limit < 1):
                        $stock_qty  = (!empty($infoStocks->stock_qty) ? $infoStocks->stock_qty : 0) - (!empty($item['qty']) ? $item['qty'] : 0);
                        $values_stock = array(
                            'stock_qty' => $stock_qty
                        );

                        $this->carts_m->update_stocks($info->product_id, $item['key'], $values_stock);

                    endif;
                
                endif;
                
                $data_arr[] = $info;

            endforeach;
            
            $this->carts_m->insert_reserves_detail($values_arr);

        endif;

        $user_address_id        = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        $data['user_address']   = $this->orders_m->get_orders_user_address_send_by_id($user_address_id)->row();

        $data['infoBanks']      = $this->banks_m->get_banks_by_id()->result();
        $data['info']           = $data_arr;
        $data['deliverys']      = $deliverys_arr;
        $data['reserve_code']   = $this->session->userdata('reserve_data');

        $data['transport']      = $this->orders_m->get_transport_all()->result();

        $this->load->view('template/body', $data);
    }

    public function ajax_carts()
    {
        // $this->session->sess_destroy();
        // exit();
        $input  = $this->input->post();
        
        // $this->session->unset_userdata('cart_data');
        $cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        } 

        $product_id = $this->input->post('product_id');
        $key        = $this->input->post('key');
        $option     = $this->input->post('option');
        $post_num   = $this->input->post('qty');
        $num        = $this->input->post('num');

        if(!empty($post_num)):
            $post_num   = $this->input->post('qty');
        else:
            $post_num   = 1;
        endif;

        if(!empty($option)):
            $option   = $this->input->post('option');
        else:
            $option   = $cart_data[$product_id]['option'];
        endif;

        if(!empty($key)):
            $key   = $this->input->post('key');
        else:
            $key   = !empty($cart_data[$product_id]['key']) ? $cart_data[$product_id]['key'] : '';
        endif;

        if(!empty($num)):
            $total = $num;
        else:
            // $total = $post_num + @$cart_data[$product_id]['qty'];
            $total = $post_num;
        endif;


    
            $cart_data[$key] = array(
                'product_id' => $product_id,
                'key'        => $key,
                'option'     => $option,
                'qty'        => $total
            );
        
        
        
        $this->session->set_userdata('cart_data', $cart_data);

        $cart_data_arr = $this->session->userdata('cart_data');


        $data['cart_qty'] = !empty($cart_data_arr) ? count($cart_data_arr) : 0;

        $toastr['type']     = 'success';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'เพิ่มสินค้าในตะกร้า';
        $data['toastr']     = $toastr;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function ajax_carts_delete()
    {
        $input  = $this->input->post();
        
        $cart_data = $this->session->userdata('cart_data');
        if($cart_data == null || count($cart_data) == 0){
            $cart_data = array();
        } 
       
        $product_id = $this->input->post('product_id');

        if(count($cart_data) > 0){

            $listDel = array();
            foreach ($cart_data as $key => $value) {
                if($key != $product_id){
                    $listDel[$key] = $value;
                }
            }
            $cart_data = $listDel;
        } else {
            $cart_data = array();
        }

        $this->session->set_userdata('cart_data', $cart_data);
        $data['status']     = 1;
        $toastr['type']     = 'success';
        $toastr['lineOne']  = config_item('appName');
        $toastr['lineTwo']  = 'ลบสินค้าในตะกร้า';
        $data['toastr']     = $toastr;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode( $data));
    }

    public function ajax_carts_payment()
    {

        $toastr['type']         = 'error';
        $toastr['lineOne']      = config_item('appName');
        $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
        $data['success']        = false;
        $data['toastr']         = $toastr;
        $result                 = 0;
        $input                  = $this->input->post(); 
        $countCheckReserves     = $this->getCheckReserves($input);
        if(!empty($countCheckReserves)){ 
            $value  = $this->_build_data($input);
            $result = $this->orders_m->insert($value);
            if ( $result ) {

                $value_ =  $this->_build_detail_data($result, $input);
                if($this->orders_m->insert_orders_detail($value_)):
                    $info = $this->carts_m->get_reserves_by_id($input)->row();
                    $this->carts_m->delete_reserves($input['reserve_code']);
                    $this->carts_m->delete_reserves_detail($info->reserve_id);
                endif;

                if(!empty($this->session->userData['UID'])):
                    $user_address_id = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
                    if(empty($this->orders_m->get_orders_user_address_send_by_id($user_address_id)->row())){
                        $address_value = $this->_build_order_address_data($input);
                        $this->orders_m->insert_orders_user_address($address_value);
                    }
                endif; 

                $data['order_id']   = $result;
                $data['status']     = $value['status'];
                $data['is_process'] = 0;
                $data['title']      = null;
                $data['process']    = null;
                $this->set_orders_status_action($data); 

                if(empty($input['order_code'])):
                    $value_code['order_code'] = $this->orders_m->set_order_id()->order_code; 
                    $transport = $this->orders_m->get_transport_by_id($value['transport_id'])->row();
                    if(!empty($transport->title)):
                        if($transport->title == 'DHL'):
                            $value_code['tracking_code'] = 'THDFLWEB'.$value_code['order_code'];
                        endif;
                    endif; 
                    $this->db->update('orders',$value_code, array('order_id' => $result));
                endif;

                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';

                $this->session->unset_userdata('cart_data');
                $this->session->unset_userdata('reserve_data');

                $transport = $this->orders_m->get_transport_by_id($value['transport_id'])->row();
                if(!empty($transport->title)):
                    if($transport->title == 'DHL'):
                        $data_arr['name']              = $value['name'].' '.$value['lasname']; 
                        $data_arr['email']             = $value['email']; 
                        $data_arr['url']               = base_url('carts/pdf?code='.urlencode(base64_encode($result)));
                        $this->sentMail($data_arr); 
                    endif;
                endif; 

            } else {
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            } 
        }else{
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาตรวจสอบรายการสินค้า';
        }
        $data['success']    = $result;
        $data['toastr']     = $toastr;

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    private function _build_detail_data($id, $input)
    {
        $value = array();
        $info = $this->carts_m->get_reserves_join_by_id($input)->result();
        if(!empty($info)):
            foreach($info as $item):
                $value[] = array(
                    'order_id'          => $id,
                    'product_id'        => $item->product_id,
                    'product_price'     => $item->product_price,
                    'quantity'          => $item->quantity,
                    'attributes_option' => $item->attributes_option,
                    'created_at'        => db_datetime_now()
                );
            endforeach;
        endif;
       
        return $value;
    }

    private function getCheckReserves($input)
    {
        return $this->carts_m->get_reserves_join_by_id($input)->result();
    }
    
    private function _build_data($input)
    { 
        $value['name']              = $input['name'];
        $value['lasname']           = $input['lasname'];
        $value['address']           = $input['address'];
        $value['provinces']         = $input['provinces'];
        $value['amphures']          = $input['amphures'];
        $value['districts']         = $input['districts'];
        $value['zip_code']          = $input['zip_code'];
        $value['tel']               = $input['tel'];
        $value['email']             = $input['email'];
        $value['payment_type']      = !empty($input['payment_type']) ? $input['payment_type'] : 0;
        $value['premise']           = !empty($input['premise']) ? $input['premise'] : null;
        $value['status']            = !empty($input['status']) ? $input['status'] : 1;
        $value['reserve_code']      = $input['reserve_code'];
        $value['paypal_process']    = !empty($input['paypal_process']) ? $input['paypal_process'] : 0;
        $discount                   = !empty($input['discount']) ? $input['discount'] : 0;
        $value['transport_id']      = !empty($input['transport_id']) ? $input['transport_id'] : null;
        
        if($value['payment_type'] > 0):
            $total         =  $discount +  $value['paypal_process'];
        else:
            $total         = $discount;    
        endif;

        $value['discount']          = $total;
        $value['deliverys_price']   = !empty($input['deliverys_price']) ? $input['deliverys_price'] : 0;
       
        
        $is_invoice                 = !empty($input['is_invoice']) ? $input['is_invoice'] : 0;
        $invoice_type               = !empty($input['invoice_type']) ? $input['invoice_type'] : 0;
        if($is_invoice > 0 && $invoice_type < 1):
            $value['invoice_no']            = $input['invoice_no'];
            $value['invoice_address']       = $input['invoice_address'];
            $value['invoice_provinces_id']  = $input['invoice_provinces_id'];
            $value['invoice_amphures_id']   = $input['invoice_amphures_id'];
            $value['invoice_districts_id']  = $input['invoice_districts_id'];
            $value['invoice_tel']           = $input['invoice_tel'];
            $value['invoice_zip_code']      = $input['invoice_zip_code'];
        elseif($is_invoice > 0 && $invoice_type > 0):
            $value['invoice_no']            = $input['invoice_no'];
        endif;

        $value['is_invoice']      = $is_invoice;
        $value['invoice_type']    = $invoice_type;

        if(!empty($input['coupon_id'])):
            $value['coupon_id'] = $input['coupon_id'];
        endif; 
        
        if($is_invoice > 0){
            $value['invoice_code']  = $this->orders_m->set_invoice_id()->invoice_code;
        }

        if(!empty($input['deliverys_price'])):
            $value['transfer_amount']   = $input['deliverys_price'];
        endif;

        if(!empty($input['transfer_datetime'])):
            $value['transfer_datetime'] = $input['transfer_datetime'];
        endif;

        // Add file form product
        $path                = 'payment';
        $upload              = $this->uploadfile_library->do_upload('file',TRUE,$path);
        $file                = '';
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name']; 
            $value['file']      = $file;
        }

        if(!empty($this->session->userData['UID'])):
            $value['user_id'] = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0;
        endif;
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = 0;
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = 0;
       
        return $value;
    }

    private function _build_order_address_data($input)
    { 
        $value['name']              = $input['name'];
        $value['lasname']           = $input['lasname'];
        $value['address']           = $input['address'];
        $value['provinces']         = $input['provinces'];
        $value['amphures']          = $input['amphures'];
        $value['districts']         = $input['districts'];
        $value['zip_code']          = $input['zip_code'];
        $value['tel']               = $input['tel'];
        $value['email']             = $input['email'];
        $value['user_id'] = !empty($this->session->userData['UID']) ? $this->session->userData['UID'] : 0; 
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = 0;
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = 0;
       
        return $value;
    }

    public function basket()
    {
        $cart_data      = $this->session->userdata('cart_data');
        $data_arr = array();
        $data_stock_arr = array();
        if(!empty($cart_data)):
            foreach($cart_data as $item):
                $input['product_id'] = $item['product_id'];
                $info                = $this->products_m->get_products($input)->row();

                if(!empty($info)):
                    $inputs['product_id']               = $info->product_id;
                    $inputs['stock_id']                 = $item['key'];
                    $infoStocks                         = $this->stocks_m->get_stock_by_id($inputs)->row();
                    $info->qty                          = !empty($item['qty']) ? $item['qty'] : 0;
                    $info->price                        = !empty($infoStocks->product_price) ? $infoStocks->product_price : 0;

                    $inputs_['product_attribute_id']    = $item['option'];
                    $info->attributes_option            = $this->product_attributes_m->get_product_attributes_by_id($inputs_)->result();
                    if(empty($reserve_data)):
                        $option = '';
                        if(!empty($item['option'])):
                            foreach($item['option'] as $key => $val):
                                if($key > 0):
                                    $option.=','.$val;
                                else:
                                    $option.= $val;
                                endif;
                            endforeach;
                        endif;
                    endif;
                endif;
                $data_arr[] = $info;
                $data_stock_arr[]                 = $item['key'];
            endforeach;
        endif;

        $data['stocks']    = $data_stock_arr;
        $data['infoBasket']  = $data_arr;
        $data['basketCount'] = count($data_arr);
        $this->load->view('basket', $data);
    }

    public function basket_home()
    {
        $cart_data      = $this->session->userdata('cart_data');
        $data_arr = array();
        if(!empty($cart_data)):
            foreach($cart_data as $item):
                $input['product_id'] = $item['product_id'];
                $info                = $this->products_m->get_products($input)->row();
                if(!empty($info)):
                    $inputs['product_id']               = $info->product_id;
                    $inputs['stock_id']                 = $item['key'];
                    $infoStocks                         = $this->stocks_m->get_stock_by_id($inputs)->row();
                    $info->qty                          = !empty($item['qty']) ? $item['qty'] : 0;
                    $info->price                        = !empty($infoStocks->product_price) ? $infoStocks->product_price : 0;
                    $inputs_['product_attribute_id']    = $item['option'];
                    $info->attributes_option            = $this->product_attributes_m->get_product_attributes_by_id($inputs_)->result();
                    if(empty($reserve_data)):
                        $option = '';
                        if(!empty($item['option'])):
                            foreach($item['option'] as $key => $val):
                                if($key > 0):
                                    $option.=','.$val;
                                else:
                                    $option.= $val;
                                endif;
                            endforeach;
                        endif;
                    endif;
                endif;
                $data_arr[] = $info;
                $data_stock_arr[]                 = $item['key'];
            endforeach;
        endif;
        
        $data['infoBasket']  = $data_arr;
        $data['basketCount'] = count($data_arr);

        $this->load->view('basket_home', $data);
    }

    public function ajax_carts_tab()
    {
        $cart_data  = $this->session->userdata('cart_data');
        $data_arr   = array();
        if(!empty($cart_data)):
            foreach($cart_data as $item):
                $input['product_id'] = $item['product_id'];
                $info                = $this->products_m->get_products($input)->row();
                if(!empty($info)):
                    $inputs['product_id']               = $info->product_id;
                    $inputs['stock_id']                 = $item['key'];
                    $infoStocks                         = $this->stocks_m->get_stock_by_id($inputs)->row();
                    $info->qty                          = !empty($item['qty']) ? $item['qty'] : 0;
                    $info->price                        = !empty($infoStocks->product_price) ? $infoStocks->product_price : 0;
                    $inputs_['product_attribute_id']    = $item['option'];
                    $info->attributes_option            = $this->product_attributes_m->get_product_attributes_by_id($inputs_)->result();
                    if(empty($reserve_data)):
                        $option = '';
                        if(!empty($item['option'])):
                            foreach($item['option'] as $key => $val):
                                if($key > 0):
                                    $option.=','.$val;
                                else:
                                    $option.= $val;
                                endif;
                            endforeach;
                        endif;
                    endif;
                endif;
                $data_arr[] = $info;
                $data_stock_arr[]                 = $item['key'];
            endforeach;
        endif;
        
        $data['stocks']    = $data_stock_arr;
        $data['infoBasket']  = $data_arr;
        $data['basketCount'] = count($data_arr);
        $result = $this->load->view('basket_home', $data);
        json_encode($result);
    }

    public function set_orders_status_action($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['status']        = $data['status'];
        $value['is_process']    = $data['is_process'];
        $value['title']         = $data['title'];
        $value['process']       = $data['process'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = 0;

        $this->orders_m->insert_orders_status_action($value);
    }

    private function sentMail($data)
    {

        $type = 'mail';
        $info = $this->config_m->get_config($type);
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        } 

        if(!empty($temp)):
            // load mail config  
            $Host 		= !empty($temp['SMTPserver']) ? $temp['SMTPserver'] : '';
            $Username 	= !empty($temp['SMTPusername']) ? $temp['SMTPusername'] : '';
            $Password 	= !empty($temp['SMTPpassword']) ? $temp['SMTPpassword'] : '';
            $SMTPSecure = 'tls';
            $Port 		= !empty($temp['SMTPport']) ? $temp['SMTPport'] : '';  
            $viewMail = $this->load->view('email/email', $data, TRUE);

            // load mail send config 
            
            require 'app_frontend/third_party/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer;
            $mail->SMTPDebug = 0;                               	// Enable verbose debug output
            
            $mail->isSMTP();                                      	// Set mailer to use SMTP
            $mail->Host 		= $Host;              				// Specify main and backup SMTP servers
            $mail->SMTPAuth 	= true;                             // Enable SMTP authentication
            $mail->Username 	= $Username;                		// SMTP username
            $mail->Password 	= $Password;                        // SMTP password
            $mail->SMTPSecure 	= $SMTPSecure;                      // Enable TLS encryption, `ssl` also accepted
            $mail->Port 		= $Port;                            // TCP port to connect to
            $mail->CharSet 		= 'UTF-8';
            
            $mail->From 		= $Username;
            $mail->FromName 	= $Username;

            $email_to 			= $data['email'];

            $mail->addAddress($email_to);               			// Name is optional
            $mail->isHTML(false);                                  	// Set email format to HTML

            $mail->Subject = $email_to;
            $mail->Body    = $viewMail;
            $mail->AltBody = $viewMail;
            $mail->Send();
            // $mail->ErrorInfo; 
        endif;
    }
    
    public function pdf()
    {  
        $input['order_id']  = urldecode(base64_decode($this->input->get('code')));
        $info               = $this->orders_m->get_orders_param($input); 
        $info               = $info->row();
        $data['info']       = $info;
        
        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result();
        if(!empty($infoDetail)):
            foreach($infoDetail as $item):
                $attributes_option = explode(',',$item->attributes_option);
                $item->attributes = $this->product_attributes_m->get_product_attributes_by_in($attributes_option)->result();
                if(!empty($item->attributes)):
                    foreach($item->attributes as $attribute):
                        $attribute->headattributes = $this->product_attributes_m->get_product_attributes_by_in($attribute->parent_id)->row();
                    endforeach;
                endif;
            endforeach;
        endif;
        $data['details']        = $infoDetail;

        //ดึงข้อมูลบริษัท
        $info_com           = $this->info_m->get_rows('');
        $info_com           = $info_com->row();
        $info_com->file     = base_url($info_com->file); 
        $data['company']    = $info_com; 
        header('Content-Type: image/jpeg;');
        $transport              = $this->transports_m->get_transport_by_id($info->transport_id)->row();
        $data['tracking_title'] = !empty($transport->title) ? $transport->title :'ไม่ระบุ'; 
        if(!empty($transport)):
            if($transport->title == "DHL"): 
                $code                = "THDFLWEB".$info->order_code;
                $generator           = new Picqer\Barcode\BarcodeGeneratorHTML();
                $generator_img       = new Picqer\Barcode\BarcodeGeneratorPNG(); 
                $border              = 2;
                $height              = 50;
                $data['order_code']  = $code; 
                $data['img_barcode'] = '<img src="data:image/png;base64,' . base64_encode($generator_img->getBarcode($code, $generator_img::TYPE_CODE_128,$border,$height)) . '">';
                $data['barcode']     = $generator->getBarcode($code , $generator::TYPE_CODE_128,$border,$height);
            endif;
        endif; 

        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
 
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view('print_pdf', $data, true); 
        $mpdf->WriteHTML($content); 
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "orders.pdf";
        $pathFile = "uploads/pdf/orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
        
        // page detail 
    }
}
