<?php $checklogin = Modules::run('checklogin/login_link'); 
if (!empty($this->session->userdata('userData'))) {
    //arr($this->session->userdata('userData'));
}
?>

<!-- The Modal -->
<div class="modal fade" id="loginface">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">เข้าสู่ระบบ</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body" style="display: block;">
                <form id="LoginForm" class="form-signin" action="<?=site_url('login/login_user');?>" method="post" style="width: 100%;">
                    <div class="form-group">
                        <label for="username">Email และ เบอร์โทรศัพท์:</label>
                        <input type="text" class="form-control" id="username" name="email" placeholder="Email และ เบอร์โทรศัพท์" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="password">รหัสผ่าน:</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="รหัสผ่าน" autocomplete="off">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn menu-btn btn-hover" style="height: 47px;cursor: pointer;">เข้าสู่ระบบ</button>
                        <p style="margin-top: 16px;">
                            <button class="btn menu-btn btn-hover" id="btn-register" style="height: 47px;cursor: pointer;background: #25b09e;">ลงทะเบียน</button>
                        </p>
                    </div>
                    <div class="form-group text-center">
                    <hr style="margin: 20px;">
                        <a href="<?=$checklogin['authURL'];?>" class="btn btn-login-facebook"> 
                            Login with Facebook 
                        </a>
                    </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 50px;">ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="modal-register" style="overflow-x: hidden;overflow-y: auto;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">สมัครสมาชิก</h4>
            </div>
            <div class="modal-body" style="display: block; padding: 36px;">
                <style>
                #modal-register .error{
                    color: red;
                }
                </style>
                <form id="RegisterForm" 
                    action="<?=site_url('login/register');?>"
                    method="post" enctype="multipart/form-data" 
                    style="width: 100%;" autocomplete="off">
                    <div class="form-group">
                        <label class="Bold" for="fname">ชื่อ <span class="required">*</span></label>
                        <input type="text" id="fname" name="fname" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="lname">นามสกุล <span class="required">*</span></label>
                        <input type="text" id="lname" name="lname" class="form-control" placeholder="นามสกุล">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="phone">เบอร์โทร</label>
                        <input type="text" id="phone" name="phone" class="form-control" placeholder="เบอร์โทร">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="email">Email <span class="required">*</span></label>
                        <input type="email" id="email" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="password">รหัสผ่าน <span class="required">*</span></label>
                        <input type="password" id="user_password" name="password" class="form-control"
                            placeholder="รหัสผ่าน">
                    </div>
                    <div class="form-group">
                        <label class="Bold" for="confirm_password">ยืนยันรหัสผ่าน <span class="required">*</span></label>
                        <input type="password" id="confirm_password" name="confirm_password"
                            class="form-control" placeholder="ยืนยันรหัสผ่าน">
                    </div>
                    <div class="form-group">									
                        <input type="checkbox" id="icheck-profile" class="" name="is_profile" value="1" style="display: none;"/>
                        <label for="icheck-profile"><span></span>ต้องการอัพโหลดโปรไฟล์</label>
                    </div>
                    <div id="profile-img" class="form-group d-none">
                        <label for="file">ไฟล์</label>
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn menu-btn btn-hover" style="height: 47px;cursor: pointer;">เข้าสู่ระบบ</button>
                    </div>
                    <div class="form-group text-center">
                    <hr style="margin: 20px;">
                        <a href="<?=$checklogin['authURL'];?>">
                            <img src="<?=base_url('images/logo/fb_login.png');?>" alt="" style="width: 50%;">
                        </a>
                    </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" style="border-radius: 50px;">ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>

<div class="header-cart">
    <?php if (!empty($this->session->userdata('userData'))) { ?>
    <span class="cart-dropdown-open">
        <a class="icon-cart-furniture" href="#">
            <img src="<?php echo !empty($this->session->userData['oauth_picture']) ? $this->session->userData['oauth_picture'] : "";?>" alt="" style="width: 40px;border-radius: 100%;margin-top: -18px;">
        </a>
        <ul class="cart-dropdown">
            <li style="margin-top: 10px;font-size: 18px;">
                <a href="<?=site_url('profile');?>"><span><i class="ti-user"></i> ข้อมูลส่วนตัว</span></a>
            </li>
            <li style="margin-top: 10px;font-size: 18px;">
                <a href="<?=site_url('checkorder');?>"><span><i class="ti-shopping-cart"></i> รายการสั่งซื้อ</span></a>
            </li>
            <li style="margin-top: 10px;font-size: 18px;">
                <a href="<?=site_url('checklogin/logout_face');?>"><span><i class="ti-unlock"></i> Logout</span></a>
            </li>
        </ul>
    </span>
    <?php }else{ ?>
    <a class="icon-cart-furniture" data-toggle="modal" data-target="#loginface">
        <i class="ti-user" style="font-size: 27px;cursor: pointer;"></i>
    </a>
    <?php } ?>
    <a class="icon-cart-furniture" href="#">
        <i class="ti-search cart-search" style="font-size: 27px;cursor: pointer;"></i>
    </a>
    <span class="cart-dropdown-open">
    <a class="icon-cart-furniture" href="#">
        <i class="ti-shopping-cart"></i>
        <span class="shop-count-furniture green"><?php echo !empty($basketCount) ? $basketCount : 0;?></span>
    </a>
    <ul class="cart-dropdown">
        <?php
        $sumtotal    = 0;
        if(!empty($infoBasket)):
            $text   = '';
            $total  = 0;
            foreach($infoBasket as $key => $basket):
                $qty        = (!empty($basket->qty) ? $basket->qty : 0 );
                $price      = (!empty($basket->price) ? $basket->price : 0 );
                $total      = $qty * $price;
                $sumtotal  += $total;
                if(!empty($basket->attributes_option)):
                    $text = '<br>';
                    foreach($basket->attributes_option as $option):
                        $text.= !empty($option->title) ? '('.$option->title.') ' : '';
                    endforeach;
                endif;
        ?>
       <!--  <li id="<?php echo !empty($basket->product_id) ? $basket->product_id : '';?>" class="single-product-cart"> -->
        <li id="<?php echo !empty($stocks[$key]) ? $stocks[$key] : '';?>" class="single-product-cart">

            <div class="cart-img">
                <a href="javascript:void(0)">
                    <img src="<?php echo !empty($basket->file) ? base_url($basket->file) : '';?>" onerror="this.src='<?=base_url('template/frontend/assets/img/cart/1.jpg');?>'" alt="" style="width: 85px;">
                </a>
            </div>
            <div class="cart-title">
                <h5><a href="javascript:void(0)"> <?php echo !empty($basket->title) ? $basket->title : '';?></a></h5>
                <h6><a href="javascript:void(0)"><?=$text?></a></h6>
                <span data-price="<?php echo !empty($basket->price) ? $basket->price : 0;?>" >
                ฿<?php echo !empty($basket->price) ? number_format($basket->price) : 0;?>
                x <?php echo !empty($basket->qty) ? number_format($basket->qty) : 0;?>
            </span>
            </div>
            <div class="cart-delete">
                <a href="javascript:void(0)" 
                class="text-tab-product-delete" 
                data-tr="tr-<?=$key?>"
                data-id="<?php echo !empty($stocks[$key]) ? $stocks[$key] : '';?>"
                ><i class="ti-trash"></i></a>
            </div>
        </li>
        <?php
            endforeach;
        endif;
        ?>
        
        <li class="cart-space">
            <div class="cart-sub">
                <h4>รวม</h4>
            </div>
            <div class="cart-price">
                <h4>฿<?=number_format($sumtotal)?></h4>
            </div>
        </li>
        <li class="cart-btn-wrapper">
            <a class="cart-btn btn-hover" href="<?=base_url('carts');?>">ดูสินค้าในตะกร้า</a>
            <a class="cart-btn btn-hover" href="<?=base_url('carts/checkout')?>">ชำระเงิน</a>
        </li>
    </ul>
    </span>
</div>