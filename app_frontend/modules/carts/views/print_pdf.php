<!DOCTYPE html>
<html>
<head>
	<title>บริษัท มีดี88ช็อป จำกัด</title>
</head>
<style>
     #printarea {  
    margin: 0;
    border: initial;
    border-radius: initial;
    width: initial;
    min-height: initial;
    box-shadow: initial;
    background: initial;
    page-break-after: always;
}
.table-print, .table-sender, .table-recipient{
    border-collapse: collapse;
}
.table-print tr{
    border: 1px solid;
}   
.table-print td{
    border: 1px solid;
}
.table-print th{
    border: 1px solid;
}
.table-print, .table-sender, .table-recipient p{
    margin: 0px 0px 0px 7px;
}
.table-sender{
    width: 50%;
    position: relative;
    top: 400px;
    left: 0px;
}
.table-recipient{
    width: 100%;
    position: relative;
    top: 420px;
    left: 65px;
}
.table-recipient {
    width: 100%;
    position: relative;
    top: 390px;
    left: 50px;
}
.barcode{
    display: none;
}
.collect {
    border: 2px solid #32b312;
    border-radius: 6px;
    padding: 24px 20px;
    text-align: center;
    font-size: 20px;
    color: #2aa50b;
    width: 70%;
    margin-left: 55px;
}

</style> 
<body> 
        <page id="printarea" size="A4"> 
            <div style="padding: 20px;">

                <table style="width: 100%;">
                    <tr>
                        <td > 
                            <h3><?php echo !empty($company->title)? $company->title: '';?></h3>
                        </td>
                        <td >                               
                            <h4>ใบสั่งสินค้า</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 6px;">
                        <br>   
                        <p style="width: 351px; line-height: 20px;"><?php echo !empty($company->excerpt)? $company->excerpt: '';?></p>
                            <br><br>
                            <p>Tel. <?php echo !empty($company->tel)? $company->tel: '';?></p>
                            <br><br>
                            <p>เลขประจำตัวผู้เสียภาษี  <?php echo !empty($company->tax_id)? $company->tax_id: '';?></p> 
                            <br> 
                        </td>
                    </tr>
                </table> 
                <p style="font-size: 14px;color: red">ขนส่ง : <?php echo !empty($tracking_title) ? $tracking_title : ''?></p>
                <table class="table-print" style="width: 100%;">
                    <tr>
                        <th style="width: 150px;text-align: center;"><p>เลขที่สั่งซื้อ</p></th>
                        <th><p>รายการสินค้า</p></th>
                        <th style="width: 100px;text-align: center;"><p>จำนวน</p></th>
                    </tr>
                    <?php
                    $sumtotal = 0;
                    $i =0;
                    
                    if(!empty($details) && count($details) > 0){
                        $rowspan = count($details);
                        
                        foreach($details as $key => $item){
                            $sumtotal  += $item->quantity;
                            $text_attributes = '';
                            if(!empty($item->attributes)):
                                foreach($item->attributes as $attribute):
                                    $text_attributes.= ' ('.$attribute->headattributes->title.')'.$attribute->title;
                                endforeach;
                            endif;

                            ?>
                            <tr>
                                <?php
                                if($key == 0):
                                    ?>
                                    <td valign="top" rowspan="<?=$rowspan?>" style="text-align: center;">
                                        <p style="padding-left:10px;"><?php echo !empty($info->order_code)? $info->order_code: '';?></p>
                                    </td>
                                    <?php
                                endif;
                                ?>
                                <td style="">
                                    <p style="padding-left:5px;">
                                    <?php echo !empty($item->title)? $item->title: '';?><?=$text_attributes?>
                                    </p>
                                </td>
                                <td style="text-align: center;">
                                    <p><?php echo !empty($item->quantity)? $item->quantity: 0;?></p>
                                </td>
                            </tr>
                            <?php 
                            $i++;
                        }
                    }

                    $top    = 320;
                    $top2   = 320;
                    $i      = $i * 20;
                    $top    = $top - $i;
                    $top2   = $top2 - $i;
                    ?>

                    <tr>
                        <th valign="top" colspan="2" style="text-align: right;">
                            <p style="padding-right:5px;">รวมทั้งหมด</p>
                        </th>
                        <th style="text-align: center;">
                            <p><?=$sumtotal?></p>
                        </th>
                    </tr> 
                </table>
                <br>
                <table class="table-sender" style="<?='top :'.$top.'px'?> ">
                    <tr>
                        <td valign="top">
                            <div class="box-check">
                                <p><u><strong>ผู้ส่ง</strong></u></p><br>
                                <p><?php echo !empty($company->title) ? $company->title: '';?></p><br>
                                <p><?php echo !empty($company->excerpt) ? $company->excerpt: '';?></p><br>
                                <p>Tel. <?php echo !empty($company->tel) ? $company->tel: '';?></p><br>
                                <p>เลขประจำตัวผู้เสียภาษี  <?php echo !empty($company->tax_id) ? $company->tax_id: '';?></p> 
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="table-recipient" style="<?='top :'.$top2.'px'?> ">
                    <tr>
                        <td style="width: 50%;">
                            <img style="height: 100px" src="<?php echo !empty($company->file) ? $company->file : '';?>">
                        </td>
                        <td style="width: 50%;" valign="top">
                            <div class="box-check">
                                <p><u><strong>ผู้รับ</strong></u></p><br>
                                <p><?php echo !empty($info->name) ? $info->name: '';?> <?php echo !empty($info->lasname) ? $info->lasname: '';?></p> 
                                <br><p>
                                    <?php echo !empty($info->address) ? $info->address: '';?>
                                     <br>ตำบล 
                                    <?php echo !empty($info->districts) ? $info->districts: '';?>
                                    อำเภอ  <?php echo !empty($info->amphures) ? $info->amphures: '';?>
                                    จังหวัด  <?php echo !empty($info->provinces) ? $info->provinces: '';?>
                                     <br>รหัสไปรษณีย์ <?php echo !empty($info->zip_code) ? $info->zip_code: '';?>
                                </p>
                                 <p>เบอร์โทรศัพท์. <?php echo !empty($info->tel) ? $info->tel: '';?></p>
                                <br>
                                <?php 
                                    if(!empty($order_code)){ 
                                ?>
                                    <p><strong> เลขที่สั่งซื้อสินค้า :</strong> <?php echo !empty($order_code) ? $order_code: '';?></p>  
                                    <div class="barcode" style="    margin-left: -30px;"><?php echo !empty($barcode) ? $barcode : '';?></div>
                                    <div class="barcode_img" style="    margin-left: -40px;"><?php echo !empty($img_barcode) ? $img_barcode : '';?></div>
                                    <p style="margin: 0px 0 0 50px;"><?php echo !empty($order_code) ? $order_code: '';?></p>
                                <?php 
                                }else{
                                ?>
                                <p><strong> เลขที่สั่งซื้อสินค้า :</strong> <?php echo !empty($info->order_code) ? $info->order_code: '';?></p> 
                                <?php
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                </table> 
            </div>
        </page>  
     </body>
</html>

