<?php echo Modules::run('banners/banners_home', 'products');?>
<!-- shopping-cart-area start -->
<div class="cart-main-area pt-95 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1 class="cart-heading">ตะกร้าสินค้า</h1>
                <form action="#">
                    <div class="table-content table-responsive">
                        <table id="table-cart">
                            <thead>
                                <tr>
                                    <th>ลบ</th>
                                    <th>รูป</th>
                                    <th>สินค้า</th>
                                    <th>ราคา</th>
                                    <th>จำนวน</th>
                                    <th>รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sumtotal    = 0; 
                                if(!empty($info)):
                                    $text       = '';
                                    
                                    $total      = 0;
                                    foreach($info as $key => $item):
                                        $text_input = '';
                                        $qty        = (!empty($item->qty) ? $item->qty : 0 );
                                        $price      = (!empty($item->price) ? $item->price : 0 );
                                        $total      = $qty * $price;
                                        $sumtotal  += $total;
                                        if(!empty($item->attributes_option)):
                                            $text = '<br>';
                                            foreach($item->attributes_option as $option):
                                                $text.= !empty($option->title) ? '('.$option->title.') ' : '';
                                                $text_input.= '<input type="hidden" class="text-attribute-id" 
                                                value="'.$option->product_attribute_id.'">';
                                            endforeach;
                                        endif;
                                ?>
                                <tr id="tr-<?=$key?>">
                                    <td class="product-remove">
                                        <a href="javascript:void(0)" class="text-product-delete" data-tr="tr-<?=$key?>"
                                        data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>">
                                        <i class="pe-7s-close"></i></a>
                                    </td>
                                    <td class="product-thumbnail">
                                        <a href="javascript:void(0)">
                                            <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" onerror="this.src='<?=base_url('template/frontend/assets/img/cart/1.jpg');?>'" alt="" style="width: 85px;">
                                        </a>
                                    </td>
                                    <td class="product-name">
                                        <a href="javascript:void(0)"><?php echo !empty($item->title) ? $item->title.$text : '';?> </a>
                                    </td>
                                    <td class="product-price-cart">
                                        <span class="amount" data-price="<?php echo !empty($item->price) ? $item->price : 0;?>" >฿ <?php echo !empty($item->price) ? number_format($item->price) : 0;?></span>
                                    </td>
                                    <td class="product-quantity">
                                        <input class="btn-product-qty" 
                                        value="<?php echo !empty($item->qty) ? $item->qty : 0;?>" 
                                        type="number" 
                                        data-tr="tr-<?=$key?>" 
                                        data-id="<?php echo !empty($item->product_id) ? $item->product_id : '';?>"
                                        data-stock= "<?php echo !empty($item->stock_qty) ? $item->stock_qty : 0;?>"
                                        data-limit= "<?php echo !empty($item->is_limit) ? $item->is_limit : 0;?>"
                                        oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                        >
                                        <?=$text_input;?>
                                        <input type="hidden" class="text-stock-key-qty">
                                        <input type="hidden" class="text-stock-key-id">
                                        <input type="hidden" class="text-is-limit-key">
                                    </td>
                                    <td class="product-subtotal">฿ <span class="amount"><?=number_format($total)?></span></td>
                                </tr>
                                <?php
                                    endforeach;
                                else:
                                ?>
                                <tr>
                                    <td colspan="6" class="text-center">  
                                        <h5>ไม่มีสินค้าในตะกร้า</h5>
                                    </td>
                                </tr>
                                <?php
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-5 ml-auto">
                            <div class="cart-page-total">
                                <h2>ตะกร้าสินค้าทั้งหมด</h2>
                                <ul>
                                    <li>รายละเอียด<span class="text-sumtotal"><?=number_format($sumtotal)?></span></li>
                                    <li>รวม<span class="text-sumtotal"><?=number_format($sumtotal)?></span></li>
                                </ul>
                                <a href="javascript:void(0);" id="click-checkout-page" class="contact-btn btn-hover">ชำระเงิน</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- shopping-cart-area end -->
