<?php echo Modules::run('banners/banners_home', 'products');?>
<!-- checkout-area start -->
<div class="checkout-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="coupon-accordion">
                    ACCORDION START
                    <h3>วิธีการชำระเงินแบบโอน ? <span id="showlogin">อ่านเพิ่มเติม</span></h3>
                    <div id="checkout-login" class="coupon-content">
                        <div class="coupon-info">
                            <p class="coupon-text">Quisque gravida turpis sit amet nulla posuere lacinia. Cras sed est sit amet ipsum luctus.</p>
                            <p class="coupon-text">Quisque gravida turpis sit amet nulla posuere lacinia. Cras sed est sit amet ipsum luctus.</p>
                        </div>
                    </div>
                    ACCORDION END    
                    ACCORDION START
                    <h3>วิธีการชำระเงินแบบ PayPal ? <span id="showcoupon">อ่านเพิ่มเติม</span></h3>
                    <div id="checkout_coupon" class="coupon-checkout-content">
                        <div class="coupon-info">
                            <p class="coupon-text">Quisque gravida turpis sit amet nulla posuere lacinia. Cras sed est sit amet ipsum luctus.</p>
                            <p class="coupon-text">Quisque gravida turpis sit amet nulla posuere lacinia. Cras sed est sit amet ipsum luctus.</p>
                        </div>
                    </div>
                    ACCORDION END                        
                </div> -->
            </div>
        </div>
        <form id="form-submit-cart" action="#" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="checkbox-form">						
                        <h3>รายละเอียดการจ่ายเงิน</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>ชื่อ <span class="required">*</span></label>										
                                    <input type="text" id="name" class="text-input-required name" name="name"
                                    value="<?php echo !empty($user_address->name) ? $user_address->name : '';?>" 
                                    placeholder="ชื่อ" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>นามสกุล <span class="required">*</span></label>										
                                    <input type="text" id="lasname" class="text-input-required" name="lasname" 
                                    value="<?php echo !empty($user_address->lasname) ? $user_address->lasname : '';?>" 
                                    placeholder="นามสกุล" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>ที่อยู่จัดส่ง (Address)<span class="required">*</span></label>
                                    <textarea id="address" name="address" class="text-input-required" cols="30" rows="5" 
                                    placeholder="ที่อยู่จัดส่ง" style="background: #fff none repeat scroll 0 0;border: 1px solid #e5e5e5;" required><?php echo !empty($user_address->address) ? $user_address->address : '';?></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>ตำบล (County) <span class="required">*</span></label>                                        
                                    <input type="text" class="text-input-required" name="districts" id="districts" 
                                    value="<?php echo !empty($user_address->districts) ? $user_address->districts : '';?>" 
                                    placeholder="ตำบล (County)" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>อำเภอ (City) <span class="required">*</span></label>
                                    <input type="text" class="text-input-required" name="amphures" id="amphures"  
                                    value="<?php echo !empty($user_address->amphures) ? $user_address->amphures : '';?>" 
                                    placeholder="อำเภอ" required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <input type="hidden" id="datajson" name="datajson"
                                value="<?=base_url('template/metronic/assets/plugins/jquery.Thailand.js/database/db.json');?> ">
                                <div class="checkout-form-list">
                                    <label>จังหวัด (County) <span class="required">*</span></label>
                                    <input type="text" class="text-input-required" name="provinces" id="provinces" 
                                    value="<?php echo !empty($user_address->provinces) ? $user_address->provinces : '';?>" 
                                    placeholder="จังหวัด" required/>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>รหัสไปรษณีย์ (Postcode) <span class="required">*</span></label>										
                                    <input type="text" class="text-input-required" name="zip_code" id="zip_code"  
                                    value="<?php echo !empty($user_address->zip_code) ? $user_address->zip_code : '';?>" 
                                    placeholder="หมายเลขไปรษณีย์"
                                    maxlength="5" 
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                    required/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>อีเมล (Email)</label>										
                                    <input type="email" name ="email" 
                                    value="<?php echo !empty($user_address->email) ? $user_address->email : '';?>" 
                                    placeholder="อีเมล"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkout-form-list">
                                    <label>หมายเลขโทรศัพท์ (Phone)  <span class="required">*</span></label>										
                                    <input type="text" id="tel" class="text-input-required" name ="tel" 
                                    value="<?php echo !empty($user_address->tel) ? $user_address->tel : '';?>" 
                                    placeholder="หมายเลขโทรศัพท์" 
                                    maxlength="10"
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                    required/>
                                </div>
                            </div>	
                            <div class="col-md-12">
                                <div class="checkout-form-list">
                                    <label>ใบกำกับภาษี</label>										
                                    <input type="checkbox" id="is_invoice" class="text-input-required" name="is_invoice" value="1"/>
                                    <label for="is_invoice"><span></span>ต้องการใบกำกับภาษี</label>
                                    &nbsp; &nbsp; &nbsp;
                                    <input type="checkbox" id="invoice_type" class="text-input-required" name="invoice_type" value="1"/>
                                    <label for="invoice_type"><span></span>ใช้ที่อยู่จัดส่ง</label>
                                </div>
                            </div>	
                            <!-- <div id="display-vat" style="display:none;"> -->
                            <div class="col-md-12 display-vat invoice_no" style="display:none;">
                                <div class="checkout-form-list">
                                    <label>เลขที่ผู้เสียภาษี  <span class="required">*</span></label>										
                                    <input type="text" id="invoice_no" class="text-input-required" name ="invoice_no" placeholder="เลขที่ผู้เสียภาษี" required/>
                                </div>
                            </div>	
                            <div class="col-md-12 display-vat">
                                <div class="checkout-form-list">
                                    <label>ที่อยู่จัดส่ง (Address)<span class="required">*</span></label>
                                    <textarea id="invoice_address" name="invoice_address" class="text-input-required" cols="30" rows="5" placeholder="ที่อยู่จัดส่ง" style="background: #fff none repeat scroll 0 0;border: 1px solid #e5e5e5;" required></textarea>
                                </div>
                            </div>

                            <div class="col-md-6 display-vat">
                                <div class="checkout-form-list">
                                    <label>ตำบล (County) <span class="required">*</span></label>                                        
                                    <input type="text" class="text-input-required" name="invoice_districts_id" id="invoice_districts_id" placeholder="ตำบล (County)" required/>
                                </div>
                            </div>
                            <div class="col-md-6 display-vat">
                                <div class="checkout-form-list">
                                    <label>อำเภอ (City) <span class="required">*</span></label>
                                    <input type="text" class="text-input-required" name="invoice_amphures_id" id="invoice_amphures_id"  placeholder="อำเภอ" required/>
                                </div>
                            </div>
                            <div class="col-md-6 display-vat">
                            <input type="hidden" id="datajson" name="datajson"
                                value="<?=base_url('template/metronic/assets/plugins/jquery.Thailand.js/database/db.json');?> ">
                                <div class="checkout-form-list">
                                    <label>จังหวัด (County) <span class="required">*</span></label>
                                    <input type="text" class="text-input-required" name="invoice_provinces_id" id="invoice_provinces_id" placeholder="จังหวัด" required/>
                                </div>
                            </div>
                            <div class="col-md-6 display-vat">
                                <div class="checkout-form-list">
                                    <label>รหัสไปรษณีย์ (Postcode) <span class="required">*</span></label>										
                                    <input type="text" class="text-input-required" name="invoice_zip_code" id="invoice_zip_code" placeholder="หมายเลขไปรษณีย์"
                                    maxlength="5"
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                                    required/>
                                </div>
                            </div>
                            <div class="col-md-6 display-vat">
                                <div class="checkout-form-list">
                                    <label>หมายเลขโทรศัพท์ (Phone)  <span class="required">*</span></label>										
                                    <input type="text" id="invoice_tel" class="text-input-required" name ="invoice_tel" placeholder="หมายเลขโทรศัพท์"
                                    maxlength="10"
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" 
                                    required/>
                                </div>
                            </div>	
                            
                        </div>												
                    </div>
                </div>	
                <div class="col-lg-6 col-md-12 col-12">
                    <div class="your-order">
                        <h3>รายการสั่งซื้อของคุณ (อ้างอิง : <?php echo !empty($reserve_code) ? $reserve_code : '';?>)</h3>
                        <input type="hidden" id="text-reserve-code" name="reserve_code" value="<?php echo !empty($reserve_code) ? $reserve_code : '';?>">
                        <div class="your-order-table table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product-name">สินค้า</th>
                                        <th class="product-total">รวม</th>
                                    </tr>							
                                </thead>
                                <tbody>
                                <?php
                                    $deliverys_price    = 0;
                                    $sumtotal           = 0;
                                    $total              = 0;
                                    if(!empty($info)):
                                        $text   = ''; 
                                        
                                        if(!empty($deliverys)){
                                                foreach($deliverys as $key => $delivery){

                                                    if($delivery['is_item'] > 0){
                        
                                                        $deliverys_price += $delivery['price'] * $qty;
                                                    }else{

                                                        $deliverys_price += $delivery['price'];
                                                    }
                                                    
                                                }
                                            }
                                         
                                        foreach($info as $key => $item):
                                            $qty        = (!empty($item->qty) ? $item->qty : 0 );
                                            $price      = (!empty($item->price) ? $item->price : 0 );
                                            $total      = $qty * $price;
                                            $sumtotal  += $total;
                                            if(!empty($item->attributes_option)):
                                                $text = '';
                                                foreach($item->attributes_option as $option):
                                                    $text.= !empty($option->title) ? '('.$option->title.') ' : '';
                                                endforeach;
                                            endif;
                                            

                                    ?>
                                    <tr class="cart_item">
                                        <td class="product-name">
                                            <?php echo !empty($item->title) ? $item->title : '';?>
                                            <strong class="product-quantity"> × <?php echo !empty($item->qty) ? $item->qty : 0;?></strong>
                                            <p><?=$text?></p>
                                        </td>
                                        <td class="product-total">
                                            ฿<span class="amount"><?=number_format($total)?></span>
                                        </td>
                                    </tr>
                                    <?php
                                        endforeach;
                                    endif;

                                    $sumtotal  =  $sumtotal + $deliverys_price;

                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr class="cart-subtotal">
                                        <th>ค่าจัดส่ง</th>
                                        <td><span class="amount">฿<?=number_format($deliverys_price)?></span></td>
                                    </tr>
                                    <tr class="cart-subtotal">
                                        <th>รวม</th>
                                        <td><span class="amount">฿<?=number_format($sumtotal)?></span></td>
                                    </tr>
                                    
                                    <tr class="order-total">
                                        <th>ราคาสุทธิ</th>
                                        <td>
                                            <strong><span class="amount">฿ <?=number_format($sumtotal)?></span></strong>
                                            <input type="hidden" id="price_paypal" name="discount" value="<?php echo !empty($sumtotal) ? $sumtotal : 0;?>">
                                            <input type="hidden" id="payment_type" name="payment_type" value="0">
                                            <input type="hidden" id="deliverys_price" name="deliverys_price" value="<?php echo !empty($deliverys_price) ? $deliverys_price : 0;?>">
                                            <input type="hidden" id="premise" name="premise" value="">
                                            <input type="hidden" id="paypal_process" name="paypal_process" value="0">
                                            <input type="hidden" id="status" name="status" value="">
                                            
                                        </td>
                                    </tr>								
                                </tfoot>
                            </table>
                        </div>
                        <h3>บริษัทจัดส่ง</h3>
                        <div class="your-order-table table-responsive">
                            <div class="checkout-form-list">
                                <?php 
                                if(!empty($transport)):
                                    
                                    foreach ($transport as $key => $transports):
                                        if($key == 0):
                                            $checked = 'checked';
                                        else:
                                            $checked = '';
                                        endif;
                                ?>
                                    <input type="checkbox" <?=$checked?> id="transport_<?php echo !empty($transports->transport_id) ? $transports->transport_id : '';?>" class="text-input-required text-input-transport" name="transport_id" value="<?php echo !empty($transports->transport_id) ? $transports->transport_id : '';?>"/>
                                    <label for="transport_<?php echo !empty($transports->transport_id) ? $transports->transport_id : '';?>" style="display: unset;"><span></span><?php echo !empty($transports->title) ? $transports->title : '';?></label>
                                <?php 
                                    endforeach;
                                endif;
                                ?> 
                            </div>
                        </div>
                        <div class="payment-method">
                            <div class="payment-accordion">
                                <div class="panel-group" id="faq">
                                    <div class="panel panel-default hidden">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"><a data-toggle="collapse" aria-expanded="true" data-parent="#faq" href="#payment-1">โอนเงินผ่านธนาคาร</a></h5>
                                        </div>
                                        <div id="payment-1" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <p><span>1.โอนเงินผ่านบัญชีบริษัท</span></p>
                                                <p><span>2.กรอกยอดโอน วันที่และเวลาโอน</span></p>
                                                <p><span>3.แนบหลักฐานการโอนเงิน</span></p>
                                            </div>
                                            <div class="order-button-payment">
                                                <input type="button" id="btn-payment-order" class="contact-btn btn-hover" value="แนบไฟล์หลักฐานการโอนเงิน" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default hidden" style="display: none;">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"><a class="collapsed" data-toggle="collapse" aria-expanded="false" data-parent="#faq" href="#payment-3">ชำระด้วยบัตรเครดิต</a></h5>
                                        </div>
                                        <div id="payment-3" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <p><span>ชำระเงินผ่านบัตรเครดิต</span></p>
                                            </div>
                                            <div class="order-button-payment">
                                                <div id="paypal-button-container"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title"><a class="collapsed" data-toggle="collapse" aria-expanded="false" data-parent="#faq" href="#payment-2">เก็บเงินปลายทาง</a></h5>
                                        </div>
                                        <div id="payment-2" class="panel-collapse collapse show">
                                            <div class="panel-body">
                                                <p><span>สินค้าบางรายการอาจมีค่าบริการเก็บเงินปลายทางเพิ่มเติม (ตามยอดค่าจัดส่ง)</span></p>
                                            </div>
                                            <div class="order-button-payment">
                                                <input type="button" id="btn-payment-delivery" class="contact-btn btn-hover" value="เก็บเงินปลายทาง" />
                                            </div>
                                        </div>
                                    </div>
                                </div>								
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-order-cart" tabindex="-1" role="dialog" aria-hidden="true">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="pe-7s-close" aria-hidden="true"></span>
                </button>
                <div class="modal-dialog modal-quickview-width" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">เลือกธนาคาร</h4>
                    </div>
                        <div class="modal-body">
                            <div class="your-order-table"> 
                                <div class="row">
                                    <div class="col-md-5">
                                    <?php 
                                        if(!empty($infoBanks)):
                                            foreach($infoBanks as $banks):
                                        ?>
                                        <div class="media">
                                            <div class="media-left" style="margin-right: 10px;">
                                                <a href="javascript:void(0)" class="thumbnail">
                                                    <img src="<?php echo !empty($banks->file) ? base_url($banks->file) : '';?>" style="width: 80px;" 
                                                    alt="" 
                                                    onerror="this.src='<?=base_url('template/frontend/assets/img/quick-view/s1.jpg');?>'">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading">ธนาคาร</h5>
                                                <p><strong>สาขา</strong> : <?php echo !empty($banks->branch) ? $banks->branch : '';?></p>
                                                <p><strong>เลขที่บัญชี</strong> :
                                                    <div class="input-group">
                                                        <input type="text" id="Clipboard" class="form-control" value="<?php echo !empty($banks->accountNumber) ? $banks->accountNumber : '';?>">
                                                        <div onclick="Clipboard()" class="input-group-append" style="padding: 7px;background: #edeff8;border: 1px solid #ced4da;cursor: pointer;" data-toggle="tooltip" title="Copy !">
                                                            <span class="input-group-text"><i class="ti-files" style="font-size: 22px;"></i></span>
                                                        </div>
                                                    </div>
                                                </p>
                                                <p><strong>ประเภทบัญชี</strong> : <?php echo !empty($banks->type) ? $banks->type : '';?></p>
                                                <p><strong>ชื่อบัญชี</strong> : <?php echo !empty($banks->name) ? $banks->name : '';?></p>
                                            </div>
                                        </div>
                                        <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </div>
                                    <div class="col-md-7" style="background-color: #ececec36; padding: 20px; border-radius: 10px;">
                                        <div class="form-group">
                                            <label for="transfer_amount">ยอดที่โอน</label>
                                            <input type="number" id="transfer_amount" name="transfer_amount" placeholder="ยอดที่โอน" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="transfer_datetime">วันที่เวลาที่โอน</label>
                                            <input type="text" id="transfer_datetime" name="transfer_datetime" placeholder="วันที่เวลาที่โอน" required/>
                                        </div>
                                        <div class="form-group">
                                            <label for="files">ใบเสร็จแจ้งโอน</label>
                                            <input type="file" id="files" name="file" placeholder="ใบเสร็จแจ้งโอน" required/>
                                        </div>
                                    </div>
                                </div>  
                            </div> 
                        </div>
                        <div class="modal-footer coupon-all">
                            <div class="coupon2 order-button-payment">
                                <input id="btn-transfer" class="button contact-btn btn-hover" name="" value="ชำระเงิน" type="button">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- checkout-area end -->
