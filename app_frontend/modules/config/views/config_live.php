<?php
if(!empty($live['active'])):
?>
<!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>
<div class="banner-area pt-60 pb-60">
    <div class="container">
        <div class="section-title text-center mb-60 mt-60">
            <h2>Live สด</h2>
        </div>
        <div class="row">
            <div class="ml-auto col-lg-12">
                <div class="offer-img">
                    <!-- <iframe
                        src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fmeedeephoneshop%2Fvideos%2F435926573855290%2F&show_text=0&width=269"
                        width="330" height="376" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"
                        allowFullScreen="true"></iframe> -->

                        <?=!empty($live['url_live']) ? html_entity_decode($live['url_live']) : '';?>

                </div>
            </div>
            <div class="col-lg-12">
                <div class="">
                    <?php echo !empty($live['detail']) ? html_entity_decode($live['detail']) : '';?>
                    <div class="offer-price-btn">
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
endif;
?>