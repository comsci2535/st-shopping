<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends MX_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('config_m');
    }
    
    public function config_home()
    {
        $type = 'live';
        $info = $this->config_m->get_config($type);
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        }
        $data['live'] = $temp;
        $this->load->view('config_live', $data);
    }
}
