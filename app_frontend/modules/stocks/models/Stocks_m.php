<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stocks_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_stock_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        if (isset($param['attributes_group'])):
            $this->db->where('a.attributes_group', $param['attributes_group']);
        endif;

        if (isset($param['attributes_option'])):
            $this->db->where('a.attributes_option', $param['attributes_option']);
        endif;

        $this->db->order_by('a.product_price', 'ASC');

        $query = $this->db
                        ->select('a.*')
                        ->from('stocks a')
                        ->get();
        return $query;
    }

    public function get_stock_detail_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        $query = $this->db
                        ->select('a.*')
                        ->from('stocks_detail a')
                        ->get();
        return $query;
    }

    public function count_stock_detail_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;
        
        return $this->db->count_all_results('stocks_detail a');;
    }

    public function get_stock_detail_join_attributes_group($param)
    {
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        if (isset($param['attributes_group'])):
            $this->db->where('a.attributes_group', $param['attributes_group']);
        endif;

        if (isset($param['attributes_option'])):
            $this->db->where('a.attributes_option', $param['attributes_option']);
        endif;

        $query = $this->db
                        ->select('a.*,b.title as attributes_title')
                        ->from('stocks_detail a')
                        ->join('product_attributes b', 'a.attributes_group = b.product_attribute_id')
                        ->get();
        $query_arr = array();
        if(!empty($query)):
            foreach($query->result() as $item):
                $query_arr[$item->attributes_group] = array(
                    'stock_detail_id'   => $item->stock_detail_id,
                    'stock_id'          => $item->stock_id,
                    'product_id'        => $item->product_id,
                    'attributes_group'  => $item->attributes_group,
                    'attributes_title'  => $item->attributes_title
                );
            endforeach;
        endif;

        return $query_arr;
    }

    public function get_stock_detail_join_attributes_option($param)
    {
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        if (isset($param['attributes_group'])):
            $this->db->where('a.attributes_group', $param['attributes_group']);
        endif;

        if (isset($param['attributes_option'])):
            $this->db->where('a.attributes_option', $param['attributes_option']);
        endif;

        $query = $this->db
                        ->select('a.*,b.title as attributes_title')
                        ->from('stocks_detail a')
                        ->join('product_attributes b', 'a.attributes_option = b.product_attribute_id')
                        ->get();
        $query_arr = array();
        if(!empty($query)):
            foreach($query->result() as $item):
                $query_arr[$item->attributes_group][$item->attributes_option] = array(
                    'stock_detail_id'   => $item->stock_detail_id,
                    'stock_id'          => $item->stock_id,
                    'product_id'        => $item->product_id,
                    'attributes_option' => $item->attributes_option,
                    'attributes_title'  => $item->attributes_title
                );
            endforeach;
        endif;

        return $query_arr;
    }

    public function get_stock_join_attributes_option($param)
    {
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        if (isset($param['attributes_group'])):
            $this->db->where_in('a.attributes_group', $param['attributes_group']);
        endif;

        if (isset($param['attributes_option'])):
            $this->db->where_in('a.attributes_option', $param['attributes_option']);
        endif;

        $query = $this->db
                        ->select('a.*')
                        ->from('stocks a')
                        ->get();

        $query_arr = array();
        if(!empty($query)):
            foreach($query->result() as $item):

                $option_arr     = explode(',',$item->attributes_option);

                $options_arr    = array();
                $group_arr      = array();

                if(!empty($option_arr)):
                    foreach($option_arr as $option):
                        $this->db->where('product_attribute_id', $option);
                        $query_option = $this->db->get('product_attributes')->row();
                        if(!empty($query_option)):

                            $this->db->where('product_attribute_id', $query_option->parent_id);
                            $query_group = $this->db->get('product_attributes')->row();
                            
                            $group_arr[$query_group->product_attribute_id] = array(
                                'title'     => !empty($query_group->title) ? $query_group->title: '',
                                'product_attribute_id' => !empty($query_group->product_attribute_id) ? $query_group->product_attribute_id: ''
                            );

                            $options_arr[$query_option->product_attribute_id] = array(
                                'title'     => !empty($query_option->title) ? $query_option->title: '',
                                'product_attribute_id' => !empty($query_option->product_attribute_id) ? $query_option->product_attribute_id: '',
                                'parent_id' => !empty($query_option->parent_id) ? $query_option->parent_id: ''
                            );
                        endif;
                    endforeach;
                endif;

                $query_arr[] = array(
                    'option' => $options_arr,
                    'group'  => $group_arr
                );

                // $query_arr[$item->attributes_group][$item->attributes_option] = array(
                //     'stock_detail_id'   => $item->stock_detail_id,
                //     'stock_id'          => $item->stock_id,
                //     'product_id'        => $item->product_id,
                //     'attributes_option' => $item->attributes_option,
                //     'attributes_title'  => $item->attributes_title
                // );
            endforeach;
        endif;

        return $query_arr;
    }
   
}
