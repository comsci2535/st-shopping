<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_orders($param) 
    {
          
        if(!empty($param['user_id'])):
            if(!empty($param['name'])):
                $this->db->where('a.name', $param['name']);  
            endif;
            if(!empty($param['lasname'])):
                $this->db->where('a.lasname', $param['lasname']); 
            endif;
            $this->db->where('a.user_id', $param['user_id']);
        else:
            $this->db->where('a.name', $param['name']);  
            $this->db->where('a.lasname', $param['lasname']);  
        endif;
        
        $this->db->where('a.recycle', 0);
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query;
    }

    public function get_orders_param($param) 
    {
         
        if(!empty($param['order_id'])):
            $this->db->or_where('a.order_id', $param['order_id']);
        endif;
        if(!empty($param['user_id'])):
            $this->db->or_where('a.user_id', $param['user_id']);
        endif;
        
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query;
    }

    public function get_orders_detail_by_order_id($order_id)
    {
        $this->db->where('a.order_id', $order_id);
        $query = $this->db
                ->select('a.*,b.title, b.excerpt')
                ->from('orders_detail a')
                ->join('products b','a.product_id = b.product_id')
                ->get();
        return $query; 
    }

    public function get_orders_status_action_by_order_id($order_id, $status)
    {
        $this->db->where('a.order_id', $order_id);
        $this->db->where_in('a.status', $status);
        $this->db->where_in('a.is_process', 0);
        
        $query = $this->db
                ->select('a.*,b.title as status_title')
                ->from('orders_status_action a')
                ->join('status b','a.status = b.status_id', 'left')
                ->order_by('order_status_action_id', 'asc')
                ->get();
        return $query; 
    }

    public function get_orders_product_by_id($product_id)
    {
        $query = $this->db
                ->select_sum('quantity')
                ->from('orders_detail')
                ->where('product_id', $product_id)
                ->get();
        return $query; 
    }
    
    public function insert($value) 
    {
        $this->db->insert('orders', $value);
        return $this->db->insert_id();
    }

    public function insert_orders_user_address($value) 
    {
        $this->db->insert('orders_user_address_send', $value);
        return $this->db->insert_id();
    }
    
    public function insert_orders_status_action($value) 
    {
        $this->db->insert('orders_status_action', $value);
        return $this->db->insert_id();
    }

    public function insert_orders_detail($value) 
    {
        return $this->db->insert_batch('orders_detail', $value);
    }

    public function insert_invoice($value) 
    {
        $this->db->insert('invoice', $value);
        return $this->db->insert_id();
    }

    public function get_invoice()
    {
        $query = $this->db->select('*')
                ->from('invoice')
                ->order_by('invoice_number', 'DESC')
                ->get();
        return $query;
    }

    public function get_transport_by_id($id)
    {
        $query = $this->db->select('*')
                ->from('transports')
                ->where('transport_id', $id)
                ->get();
        return $query;
    }

    public function get_transport_all()
    {
        $query = $this->db->select('*')
                ->from('transports')
                ->where('active', 1)
                ->where('recycle', 0)
                ->get();
        return $query;
    }

    public function get_orders_user_address_send_by_id($id)
    {
        $query = $this->db->select('*')
                ->from('orders_user_address_send')
                ->where('user_id', $id)
                ->get();
        return $query;
    }

    public function set_order_id()
    {

        $query = "INSERT
        INTO `auto_order_id`
        (`id`)
        VALUES (
            # ใช้ฟังก์ชั่นนี้เชื่อมต่อสตริงเข้าด้วยกัน
            CONCAT(          
                # ปีและเดือนของเวลาปัจจุบันตามด้วยขีด เช่น 2013-03-
                DATE_FORMAT(NOW(), '%Y%m'),
                # ใช้ LPAD() เพื่อเติมตัวอักษรตามที่ต้องการเข้าข้างหน้าตัวเลข (ในที่นี้คือ 0)
                LPAD(
                    IFNULL(
                        # Sub Query ที่จะเลือก 'ตัวเลขสุดท้าย' ของ id ในปีปัจจุบัน
                        (SELECT
                            # จำนวนสูงจุดของ ส่วนหลังเครื่องหมาย - ของ id
                            MAX(SUBSTR(`id`, 9))
                            FROM `auto_order_id` AS `alias`
                            # โดยหาเฉพาะปีและเดือนปัจจุบัน
                            WHERE SUBSTR(`id`, 1, 6) = DATE_FORMAT(NOW(), '%Y%m')
                            # เรียงตามลำดับ id จากมากไปหาน้อย เพื่อเอาค่าล่าสุดออกมา
                            ORDER BY `id` DESC
                            LIMIT 1
                        )
                        # และ + ด้วย 1 เสมอ ซึ่งจะทำให้ได้เลขที่เรียงกันไป
                        + 1,
                        # หาก Sub Query ข้างบนคืนแถวกลับมาเป็น NULL ก็ให้ใช้ค่า 1 (เริ่มแถวแรกของปี)
                        1
                    ),
                    6,  # โดยให้เป็นตัวเลข 5 หลัก
                    '0' # ตัวอักษรที่จะเติมข้างหน้าตัวเลข
                )
            )
        )";
        $order_code = null;
        $query = $this->db->query($query);
        if($query){
            $this->db->select_max('id','order_code');
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);
            $order_code = $this->db->get('auto_order_id')->row();
        }
        return $order_code;
    }

    public function set_invoice_id()
    {

        $query = "INSERT
        INTO `auto_invoice_id`
        (`id`)
        VALUES (
            # ใช้ฟังก์ชั่นนี้เชื่อมต่อสตริงเข้าด้วยกัน
            CONCAT(          
                # ปีและเดือนของเวลาปัจจุบันตามด้วยขีด เช่น 2013-03-
                DATE_FORMAT(NOW(), 'Rcpt%y%m'),
                # ใช้ LPAD() เพื่อเติมตัวอักษรตามที่ต้องการเข้าข้างหน้าตัวเลข (ในที่นี้คือ 0)
                LPAD(
                    IFNULL(
                        # Sub Query ที่จะเลือก 'ตัวเลขสุดท้าย' ของ id ในปีปัจจุบัน
                        (SELECT
                            # จำนวนสูงจุดของ ส่วนหลังเครื่องหมาย - ของ id
                            MAX(SUBSTR(`id`, 11))
                            FROM `auto_invoice_id` AS `alias`
                            # โดยหาเฉพาะปีและเดือนปัจจุบัน
                            WHERE SUBSTR(`id`, 1, 8) = DATE_FORMAT(NOW(), 'Rcpt%y%m')
                            # เรียงตามลำดับ id จากมากไปหาน้อย เพื่อเอาค่าล่าสุดออกมา
                            ORDER BY `id` DESC
                            LIMIT 1
                        )
                        # และ + ด้วย 1 เสมอ ซึ่งจะทำให้ได้เลขที่เรียงกันไป
                        + 1,
                        # หาก Sub Query ข้างบนคืนแถวกลับมาเป็น NULL ก็ให้ใช้ค่า 1 (เริ่มแถวแรกของปี)
                        1
                    ),
                    6,  # โดยให้เป็นตัวเลข 5 หลัก
                    '0' # ตัวอักษรที่จะเติมข้างหน้าตัวเลข
                )
            )
        )";
        $invoice_code = null;
        $query = $this->db->query($query);
        if($query){
            $this->db->select_max('id','invoice_code');
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);
            $invoice_code = $this->db->get('auto_invoice_id')->row();
        }
        return $invoice_code;
    }
   
}
