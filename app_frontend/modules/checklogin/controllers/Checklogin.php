<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checklogin extends MX_Controller {
    function __construct() {
        parent::__construct();
        
        // Load facebook library
        $this->load->library('facebook');
        
        //Load user model
        $this->load->model('checklogin_m');
    }

    public function login_link(){
        $data['authURL'] =  $this->facebook->login_url();
        return $data;
    }
    
    public function login_face(){
        $userData = array();
        
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['fname']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['lname']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            // $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['oauth_picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['file']        = !empty($fbUser['link'])?$fbUser['link']:'';
            
            // Insert or update user data
            $userID = $this->checklogin_m->checkUser($userData);
            
            // Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $userData['UID']  = $userID;
                $this->session->set_userdata('userData', $userData);
            }else{
               $data['userData'] = array();
            }
            
            // Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        }else{
            // Get login URL
            $data['authURL'] =  $this->facebook->login_url();
        }
        
        // Load login & profile view
        // return $data;
        redirect(site_url(), 'refresh'); 
        // $this->load->view('profile',$data);
    }

    public function logout_face() {
        // Remove local Facebook session
        $this->facebook->destroy_session();
        // Remove user data from session
        $this->session->unset_userdata('userData');
        // Redirect to login page
        redirect();
    }
}