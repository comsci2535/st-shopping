<?php echo Modules::run('banners/banners_home', 'contact');?>
<!-- shopping-cart-area start -->
<div class="contact-area ptb-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="contact-map-wrapper">
                    <div class="contact-map mb-40">
                        <!-- <div id="hastech2"></div> -->
                        <?php echo !empty($contact['google_map']) ? html_entity_decode($contact['google_map']) : '';?>
                    </div>
                    <div class="contact-message">
                        <div class="contact-title">
                            <h4>ข้อมูลติดต่อ</h4>
                        </div>
                        <form id="form-submit-contact" class="contact-form" action="#" method="post" autocomplete="off">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="contact-input-style mb-30">
                                        <label>ชื่อ*</label>
                                        <input id="fname" name="fname" required type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="contact-input-style mb-30">
                                        <label>นามสกุล*</label>
                                        <input id="lname" name="lname" required type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="contact-input-style mb-30">
                                        <label>อีเมล*</label>
                                        <input id="email" name="email" required type="email">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="contact-input-style mb-30">
                                        <label>หมายเลขโทรศัพท์</label>
                                        <input id="phone" name="phone" type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="contact-input-style mb-30">
                                        <label>หัวข้อการติดต่อ*</label>
                                        <input id="title" name="title" required type="text">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="contact-textarea-style mb-30">
                                        <label>รายละเอียด*</label>
                                        <textarea id="detail" class="form-control2" name="detail" required></textarea>
                                    </div>
                                    <button class="submit contact-btn btn-hover" type="submit">
                                        ส่งข้อมูล 
                                    </button>
                                </div>
                            </div>
                        </form>
                        <p class="form-messege"></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="contact-info-wrapper">
                    <div class="contact-title">
                        <h4>สถานที่ตั้ง & รายละเอียด</h4>
                    </div>
                    <div class="contact-info">
                        <div class="single-contact-info">
                            <div class="contact-info-icon">
                                <i class="ti-location-pin"></i>
                            </div>
                            <div class="contact-info-text">
                                <p><span>สถานที่ตั้ง:</span>  <?php echo !empty($contact['detail']) ? $contact['detail'] : '';?></p>
                            </div>
                        </div>
                        <div class="single-contact-info">
                            <div class="contact-info-icon">
                                <i class="pe-7s-mail"></i>
                            </div>
                            <div class="contact-info-text">
                                <p><span>อีเมล: </span> <?php echo !empty($contact['email']) ? $contact['email'] : '';?></p>
                            </div>
                        </div>
                        <div class="single-contact-info">
                            <div class="contact-info-icon">
                                <i class="pe-7s-mail"></i>
                            </div>
                            <div class="contact-info-text">
                                <p><span>ไลน์: </span> <?php echo !empty($contact['line']) ? $contact['line'] : '';?></p>
                            </div>
                        </div>
                        <div class="single-contact-info">
                            <div class="contact-info-icon">
                                <i class="pe-7s-mail"></i>
                            </div>
                            <div class="contact-info-text">
                                <p><span>Facebook: </span> <?php echo !empty($contact['facebook']) ? $contact['facebook'] : '';?></p>
                            </div>
                        </div>
                        <div class="single-contact-info">
                            <div class="contact-info-icon">
                                <i class="pe-7s-call"></i>
                            </div>
                            <div class="contact-info-text">
                                <p><span>หมายเลขโทรศัพท์: </span>  <?php echo !empty($contact['phone']) ? $contact['phone'] : '';?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script>
    var latitude    = <?php echo !empty($contact['latitude']) ? $contact['latitude'] : 13.756331;?>;
    var longitude   = <?php echo !empty($contact['longitude']) ? $contact['longitude'] : 100.501762;?>;
</script> -->