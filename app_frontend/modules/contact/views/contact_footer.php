<div class="col-xl-6 col-md-6">
    <div class="footer-widget mb-40">
        <h3 class="footer-widget-title">ติดต่อเรา</h3>
        <div class="footer-newsletter">
            <p><?php echo !empty($contact['detail']) ? $contact['detail'] : '';?></p>
            <br>
            <div class="footer-contact">
                <?php
                if(!empty($contact['email'])):
                ?>
                <p>
                    <span>
                        <i class="ti-email"></i>
                    </span> 
                    <?php echo !empty($contact['email']) ? $contact['email'] : '';?>
                </p>
                <?php
                endif;
                ?>
                <?php
                if(!empty($contact['line'])):
                ?>
                <p>
                    <span>
                        <img src="<?=base_url('template/frontend/img/line.png');?>" style="width: 23px;">
                    </span> 
                    <a href="<?php echo !empty($contact['link_line']) ? $contact['link_line'] : 'javascript:void(0)';?>" style="color: #999999;" target="<?php echo !empty($contact['link_line']) ? '_blank' : '';?>">
                    <?php echo !empty($contact['line']) ? $contact['line'] : '';?>
                    </a>
                </p>
                <?php
                endif;
                ?>
                <?php
                if(!empty($contact['phone'])):
                ?>
                <p>
                    <span>
                        <i class="ti-headphone-alt "></i>
                    </span> 
                    +88 <?php echo !empty($contact['phone']) ? $contact['phone'] : '';?>
                </p>
                <?php
                endif;
                ?>
            </div>
        </div>
    </div>
</div>