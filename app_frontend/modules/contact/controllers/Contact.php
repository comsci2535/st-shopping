<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MX_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('seos/seos_m');
        $this->load->model('contact/contact_m');
        $this->load->model('config/config_m');
        
    }
    
    private function seo()
    {

        $obj_seo = $this->seos_m->get_seos_by_display_page('home')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | ซื้อของออนไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {
        $type = 'contactus';
        $info = $this->config_m->get_config($type);
        
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        }
        
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'contact',
            'footer'  => 'footer',
            'function'=>  array('contact'),
        );
        
        $data['contact'] = $temp;
       
        $this->load->view('template/body', $data);
    }

    public function contact_footer()
    {
        $type = 'contactus';
        $info = $this->config_m->get_config($type);
        
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        }

        $data['contact'] = $temp;
        $this->load->view('contact_footer', $data);
    }
    
    public function ajax_contact_save()
    {
        $data   = array();
        $status = 0;
        $input  = $this->input->post(null, true);
        $value  = $this->_build_data($input);
        $result = $this->contact_m->insert($value);
        if ( $result ) {
            $status = 1;
        }

        $data['status'] = $status;

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }

    private function _build_data($input)
    {
        
        $value['fname']         = $input['fname'];
        $value['lname']         = $input['lname'];
        $value['email']         = $input['email'];
        $value['phone']         = $input['phone'];
        $value['title']         = $input['title'];
        $value['detail']        = $input['detail'];
        $value['active']        = 1;
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = 0;
        $value['updated_at']    = db_datetime_now();
        $value['updated_by']    = 0;
       
        return $value;
    }
}
