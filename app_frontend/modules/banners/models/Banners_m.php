<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Banners_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_banners($param)
    {
        
        if(!empty($param['banner_id'])):
            $this->db->where('a.banner_id', $param['banner_id']);
        endif;

        if(!empty($param['type'])):
            $this->db->where('a.type', $param['type']);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('a.*');
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db->get('banners a');
        return $query;
    }
   
}
