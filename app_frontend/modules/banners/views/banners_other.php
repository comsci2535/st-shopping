<div class="slider-area">
    <div class="sm-none slider-active owl-carousel">
    <?php
    if(!empty($info)):
        foreach($info as $item):
            $img = !empty($item->file) ? base_url($item->file) : base_url("template/frontend/assets/img/product/fashion-colorful/1.jpg");
    ?>
    <div class="item">
    <a <?php if ($item->link) {echo 'href="'.$item->link.'" target="_blank" ';} else {echo 'href="#" ';}?>>
        <!-- <div class="single-slider-4 slider-height-5 bg-img" style="background-image: url(<?=$img;?>)">
            <div class="container">
                <div class="slider-content-5 fadeinup-animated"> -->
                    <!-- <h2 class="animated"><?php echo !empty($item->title) ? $item->title : '';?></h2>
                    <p class="animated"><?php echo !empty($item->excerpt) ? $item->excerpt : '';?></p>
                    <a class="handicraft-slider-btn btn-hover animated" href="product-details.html">Shop Now</a> -->
                <!-- </div>
            </div>
        </div> -->
        <div class="container-fluid" style="padding: 0 8vh;">
            <img src="<?=$img;?>" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'" alt="">
        </div>
    </a>
    </div>
    <div class="item">
    <a <?php if ($item->link) {echo 'href="'.$item->link.'" target="_blank" ';} else {echo 'href="#" ';}?>>
        <!-- <div class="single-slider-4 slider-height-5 bg-img" style="background-image: url(<?=$img;?>)">
            <div class="container">
                <div class="slider-content-5 fadeinup-animated"> -->
                    <!-- <h2 class="animated"><?php echo !empty($item->title) ? $item->title : '';?></h2>
                    <p class="animated"><?php echo !empty($item->excerpt) ? $item->excerpt : '';?></p>
                    <a class="handicraft-slider-btn btn-hover animated" href="product-details.html">Shop Now</a> -->
                <!-- </div>
            </div>
        </div> -->
        <div class="container-fluid" style="padding: 0 8vh;">
            <img src="<?=$img;?>" onerror="this.src='<?=base_url('template/frontend/assets/img/product/fashion-colorful/1.jpg');?>'" alt="">
        </div>
    </a>
    </div>
    <?php
        endforeach;
    endif;
    ?>
    </div>

    <div class="sm-block slider-active owl-carousel">
    <?php
    if(!empty($info)):
        foreach($info as $item):
            $img2 = !empty($item->file_mb) ? base_url($item->file_mb) : base_url("template/frontend/assets/img/product/fashion-colorful/1.jpg");
    ?>
    <a <?php if ($item->link) {echo 'href="'.$item->link.'" target="_blank" ';} else {echo 'href="#" ';}?>>
        <div class="single-slider-4 slider-height-5 bg-img" style="background-image: url(<?=$img2;?>)">
            <div class="container">
                <div class="slider-content-5 fadeinup-animated">
                    <!-- <h2 class="animated"><?php echo !empty($item->title) ? $item->title : '';?></h2>
                    <p class="animated"><?php echo !empty($item->excerpt) ? $item->excerpt : '';?></p>
                    <a class="handicraft-slider-btn btn-hover animated" href="product-details.html">Shop Now</a> -->
                </div>
            </div>
        </div>
    </a>
    <?php
        endforeach;
    endif;
    ?>
    </div>
</div>
