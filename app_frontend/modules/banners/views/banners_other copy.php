<?php
$img = !empty($info->file) ? base_url($info->file) : base_url("template/frontend/assets/img/product/fashion-colorful/1.jpg");
?>
<!-- <div class="breadcrumb-area pt-205 breadcrumb-padding pb-210" style="background-image: url(<?=$img?>)">
    <div class="container-fluid">
        <div class="breadcrumb-content text-center">
            <h2> <?php echo !empty($info->title) ? $info->title : '';?></h2>
        </div>
    </div>
</div> -->


<div class="slider-area">
    <div class="slider-active owl-carousel">
    <?php
    
    if(!empty($info)):
        foreach($info as $item):
            $img = !empty($item->file) ? base_url($item->file) : base_url("template/frontend/assets/img/product/fashion-colorful/1.jpg");
    ?>
        <div class="single-slider-4 slider-height-5 bg-img" style="background-image: url(<?=$img;?>)">
            <div class="container">
                <div class="slider-content-5 fadeinup-animated">
                    <h2 class="animated"><?php echo !empty($item->title) ? $item->title : '';?></h2>
                    <p class="animated"><?php echo !empty($item->excerpt) ? $item->excerpt : '';?></p>
                    <a class="handicraft-slider-btn btn-hover animated" href="product-details.html">Shop Now</a>
                </div>
            </div>
        </div>
    <?php
        endforeach;
    endif;
    ?>
    </div>
</div>
