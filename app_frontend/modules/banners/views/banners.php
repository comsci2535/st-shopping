<div class="slider-area ">
    <div class="sm-none slider-active owl-carousel">
        <?php
        $img = !empty($info->file) ? base_url($info->file) : base_url("template/frontend/assets/img/product/fashion-colorful/1.jpg");
        ?>
        <a <?php if ($info->link) {echo 'href="'.$info->link.'" target="_blank" ';} else {echo 'href="#" ';}?>>
            <div class="single-slider single-slider-hm1 bg-img height-100vh" 
                style="background-image: url(<?=$img;?>)">
                <div class="slider-content slider-animation slider-content-style-1 slider-animated-1">
                    <!-- <h1 class="animated"><?php echo !empty($info->title) ? $info->title : '';?></h1>
                    <p class="animated"><?php echo !empty($info->excerpt) ? $info->excerpt : '';?>. </p> -->
                </div>
            </div>
        </a>
    </div>

    <div class="sm-block slider-active owl-carousel">
        <?php
        $img2 = !empty($info->file_mb) ? base_url($info->file_mb) : base_url("template/frontend/assets/img/product/fashion-colorful/1.jpg");
        ?>
        <a <?php if ($info->link) {echo 'href="'.$info->link.'" target="_blank" ';} else {echo 'href="#" ';}?>>
            <div class="single-slider single-slider-hm1 bg-img height-100vh" 
                style="background-image: url(<?=$img2;?>)">
                <div class="slider-content slider-animation slider-content-style-1 slider-animated-1">
                    <!-- <h1 class="animated"><?php echo !empty($info->title) ? $info->title : '';?></h1>
                    <p class="animated"><?php echo !empty($info->excerpt) ? $info->excerpt : '';?>. </p> -->
                </div>
            </div>
        </a>
    </div>
</div>