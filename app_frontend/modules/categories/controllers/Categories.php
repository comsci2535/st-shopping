<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MX_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('seos/seos_m');
        $this->load->model('categories_m');
        
        
    }
    
    private function seo()
    {

        $obj_seo = $this->seos_m->get_seos_by_display_page('home')->row();
        $title          = !empty($obj_seo->title)? config_item('siteTitle').' | '.$obj_seo->title : config_item('siteTitle').' | ซื้อของออนไลน์';
        $robots         = !empty($obj_seo->title)? $obj_seo->title : config_item('siteTitle');
        $description    = !empty($obj_seo->excerpt)? $obj_seo->excerpt : config_item('metaDescription');
        $keywords       = !empty($obj_seo->detail)? $obj_seo->detail : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('images/logo/logo.png');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {
		
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'header'  => 'header_product',
            'content' => 'products',
            'footer'  => 'footer',
            'function'=>  array('products'),
        );

        $this->load->view('template/body', $data);
    }

    public function categories_home()
    {
        $info               = $this->categories_m->get_categorie_by_group()->result();
        $categories         = array();
        $parent_arr         = array();
        if(!empty($info)):
            foreach($info as $item):
                $param_['categorie_id']  = $item->parent_id;
                $infoParent = $this->categories_m->get_categories_by_param($param_)->row();
                if(!empty($infoParent)):
                        $categories[$infoParent->categorie_id] = array(
                            'categorie_id'  => $infoParent->categorie_id,
                            'title'         => $infoParent->title,
                            'slug'          => $infoParent->slug
                        );
                endif;
                
                $param['categorie_id']  = $item->categorie_id;
                $numQty = $this->categories_m->count_products_categorie($param);
                if($numQty > 0){
                    $parent_arr[$item->parent_id][] = array(
                                                'categorie_id'  => $item->categorie_id,
                                                'title'         => $item->title,
                                                'slug'          => $item->slug,
                                                'numQty'        => $numQty
                                            );
                }
            endforeach;
        endif;
        
        $info_arr = array();
        if(!empty($categories)):
            foreach($categories as $item){
                if(!empty($parent_arr[$item['categorie_id']])){
                    array_push($info_arr, array(
                                        'categorie_id' => $item['categorie_id'],
                                        'title'        => $item['title'],
                                        'slug'         => $item['slug'],
                                        'parents'      => $parent_arr[$item['categorie_id']]
                                        ));
                }
            }
        endif;
        $data['infoCate']   = $info_arr;
        $this->load->view('categories_home', $data);
    }

    public function categories_home_mobile()
    {
        $info               = $this->categories_m->get_categorie_by_group()->result();
        $categories         = array();
        $parent_arr         = array();
        if(!empty($info)):
            foreach($info as $item):
                $param_['categorie_id']  = $item->parent_id;
                $infoParent = $this->categories_m->get_categories_by_param($param_)->row();
                if(!empty($infoParent)):
                        $categories[$infoParent->categorie_id] = array(
                            'categorie_id'  => $infoParent->categorie_id,
                            'title'         => $infoParent->title,
                            'slug'          => $infoParent->slug
                        );
                endif;
                
                $param['categorie_id']  = $item->categorie_id;
                $numQty = $this->categories_m->count_products_categorie($param);
                if($numQty > 0){
                    $parent_arr[$item->parent_id][] = array(
                                                'categorie_id'  => $item->categorie_id,
                                                'title'         => $item->title,
                                                'slug'          => $item->slug,
                                                'numQty'        => $numQty
                                            );
                }
            endforeach;
        endif;
        
        $info_arr = array();
        if(!empty($categories)):
            foreach($categories as $item){
                if(!empty($parent_arr[$item['categorie_id']])){
                    array_push($info_arr, array(
                                        'categorie_id' => $item['categorie_id'],
                                        'title'        => $item['title'],
                                        'slug'         => $item['slug'],
                                        'parents'      => $parent_arr[$item['categorie_id']]
                                        ));
                }
            }
        endif;
        $data['infoCate']   = $info_arr;
        $this->load->view('categories_home_mobile', $data);
    }

    public function categories_products()
    {

        
        $info               = $this->categories_m->get_categorie_by_group()->result();
       
        $categories         = array();
        $parent_arr         = array();
        if(!empty($info)):
            foreach($info as $item):
                $param_['categorie_id']  = $item->parent_id;
                $infoParent = $this->categories_m->get_categories_by_param($param_)->row();
                if(!empty($infoParent)):
                        $categories[$infoParent->categorie_id] = array(
                            'categorie_id'  => $infoParent->categorie_id,
                            'title'         => $infoParent->title,
                            'slug'          => $infoParent->slug
                        );
                endif;
                
                $param['categorie_id']  = $item->categorie_id;
                $numQty = $this->categories_m->count_products_categorie($param);
                if($numQty > 0){
                    $parent_arr[$item->parent_id][] = array(
                        'categorie_id'  => $item->categorie_id,
                        'title'         => $item->title,
                        'slug'          => $item->slug,
                        'numQty'        => $numQty
                    );
                }
                
            endforeach;
        endif;
        
        $info_arr = array();
        if(!empty($categories)):
            foreach($categories as $item){
                if(!empty($parent_arr[$item['categorie_id']])){
                    array_push($info_arr, array(
                        'categorie_id' => $item['categorie_id'],
                        'title'        => $item['title'],
                        'slug'         => $item['slug'],
                        'parents'      => $parent_arr[$item['categorie_id']]
                        ));
                }
                
            }
        endif;

        $data['info']       = $info_arr;
        $this->load->view('categories_products', $data);
    }
}
