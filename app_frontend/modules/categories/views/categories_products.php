
<div class="sidebar-widget mb-45">
    <h3 class="sidebar-title">ประเภท</h3>
    <div class="sidebar-categories">
        <ul>
            <?php 
            if(!empty($info)):
                foreach($info as $items):
                    if($this->input->get('groug') == $items['slug']){
                        $actives = 'active';
                    }else{
                        $actives = '';
                    }
            ?>
            <li class="tab-menu-categories" data-id="categories-<?php echo !empty($items['categorie_id']) ? $items['categorie_id'] : '';?>" style="border-bottom: 1px solid #c1c1c1;">
                <a href="<?php echo !empty($items['slug']) ? base_url('products?groug='.$items['slug']) : '#';?>"
                class="<?=$actives?>"
                >
                    <i class="ti-control-record"></i> 
                    <?php echo !empty($items['title']) ? $items['title'] : '';?> 
                </a>
                <i class="ti-angle-down click-tab-menu-up-down" data-up="0" style="float: right;cursor: pointer;"></i>
            </li>

                <?php
                
                if(!empty($items['parents'])):
                    $active = '';
                    foreach($items['parents'] as $list):
                        if($this->input->get('cat') == $list['slug']){
                            $active = 'active';
                        }else{
                            $active = '';
                        }
                ?>
                <li class="categories-<?php echo !empty($items['categorie_id']) ? $items['categorie_id'] : '';?>">
                    <a href="<?php echo !empty($list['slug']) ? base_url('products?cat='.$list['slug']) : '#';?>" 
                    class="btn-fiter-categories <?=$active?>" 
                    data-id="<?php echo !empty($list['categorie_id']) ? $list['categorie_id'] : '';?>" 
                    style="margin-left: 18px;">
                    <?php echo !empty($list['title']) ? $list['title'] : '';?>
                    <span><?php echo !empty($list['numQty']) ? $list['numQty'] : 0;?></span></a>
                </li>
                <?php
                    endforeach;
                endif;
                ?>
            <?php
                endforeach;
            endif;
            ?>
        </ul>
    </div>
</div>