
<div class="category-menu-dropdown shop-menu">
    <?php
    
    if(!empty($infoCate)):
        foreach($infoCate as $item):
            if($this->input->get('groug') == $item['slug']){
                $activesc = '#fb6830';
            }else{
                $activesc = '#505050';
            }
    ?>
    <div class="category-dropdown-style category-common2 mb-30">
        <h4 class="categories-subtitle"> 
            <a href="<?php echo !empty($item['slug']) ? base_url('products?groug='.$item['slug']) : '#';?>"
            style="color:<?=$activesc?>;">
            <?php echo !empty($item['title']) ? $item['title'] : '';?></a>
        </h4>
        <ul>
            <?php
           if(!empty($item['parents'])):
            $active = '';
            foreach($item['parents'] as $list):
                if($this->input->get('cat') == $list['slug']){
                    $active = 'active';
                }else{
                    $active = '';
                }
            ?>
            <li>
                <a href="<?php echo !empty($list['slug']) ? base_url('products?cat='.$list['slug']) : '#';?>" 
                    class="<?=$active?>"> 
                    <?php echo !empty($list['title']) ? $list['title'] : '';?>
                </a>
            </li>
            <?php
                endforeach;
            endif;
            ?>
        </ul>
    </div>
    <?php
        endforeach;
    endif;
    ?>
</div>