<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categories_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_categories_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['categorie_id'])):
            $this->db->where('a.categorie_id', $param['categorie_id']);
        endif;

        $query = $this->db
                        ->select('a.*')
                        ->from('product_categorie_map a')
                        ->get();
        return $query;
    }

    public function get_categories_by_param($param) 
    {
        
        if (isset($param['parent_id'])):
            $this->db->where('a.parent_id', $param['parent_id']);
        endif;

        if (isset($param['categorie_id'])):
            $this->db->where('a.categorie_id', $param['categorie_id']);
        endif;

        if (isset($param['slug'])):
            $this->db->where('a.slug', $param['slug']);
        endif;
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('categories a')
                        ->get();
        return $query;
    }

    public function get_categories_join_products($param)
    {
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['categorie_id'])):
            $this->db->where('a.categorie_id', $param['categorie_id']);
        endif;

        if(isset($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;

        $this->db->where('b.active', 1);
        $this->db->where('b.recycle', 0);
        $this->db->order_by('b.created_at', 'DESC');
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('product_categorie_map a')
                        ->join('products b', 'a.product_id = b.product_id')
                        ->get();
        return $query;
    }

    public function get_categorie_by_parent_id($param)
    {
        $this->db
                ->order_by('parent_id', 'asc')
                ->order_by('order', 'asc')
                ->order_by('categorie_id', 'asc'); 
        $query = $this->db
                        ->from('categories a')
                        ->select('a.categorie_id, a.title, a.parent_id, a.order')
                        ->where('a.recycle', $param['recycle'])
                        ->where('a.active', $param['active'])
                        ->where('a.parent_id', 0)
                        ->get()->result();
        
        if(!empty($query)):
            foreach($query as $item):
                $item->parent = $this->db
                                ->from('categories a')
                                ->select('a.categorie_id, a.title, a.parent_id, a.order, a.slug')
                                ->where('a.recycle', $param['recycle'])
                                ->where('a.active', $param['active'])
                                ->where('a.parent_id', $item->categorie_id)
                                ->order_by('parent_id', 'asc')
                                ->order_by('order', 'asc')
                                ->order_by('categorie_id', 'asc')
                                ->get()->result();
            endforeach;
        endif;
                        
        return $query;
    }

    public function count_products_categorie($param) 
    {
        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if(!empty($param['categorie_id'])):
            $this->db->where('a.categorie_id', $param['categorie_id']);
        endif;

        $this->db->where('b.recycle', 0);
        $this->db->where('b.active', 1);
        $this->db->join('products b', 'a.product_id = b.product_id');
        return $this->db->count_all_results('product_categorie_map a');
    }

    public function get_categorie_by_group()
    {
        $query = $this->db->query('SELECT * 
                        FROM
                            categories as a
                        INNER JOIN product_categorie_map as b on a.categorie_id = b.categorie_id
                        WHERE
                            a.parent_id IN ( SELECT categorie_id FROM categories WHERE parent_id = 0 ) 
                            AND a.active = 1 AND a.recycle = 0
                            GROUP BY a.categorie_id');
        return $query;
    }

    public function get_categorie_by_group_slug($slug)
    {
        $query = $this->db->query('SELECT
        a.*
        FROM
            categories AS a 
        WHERE
        a.parent_id = ( SELECT b.categorie_id FROM categories AS b WHERE b.parent_id = 0 and b.slug ="'.$slug.'" and b.active = 1 and b.recycle = 0)
        and a.active = 1 and a.recycle = 0
        ORDER BY a.parent_id');
        return $query;
    }

    
}
