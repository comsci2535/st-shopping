<?php
if(!empty($reviews)):
    foreach($reviews as $review):
?>
<div class="single-blog-replay">
    <div class="replay-img">
        <img class="img-thumbnail" src="<?php echo !empty($review->file) ? $review->file : '';?>" 
        onerror="this.src='<?=base_url('template/frontend/assets/img/user_null.png');?>'"
        alt="">
    </div>
    <div class="replay-info-wrapper">
        <div class="replay-info">
            <div class="replay-name-date">
                <h5><?php echo !empty($review->title) ? $review->title : '';?></h5>
                <span><?php echo !empty($review->created_at) ? DateThaiTime($review->created_at) : '';?></span>
            </div>
        </div>
        <p><?php echo !empty($review->excerpt) ? $review->excerpt : '';?></p>
        <?php
        if(!empty($review->review_file)):
        ?>
        <p><img class="img-thumbnail" src="<?php echo !empty($review->review_file) ? base_url($review->review_file) : '';?>" 
        style="width: 200px;" onerror="this.src='<?=base_url('images/no-image.png');?>'"
        alt=""></p>
        <?php
        endif;
        ?>
    </div>
</div>
<?php
    endforeach;
endif;
?>