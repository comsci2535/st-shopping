<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reviews_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_reviews($param)
    {
        
        if(!empty($param['review_id'])):
            $this->db->where('a.review_id', $param['review_id']);
        endif;

        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if( isset($param['length']) ):
            $this->db->limit($param['length'], $param['start']);
        endif;

        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $this->db->select('a.*');
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db->get('reviews a');
        return $query;
    }

    public function count_reviews($param) 
    {
        
        if(!empty($param['review_id'])):
            $this->db->where('a.review_id', $param['review_id']);
        endif;

        if(!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        
        return $this->db->count_all_results('reviews a');;
    }

    public function insert($value) {
        $this->db->insert('reviews', $value);
        return $this->db->insert_id();
    }

}
