<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Banks_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_banks_by_id($bank_id = '') 
    {
        
        if(!empty($$bank_id)):
            $this->db->where('a.bank_id', $bank_id);
        endif;

        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('banks a')
                        ->get();
        return $query;
    }
   
}
