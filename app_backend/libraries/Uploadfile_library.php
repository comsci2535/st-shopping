<?php

/**
 *
 */
class UploadFile_library {
	public $CI;

	function __construct() {
		$this->CI = &get_instance();
	}

	public function do_upload($file,$type = TRUE ,$path) {
		$upload_path='/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';
		if (!is_dir($upload_path ))
		{
			@mkdir('./'.$upload_path , 0777, true);
		}
		$config['upload_path']          = 'uploads/'.$path.'/'.date('Y').'/'.date('m');
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|bmp';
		//$config['max_size']             = 30000;
		$config['encrypt_name']         = TRUE;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->CI->load->library('upload', $config);
        
		if ( ! $this->CI->upload->do_upload($file))
		{
			$data = array('error' => $this->CI->upload->display_errors());
		}
		else
		{
			$data = array('index' => $this->CI->upload->data());
		}
		
		return $data;
		
	}

	public function do_uploadSetWH($file,$type = TRUE ,$path) {
		$upload_path='/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';
		if (!is_dir($upload_path ))
		{
			@mkdir('./'.$upload_path , 0777, true);
		}
		$config['upload_path']          = 'uploads/'.$path.'/'.date('Y').'/'.date('m');
		$config['allowed_types']        = 'jpg|jpeg|png';
		$config['max_size']             = 300;
		$config['encrypt_name']         = TRUE;
		$config['max_width']            = 800;
		$config['max_height']           = 800;

		$this->CI->load->library('upload', $config);
        
		if ( ! $this->CI->upload->do_upload($file))
		{
			$data = array('error' => $this->CI->upload->display_errors());
		}
		else
		{
			$data = array('index' => $this->CI->upload->data());
		}
		
		return $data;
		
	}

	public function do_uploadMultiple($file, $type = TRUE, $path) {
		
		$upload_path='/uploads/'.$path.'/'.date('Y').'/'.date('m').'/';
		if (!is_dir($upload_path )):
			@mkdir('./'.$upload_path , 0777, true);
		endif;

		//config file
		$config['upload_path']          = 'uploads/'.$path.'/'.date('Y').'/'.date('m');
		$config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|bmp';
		$config['encrypt_name']         = TRUE;

		//rename input file name array
		$fileReNamePost 				= explode('[]', $file);
		$data 							= array();

		// Looping all files
		  
		if(isset($_FILES[$fileReNamePost[0]]['name']) && count($_FILES[$fileReNamePost[0]]['name']) > 0):

			$i = 0;

			foreach($_FILES[$fileReNamePost[0]]['name'] as $filename):

				$_FILES['files[]']['name'] 		=  $filename;
				$_FILES['files[]']['type'] 		=  $_FILES[$fileReNamePost[0]]['type'][$i];
				$_FILES['files[]']['tmp_name'] 	=  $_FILES[$fileReNamePost[0]]['tmp_name'][$i];
				$_FILES['files[]']['error'] 		=  $_FILES[$fileReNamePost[0]]['error'][$i];
				$_FILES['files[]']['size'] 		=  $_FILES[$fileReNamePost[0]]['size'][$i];
				$config['file_name'] 			=  $filename;

				//Load upload library
				$this->CI->load->library('upload', $config);

				if (!$this->CI->upload->do_upload('files[]')):
					$data['filenames'][] = array('error' => $this->CI->upload->display_errors());
				else:
					$data['filenames'][] = array('index' => $this->CI->upload->data());
				endif;
			$i++;

			endforeach;	

		endif;

		return $data;
	}
}

?>
