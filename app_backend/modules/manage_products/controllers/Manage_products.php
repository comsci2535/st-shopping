<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Manage_products extends MX_Controller 
{

    private $_title             = "จัดการยอดรีวิวสินค้า";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับจัดการยอดรีวิวสินค้า";
    private $_grpContent        = "manage_products";
    private $_requiredExport    = true;
    private $_permission;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("manage_products_m");
        $this->load->model("products/products_m");
        $this->load->model("categories/categories_m");
        $this->load->model("product_attributes/product_attributes_m");
        $this->load->model("stocks/stocks_m");
        $this->load->model("products_deliverys/products_deliverys_m");
        $this->load->model("orders/orders_m");
        
    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf'   => site_url("{$this->router->class}/pdf"),
        );
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        //$action[1][]      = action_filter();
        $action[1][]        = action_add(site_url("{$this->router->class}/create"));
        //$action[2][]      = action_export_group($export);
        $action[3][]        = action_trash_multi("{$this->router->class}/destroy");
        $action[3][]        = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        $info               = $this->products_m->get_rows($input);
        $infoCount          = $this->products_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        $i = $input['start'] + 1;
        foreach ($info->result() as $key => $rs) {

            
            $id                         = encode_id($rs->product_id);
            $action                     = array();
            $action[1][]  = table_print(site_url("{$this->router->class}/edit/{$id}"), array(
                'title' => 'เพิ่มจำนวนสต็อก',
                'icon'  => 'plus'
                ));

            $infoOrders    = $this->orders_m->get_orders_product_by_id($rs->product_id)->row();
            $quantity      = 0;
            if(!empty($infoOrders)):
                $quantity = !empty($infoOrders->quantity) ? $infoOrders->quantity : 0;
            endif;

            $order_qty = !empty($rs->order_qty) ? '<br> จำนวนเพิ่มเอง <span class="m-widget1__number m--font-danger">(+' .$rs->order_qty.')</span>': '';

            $text_order_qty = 'จำนวนขายจริง <span class="m-widget1__number m--font-primary">('.$quantity.')</span>'.$order_qty;

            $input_order_qty =  '<div id="text-order-qty-'.$rs->product_id.'">
                                        <div class="m-radio-inline">
                                            <label class="m-radio m-radio--success">
                                                <input type="radio" class="check-order-qty" name="check_id['.$key.']" value="1" checked> บวก
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--success">
                                                <input type="radio" class="check-order-qty" name="check_id['.$key.']" value="0"> ลบ
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="input-group" >
                                            <input type="text" class="form-control text-order-qty" value="" placeholder="ระบุจำนวนที่ขาย">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-primary btn-add-order-qty" data-id="'.$id.'" data-row-id="text-order-qty-'.$rs->product_id.'"><i class="fa fa-save" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>';
            $input_view_qty =  '<div id="text-view-qty-'.$rs->product_id.'">
                                    <div class="m-radio-inline">
                                        <label class="m-radio m-radio--success">
                                            <input type="radio" class="check-view-qty" name="check_view_id['.$key.']" value="1" checked> บวก
                                            <span></span>
                                        </label>
                                        <label class="m-radio m-radio--success">
                                            <input type="radio" class="check-view-qty" name="check_view_id['.$key.']" value="0"> ลบ
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="input-group" >
                                        <input type="text" class="form-control text-view-qty" value="" placeholder="ระบุ จำนวนคนดู">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-primary btn-add-view-qty" data-id="'.$id.'" data-row-id="text-view-qty-'.$rs->product_id.'"><i class="fa fa-save" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>';
                
            $column[$key]['DT_RowId']           = $id;
            $column[$key]['no']                 = $i;
            $column[$key]['title']              = $rs->title;
            $column[$key]['order_qty']          = $text_order_qty;
            $column[$key]['view_qty']           = $rs->view_qty;
            $column[$key]['input_order_qty']    = $input_order_qty;
            $column[$key]['input_view_qty']     = $input_view_qty;
            $column[$key]['updated_at']         = datetime_table($rs->updated_at);
            // $column[$key]['action']             = '';//Modules::run('utils/build_button_group', $action);
            $i++;
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function edit($id="")
    {
        $this->load->module('template');
        
        $id                 = decode_id($id);
        $input['repoId']    = $id;
        $info               = $this->manage_products_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info                   = $info->row();
        $data['info']           = $info;
        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update()
    {
        $input  = $this->input->post(null, true);
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->manage_products_m->update($id, $value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input)
    {
        
        $value['title']     = $input['title'];
        $value['excerpt']   = $input['excerpt'];
        $value['detail']    = $input['detail'];
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else{
            $input  = $this->input->post();
            
            if ( $type == "view_qty" ) {
                $input_['product_id'] = decode_id($input['id']);
                $info   = $this->products_m->get_rows($input_)->row();
                $total  = 0;
                if(!empty($info)):
                    $total = !empty($info->view_qty) ? $info->view_qty : 0;
                endif;

                $total_sum = 0; 
                if(!empty($input['checked']) > 0):
                    $total_sum = $total + $input['view_qty'];
                else:
                    $total_sum = $total - $input['view_qty'];
                endif;

                if($total_sum < 0 ):
                    $total_sum = 0;
                endif;

                $value['view_qty'] = $total_sum;
                $result  = $this->products_m->update(decode_id($input_['product_id']), $value);
 
            }
            
            if ( $type == "order_qty" ) {

                $input_['product_id'] = decode_id($input['id']);

                $info   = $this->products_m->get_rows($input_)->row();
                $total  = 0;
                if(!empty($info)):
                    $total = !empty($info->order_qty) ? $info->order_qty : 0;
                endif;

                $total_sum = 0; 
                if(!empty($input['checked']) > 0):
                    $total_sum = $total + $input['order_qty'];
                else:
                    $total_sum = $total - $input['order_qty'];
                endif;

                if($total_sum < 0 ):
                    $total_sum = 0;
                endif;

                $value['order_qty'] = $total_sum;

                $result  = $this->products_m->update(decode_id($input_['product_id']), $value);
 
            } 
            if ( $result ) {
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
}
