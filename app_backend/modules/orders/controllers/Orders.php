<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require APPPATH.'libraries/barcode/BarcodeGenerator.php'; 
require APPPATH.'libraries/barcode/BarcodeGeneratorHTML.php';
require APPPATH.'libraries/barcode/BarcodeGeneratorPNG.php'; 

class Orders extends MX_Controller 
{

    private $_title             = "จัดการข้อมูลสั่งซื้อสินค้า";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับจัดการข้อมูลสั่งซื้อสินค้า";
    private $_grpContent        = "orders";
    private $_requiredExport    = true;
    private $_permission;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }

        $this->payment = array('ชำระผ่านการโอน', 'จ่ายผ่าน PayPal', 'เก็บเงินปลายทาง');

        $this->load->library('ckeditor');
        $this->load->model("orders_m");
        $this->load->model("stocks/stocks_m"); 
        $this->load->model("status/status_m");
        $this->load->model("product_attributes/product_attributes_m");
        $this->load->model("info/info_m");
        $this->load->model("transports/transports_m");

    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf'   => site_url("{$this->router->class}/pdf"),
        );

        $action[0][]        = action_send_order(site_url("{$this->router->class}"));
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        
        $info               = $this->orders_m->get_rows($input);
        $infoCount          = $this->orders_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->order_id);
            $action                     = array();
            $action[1][]                = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $action[1][]                = table_print(site_url("{$this->router->class}/print/{$id}"), array(
                                                                                                    'title' => 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง',
                                                                                                    'icon'  => 'print'
                                                                                                    ));
            if($rs->is_invoice > 0):                                                                                     
                $action[1][]  = table_print(site_url("{$this->router->class}/invoice/{$id}"), array(
                                                                                                'title' => 'ปริ้นใบกำกับภาษี',
                                                                                                'icon'  => 'file-text-o'
                                                                                                ));
            endif;
            if($rs->payment_type == 0):
                $payment = '<p class="m-badge m-badge--primary m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;
            if($rs->payment_type == 1):
                $payment = '<p class="m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;
            if($rs->payment_type == 2):
                $payment = '<p class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;

            $status_action  = $this->orders_m->get_orders_status_action_by_order_id($rs->order_id)->result();
            
            $log_status     = table_status_action($status_action);
            $infoStatus     = $this->status_m->get_status_by_id($rs->status)->row();

            $reserve_code  ='';
            if(!empty($rs->reserve_code)):
                $reserve_code   = '<br>(Order ref : '.$rs->reserve_code.')';
            endif; 

            $column[$key]['DT_RowId']       = $id;
            $column[$key]['checkbox']       = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name']           = $rs->name.' '.$rs->lasname;
            $column[$key]['order_code']     = $rs->order_code.$reserve_code.$log_status;
            $column[$key]['tel']            = $rs->tel;
            $column[$key]['email']          = $rs->email;
            $column[$key]['tracking_code']  = $rs->tracking_code;
            $column[$key]['discount']       = number_format($rs->discount);
            $column[$key]['payment_type']   = $payment;
            $column[$key]['status']         = !empty($infoStatus->title) ? $infoStatus->title : '';
            $column[$key]['created_at']     = datetime_table($rs->created_at);
            $column[$key]['updated_at']     = datetime_table($rs->updated_at);
            $column[$key]['action']         = Modules::run('utils/build_button_group', $action);
            
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create()
    {
        $this->load->module('template');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage()
    {
        $input  = $this->input->post(null, true);
        $value  = $this->_build_data($input);
        $result = $this->orders_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id="")
    {
        $this->load->module('template');
        
        $id                 = decode_id($id);
        $input['order_id']    = $id;
        $info               = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info                   = $info->row();

        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result();
        if(!empty($infoDetail)):
            foreach($infoDetail as $item):
                $attributes_option = explode(',',$item->attributes_option);
                $item->attributes = $this->product_attributes_m->get_product_attributes_by_in($attributes_option)->result();
                if(!empty($item->attributes)):
                    foreach($item->attributes as $attribute):
                        $attribute->headattributes = $this->product_attributes_m->get_product_attributes_by_in($attribute->parent_id)->row();
                    endforeach;
                endif;
            endforeach;
        endif;

        $data['payment']        = $this->payment[$info->payment_type];
        $data['status_title']   = $this->status_m->get_status_by_id($info->status)->row()->title;

        $transport              = $this->transports_m->get_transport_by_id($info->transport_id)->row();
        $data['tracking_title'] = !empty($transport->title) ? '(ส่งโดย) '.$transport->title :'ไม่ระบุ'; 
        $data['details']        = $infoDetail;
        $data['info']           = $info;
        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update()
    {
        $input  = $this->input->post(null, true);
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->orders_m->update($id, $value);
        if ( $result ) {

            if($value['status'] == 6):
                $param['order_id'] = $id;
                $orders = $this->orders_m->get_orders_detail_by_param($param)->result();
                if(!empty($orders)):
                    foreach ($orders as $item):
                        $param_['product_id']         = $item->product_id;
                        $quantity                     = !empty($item->quantity) ? $item->quantity : 0;
                        $param_['attributes_option']  = explode(',',$item->attributes_option);
                        $stocks = $this->stocks_m->get_stock_by_param($param_)->row();
                        if(!empty($stocks)):
                            $stock_qty           = !empty($stocks->stock_qty) ? $stocks->stock_qty : 0;
                            $stock_id            = $stocks->stock_id;
                            $total               = $stock_qty + $quantity;
                            $value_['stock_qty'] = $total;
                            $this->stocks_m->update($stock_id, $value_);
                        endif;
                    endforeach;
                endif;
            endif;

            if(!empty($input['status'])): 
                $data['order_id']   = $id;
                $data['status']     = $input['status'];
                $data['is_process'] = 0;
                $data['title']      = null;
                $data['process']    = null;
                $this->set_orders_status_action($data);
            endif;

            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input)
    {
        
        $value['name']          = $input['name'];
        $value['lasname']       = $input['lasname'];
        $value['address']       = $input['address'];
        $value['provinces']     = $input['provinces'];
        $value['amphures']      = $input['amphures'];
        $value['districts']     = $input['districts'];
        $value['zip_code']      = $input['zip_code'];
        $value['tel']           = $input['tel'];
        $value['email']         = $input['email'];
        if(!empty($input['tracking_code'])):
            $value['tracking_code'] = $input['tracking_code'];
        endif;
        if(!empty($input['status'])): 
            $value['status']        = $input['status'];
        endif;

        $is_invoice                 = isset($input['is_invoice']) ? $input['is_invoice'] : 0;
        $invoice_type               = isset($input['invoice_type']) ? $input['invoice_type'] : 0;
        if($is_invoice > 0 && $invoice_type < 1):
            $value['invoice_no']            = $input['vat_code'];
            $value['invoice_address']       = $input['customer_address_vat'];
            $value['invoice_provinces_id']  = $input['invoice_provinces_id'];
            $value['invoice_amphures_id']   = $input['invoice_amphures_id'];
            $value['invoice_districts_id']  = $input['invoice_districts_id'];
            $value['invoice_tel']           = $input['invoice_tel'];
            $value['invoice_zip_code']      = $input['invoice_zip_code'];
        elseif($is_invoice > 0 && $invoice_type > 0): 
        $value['invoice_no']            = $input['vat_code'];  
        endif;

        $value['is_invoice']      = $is_invoice;
        $value['invoice_type']    = $invoice_type;
        
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    public function set_orders_status_action($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['status']        = $data['status'];
        $value['is_process']    = $data['is_process'];
        $value['title']         = $data['title'];
        $value['process']       = $data['process'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = $this->session->users['user_id'];

        $this->orders_m->insert_orders_status_action($value);
    }
    
    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->orders_m->get_rows($input);
        $fileName       = "orders";
        $sheetName      = "Sheet name";
        $sheetTitle     = "Sheet title";
        
        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->orders_m->get_rows($input);
        $data['info']   = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "orders.pdf";
        $pathFile = "uploads/pdf/orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash()
    {
        $this->load->module('template');
        
        // toobar
        $action[1][]            = action_list_view(site_url("{$this->router->class}"));
        $action[2][]            = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction']      = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input              = $this->input->post();
        $input['recycle']   = 1;
        $info               = $this->orders_m->get_rows($input);
        $infoCount          = $this->orders_m->get_count($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->order_id);
            $action                     = array();
            $action[1][]                = table_restore("{$this->router->class}/restore");         
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action']     = Modules::run('utils/build_toolbar', $action);
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input  = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 1;
                $value['recycle_at']    = $dateTime;
                $value['recycle_by']    = $this->session->users['user_id'];
                $result                 = $this->orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 0;
                $result                 = $this->orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 2;
                $result                 = $this->orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result                 = false;

                if ( $type == "active" ) {
                    $value['active']    = $input['status'] == "true" ? 1 : 0;
                    $result             = $this->orders_m->update_in($input['id'], $value);
                }
                
                if ( $result ) {
                    $toastr['type']     = 'success';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
                } else {
                    $toastr['type']     = 'error';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                }

                $data['success']    = $result;
                $data['toastr']     = $toastr;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function send_order()
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input  = $this->input->post();
            $error  = 0;
            $arr =  array();
            foreach ( $input['id'] as $rs ):
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $input['order_id']      = $rs; 
                $info                   = $this->orders_m->get_rows($input)->row();
                $value['status']        = 4;
                if(!empty($info->transport_id)):
                    $transport   = $this->transports_m->get_transport_by_id($info->transport_id)->row();
                    if(!empty($transport->title)):
                        if($transport->title == 'DHL'):
                            $value['status']  = 5;
                        endif;
                    endif;
                endif; 

                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result = $this->orders_m->update($rs, $value);
                if($result):
                    $data['order_id']   = $rs;
                    $data['status']     = $value['status'];
                    $data['is_process'] = 0;
                    $data['title']      = null;
                    $data['process']    = null;
                    $this->set_orders_status_action($data);
                else:
                    $error++;
                endif;
                
            endforeach;

            if ( $error == 0) {
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr; 
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data)); 
        }       
    }

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path       = 'content';
        $upload     = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function print($id="")
    {
        $this->load->module('template');
    
        $id = decode_id($id);
        $input['order_id'] = $id;
        $info = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        
        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result();
        if(!empty($infoDetail)):
            foreach($infoDetail as $item):
                $attributes_option = explode(',',$item->attributes_option);
                $item->attributes = $this->product_attributes_m->get_product_attributes_by_in($attributes_option)->result();
                if(!empty($item->attributes)):
                    foreach($item->attributes as $attribute):
                        $attribute->headattributes = $this->product_attributes_m->get_product_attributes_by_in($attribute->parent_id)->row();
                    endforeach;
                endif;
            endforeach;
        endif;
        $data['details']        = $infoDetail;

        //ดึงข้อมูลบริษัท
        $info_com           = $this->info_m->get_rows('');
        $info_com           = $info_com->row();
        $data['company']    =  $info_com;

        //add status action form order
        $input_action['process']        = 'print';
        $input_action['is_process']     = 1;
        $input_action['order_id']       = $id;
        $count_action                   = $this->orders_m->get_count_orders_status_action($input_action);
        if($count_action == 0):
            $status_action['order_id']   = $id;
            $status_action['status']     = 0;
            $status_action['is_process'] = 1;
            $status_action['title']      = 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง';
            $status_action['process']    = 'print';
            $this->set_orders_status_action($status_action);
        endif;

        $transport              = $this->transports_m->get_transport_by_id($info->transport_id)->row();
        $data['tracking_title'] = !empty($transport->title) ? $transport->title :'ไม่ระบุ'; 
        if(!empty($transport)):
            if($transport->title == "DHL"): 
                $code                = "THDFLWEB".$info->order_code;
                $generator           = new Picqer\Barcode\BarcodeGeneratorHTML();
                $generator_img       = new Picqer\Barcode\BarcodeGeneratorPNG(); 
                $border              = 2;
                $height              = 50;
                $data['order_code']  = $code;
                $data['img_barcode'] = '<img src="data:image/png;base64,' . base64_encode($generator_img->getBarcode($code, $generator_img::TYPE_CODE_128,$border,$height)) . '">';
                $data['barcode']     = $generator->getBarcode($code , $generator::TYPE_CODE_128,$border,$height);
            endif;
        endif;  
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/print";
        $data['pageScript'] = "scripts/backend/orders/print.js";
    
        $this->template->layout($data);
    }

    public function invoice($id="")
    {
        $this->load->module('template');
    
        $id = decode_id($id);
        $input['order_id'] = $id;
        $info = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();

        if($info->invoice_type < 1):
            $info->address    = $info->invoice_address;
            $info->districts  = $info->invoice_districts_id;
            $info->amphures   = $info->invoice_amphures_id;
            $info->provinces  = $info->invoice_provinces_id;
            $info->zip_code   = $info->invoice_zip_code;
            $info->tel        = $info->invoice_tel;
        endif;

        $data['info'] = $info;
        
        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result();
        if(!empty($infoDetail)):
            foreach($infoDetail as $item):
                $attributes_option = explode(',',$item->attributes_option);
                $item->attributes = $this->product_attributes_m->get_product_attributes_by_in($attributes_option)->result();
                if(!empty($item->attributes)):
                    foreach($item->attributes as $attribute):
                        $attribute->headattributes = $this->product_attributes_m->get_product_attributes_by_in($attribute->parent_id)->row();
                    endforeach;
                endif;
            endforeach;
        endif;
        $data['details']        = $infoDetail;

        //add status action form order
        $input_action['process']        = 'print_invoice';
        $input_action['is_process']     = 1;
        $input_action['order_id']       = $id;
        $count_action                   = $this->orders_m->get_count_orders_status_action($input_action);
        if($count_action == 0):
            $status_action['order_id']   = $id;
            $status_action['status']     = 0;
            $status_action['is_process'] = 1;
            $status_action['title']      = 'ปริ้นใบกำกับภาษี';
            $status_action['process']    = 'print_invoice';
            $this->set_orders_status_action($status_action);
        endif;

    
            //ดึงข้อมูลบริษัท
        $info_com = $this->info_m->get_rows('');
        $info_com = $info_com->row();
        $data['company']  =  $info_com;
    
            // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/invoice"));
    
            // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/invoice";
        $data['pageScript'] = "scripts/backend/orders/invoice.js";
    
        $this->template->layout($data);
    }

    public function get_order_param($data)
    {
        $param['recycle']           = !empty($data['recycle'])? $data['recycle'] :'';
        $param['length']            = !empty($data['length'])? $data['length'] :'';
        $param['start']             = !empty($data['start'])? $data['start'] :'';
        $param['order_by']          = !empty($data['order_by'])? $data['order_by'] :'';
        $param['statuss']           = !empty($data['statuss'])? $data['statuss'] : '';
        $param['created_at']        = !empty($data['createDateRange'])? $data['createDateRange'] :'';
        $param['updated_at']        = !empty($data['updateDateRange'])? $data['updateDateRange'] : ''; 
        $orders             = $this->orders_m->get_rows($param)->result();
        $orders_count       = $this->orders_m->get_count($param);
        
        return array($orders, $orders_count);
    }

    public function updateprintcheck(){
        $input = $this->input->post();
        $value['print_check']     = 1;
    }

    
}
