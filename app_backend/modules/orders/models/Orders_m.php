<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query;
    }

    public function get_rows_export($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select("a.*,CONCAT(address,' ตำบล', districts ,' อำเภอ',amphures,' จังหวัด',provinces,' รหัสไปรษณีย์',zip_code) AS myaddress")
                        ->from("orders a")
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.order_code', $param['search']['value'])
                    ->or_like('a.name', $param['keyword'])
                    ->or_like('a.lasname', $param['keyword'])
                    ->or_like('a.tel', $param['keyword'])
                    ->or_like('a.email', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.order_code', $param['search']['value'])
                    ->or_like('a.name', $param['search']['value'])
                    ->or_like('a.lasname', $param['search']['value'])
                    ->or_like('a.tel', $param['search']['value'])
                    ->or_like('a.email', $param['search']['value'])
                    ->group_end();
        }

        if (!empty($param['order_year'])):
            $this->db->where('YEAR(a.created_at)', $param['order_year']);
        endif;

        if (!empty($param['order_month'])):
            $this->db->where('MONTH(a.created_at)', $param['order_month']);
        endif;

        if (!empty($param['order_day'])):
            $this->db->where('DAY(a.created_at)', $param['order_day']);
        endif;

        if (!empty($param['created_at'])):
            $this->db->where('DATE(a.created_at)', $param['created_at']);
        endif;

        if (!empty($param['updated_at'])):
            $this->db->where('DATE(a.updated_at)', $param['updated_at']);
        endif;

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.created_at";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.order_code";
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.name";            
            if ( $this->router->method =="data_index" ) {
                //if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['order_id']) ) 
            $this->db->where('a.order_id', $param['order_id']);

        
        if(!empty($param['status'])){
            $this->db->where_in('a.status', $param['status']);
        }

        if(!empty($param['statuss'])){
            $this->db->where('a.status', $param['statuss']);
        }

        if(!empty($param['order_by'])){
            $this->db->order_by('a.created_at', $param['order_by']);
        }
        
        if (!empty($param['transports']) ):
            $this->db->where_in('a.transport_id', $param['transports']);
        endif;

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }

    public function get_orders_detail_by_id($order_detail_id)
    {
        $this->db->where('order_detail_id', $order_detail_id);
        $query = $this->db
                ->select('a.*')
                ->from('orders_detail a')
                ->get();
        return $query; 
    }

    public function get_orders_detail_by_param($param)
    {
        if(!empty($param['order_detail_id'])):
            $this->db->where('order_detail_id', $param['order_detail_id']);
        endif;
        if(!empty($param['order_id'])):
            $this->db->where('order_id', $param['order_id']);
        endif;
        if(!empty($param['product_id'])):
            $this->db->where('product_id', $param['product_id']);
        endif;
       
        $query = $this->db
                ->select('a.*')
                ->from('orders_detail a')
                ->get();
        return $query; 
    }

    public function get_orders_product_by_id($product_id)
    {
        $query = $this->db
                ->select_sum('quantity')
                ->from('orders_detail')
                ->where('product_id', $product_id)
                ->get();
        return $query; 
    }

    public function get_orders_product_by_param($param)
    {

        if(!empty($param['order_detail_id'])):
            $this->db->where('order_detail_id', $param['order_detail_id']);
        endif;
        if(!empty($param['order_id'])):
            $this->db->where('order_id', $param['order_id']);
        endif;
        if(!empty($param['product_id'])):
            $this->db->where('product_id', $param['product_id']);
        endif;
        $query = $this->db
                ->select('SUM((product_price * quantity)) as price, SUM(quantity) as quantity, SUM(product_price) as product_price') 
                ->from('orders_detail')
                ->get();
        return $query; 
    }

    public function get_orders_detail_by_order_id($order_id)
    {
        $this->db->where('a.order_id', $order_id);
        $query = $this->db
                ->select('a.*,b.title')
                ->from('orders_detail a')
                ->join('products b','a.product_id = b.product_id')
                ->get();
        return $query; 
    }

    public function get_orders_status_action_by_order_id($order_id)
    {
        $this->db->where('a.order_id', $order_id);
        $query = $this->db
                ->select('a.*,b.title as status_title')
                ->from('orders_status_action a')
                ->join('status b','a.status = b.status_id', 'left')
                ->order_by('order_status_action_id', 'asc')
                ->get();
        return $query; 
    }

    public function get_count_orders_status_action($param)
    {
        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif;

        if($param['is_process'] != ''):
            $this->db->where('a.is_process', $param['is_process']);
        endif;

        if(!empty($param['process'])):
            $this->db->where('a.process', $param['process']);
        endif;

        if(!empty($param['status'])):
            $this->db->where('a.status', $param['status']);
        endif;
        
        return $this->db->count_all_results('orders_status_action a');; 
    }

    public function get_count_orders($param)
    {
        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif; 

        if(!empty($param['recycle'])):
            $this->db->where('a.recycle', $param['recycle']);
        endif;

        if(!empty($param['status'])):
            $this->db->where('a.status', $param['status']);
        endif;
        
        return $this->db->count_all_results('orders a');; 
    }
    
    public function insert($value) {
        $this->db->insert('orders', $value);
        return $this->db->insert_id();
    }

    public function insert_orders_status_action($value) {
        $this->db->insert('orders_status_action', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('order_id', $id)
                        ->update('orders', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('order_id', $id)
                        ->update('orders', $value);
        return $query;
    }    

}
