
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">

            <form  role="form">
                <table id="data-list" class="table table-hover dataTable" width="100%">
                    <thead>
                        <tr>
                            <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                            <th>วันที่สั่งซื้อ</th>
                            <th>เลขที่สั่งซื้อ</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th>จำนวนเงิน</th>
                            <th>ประเภทชำระเงิน</th>
                            <th>สถานะ</th>
                            <th>จัดการ</th>
                        </tr>
                    </thead>
                </table>
            </form>

        </div>
    </div>
</div>