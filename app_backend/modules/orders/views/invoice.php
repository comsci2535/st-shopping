<style>
    page {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
    }
    page[size="A4"] {  
        width: 21cm;
        height: 30.7cm; 
    }

    table{
        width: 100%;
    }
    .table-print th{
        border-bottom: 0.1px solid;
        border-top: 0.1px solid;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .table-print p{
        margin: 0px 7px 0px 7px;
    }
    .p-border{
        padding: 10px;
    }
    .table-bottom{
        width: 100%;
        position: relative;
        top: 305px;
    }
    .note{
        position: relative;
        width: 100%;
        top: 350px;
        border-top: 0.1px solid;
    }

    .customer{
        border-top: 0.1px solid;
    }

    .note tr td p{
        margin: 0px 7px 0px 7px;
    }
    .line-th{
        border-top: 1px  solid #000;
        border-bottom: 1px  solid #000;
    }
    .price-th{
        text-align: center;
    }
</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม print
                   </h3>
               </div>
           </div>
           <div class="m-portlet__head-tools">
            <input id="order-id-printinvoice" type="hidden" value="<?=$info->order_id?>">
            <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> ปริ้นต้นฉบับ</button>&nbsp
            <button type='button' id="btn-print-copy" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> ปริ้นสำเนา</button>
        </div>
    </div>
    <div  class="m-portlet__body">
        <page id="printarea" size="A4">

            <style type="text/css"  media="print">
                @media print {
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    #printarea .table-print {
                        border-collapse: collapse;
                    }
                    #printarea .table-print th{
                        border-bottom: 0.1px solid;
                        border-top: 0.1px solid;
                        padding-top: 5px;
                        padding-bottom: 5px;
                    }
                    #printarea .table-print p{
                        margin: 0px 6px 0px 7px;
                    }
                    #printarea .p-border{
                        padding: 0px;
                    }
                    #printarea .table-bottom{
                        width: 98%;
                        position: absolute; 
                        bottom: 0;
                    }
                    #printarea .customer{
                        border-top: 0.1px solid;
                    }
                    #printarea .note{
                        position: absolute;
                        width: 98%;
                        bottom: 100px;
                        border-top: 0.1px solid;
                    }
                    #printarea .note tr td p{
                        margin: 0px 7px 0px 7px;
                    }
                }
            </style>
            <div class="p-border">
                <table style="width: 100%;">
                    <tr valign="top" >
                        <td > 
                            <h3><?=$company->title?></h3>
                        </td>
                        <td style="text-align: right;">  
                            <h4>ใบเสร็จรับเงิน/ใบกำกับภาษี <br><small>(ต้นฉบับ)</small></h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 6px;">
                            <p style="width: 351px; line-height: 20px;"><?=$company->excerpt?></p>
                            <p>Tel. <?=$company->tel?></p>
                            <p>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></p> 
                        </td>
                    </tr>
                </table>

                <table class="table-print" style="width: 100%; margin-top: 10px;">
                    <tr class="customer" >
                        <td colspan="3" style="width: 350px; padding-top: 10px; padding-bottom: 10px;">
                            <p>
                                <p>ชื่อลูกค้า : 
                                    <?php echo !empty($info->name)? $info->name:'' ?> 
                                    <?php echo !empty($info->lasname)? $info->lasname:'' ?>
                                </p>
                                <p>ที่อยู่ : 
                                    <?php echo !empty($info->address)? $info->address:'' ?>
                                    <br>
                                    ตำบล <?php echo !empty($info->districts)? $info->districts:'' ?>
                                    อำเภอ <?php echo !empty($info->amphures)? $info->amphures:'' ?> 
                                    จังหวัด <?php echo !empty($info->provinces)? $info->provinces:'' ?> 
                                    <?php echo !empty($info->zip_code)? $info->zip_code:'' ?></p>
                                <p><strong> เลขประจำตัวผู้เสียภาษี:</strong> 
                                <?=isset($info->invoice_no)? $info->invoice_no : '-';?></p>
                                <p>เบอร์โทร : <?php echo !empty($info->tel)? $info->tel:'' ?></p>
                            </p>
                        </td>
                        <?php $no_ID = 'IV'.substr($info->order_code, 2,4 ).substr($info->created_at,8,2).'/'.substr($info->order_code, 6 ); ?>
                        <td valign="top" colspan="2" style="padding-top: 10px; padding-bottom: 10px;">
                            <p>
                                <p><strong>เลขที่บิล :</strong> <?=$no_ID;?></p>
                                <p>วันที่ : <?=DateThai($info->created_at)?></p>
                                <p>เงื่อนไขชำระ : -</p>
                                <p><strong>เลขที่สั่งซื้อสินค้า :</strong> <?=$info->order_code;?></p>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: center;"><p>ลำดับ</p></th>
                        <th><p>รายการสินค้า</p></th>
                        <th style="text-align: center;"><p>จำนวน</p></th>
                        <th style="text-align: right;"><p>หน่วยละ</p></th>
                        <th style="text-align: right;"><p>จำนวน</p></th>
                    </tr>
                    <?php 
                    $sumtotal = 0;
                    $total = 0;
                    
                    if(isset($details) && count($details) > 0){
                        foreach($details as $key => $item){
                            $total      = $item->product_price*$item->quantity;
                            $sumtotal  += $total; 
                            $text_attributes = '<br>';
                            if(!empty($item->attributes)):
                                foreach($item->attributes as $attribute):
                                    $text_attributes.= ' ('.$attribute->headattributes->title.')'.$attribute->title;
                                endforeach;
                            endif;
                            ?>
                            <tr>
                                <td style="text-align: center;"><p><?=$key+1?></p></td>
                                <td><p><?=$item->title?> <?=$text_attributes?></p></td>
                                <td style="text-align: center;"><p><?=$item->quantity?></p></td>
                                <td style="text-align: right;"><p><?=number_format($item->product_price,2)?></p></td>
                                <td style="text-align: right;"><p><?=number_format($total,2)?></p></td>
                            </tr>
                        <?php 
                        }
                    }
                    ?>


                    </table>

                    <table class="note">
                        <tr>
                            <td valign="top" colspan="3" style="padding-top: 10px;">
                                <p>หมายเหตุ</p>
                            </td>
                            <td style="text-align: right; padding-top: 10px;">
                                <p>จำนวนเงิน :</p>
                                <p>ส่วนลด : </p>
                                <p>จำนวนภาษีมูลค่าเพิ่ม 7% : </p>
                                <p>ราคาสินค้า :</p>
                            </td>
                            <td style="text-align: right; padding-top: 10px;">
                                <p><?=number_format($sumtotal,2)?></p>
                                <p>0</p>
                                <p><?=number_format(($sumtotal*7)/107,2)?></p>
                                <p><?=number_format($sumtotal-($sumtotal*7)/107,2)?></p>
                            </td>
                        </tr> 
                        <tr >
                            <td colspan="5" class="price-th" style="text-align: center;padding: 8px;border-top: 0.1px solid;"> 
                                <strong><?="(".baht_text($sumtotal-($sumtotal*7)/107).")";?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"> 
                                <p>
                                    ได้รับสินค้า / บริการ  ตามรายกานี้ข้างบนนี้ไว้ถูกต้อง
                                    <br>
                                และอยู่ในสภาพที่เรียบร้อยทุกประการ</p>
                            </td>
                        </tr>
                    </table>   

                    <table class="table-bottom" style="">
                        <tr>
                            <td style="border: 1px solid;">
                                <div style="text-align: center;margin-top: 60px;">
                                    <p>ผู้รับสินค้า________________วันที่________/________/________</p>
                                </div>
                            </td>
                            <td style="border: 1px solid;">
                                <div style="text-align: center;">
                                    <p>ในนาม บริษัท มีดี88ช็อป จำกัด</p>
                                    <img width="75" src="<?php echo $this->config->item('root_url').'images/sen.png';?>" >
                                    <p style="margin-bottom: 0;margin-top: -15px;">_____________________</p>
                                    <p>ผู้รับมอบอำนาจ</p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </page>
            <div style="display:show;">
                <page id="printarea_copy" size="A4">

                    <style type="text/css"  media="print">
                        @media print {
                            #printarea_copy {  
                                margin: 0;
                                border: initial;
                                border-radius: initial;
                                width: initial;
                                min-height: initial;
                                box-shadow: initial;
                                background: initial;
                                page-break-after: always;
                            }
                            #printarea_copy .table-print {
                                border-collapse: collapse;
                            }
                            #printarea_copy .table-print th{
                                border-bottom: 0.1px solid;
                                border-top: 0.1px solid;
                                padding-top: 5px;
                                padding-bottom: 5px;
                            }
                            #printarea_copy .table-print p{
                                margin: 0px 6px 0px 7px;
                            }
                            #printarea_copy .p-border{
                                padding: 0px;
                            }
                            #printarea_copy .table-bottom{
                                width: 98%;
                                position: absolute; 
                                bottom: 0;
                            }
                            #printarea_copy .customer{
                                border-top: 0.1px solid;
                            }
                            #printarea_copy .note{
                                position: absolute;
                                width: 98%;
                                bottom: 100px;
                                border-top: 0.1px solid;
                            }
                            #printarea_copy .note tr td p{
                                margin: 0px 7px 0px 7px;
                            }
                        }
                    </style>
                    <div class="p-border">
                        <table style="width: 100%;">
                            <tr valign="top" >
                                <td > 
                                    <h3><?=$company->title?></h3>
                                </td>
                                <td style="text-align: right;">  
                                    <h4>ใบเสร็จรับเงิน/ใบกำกับภาษี <br><small>(สำเนา)</small></h4>
                                </td>
                            </tr>
                            <tr>
                                <td style="line-height: 6px;">
                                    <p style="width: 351px; line-height: 20px;"><?=$company->excerpt?></p>
                                    <p>Tel. <?=$company->tel?></p>
                                    <p>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></p> 
                                </td>
                            </tr>
                        </table>

                        <table class="table-print" style="width: 100%; margin-top: 10px;">
                            <tr class="customer" >
                                <td colspan="3" style="width: 350px; padding-top: 10px; padding-bottom: 10px;">
                                    <p>
                                        <p>ชื่อลูกค้า : 
                                            <?php echo !empty($info->name)? $info->name:'' ?> 
                                            <?php echo !empty($info->lasname)? $info->lasname:'' ?>
                                        </p>
                                        <p>ที่อยู่ : 
                                            <?php echo !empty($info->address)? $info->address:'' ?>
                                            <br>
                                            ตำบล <?php echo !empty($info->districts)? $info->districts:'' ?>
                                            อำเภอ <?php echo !empty($info->amphures)? $info->amphures:'' ?> 
                                            จังหวัด <?php echo !empty($info->provinces)? $info->provinces:'' ?> 
                                            <?php echo !empty($info->zip_code)? $info->zip_code:'' ?></p>
                                        <p><strong> เลขประจำตัวผู้เสียภาษี:</strong> 
                                        <?=isset($info->invoice_no)? $info->invoice_no : '-';?></p>
                                        <p>เบอร์โทร : <?php echo !empty($info->tel)? $info->tel:'' ?></p>
                                    </p>
                                </td>
                                <?php $no_ID = 'IV'.substr($info->order_code, 2,4 ).substr($info->created_at,8,2).'/'.substr($info->order_code, 6 ); ?>
                                <td valign="top" colspan="2" style="padding-top: 10px; padding-bottom: 10px;">
                                    <p>
                                        <p><strong>เลขที่บิล :</strong> <?=$no_ID;?></p>
                                        <p>วันที่ : <?=DateThai($info->created_at)?></p>
                                        <p>เงื่อนไขชำระ : -</p>
                                        <p><strong>เลขที่สั่งซื้อสินค้า :</strong> <?=$info->order_code;?></p>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th style="text-align: center;"><p>ลำดับ</p></th>
                                <th><p>รายการสินค้า</p></th>
                                <th style="text-align: center;"><p>จำนวน</p></th>
                                <th style="text-align: right;"><p>หน่วยละ</p></th>
                                <th style="text-align: right;"><p>จำนวน</p></th>
                            </tr>
                            <?php 
                            $sumtotal = 0;
                            $total = 0;

                            if(isset($details) && count($details) > 0){
                                foreach($details as $key => $item){
                                    $total      = $item->product_price*$item->quantity;
                                    $sumtotal  += $total; 
                                    $text_attributes = '<br>';
                                    if(!empty($item->attributes)):
                                        foreach($item->attributes as $attribute):
                                            $text_attributes.= ' ('.$attribute->headattributes->title.')'.$attribute->title;
                                        endforeach;
                                    endif;
                                    ?>
                                    <tr>
                                        <td style="text-align: center;"><p><?=$key+1?></p></td>
                                        <td><p><?=$item->title?> <?=$text_attributes?></p></td>
                                        <td style="text-align: center;"><p><?=$item->quantity?></p></td>
                                        <td style="text-align: right;"><p><?=number_format($item->product_price,2)?></p></td>
                                        <td style="text-align: right;"><p><?=number_format($total,2)?></p></td>
                                    </tr>
                                <?php 
                                }
                            }
                            ?>

                            </table>

                            <table class="note">
                                <tr>
                                    <td valign="top" colspan="3" style="padding-top: 10px;">
                                        <p>หมายเหตุ</p>
                                    </td>
                                    <td style="text-align: right; padding-top: 10px;">
                                        <p>จำนวนเงิน :</p>
                                        <p>ส่วนลด : </p>
                                        <p>จำนวนภาษีมูลค่าเพิ่ม 7% : </p>
                                        <p>ราคาสินค้า :</p>
                                    </td>
                                    <td style="text-align: right; padding-top: 10px;">
                                        <p><?=number_format($sumtotal,2)?></p>
                                        <p>0</p>
                                        <p><?=number_format((($sumtotal*7)/107),2)?></p>
                                        <p><?=number_format($sumtotal-(($sumtotal*7)/107),2)?></p>
                                    </td>
                                </tr> 
                                <tr >
                                    <td colspan="5" class="price-th" style="text-align: center;padding: 8px; border-top: 0.1px solid; "> 
                                        <strong><?="(".baht_text($sumtotal-($sumtotal*7)/107).")";?></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"> 
                                        <p>
                                            ได้รับสินค้า / บริการ  ตามรายกานี้ข้างบนนี้ไว้ถูกต้อง
                                            <br>
                                        และอยู่ในสภาพที่เรียบร้อยทุกประการ</p>
                                    </td>
                                </tr>
                            </table>   

                            <table class="table-bottom" style="">
                                <tr>
                                    <td style="border: 1px solid;">
                                        <div style="text-align: center;margin-top: 60px;">
                                            <p>ผู้รับสินค้า________________วันที่________/________/________</p>
                                        </div>
                                    </td>
                                    <td style="border: 1px solid;">
                                        <div style="text-align: center;">
                                            <p>ในนาม บริษัท มีดี88ช็อป จำกัด</p>
                                            <img width="75" src="<?php echo $this->config->item('root_url').'images/sen.png';?>" >
                                            <p style="margin-bottom: 0;margin-top: -15px;">_____________________</p>
                                            <p>ผู้รับมอบอำนาจ</p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </page>
                </div>       
            </div>    
        </div>
    </div>




