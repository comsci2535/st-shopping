<style>
    page {
      background: white;
      display: block;
      margin: 0 auto;
      margin-bottom: 0.5cm;
      box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"] {  
      width: 21cm;
      height: 23.7cm; 
      height: auto;
      padding-bottom: 70px;
  }

  table{
    width: 100%;
}
.table-print tr td{
    border: 1px solid;
}

.table-print td{
    border: 1px solid;
}
.table-print th{
    border: 1px solid;
}
.table-print p, .table-sender p, .table-recipient p{
    margin: 0px 0px 0px 7px;
}

.table-sender tr td{
    width: 24%;
}

.table-recipient{
    width: 100%;
    position: relative;
    top: 460px;
}
.table-sender{
    width: 50%;
    position: relative;
    top: 491px;
    left: 0px;
}
.collect {
        border: 2px solid #32b312;
        border-radius: 6px;
        padding: 24px 20px;
        text-align: center;
        font-size: 20px;
        color: #2aa50b;
        width: 70%;
        margin-left: 55px;
    }
    .barcode_img{
        display: none;
    }
    .collect {
        border: 2px solid #32b312;
        border-radius: 6px;
        padding: 24px 20px;
        text-align: center;
        font-size: 20px;
        color: #2aa50b;
        width: 80%;
        margin-left: 55px;
    }
@media (min-width: 992px) { 
    .table-recipient{
        width: 100%;
        position: relative;
        top: 460px;
    }
    .table-sender{
        width: 100%;
        position: relative;
        top: 491px;
        left: 0px;
    }
}
@media (min-width: 1200px) { 
    .table-recipient{
        width: 100%;
        position: relative;
        top: 460px;
    }
    .table-sender{
        width: 100%;
        position: relative;
        top: 491px;
        left: 0px;
    }
}

</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     แบบฟอร์ม
                 </h3>
             </div>
         </div>
         <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                <input id="order-id-printcheck" type="hidden" value="<?=$info->order_id?>">
                <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
            </div>
        </div>
    </div>
    <div  class="m-portlet__body">
        <page id="printarea" size="A4">
            <style type="text/css"  media="print">
                @media print {
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    .table-print, .table-sender, .table-recipient{
                        border-collapse: collapse;
                    }
                    .table-print tr{
                        border: 1px solid;
                    }   
                    .table-print td{
                        border: 1px solid;
                    }
                    .table-print th{
                        border: 1px solid;
                    }
                    .table-print p, .table-sender p, .table-recipient p{
                        margin: 0px 0px 0px 7px;
                    }
                    .table-sender{
                        width: 100%;
                        position: relative;
                        top: 400px;
                        left: 0px;
                    }
                    .table-recipient{
                        width: 100%;
                        position: relative;
                        top: 420px;
                    }
                    .table-recipient {
                                width: 100%;
                                position: relative;
                                top: 390px;
                            }
                            .barcode{
                                display: none;
                            }
                            .collect {
                                border: 2px solid #32b312;
                                border-radius: 6px;
                                padding: 24px 20px;
                                text-align: center;
                                font-size: 20px;
                                color: #2aa50b;
                                width: 70%;
                                margin-left: 55px;
                            }
                }
            </style>

            <div style="padding: 20px;">
                <div class="perpage1">
                <table style="width: 100%;">
                    <tr>
                        <td > 
                            <h3><?php echo !empty($company->title)? $company->title: '';?></h3>
                        </td>
                        <td >                               
                            <h4 style="border-radius: 10px; border: 2px solid; text-align: center; padding: 10px;">ใบสั่งสินค้า</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="line-height: 6px;">
                            <p style="width: 351px; line-height: 20px;"><?php echo !empty($company->excerpt)? $company->excerpt: '';?></p>
                            <p>Tel. <?php echo !empty($company->tel)? $company->tel: '';?></p>
                            <p>เลขประจำตัวผู้เสียภาษี  <?php echo !empty($company->tax_id)? $company->tax_id: '';?></p> 
                            <br>
                        </td>
                    </tr>
                </table>
                <p style="font-size: 16px;color: red;margin-bottom: 0;">ขนส่ง : <?php echo !empty($tracking_title) ? $tracking_title : ''?></p>
                <table class="table-print" style="width: 100%;">
                    <tr>
                        <th style="width: 150px;text-align: center;"><p>เลขที่สั่งซื้อ</p></th>
                        <th><p>รายการสินค้า</p></th>
                        <th style="width: 100px;text-align: center;"><p>จำนวน</p></th>
                    </tr>
                    <?php
                    $sumtotal = 0;
                    $i =0;
                    
                    if(!empty($details) && count($details) > 0){
                        $rowspan = count($details);
                        
                        foreach($details as $key => $item){
                            $sumtotal  += $item->quantity;
                            $text_attributes = '';
                            if(!empty($item->attributes)):
                                foreach($item->attributes as $attribute):
                                    $text_attributes.= ' ('.$attribute->headattributes->title.')'.$attribute->title;
                                endforeach;
                            endif;

                            ?>
                            <tr>
                                <?php
                                if($key == 0):
                                    ?>
                                    <td valign="top" rowspan="<?=$rowspan?>" style="text-align: left;">
                                        <p style="padding-left:5px;"><?php echo !empty($info->order_code)? $info->order_code: '';?></p>
                                    </td>
                                    <?php
                                endif;
                                ?>
                                <td style="">
                                    <p style="padding-left:5px;">
                                    <?php echo !empty($item->title)? $item->title: '';?><?=$text_attributes?>
                                    </p>
                                </td>
                                <td style="text-align: center;">
                                    <p><?php echo !empty($item->quantity)? $item->quantity: 0;?></p>
                                </td>
                            </tr>
                            <?php 
                            $i++;
                        }
                    }

                    $top    = 140;
                    $top2   = 140;
                    $i      = $i * 20;
                    $top    = $top - $i;
                    $top2   = $top2 - $i;
                    $top    = 50;
                    $top2   = 70;
                    ?>

                    <tr>
                        <th valign="top" colspan="2" style="text-align: right;">
                            <p style="padding-right:5px;">รวมทั้งหมด</p>
                        </th>
                        <th style="text-align: center;">
                            <p><?=$sumtotal?></p>
                        </th>
                    </tr> 
                </table>
                </div>
                <!-- div2 -->
                <div class="perpage2">
                <table class="table-sender" style="<?='top :'.$top.'px'?> ">
                    <tr>
                        <td valign="top" style="width: 50%;">
                            <div class="box-check">
                                <p><u><strong>ผู้ส่ง</strong></u></p>
                                <p><?php echo !empty($company->title) ? $company->title: '';?></p>
                                <p><?php echo !empty($company->excerpt) ? $company->excerpt: '';?></p>
                                <p>Tel. <?php echo !empty($company->tel) ? $company->tel: '';?></p>
                                <p>เลขประจำตัวผู้เสียภาษี  <?php echo !empty($company->tax_id) ? $company->tax_id: '';?></p> 
                            </div>
                        </td>
                        <td style="width: 50%;">
                            <?php
                                if($info->payment_type ==2){
                            ?>
                            <div class="box-check">
                                <div class="collect">
                                 เก็บเงินปลายทาง<br>
                                 จำนวน <?php echo number_format($info->discount); ?> บาท
                             </div>
                            </div>
                        <?php } ?>
                     </td>
                    </tr>
                </table>
                <table class="table-recipient" style="<?='top :'.$top2.'px'?> ">
                    <tr>
                        <td style="width: 50%;">
                            <img style="height: 100px" src="<?php echo !empty($company->file) ? $this->config->item('root_url').$company->file : '';?>">
                            <div class="text-">
                            <p>www.meedee88shop.com</p>
                            <p>Line @meedee88</p>
                            <p>โทร. 02-297-0360</p>
                            <p>เพจ Android ถูกสุดสุด บริษัท มีดี88ช็อป จำกัด</p>
                            </div>
                        </td>
                        <td style="width: 50%;" valign="top">
                            <div class="box-check" >
                                <div style="margin-left: 55px;">
                                    <p><u><strong>ผู้รับ</strong></u></p>
                                    <p><?php echo !empty($info->name) ? $info->name: '';?> <?php echo !empty($info->lasname) ? $info->lasname: '';?></p>
                                    <p>
                                        <?php echo !empty($info->address) ? $info->address: '';?>
                                        <br>ตำบล 
                                        <?php echo !empty($info->districts) ? $info->districts: '';?>
                                        อำเภอ  <?php echo !empty($info->amphures) ? $info->amphures: '';?>
                                        จังหวัด  <?php echo !empty($info->provinces) ? $info->provinces: '';?>
                                        <br>รหัสไปรษณีย์ <?php echo !empty($info->zip_code) ? $info->zip_code: '';?>
                                    </p>
                                    <p>เบอร์โทรศัพท์. <?php echo !empty($info->tel) ? $info->tel: '';?></p>
                                    <br>
                                </div>
                                <?php 
                                    if(!empty($order_code)){ 
                                ?>
                                    <p><strong> เลขที่สั่งซื้อสินค้า :</strong> <?php echo !empty($order_code) ? $order_code: '';?></p>  
                                    <div class="barcode" style=""><?php echo !empty($barcode) ? $barcode : '';?></div>
                                    <div class="barcode_img" style=""><?php echo !empty($img_barcode) ? $img_barcode : '';?></div>
                                    <p style="margin: 0px 0 0 50px;"><?php echo !empty($order_code) ? $order_code: '';?></p>
                                <?php 
                                }else{
                                ?>
                                <p><strong> เลขที่สั่งซื้อสินค้า :</strong> <?php echo !empty($info->order_code) ? $info->order_code: '';?></p> 
                                <?php
                                }
                                ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            </div>
        </page>                 
    </div>    
</div>
</div>




