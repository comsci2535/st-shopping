<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="order_code">เลขที่สั่งซื้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->order_code) ? $info->order_code : NULL ?>" type="text" class="form-control m-input " name="order_code" id="order_code" placeholder="ระบุเลขที่สั่งซื้อ" disabled required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="order_code">Order ref</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->reserve_code) ? $info->reserve_code : NULL ?>" type="text" class="form-control m-input " name="reserve_code" id="reserve_code" placeholder="ระบุเลขที่ Order ref" disabled required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="name">ชื่อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->name) ? $info->name : NULL ?>" type="text" class="form-control m-input " name="name" id="name" placeholder="ระบุชื่อ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="lasname">นามสกุล</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->lasname) ? $info->lasname : NULL ?>" type="text" class="form-control m-input " name="lasname" id="lasname" placeholder="ระบุนามสกุล" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="address">ที่อยู่</label>
                    <div class="col-sm-7">
                        <textarea name="address" rows="3" class="form-control" id="address" placeholder="ระบุ" required><?php echo isset($info->address) ? $info->address : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <input type="hidden" id="datajson" name="datajson"
                    value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                    <label class="col-sm-2 col-form-label" for="districts">ตำบล <span class="text-danger"> *</span></label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control m-input" id="districts" placeholder="ตำบล"
                        name="districts"
                        value="<?php echo isset($info->districts) ? $info->districts : NULL ?>">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="amphures">อำเภอ <span class="text-danger"> *</span></label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control m-input" id="amphures" placeholder="อำเภอ" name="amphures"
                        value="<?php echo isset($info->amphures) ? $info->amphures : NULL ?>">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="provinces">จังหวัด <span class="text-danger"> *</span></label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control m-input" id="provinces" placeholder="จังหวัด"
                        name="provinces"
                        value="<?php echo isset($info->provinces) ? $info->provinces : NULL ?>">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="zip_code">รหัสไปรษณีย์ <span class="text-danger">
                    *</span></label>
                    <div class="col-lg-7">
                        <input type="text" class="form-control m-input" id="zip_code" placeholder="รหัสไปรษณีย์"
                        name="zip_code" value="<?php echo isset($info->zip_code) ? $info->zip_code : NULL ?>">
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tel">เบอร์โทรศัพท์</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->tel) ? $info->tel : NULL ?>" type="text" class="form-control m-input " name="tel" id="tel" placeholder="ระบุเบอร์โทรศัพท์" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="email">อีเมล</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->email) ? $info->email : NULL ?>" type="text" class="form-control m-input " name="email" id="email" placeholder="ระบุอีเมล">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="order_code">ใบกำกับภาษี</label>
                    <div class="col-sm-7">
                        <div class="m-2 m-checkbox-inline">
                            <label class="m-checkbox">
                                <input type="checkbox" <?php if(isset($info->is_invoice) &&  $info->is_invoice=='1'){ echo "checked";}?>  class="text-attributes-line-checkbox" id="is_invoice" name="is_invoice" value="1"> ต้องการใบกำกับภาษี
                                <span></span>
                            </label>    
                            <label class="m-checkbox">
                                <input type="checkbox" <?php if(isset($info->invoice_type) &&  $info->invoice_type=='1'){ echo "checked";}?>  class="text-attributes-line-checkbox" id="invoice_type" name="invoice_type" value="1"> ใช้ที่อยู่จัดส่ง
                                <span></span>
                            </label>    
                        </div>
                    </div>
                </div>
                <?php
                $text = '';
                if(!isset($info->is_invoice) ? $info->is_invoice : 0  > 0 && !isset($info->invoice_type)? $info->invoice_type : 0 > 0):
                    $text = 'none';
                else:
                    $text = 'show';
                    
                endif;
                ?> 
                <div class="m-view display-vat vat-all" style="display: none;"> 
                    <div class="form-group m-form__group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-7">
                                <h5>ข้อมูลใบกำกับภาษี</h5>
                            </div>
                        </div> 
                        <div class="form-group m-form__group row display-vat invoice_no">
                            <label class="col-sm-2 form-col-form-label">เลขที่ผู้เสียภาษี<span class="text-danger"> *</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="vat_code" class="form-control m-input vat-code" required
                                placeholder="เลขที่ผู้เสียภาษี" value="<?php echo isset($info->invoice_no) ? $info->invoice_no : '' ?>"  >
                            </div>
                        </div>
                        <div class="m-view-vat">   
                            <div class="form-group m-form__group row">
                                <label class="col-sm-2 form-col-form-label">เบอร์โทรศัพท์<span class="text-danger"> *</span></label>
                                <div class="col-sm-7">
                                    <input type="text" name="invoice_tel" class="form-control m-input phone-with-add"
                                    placeholder="เบอร์โทรศัพท์" value="<?php echo isset($info->invoice_tel) ? $info->invoice_tel : '' ?>" >
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-sm-2 form-col-form-label">ที่อยู่<span class="text-danger"> *</span></label>
                                <div class="col-sm-7">
                                    <textarea name="customer_address_vat"  rows="3" class="form-control m-input"
                                    placeholder="ที่อยู่" ><?php echo isset($info->invoice_address) ? $info->invoice_address : '' ?></textarea>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <input type="hidden" id="datajson_vat" name="datajson_vat"
                                value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                                <div class="col-sm-3 offset-sm-2 m-form__group-sub">
                                    <label class="form-col-form-label">ตำบล<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_districts_id" placeholder="ตำบล"
                                    name="invoice_districts_id" value="<?php echo isset($info->invoice_districts_id) ? $info->invoice_districts_id : '' ?>">
                                </div>
                                <div class="col-sm-3 offset-sm-1 m-form__group-sub">
                                    <label class="form-col-form-label">อำเภอ<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_amphures_id" placeholder="อำเภอ"
                                    name="invoice_amphures_id" value="<?php echo isset($info->invoice_amphures_id) ? $info->invoice_amphures_id : '' ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-sm-3 offset-sm-2 m-form__group-sub">
                                    <label class="form-col-form-label">จังหวัด<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_provinces_id" placeholder="จังหวัด"
                                    name="invoice_provinces_id" value="<?php echo isset($info->invoice_provinces_id) ? $info->invoice_provinces_id : '' ?>">
                                </div>
                                <div class="col-sm-3 offset-sm-1 m-form__group-sub">
                                    <label class="form-col-form-label">รหัสไปรษณีย์<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_zip_code" placeholder="รหัสไปรษณีย์"
                                    name="invoice_zip_code" value="<?php echo isset($info->invoice_zip_code) ? $info->invoice_zip_code : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                if(!empty($info->status) && $info->status == 5):
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tracking_code">เลขที่นำส่ง</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->tracking_code) ? $info->tracking_code : NULL ?>" type="text" class="form-control m-input " name="tracking_code" id="tracking_code" placeholder="ระบุเลขที่นำส่ง" required>
                    </div>
                </div>
                <?php
                endif;
                ?>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="">รายการสินค้า</label>
                    <div class="col-sm-8">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width:5%">ลำดับ</th>
                                    <th class="text-left" style="width:20%">ชื่อสินค้า</th>
                                    <th class="text-left" style="width:20%">ลักษณะสินค้า</th>
                                    <th style="width:7%">ราคา</th>
                                    <th style="width:7%">จำนวน</th>
                                    <th style="width:7%">รวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // arr($details);
                                $sumTotal = 0;
                                if(!empty($details)):
                                    $total = 0;
                                    foreach($details as $key => $item):
                                        $total      = $item->product_price * $item->quantity;
                                        $sumTotal  += $total;
                                ?>
                                <tr>
                                    <td class="text-center"><?=($key+1);?></td>
                                    <td><?php echo !empty($item->title) ? $item->title : '';?></td>
                                    <td>
                                        <div class="m-list-timeline m-list-timeline--skin-light">
                                        <?php
                                        if(!empty($item->attributes)):
                                            foreach($item->attributes as $key => $attribute):
                                        ?>
                                            <div class="m-list-timeline__items">
                                                <div class="m-list-timeline__item">
                                                    <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                                    <span class="m-list-timeline__text">
                                                        <?php echo !empty($attribute->headattributes->title) ? '('.$attribute->headattributes->title.')' : '';?>
                                                        <?php echo !empty($attribute->title) ? $attribute->title : '';?>
                                                    </span>
                                                </div>
                                            </div>
                                        <?php
                                            endforeach;
                                        endif;
                                        ?>
                                        </div>
                                    </td>
                                    <td class="text-right"><?php echo !empty($item->product_price) ? number_format($item->product_price) : 0;?></td>
                                    <td class="text-center"><?php echo !empty($item->quantity) ? $item->quantity : 0;?></td>
                                    <td class="text-right"><?=number_format($total);?></td>
                                </tr>
                                <?php
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5" class="text-left">รวมเงิน</th>
                                    <th class="text-right"><?=number_format($sumTotal);?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="payment">ประเภทการชำระเงิน</label>
                    <div class="col-sm-8">
                        <h5 class="m-2 m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i> <?php echo !empty($payment) ? $payment : '';?></h5>
                        <?php
                        if($info->payment_type == 1):
                        ?>
                        <h5 class="m-2">เลขที่อ้างอิง : <?php echo isset($info->premise) ? $info->premise : NULL ?></h5>
                        <?php
                        endif;
                        ?>

                        <?php
                        if($info->payment_type == 0):
                        ?>
                        <br>
                        <img class="img-thumbnail" src="<?php echo !empty($info->file) ? $this->config->item('root_url').$info->file : '';?>" onerror="this.src='<?=$this->config->item('root_url').'images/no-image.png';?>'" style="width:350px">
                        <?php
                        endif;
                        ?>
                        <h5 class="m-2">จำนวนเงิน(บาท) : <?php echo isset($info->discount) ? number_format($info->discount) : 0 ?></h5>
                        
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="transports">บริษัทจัดส่ง</label>
                    <div class="col-sm-8">
                        <h5 class="m-2 m-badge m-badge--success m-badge--wide"><i class="fa fa-truck"></i>
                            <?php echo !empty($tracking_title) ? $tracking_title : '';?>
                        </h5> 
                        <h5 class="m-2">เลขที่นำส่ง : <?php echo isset($info->tracking_code) ? $info->tracking_code : 'ไม่ระบุ'; ?></h5>  
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="order_code">สถานะการสั่งซื้อ</label>
                    <div class="col-sm-7">
                        <p class="m-2 m-badge m-badge--primary m-badge--wide"><i class="fa fa-check-circle"></i> <?php echo !empty($status_title) ? $status_title : '';?></p>
                        <?php
                        if(!empty($info->status) && $info->status == 1):
                        ?>
                        <div class="m-2 m-checkbox-inline">
                            <label class="m-checkbox">
                                <input type="checkbox" class="text-attributes-line-checkbox" name="status" value="2" checked> อยู่ระหว่างการตรวจสอบ
                                <span></span>
                            </label>    
                        </div>
                        <?php
                        endif;
                        ?>
                        <div class="m-2 m-checkbox-inline">
                            <label class="m-checkbox">
                                <input type="checkbox" class="text-attributes-line-checkbox" name="status" value="6"> ยกเลิก
                                <span></span>
                            </label>    
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="orders">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->order_id) ? encode_id($info->order_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>





