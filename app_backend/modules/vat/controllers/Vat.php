<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Vat extends MX_Controller 
{

    private $_title             = "ใบกำกับภาษีอย่างย่อ";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับใบกำกับภาษีอย่างย่อ";
    private $_grpContent        = "sent_successfully";
    private $_requiredExport    = true;
    private $_permission;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }

        $this->payment = array('ชำระผ่านการโอน', 'จ่ายผ่าน PayPal', 'เก็บเงินปลายทาง');

        $this->load->library('ckeditor');
        $this->load->model("orders/orders_m");
        $this->load->model("status/status_m");
        $this->load->model("product_attributes/product_attributes_m");
        $this->load->model("info/info_m");
        $this->load->model("transports/transports_m");
        
    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            //'pdf'   => site_url("{$this->router->class}/pdf"),
        );
        $action[0][]        = action_printvat(site_url("{$this->router->class}"));
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        //$action[2][]        = action_export_group($export);
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $input['recycle']       = 0;
        $input['active']        = 1;
        
        $data['status']         =  $this->status_m->get_rows($input)->result();
        $data['transports']     =  $this->transports_m->get_rows($input)->result();
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input                  = $this->input->post();
        $input['recycle']       = 0;
        if(!empty($input['transport_id'])):
            $transport_arr = array();
            foreach ($input['transport_id'] as $transport):
                array_push($transport_arr, $transport);
            endforeach;
            $input['transports']  = $transport_arr;
        endif; 
        
        $info               = $this->orders_m->get_rows($input);
        $infoCount          = $this->orders_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        $i = $input['start'] + 1;
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->order_id);
            $action                     = array();
            $action[1][]                = table_edit(site_url("orders/edit/{$id}"));
            
           
            if($rs->payment_type == 0):
                $payment = '<p class="m-badge m-badge--primary m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;
            if($rs->payment_type == 1):
                $payment = '<p class="m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;
            if($rs->payment_type == 2):
                $payment = '<p class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;

            $active = $rs->active ? "checked" : null;
            $status_print = '';
            
            if(!empty($rs->vat_print)):
                $status_print .= '<span class="m--font-bold m--font-danger"><i class="fa fa-check"></i> ใบกำกับภาษีอย่างย่อ</span><br>';
            endif;

            $status_action  = $this->orders_m->get_orders_status_action_by_order_id($rs->order_id)->result();
            
            $log_status     = table_status_action($status_action);

            $infoStatus     = $this->status_m->get_status_by_id($rs->status)->row();

            $tracking_code  = !empty($rs->tracking_code) ? '<br>(เลขที่นำส่ง) <span class="text-tracking-code" data-tracking="'.$rs->tracking_code.'">'.$rs->tracking_code.'</span>' :'';

            $transport      = $this->transports_m->get_transport_by_id($rs->transport_id)->row();
            $tracking_title = !empty($transport->title) ? '<br>(ส่งโดย) <span class="text-tracking-id" data-transport="'.$rs->transport_id.'">'.$transport->title.'</span>' :'';
            $reserve_code  ='';
            if(!empty($rs->reserve_code)):
                $reserve_code   = '<br>(Order ref : '.$rs->reserve_code.')';
            endif; 
            $column[$key]['DT_RowId']       = $id;
            $column[$key]['checkbox']       = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name']           = $rs->name.' '.$rs->lasname;
            $column[$key]['order_code']     = $rs->order_code.$reserve_code.$log_status;
            $column[$key]['tel']            = $rs->tel;
            $column[$key]['email']          = $rs->email;
            $column[$key]['tracking_code']  = $rs->tracking_code;
            $column[$key]['discount']       = number_format($rs->discount);
            $column[$key]['payment_type']   = $payment.$tracking_code.$tracking_title;
            $column[$key]['status']         = !empty($infoStatus->title) ? $infoStatus->title : '';
            $column[$key]['created_at']     = datetime_table($rs->created_at);
            $column[$key]['updated_at']     = datetime_table($rs->updated_at);
            $column[$key]['action']         = Modules::run('utils/build_button_group', $action);
            $column[$key]['address']        = $rs->address.' ตำบล'.$rs->districts.' อำเภอ'.$rs->amphures.' จังหวัด'.$rs->provinces.'  รหัสไปรษณีย์ '.$rs->zip_code;
            $i++;
            
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->orders_m->get_rows_export($input);
        $fileName       = "รายการสั่งซื้อสินค้า";
        $sheetName      = "ข้อมูลสั่งซื้อ";
        $sheetTitle     = "ข้อมูลรายการสั่งซื้อสินค้า";
        
        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'วันที่สั่งซื้อ', 'width'=>50),
            'B' => array('data'=>'เลขที่สั่งซื้อ','width'=>50),
            'C' => array('data'=>'ชื่อ-นามสกุล', 'width'=>16),            
            'D' => array('data'=>'เบอร์โทรศัพท์', 'width'=>16),
            'E' => array('data'=>'ที่อยู่', 'width'=>100),
            'F' => array('data'=>'จำนวนเงิน', 'width'=>16),
            'G' => array('data'=>'ประเภทชำระเงิน', 'width'=>16),
            'H' => array('data'=>'เลขที่นำส่ง', 'width'=>16),
            'I' => array('data'=>'ส่งโดย', 'width'=>16),  
            'J' => array('data'=>'สถานะ', 'width'=>16), 
        );
        $fields = array(
            'A' => 'created_at',
            'B' => 'order_code',
            'C' => 'name',            
            'D' => 'tel',
            'E' => 'myaddress', 
            'F' => 'discount',
            'G' => 'payment_type',
            'H' => 'tracking_code',
            'I' => 'tracking_name', 
            'J' => 'status', 
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = !empty($row->{$field}) ? $row->{$field} : $field;
                if ( $field == 'created_at' || $field == 'updated_at' ):
                    $value = datetime_table ($value);
                endif;
                if ( $field == 'discount'):
                    $value = number_format($value);
                endif;

                if ( $field == 'name'):
                    $value = $row->name.' '.$row->lasname;
                endif;

                if($field == 'status'):
                    $infoStatus = $this->status_m->get_status_by_id($row->status)->row();
                    $value      = !empty($infoStatus->title) ? $infoStatus->title : '';
                endif;
                if($field == 'payment_type'): 
                    $value      = $this->payment[$row->payment_type];
                endif;
                if($field == 'tracking_name'):
                    $transport  = $this->transports_m->get_transport_by_id($row->transport_id)->row();
                    $value      = !empty($transport->title) ? $transport->title :'';
                endif;
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->orders_m->get_rows($input);
        $data['info']   = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "orders.pdf";
        $pathFile = "uploads/pdf/orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }

    public function printvat(){

        $this->load->module('template');

        $input = $this->input->post();
        $arrayId = $input['arr_code'];

        $column = array();

        if(sizeof($arrayId) === 0){
            redirect($uri = '/vat', $method = 'auto', $code = NULL);
        }

            $page = ceil(sizeof($arrayId)/4);
            $i = 0;

            foreach($arrayId as $key => $item_id){
             
            
            $input['order_id'] = decode_id($item_id);

            $info = $this->orders_m->get_rows($input)->row();

            $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result();
            if(!empty($infoDetail)):
                foreach($infoDetail as $item):
                    $attributes_option = explode(',',$item->attributes_option);
                    $item->attributes = $this->product_attributes_m->get_product_attributes_by_in($attributes_option)->result();
                    if(!empty($item->attributes)):
                        foreach($item->attributes as $attribute):
                            $attribute->headattributes = $this->product_attributes_m->get_product_attributes_by_in($attribute->parent_id)->row();
                        endforeach;
                    endif;
                endforeach;
            endif;
         
            $detail_arr        = $infoDetail;

            if($info->is_invoice > 0){
                if($info->invoice_type > 0){
                    $address_vat =  $info->address." ตําบล/แขวง".$info->districts." อำเภอ/เขต".$info->amphures." จังหวัด".$info->provinces." รหัสไปรษรีย์ ".$info->zip_code; 
                    $tel = $info->tel;
                }else{
                    $address_vat =  $info->invoice_address." ตําบล/แขวง".$info->invoice_districts_id." อำเภอ/เขต".$info->invoice_amphures_id." จังหวัด".$info->invoice_provinces_id." รหัสไปรษรีย์ ".$info->invoice_zip_code;
                    $tel =  $info->invoice_tel;
                }
               
            }else{
              $address_vat =  $info->address." ตําบล/แขวง".$info->districts." อำเภอ/เขต".$info->amphures." จังหวัด".$info->provinces." รหัสไปรษรีย์ ".$info->zip_code;
              $tel = $info->tel;
            }
            

            $column[$key] = array(
                'order_code'        => $info->order_code,
                'customer_fullname' => $info->name." ".$info->lasname,
                'customer_tel'      => $tel,
                'customer_address'  => $address_vat,
                'order_detail'      => $detail_arr,
                'order_date'        => $info->created_at

            );

        }

        $data['info'] = $column;
        $data['arrayId'] = $arrayId;

        //ดึงข้อมูลบริษัท
        $info_com = $this->info_m->get_rows('');
        $info_com = $info_com->row();
        $data['company']  =  $info_com;

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/print"));

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/printvat";
        $data['pageScript'] = "scripts/backend/vat/print.js";

        $this->template->layout($data);
    }
}
