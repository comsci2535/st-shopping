<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="form-group m-form__group row">
                <label for="order_year" class="offset-2 col-2 col-form-label">ปี</label>
                <div class="col-4">
                    <select name="order_year" id="order_year" class="form-control m-input select2"> 
                        <?php
                        for($i = 2017; $i <= date('Y'); $i++):
                            $selected = '';
                            if($i == date('Y')):
                                $selected = 'selected';
                            endif;
                            echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                        endfor;

                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_month" class="offset-2 col-2 col-form-label">เดือน</label>
                <div class="col-4">
                    <select name="order_month" id="order_month" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        $month_arr = array( "มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
                        if(isset($month_arr) && count($month_arr)):
                            foreach($month_arr as $key => $item):
                                $selectedm = '';
                                if(($key+1) == date('m')):
                                    $selectedm = 'selected';
                                endif;
                                
                                echo '<option value="'.($key+1).'" '.$selectedm.'>'.$item.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_day" class="offset-2 col-2 col-form-label">วันที่</label>
                <div class="col-4">
                    <select name="order_day" id="order_day" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        for($d = 1; $d <= 31; $d++):
                            $selected = '';
                            if($d == date('d')):
                                $selected = 'selected';
                            endif;
                            echo '<option value="'.$d.'" '.$selected.'>'.$d.'</option>';
                        endfor;

                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-2 col-form-label">เลือกการขนส่ง</label>
                <div class="col-4">
                    <select name="transport_id[]" id="transport_id" class="form-control m-input select2"  multiple>
                        <?php foreach ($transports as $key => $item) { ?>
                            <option value="<?=$item->transport_id?>"><?=$item->title?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="order_status" class="offset-2 col-2 col-form-label">สถานะ</label>
                <div class="col-4">
                    <select name="order_status" id="order_status" class="form-control m-input select2">
                        <option value="">แสดงทั้งหมด</option>
                        <?php
                        if(!empty($status) && count($status)):
                            foreach($status as $key => $item):
                                $selected = '';
                                if($item->status_id == 5):
                                    $selected = 'selected';
                                else:
                                endif;
                            echo '<option value="'.$item->status_id.'" '.$selected.'>'.$item->title.'</option>';
                            endforeach;
                        endif;
                        ?>
                        
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label for="created_at" class="offset-2 col-2 col-form-label"></label>
                <div class="col-4">
                    <button id="btn-search-order" type="button" class="btn btn-success"><i class="fa fa-search"></i> ค้นหา</button>
                    <button id="btn-search-cancel" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> ล้างข้อมูล</button>
                </div>
            </div>
            <br>
            <form  role="form">
                <table id="data-list" class="table table-hover dataTable vat" width="100%">
                    <thead>
                        <tr>
                            <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                            <th>วันที่สั่งซื้อ</th>
                            <th>เลขที่สั่งซื้อ</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th>จำนวนเงิน</th>
                            <th>ประเภทชำระเงิน</th>
                            <th>สถานะ</th>
                            <th>จัดการ</th>
                        </tr>
                    </thead>
                </table>
            </form>

        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal-transport" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="form-submit-transport" method="POST" autocomplete="off">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">แก้ไขบริษัทจัดส่ง</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"> 
                <div class="form-group m-form__group row">
                    <label for="transportID" class="offset-1 col-3 col-form-label">เลือกการขนส่ง</label>
                    <div class="col-7">
                        <select name="transportID" id="transportID" class="form-control m-input select2" style="width: 100%" required>
                            <?php foreach ($transports as $key => $item) { ?>
                                <option value="<?=$item->transport_id?>"><?=$item->title?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label for="trackingCode" class="offset-1 col-3 col-form-label">เลขที่นำส่ง</label> 
                    <div class="col-7">
                        <input type="text" class="form-control" id="trackingCode" name="trackingCode" placeholder="ระบุเลขที่นำส่ง" required>
                        <input type="hidden" id="textOrderID">
                    </div>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
                <button type="submit" class="btn btn-primary">บันทึก</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!--end::Modal-->