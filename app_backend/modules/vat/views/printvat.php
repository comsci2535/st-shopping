<style type="text/css">
	body {
		background: rgb(204,204,204); 
	}
	.book{
		margin: 0 auto;
	}
	page {
		background: white;
		display: block;
		margin: 0 auto;
		margin-bottom: 0.5cm;
		box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
	}
	page[size="A4"] {  
		width: 21cm;
		height: 29.7cm; 
		font-size: 8px;
	}
	page h4{
		font-size: 14px;
	}
	page .bd-tb{
		border-top: 1px solid;
		border-bottom: 1px solid;
	}
	page .bd-b{
		border-bottom: 1px solid #eee;
	}
	page .bd-t{
		border-top: 0.5px solid;
	}
	.fill-height-or-more{
		min-height: 14.5cm;
		padding: 6px;
	}
	.fill-height-or-more .col-6.box{
		border: 1px solid;
	}
	p.company,p.no{
		padding-top: 15px;
		text-align: left;
	}
	p.no{
		text-align: right;
	}
/* page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;  
  } */
  @page {
  	size: 4in 6in landscape;
  	margin: 0;
  }
</style>
<div class="col-md-12">
	<div class="m-portlet m-portlet--tab">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						แบบฟอร์ม
					</h3>
				</div>
			</div>
			<?php 
			$input = $this->input->post();
			$arrayId = $input['arr_code'];
			?>
			<div class="m-portlet__head-tools">
				<input type="hidden" name="ID[]" value="<?=$arrayId?>">
				<div class="btn-group mr-2" role="group" aria-label="1 group">
					<button type='button' onclick="printVat()" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">

			<div class="form-group m-form__group row">
				<div class="book" id="vat">
					<style type="text/css">
						@media print {
							body, page {
								margin: 0;
								box-shadow: 0;
								font-size: 8px;
							}
							.page {
								margin: 0;
								border: initial;
								border-radius: initial;
								width: initial;
								min-height: initial;
								box-shadow: initial;
								background: initial;
								page-break-after: always;
							}
							.row {
								display: -webkit-box;
								display: -ms-flexbox;
								display: flex;
								-ms-flex-wrap: wrap;
								flex-wrap: wrap;
								margin-right: 5px;
								margin-left: 5px;
							}
							.col-6.box{
								-webkit-box-flex: 0;
								-ms-flex: 0 0 47%;
								flex: 0 0 47%;
								max-width: 47%;
							}
							.col-6{
								-webkit-box-flex: 0;
								-ms-flex: 0 0 50%;
								flex: 0 0 50%;
								max-width: 50%;
							}
							.col-1 {
								-webkit-box-flex: 0;
								-ms-flex: 0 0 8.33333%;
								flex: 0 0 8.33333%;
								max-width: 8.33333%;
							}
							.col-4 {
								-webkit-box-flex: 0;
								-ms-flex: 0 0 33.33333%;
								flex: 0 0 33.33333%;
								max-width: 33.33333%;
							}
							.col-2 {
								-webkit-box-flex: 0;
								-ms-flex: 0 0 16.66667%;
								flex: 0 0 16.66667%;
								max-width: 16.66667%;
							}
							.col-3 {
								-webkit-box-flex: 0;
								-ms-flex: 0 0 24%;
								flex: 0 0 24%;
								max-width: 24%;
							}
							.col-12{
								-webkit-box-flex: 0;
								-ms-flex: 0 0 100%;
								flex: 0 0 100%;
								max-width: 100%;
								padding-right: 10px;
								padding-left: 5px;
							}
							.col-10 {
								-webkit-box-flex: 0;
								-ms-flex: 0 0 83.33333%;
								flex: 0 0 83.33333%;
								max-width: 83.33333%;
							}
							.fill-height-or-more{
								min-height: 50vh;
							}
							.fill-height-or-more .col-6.box{
								border: 0.5px dotted #999;
								padding: 5px;	
							}
							p.company{
								text-align: left;
							}
							p.no{
								text-align: right;
							}
							.bd-tb{
								border-top: 0.5px solid;
								border-bottom: 0.5px solid;
							}
							.bd-b{
								border-bottom: 0.5px solid #eee;
							}
							.bd-t{
								border-top: 0.5px solid;
							}
							.text-center {
								text-align: center !important;
							}
							.text-right {
								text-align: right !important;
							}
							.text-left {
								text-align: left !important;
							}
							.mt-3, .my-3 {
								margin-top: 3px !important;
							}
							h4, .h4{
								margin-bottom: 2px;
								margin-top: 3px;
								font-size: 14px;
							}

						}
					</style>

					<!--  -->
					<?php $page = ceil(sizeof($info)/4); ?>
					<page size="A4">
						<div class="page col-12">
							<?php 
							$page_num =0;
							$num = 0;
							foreach ($info as $key => $item) { ?>	

								<?php 
								//if($key==0 || $key == 2 || $key == 4){
								if($key==0 || (($key)%2==0)){
									
									echo '<div class="row  fill-height-or-more">';

								}

								$paymentDate = date('Y-m-d');
								$paymentDate=date('Y-m-d', strtotime($item['order_date']));

								$contractDateBegin = date('Y-m-d', strtotime("05/01/2019"));
								$contractDateEnd = date('Y-m-d', strtotime("05/27/2019"));

								$check = ($paymentDate >= $contractDateBegin) && ($paymentDate <= $contractDateEnd);

								if ($check){
									$vat_no  = "เลขที่ใบเสร็จรับเงิน";
									$txt_vat = "ใบเสร็จรับเงิน";
									$destxt  = "";
									$txtprice = "ราคารวมทั้งสิ้น";
								}else{
									$vat_no  = "เลขที่ใบกำกับภาษี";
									$txt_vat = "ใบกำกับภาษีอย่างย่อ";
									$destxt  = "<span>TAX INVOICE (ABB)</span>";
									$txtprice = "ราคารวมภาษีมูลค่าเพิ่มทั้งสิ้น";
								}


								$no_ID = 'IVWEB'.substr($item['order_code'], 2,4 ).substr($item['order_date'],8,2).'/'.substr($item['order_code'], 6 );
								?>
								<input type="hidden" data-value="<?=$no_ID?>" class="order_code" name="ID[]" value="<?=$item['order_code']?>">

								<div class="col-6 box">
									<div class="row">
										<div class="col-6">
											<p class="company"><?=$company->title?></p>
										</div>
										<div class="col-6">
											<p class="no"><?=$vat_no ?> : <?=$no_ID?> </p>
										</div>
									</div>
									<div class="row">
										<div class="col-10">
											<span><?=$company->excerpt?></span><br>
											<span>Tel. <?=$company->tel?></span><br>
											<span>เลขประจำตัวผู้เสียภาษี  <?=$company->tax_id?></span> 
											<br><br>
											<span>ลูกค้า : <?=$item['customer_fullname']?></span><br>
											<span>ที่อยู่ : <?=$item['customer_address']?></span><br>
											<span>เบอร์โทร : <?=$item['customer_tel']?></span><br>
										</div>
									</div>
									<div class="row">
										<div class="col-12 text-center mt-3">
											<h4><?=$txt_vat?></h4>
											<?=$destxt?>
										</div>
									</div>
									<div class="row">
										<div class="col-12 text-right mt-3">
											<span>วันที่ : <?=DateThai($item['order_date'])?></span>
										</div>
									</div>
									<div class="row text-center bd-tb">
										<div class="<?=(!$check) ? 'col-4' : 'col-8';?>">
											รายการสินค้า
										</div>
										<div class="col-2">
											จำนวน
										</div>
										<div class="col-2">
											ราคา
										</div>
										<?php if (!$check){ ?>
											<div class="col-1">
												VAT
											</div>
											<div class="col-3 text-right">
												จำนวนเงิน
											</div>
										<?php } ?>
									</div>

									<?php 
									
									$sumtotal = 0;
									$sumrprice = 0;
									$sumquantity = 0;
									$sumvat = 0;
									$j= 0;

									foreach ($item['order_detail'] as $i => $value) { 
										$j++;
										$total = 0;
										$price = 0;
										$vat = ((($value->product_price*$value->quantity) * 7) /107);
										$total      = ($value->product_price*$value->quantity) - ((($value->product_price*$value->quantity) * 7) /107);
										$price      = $value->product_price*$value->quantity;
										$sumtotal  += $total;
										$sumquantity += $value->quantity; 
										$sumvat += $vat;
										$sumrprice  += $price; 

										?>

										<div class="row bd-b" style="padding: 3px 0;">
											
											<div class="<?=(!$check) ? 'col-4' : 'col-8';?> text-left ">
												<?=$value->title?>
												<?=isset($value->attributes) ? " (".$value->attributes[0]->title.")" : "";?>
											</div>
											<div class="col-2 text-center">
												<?=$value->quantity?>
											</div>
											<div class="col-2 text-center">
												<?=number_format($value->product_price,2)?>
											</div>
											<?php if (!$check){ ?>
												<div class="col-1 text-center ">
													<?=number_format($vat,2)?>
												</div>
												<div class="col-3 text-right">
													<?=number_format($total,2)?>
												</div>
											<?php } ?>
										</div>

									<?php }?>

									<?php if($j>1){?> 	
										<div class="row bd-t" style="padding: 3px 0;">
											
											<div class="<?=(!$check) ? 'col-4' : 'col-8';?> text-left ">
												<strong> รวม </strong>
											</div>
											<div class="col-2 text-center">
												<?=$sumquantity?>
											</div>
											<div class="col-2 text-center">
												<?=number_format($sumrprice,2)?>
											</div>
											<?php if (!$check){ ?>
												<div class="col-1 text-center ">
													<?=number_format($sumvat,2)?>
												</div>
												<div class="col-3 text-right">
													<?=number_format($sumtotal,2)?>
												</div>
											<?php }?>
										</div>	
									<?php }?>

									<div class="row bd-tb" style="padding: 3px 0;">
										<div class="col-6 text-center bd-r py-1">
											<strong><?='('.baht_text($sumrprice).')'?></strong>
										</div>
										<div class="col-4 text-center bd-r py-1">
											<?=$txtprice?>
										</div>
										<div class="col-2 text-center py-1">
											<strong><?=number_format($sumrprice,2)?></strong>
										</div>
									</div>

									<div class="row" style="margin-top: 30px;">
										<div class="col-6 ">
											ลงชื่อ________________ผู้รับสินค้า
										</div>
										<div class="col-6 ">
											ลงชื่อ________________ผู้รับเงิน
										</div>
									</div>
									<div class="row">
										<div class="col-6 ">
											วันที่______________________
										</div>
										<div class="col-6 ">
											วันที่______________________
										</div>
									</div>

								</div>


								<?php
								//$key == 1 || $key == 3 || $key == 5
								if(($key)%2==1){
									echo '</div>';
								}
								?>
								<?php
								if(($key+1)%4==0){
									$page_num ++;
									echo '</div>	
									</page>';
									if($page_num < $page ){
										echo '<page size="A4">
										<div class="page col-12">';
									}
								}
							} 
							?>

							<?php 
							if($page_num < $page ){
								echo '</div></page></div>';
							}else{
								echo '</div></page>';
							}
							?>

							<!--  -->

						</div>

					</div>
				</div>
			</div>
		</div>