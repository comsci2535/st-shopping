<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
    
    private $_title = 'แผงควบคุม';
    private $_pageExcerpt = 'แสดงรายการโดยรวมของระบบ';
    private $_grpContent = 'grpContent';
    
    public function __construct() {
        parent::__construct();
        $this->load->model('orders/orders_m');
        $this->load->model('products/products_m');
        $this->load->model("status/status_m");
        $this->load->model("users/users_m");
    }

    public function index() 
    {
        $this->load->module('template');
        
        if ( $this->session->firstTime ):
            Modules::run('utils/toastr', 'info', config_item('appName'), 'ยินดีต้อนรับ');   
        endif;

        $system_date = date('Y-m-d');

        // --------- order -------------
        $param['recycle']            = 0;
        $param['length']             = 3;
        $param['start']              = 0;
        $param['order_by']           = "DESC"; 
        $param['createDateRange']    = $system_date;
        list($orders, $orders_count) = Modules::run('orders/get_order_param', $param); 
        $data['orders']              = $orders;
        $data['orders_count']        = $orders_count; 
        // ----------- end order ----------

        // --------- order send-------------
        $param_s['recycle']             = 0;
        $param_s['length']              = 3;
        $param_s['start']               = 0;
        $param_s['statuss']             = 4; 
        $param_s['order_by']            = "DESC"; 
        $param_s['updateDateRange']     = $system_date;
        list($orders_send, $orders_send_count) = Modules::run('orders/get_order_param', $param_s);
        if(!empty($orders_send)):
            foreach($orders_send as $item):
                $item->status = $this->status_m->get_status_by_id($item->status)->row();
            endforeach;
        endif;
       

        $data['orders_send']            = $orders_send;
        $data['orders_send_count']      = $orders_send_count; 
        // ----------- end order send ----------

        
        // --------- users -------------
        $param_u['recycle']            = 0;
        $param_u['length']             = 3;
        $param_u['start']              = 0;
        $param_u['type']               = 'customer';
        $param_u['order_by']           = "DESC"; 
        $param_u['createDateRange']    = $system_date;
        list($users, $users_count) = Modules::run('users/get_user_param', $param_u); 
        $data['users']         = $users;
        $data['users_count']   = $users_count;
        // ----------- end users ----------

        $data['created_at']     = DateThaiNotTime($system_date);

        $param_p['recycle']     = 0;
        $orders_total           = $this->orders_m->get_count_orders($param_p);
        $data['orders_total']   = $orders_total;

        $param_ps['recycle']         = 0;
        $param_ps['status']          = 5;
        $orders_send_total           = $this->orders_m->get_count_orders($param_ps);
        $data['orders_send_total']   = $orders_send_total;


        $param_pr['recycle']     = 0;
        $products_total           = $this->products_m->get_count_products($param_pr);
        $data['products_total']   = $products_total;

        $param_prs['recycle']     = 0;
        $param_prs['type']        = 'customer';
        $customer_total           = $this->users_m->get_count($param_prs);
        $data['customer_total']   = $customer_total;

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/index";
             
        $this->template->layout($data);
    }

    
}
