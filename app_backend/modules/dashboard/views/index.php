<div class="col-xl-12">
	<!--begin:: Widgets/Stats-->
	<div class="m-portlet ">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
				<div class="col-md-12 col-lg-6 col-xl-3"> 
					<!--begin::Total Profit-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								ออเดอร์ทั้งหมด
							</h4><br> 
							<span class="m-widget24__stats m--font-brand">
								<?php echo !empty($orders_total) ? $orders_total : 0;?>
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-brand" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div> 
							<span class="m-widget24__change">
								รายการ
							</span> 
						</div>
					</div>

					<!--end::Total Profit-->
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">

					<!--begin::New Feedbacks-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								จำนวนนำส่งแล้ว
							</h4><br> 
							<span class="m-widget24__stats m--font-info">
								<?php echo !empty($orders_send_total) ? $orders_send_total : 0;?>
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-info" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div> 
							<span class="m-widget24__change">
								รายการ
							</span> 
						</div>
					</div>

					<!--end::New Feedbacks-->
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">

					<!--begin::New Orders-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								สินค้าทั้งหมด
							</h4><br> 
							<span class="m-widget24__stats m--font-danger">
								<?php echo !empty($products_total) ? $products_total : 0;?>
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div> 
							<span class="m-widget24__change">
								รายการ
							</span> 
						</div>
					</div>

					<!--end::New Orders-->
				</div>
				<div class="col-md-12 col-lg-6 col-xl-3">

					<!--begin::New Users-->
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								ลูกค้าทั้งหมด
							</h4><br> 
							<span class="m-widget24__stats m--font-success">
								<?php echo !empty($customer_total) ? $customer_total : 0;?> 
							</span>
							<div class="m--space-10"></div>
							<div class="progress m-progress--sm">
								<div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<span class="m-widget24__change">
								รายการ
							</span> 
						</div>
					</div>
					<!--end::New Users-->
				</div>
			</div>
		</div>
	</div> 
	<!--end:: Widgets/Stats-->
</div>
<div class="col-xl-12">
	<!--Begin::Section-->
	<div class="m-portlet">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
				<div class="col-xl-4">
					<!--begin:: Widgets/Stats2-1 -->
					<div class="m-widget1">
						<h5 class="m-widget14__title">
							สั่งซื้อล่าสุด <span class="m-badge m-badge--success m-badge--wide"><?=$orders_count?></span> <?=$created_at?>
						</h5>
						<?php 
						if(!empty($orders)):
							foreach($orders as $order):
						?>
						<div class="m-widget1__item">
							<div class="row m-row--no-padding align-items-center">
								<div class="col">
									<h3 class="m-widget1__title">
										<?php echo !empty($order->name) ? $order->name :'';?> <?php echo !empty($order->lasname) ? $order->lasname :'';?>
									</h3>
									<span class="m-widget1__desc">วันและเวลา <?php echo !empty($order->created_at) ? DateThai($order->created_at) :'';?></span>
								</div>
								<div class="col m--align-right">
									<span class="m-widget1__number m--font-brand"><?php echo !empty($order->discount) ? number_format($order->discount) : 0.0;?> บาท</span>
								</div>
								
							</div>
						</div> 
						<?php
							endforeach;
						else:
						?>
						<div class="m-widget1__item">
							<div class="row m-row--no-padding align-items-center">
								 ไม่มีรายการ
							</div>
						</div>
						<?php
						endif;
						?>  
					</div> 
					<!--end:: Widgets/Stats2-1 -->
				</div>
				<div class="col-xl-4">
					<!--begin:: Widgets/Stats2-1 -->
					<div class="m-widget1">
						<h5 class="m-widget14__title">
							รอจัดส่ง 
							<span class="m-badge m-badge--success m-badge--wide"><?=$orders_send_count?></span> 
							<?=$created_at?>
						</h5>
						<?php  
						if(!empty($orders_send)):
							foreach($orders_send as $order_send):
						?>
						<div class="m-widget1__item">
							<div class="row m-row--no-padding align-items-center">
								<div class="col">
									<h3 class="m-widget1__title">
										<?php echo !empty($order_send->order_code) ? $order_send->order_code :'';?>
									</h3>
									<span class="m-widget1__desc">วันและเวลา <?php echo !empty($order_send->updated_at) ? DateThai($order_send->updated_at) :'';?></span>
									<span class="m-badge m-badge--success m-badge--wide"><?php echo !empty($order_send->status->title) ? $order_send->status->title :'';?></span>
								</div> 
								
							</div>
						</div> 
						<?php
							endforeach;
						else:
						?>
						<div class="m-widget1__item">
							<div class="row m-row--no-padding align-items-center">
								 ไม่มีรายการ
							</div>
						</div>
						<?php
						endif;
						?>  
					</div> 
					<!--end:: Widgets/Stats2-1 -->
				</div>
				<div class="col-xl-4">
					<!--begin:: Widgets/Stats2-1 -->
					<div class="m-widget1">
						<h5 class="m-widget14__title">
							ผู้สมัครล่าสุด 
							<span class="m-badge m-badge--success m-badge--wide"><?=$users_count?></span> 
							<?=$created_at?>
						</h5>
						<?php 
						if(!empty($users)):
							foreach($users as $user):
						?>
						<div class="m-widget1__item">
							<div class="row m-row--no-padding align-items-center">
								<div class="col">
									<h3 class="m-widget1__title">
										<?php echo !empty($user->fname) ? $user->fname :'';?> <?php echo !empty($user->lname) ? $user->lname :'';?>
									</h3>
									<span class="m-widget1__desc">วันและเวลา <?php echo !empty($user->created_at) ? DateThai($user->created_at) :'';?></span>
								</div>
								<!-- <div class="col m--align-right">
									<span class="m-widget1__number m--font-brand"><?php echo !empty($user->discount) ? number_format($user->discount) : 0.0;?> บาท</span>
								</div> -->
								
							</div>
						</div> 
						<?php
							endforeach;
						else:
						?>
						<div class="m-widget1__item">
							<div class="row m-row--no-padding align-items-center">
								 ไม่มีรายการ
							</div>
						</div>
						<?php
						endif;
						?>  
					</div> 
					<!--end:: Widgets/Stats2-1 -->
				</div>
			</div>
		</div>
	</div>
</div>

<!--End::Section-->
<!-- <div class="col-xl-12">
	<div class="m-portlet m-portlet--mobile ">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Exclusive Datatable Plugin
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
				
			</div>
		</div>
		<div class="m-portlet__body"> 
			
		</div>
	</div>
</div> -->
