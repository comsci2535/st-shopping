<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Tracking_trucks extends MX_Controller 
{

    private $_title             = "จัดการเลขที่นำส่งสินค้า";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับจัดการเลขที่นำส่งสินค้า";
    private $_grpContent        = "tracking_trucks";
    private $_requiredExport    = true;
    private $_permission;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->payment = array('ชำระผ่านการโอน', 'จ่ายผ่าน PayPal', 'เก็บเงินปลายทาง');
        $this->load->library('ckeditor');
        $this->load->model("orders/orders_m");
        $this->load->model("status/status_m");
        $this->load->model("product_attributes/product_attributes_m");
        $this->load->model("info/info_m");
        $this->load->model("transports/transports_m");
    }
    
    public function index()
    {
        $this->load->module('template');
        
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        
        $info               = $this->orders_m->get_rows($input);
        $infoCount          = $this->orders_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }

        $i = $input['start'] + 1;
        $input_['recycle']   = 0;
        $transports = $this->transports_m->get_rows($input_)->result();
        
        
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->order_id);
            $action                     = array();
            $action[1][]                = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $action[1][]                = table_print(site_url("{$this->router->class}/print/{$id}"), array(
                                                                                                    'title' => 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง',
                                                                                                    'icon'  => 'print'
                                                                                                    ));

            $html_transports = '<div class="m-radio-list">';
            if(!empty($transports)){
                foreach($transports as $transport):
                    $html_transports .='<label class="m-radio m-radio--success">';
                    $html_transports .='<input type="radio" class="transport_id" name="transport_id['.$key.']" value="'.$transport->transport_id.'"> '.$transport->title;
                    $html_transports .='<span></span>';
                    $html_transports .='</label>';
                    
                endforeach;
            }
            $html_transports .='</div>';

            $status_action  = $this->orders_m->get_orders_status_action_by_order_id($rs->order_id)->result();
            
            $log_status     = table_status_action($status_action);
            $infoStatus     = $this->status_m->get_status_by_id($rs->status)->row();

            $reserve_code  ='';
            if(!empty($rs->reserve_code)):
                $reserve_code   = '<br>(Order ref : '.$rs->reserve_code.')';
            endif; 

            $column[$key]['DT_RowId']       = $id;
            $column[$key]['no']             = $i;
            $column[$key]['name']           = $rs->name.' '.$rs->lasname;
            $column[$key]['order_code']     = $rs->order_code.$reserve_code.$log_status;
            $column[$key]['tel']            = $rs->tel;
            $column[$key]['email']          = $rs->email;
            $column[$key]['tracking_code']  = $rs->tracking_code;
            $column[$key]['discount']       = number_format($rs->discount);
            $column[$key]['status']         = !empty($infoStatus->title) ? $infoStatus->title : '';
            $column[$key]['created_at']     = datetime_table($rs->created_at);
            $column[$key]['updated_at']     = datetime_table($rs->updated_at);
            $column[$key]['input']          = '<div id="text-'.$rs->order_id.'">
                                               '.$html_transports.'
                                                    <div class="input-group" >
                                                        <input type="text" class="form-control text-tracking" value="'.$rs->tracking_code.'" placeholder="ระบุ tracking">
                                                        <div class="input-group-append">
                                                            <button type="button" class="btn btn-primary btn-add-tracking" data-id="'.$id.'" data-row-id="text-'.$rs->order_id.'"><i class="fa fa-save" aria-hidden="true"></i></button>
                                                        </div>
                                                    </div>
                                                </div>';
            $i++;
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else{
            $input  = $this->input->post();
            
            if ( $type == "tracking" ) {
                $value['status']        = 5;
                $value['tracking_code'] = $input['tracking_code'];
                $value['transport_id']  = $input['transport_id'];
                
                $result                 = $this->orders_m->update(decode_id($input['id']), $value);
               
                $input_action['is_process']     = 0;
                $input_action['status']         = 5; 
                $input_action['order_id']       = decode_id($input['id']);
                $count_action                   = $this->orders_m->get_count_orders_status_action($input_action);
                if($count_action == 0): 
                    $status_action['order_id']       = decode_id($input['id']);
                    $status_action['status']         = 5;
                    $status_action['is_process']     = 0;
                    $status_action['title']          = null;
                    $status_action['process']        = null;
                    $this->set_orders_status_action($status_action);
                endif;
            } 
            if ( $result ) {
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }

    public function set_orders_status_action($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['status']        = $data['status'];
        $value['is_process']    = $data['is_process'];
        $value['title']         = $data['title'];
        $value['process']       = $data['process'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = $this->session->users['user_id'];

        $this->orders_m->insert_orders_status_action($value);
    }
}
