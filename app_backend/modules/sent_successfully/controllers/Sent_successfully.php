<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Sent_successfully extends MX_Controller 
{

    private $_title             = "จัดการส่งเรียบร้อยแล้ว";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับจัดการส่งเรียบร้อยแล้ว";
    private $_grpContent        = "sent_successfully";
    private $_requiredExport    = true;
    private $_permission;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }

        $this->payment = array('ชำระผ่านการโอน', 'จ่ายผ่าน PayPal', 'เก็บเงินปลายทาง');

        $this->load->library('ckeditor');
        $this->load->model("orders/orders_m");
        $this->load->model("status/status_m");
        $this->load->model("product_attributes/product_attributes_m");
        $this->load->model("info/info_m");
        $this->load->model("transports/transports_m");
        
    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            //'pdf'   => site_url("{$this->router->class}/pdf"),
        );
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        $action[2][]        = action_export_group($export);
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $input['recycle']       = 0;
        $input['active']        = 1;
        
        $data['status']         =  $this->status_m->get_rows($input)->result();
        $data['transports']     =  $this->transports_m->get_rows($input)->result();
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input                  = $this->input->post();
        $input['recycle']       = 0;
        if(!empty($input['transport_id'])):
            $transport_arr = array();
            foreach ($input['transport_id'] as $transport):
                array_push($transport_arr, $transport);
            endforeach;
            $input['transports']  = $transport_arr;
        endif; 
        
        $info               = $this->orders_m->get_rows($input);
        $infoCount          = $this->orders_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        $i = $input['start'] + 1;
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->order_id);
            $action                     = array();
            $action[1][]                = table_edit(site_url("orders/edit/{$id}"));
            $action[1][]                = table_print(site_url("orders/print/{$id}"), array(
                                            'title' => 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง',
                                            'icon'  => 'print'
                                            ));
            $action[1][]                = table_action($id, array(
                'title' => 'แก้ไขบิษัทจัดส่ง',
                'icon'  => 'truck',
                'class' => 'btn-edit-send'
                ));

            if($rs->is_invoice > 0):                                                                                     
                $action[1][]            = table_print(site_url("orders/invoice/{$id}"), array(
                                            'title' => 'ปริ้นใบกำกับภาษี',
                                            'icon'  => 'file-text-o'
                                            ));
            endif;
            if($rs->payment_type == 0):
                $payment = '<p class="m-badge m-badge--primary m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;
            if($rs->payment_type == 1):
                $payment = '<p class="m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;
            if($rs->payment_type == 2):
                $payment = '<p class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-check-circle"></i> '.$this->payment[$rs->payment_type].'</p>';
            endif;

            $status_action  = $this->orders_m->get_orders_status_action_by_order_id($rs->order_id)->result();
            
            $log_status     = table_status_action($status_action);

            $infoStatus     = $this->status_m->get_status_by_id($rs->status)->row();

            $tracking_code  = !empty($rs->tracking_code) ? '<span class="text-tracking-code" data-tracking="'.$rs->tracking_code.'">'.$rs->tracking_code.'</span>' :'';

            $transport      = $this->transports_m->get_transport_by_id($rs->transport_id)->row();
            $tracking_title = !empty($transport->title) ? '<span class="text-tracking-id" data-transport="'.$rs->transport_id.'">'.$transport->title.'</span>' :'';
            $reserve_code  ='';
            if(!empty($rs->reserve_code)):
                $reserve_code   = '<br>(Order ref : '.$rs->reserve_code.')';
            endif; 

            $param_['order_id'] = $rs->order_id;
            $orderObj           = $this->orders_m->get_rows($param_)->row();
            $salesObj           = $this->orders_m->get_orders_product_by_param($param_)->row();
            $quantity           = !empty($salesObj->quantity) ? $salesObj->quantity : 0;
            $money              = !empty($orderObj->discount) ? $orderObj->discount : 0;
            $vat                = (($money*7)/107);
            $product_price      = !empty($salesObj->product_price) ? $salesObj->product_price : 0;
            
            $column[$key]['DT_RowId']       = $id; 
            $column[$key]['name']           = $rs->name.' '.$rs->lasname;
            $column[$key]['order_code']     = $rs->order_code.$reserve_code.$log_status;
            $column[$key]['tel']            = $rs->tel;
            $column[$key]['sales']          = $quantity;
            $column[$key]['money']          = number_format($money,2);
            $column[$key]['vat']            = number_format($vat,2);
            $column[$key]['product_price']  = number_format($product_price,2);
            $column[$key]['cost']           = number_format(0,2);
            $column[$key]['profit']         = number_format(0,2); 
            $column[$key]['email']          = $rs->email;
            $column[$key]['tracking_code']  = $tracking_code;
            $column[$key]['tracking_title'] = $tracking_title;
            $column[$key]['invoice']        = 'IV'.substr($rs->order_code, 2,4 ).substr($rs->created_at,8,2).'/'.substr($rs->order_code, 6 ); 
            $column[$key]['discount']       = number_format($rs->discount);
            $column[$key]['payment_type']   = $payment;
            $column[$key]['status']         = !empty($infoStatus->title) ? $infoStatus->title : '';
            $column[$key]['created_at']     = datetime_table($rs->created_at);
            $column[$key]['updated_at']     = datetime_table($rs->updated_at);
            $column[$key]['action']         = Modules::run('utils/build_button_group', $action);
            $column[$key]['address']        = $rs->address.' ตำบล'.$rs->districts.' อำเภอ'.$rs->amphures.' จังหวัด'.$rs->provinces.'  รหัสไปรษณีย์ '.$rs->zip_code;
            $i++;
            
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }

        $input['order'][0]['column']    = 2;
        $input['order'][0]['dir']       = 'desc';

        $info           = $this->orders_m->get_rows_export($input);
        $fileName       = "รายการสั่งซื้อสินค้า";
        $sheetName      = "ข้อมูลสั่งซื้อ";
        $sheetTitle     = "ข้อมูลรายการสั่งซื้อสินค้า";
        
        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'ชื่อ-นามสกุล', 'width'=>50),
            'B' => array('data'=>'เลขพัสดุ','width'=>50),
            'C' => array('data'=>'ขนส่ง', 'width'=>16),            
            'D' => array('data'=>'OrderID', 'width'=>16),
            'E' => array('data'=>'เลขที่ใบกำกับภาษี', 'width'=>100),
            'F' => array('data'=>'ที่อยู่', 'width'=>100),
            'G' => array('data'=>'เบอร์โทร', 'width'=>16),
            'H' => array('data'=>'วิธีชำระเงิน', 'width'=>16),
            'I' => array('data'=>'จำนวนขายทั้งหมด (หน่วย)', 'width'=>16),
            'J' => array('data'=>'จำนวนเงิน (บ.)', 'width'=>16),  
            'K' => array('data'=>'ภาษีมูลค่าเพิ่ม (บ.)', 'width'=>16),
            'L' => array('data'=>'ราคาสินค้า (บ.)', 'width'=>16),
            'M' => array('data'=>'ต้นทุน (บ.)', 'width'=>16),
            'N' => array('data'=>'กำไร (บ.)', 'width'=>16),
            'O' => array('data'=>'เมื่อวันที่', 'width'=>16),
            'P' => array('data'=>'สถานะ', 'width'=>16), 
        );
        
        $fields = array(
            'A' => 'name',
            'B' => 'tracking_code',
            'C' => 'tracking_title',            
            'D' => 'order_code',
            'E' => 'invoice', 
            'F' => 'myaddress',
            'G' => 'tel',
            'H' => 'payment_type',
            'I' => 'sales', 
            'J' => 'money',
            'K' => 'vat', 
            'L' => 'product_price', 
            'M' => 'cost', 
            'N' => 'profit', 
            'O' => 'created_at', 
            'P' => 'status',  
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = !empty($row->{$field}) ? $row->{$field} : $field;
                if ( $field == 'created_at' || $field == 'updated_at' ):
                    $value = datetime_table ($value);
                endif;
                if ( $field == 'discount'):
                    $value = number_format($value);
                endif; 

                if ( $field == 'name'):
                    $value = $row->name.' '.$row->lasname;
                endif;

                if($field == 'status'):
                    $infoStatus = $this->status_m->get_status_by_id($row->status)->row();
                    $value      = !empty($infoStatus->title) ? $infoStatus->title : '';
                endif;
                if($field == 'payment_type'): 
                    $value      = $this->payment[$row->payment_type];
                endif;
                if($field == 'tracking_title'):
                    $transport  = $this->transports_m->get_transport_by_id($row->transport_id)->row();
                    $value      = !empty($transport->title) ? $transport->title :'';
                endif;

                if($field == 'invoice'):
                    $value  = 'IV'.substr($row->order_code, 2,4 ).substr($row->created_at,8,2).'/'.substr($row->order_code, 6 ); 
                endif;

                $param_['order_id'] = $row->order_id;
                $orderObj           = $this->orders_m->get_rows($param_)->row();
                $salesObj           = $this->orders_m->get_orders_product_by_param($param_)->row(); 
                $quantity           = !empty($salesObj->quantity) ? $salesObj->quantity : 0;
                $money              = !empty($orderObj->discount) ? $orderObj->discount : 0;
                $vat                = (($money*7)/107);
                $product_price      = !empty($salesObj->product_price) ? $salesObj->product_price : 0; 

                if($field == 'sales'):
                    $value = $quantity;
                endif;

                if($field == 'money'):
                    $value = ($row->payment_type == 0) ? 0 : number_format($money,2);
                endif;

                if($field == 'vat'):
                    $value = number_format($vat,2);
                endif;

                if($field == 'product_price'):
                    $value = number_format($product_price,2);
                endif;

                if($field == 'cost'):
                    $value = number_format(0,2);
                endif;

                if($field == 'profit'):
                    $value = number_format(0,2);
                endif;

                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->orders_m->get_rows($input);
        $data['info']   = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "orders.pdf";
        $pathFile = "uploads/pdf/orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
}
