<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stocks_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_stock_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        if (isset($param['attributes_option'])):
            $this->db->where('a.attributes_option', $param['attributes_option']);
        endif;

        $query = $this->db
                        ->select('a.*')
                        ->from('stocks a')
                        ->get();
        return $query;
    }

    public function get_stock_by_param($param) 
    {
        
        if (!empty($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (!empty($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        if (!empty($param['attributes_option'])):
            foreach ($param['attributes_option'] as $item):
                $this->db->where('FIND_IN_SET('.$item.', a.attributes_option)'); 
            endforeach; 
        endif;

        $query = $this->db
                        ->select('a.*')
                        ->from('stocks a')
                        ->get();
        return $query;
    }

    public function get_stock_detail_by_id($param) 
    {
        
        if (isset($param['product_id'])):
            $this->db->where('a.product_id', $param['product_id']);
        endif;

        if (isset($param['stock_id'])):
            $this->db->where('a.stock_id', $param['stock_id']);
        endif;

        $query = $this->db
                        ->select('a.*')
                        ->from('stocks_detail a')
                        ->get();
        return $query;
    }

    public function insert_stock($value) 
    {
        $this->db->insert('stocks', $value);
        return $this->db->insert_id();
    }

    public function insert_stock_batch($value) 
    {
        return $this->db->insert_batch('stocks', $value);
    }

    public function insert_stocks_detail($value) 
    {
        return $this->db->insert_batch('stocks_detail', $value);
    }

    public function update($id, $value)
    {
        $query = $this->db
                        ->where('stock_id', $id)
                        ->update('stocks', $value);
        return $query;
    }

    public function delete_stocks($id) 
    {
        $this->db->where('product_id', $id);
        return $this->db->delete('stocks');
    }

    public function delete_stocks_detail($id) 
    {
        $this->db->where('product_id', $id);
        return $this->db->delete('stocks_detail');
    }

}
