<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_attributes_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( !empty($param['length']) && !empty($param['start'])):
            $this->db->limit($param['length'], $param['start']);
        endif;
        
        $query = $this->db
                        ->select('a.*')
                        ->from('product_attributes a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('product_attributes a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.excerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['product_attribute_id']) ) 
            $this->db->where('a.product_attribute_id', $param['product_attribute_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }

    public function get_product_attributes()
    {
        $this->db
                ->order_by('parent_id', 'asc')
                ->order_by('order', 'asc')
                ->order_by('product_attribute_id', 'asc'); 
        $query = $this->db
                        ->from('product_attributes a')
                        ->select('a.product_attribute_id, a.title, a.parent_id, a.order')
                        ->where('a.recycle', 0)
                        ->get()
                        ->result_array();
        return $query;
    }

    public function get_product_attributes_by_in($id)
    {
        $this->db->where_in('product_attribute_id', $id);
        
        $query = $this->db
                        ->from('product_attributes a')
                        ->select('a.product_attribute_id, a.title, a.parent_id, a.order')
                        ->where('a.recycle', 0)
                        ->get();
        return $query;
    }

    public function get_product_attributes_by_parent_id($param)
    {
        $this->db
                ->order_by('parent_id', 'asc')
                ->order_by('order', 'asc')
                ->order_by('product_attribute_id', 'asc'); 
        $query = $this->db
                        ->from('product_attributes a')
                        ->select('a.product_attribute_id, a.title, a.parent_id, a.order')
                        ->where('a.recycle', $param['recycle'])
                        ->where('a.active', $param['active'])
                        ->where('a.parent_id', 0)
                        ->get()->result();
        
        if(!empty($query)):
            foreach($query as $item):
                $item->parent = $this->db
                                ->from('product_attributes a')
                                ->select('a.product_attribute_id, a.title, a.parent_id, a.order')
                                ->where('a.recycle', $param['recycle'])
                                ->where('a.active', $param['active'])
                                ->where('a.parent_id', $item->product_attribute_id)
                                ->order_by('parent_id', 'asc')
                                ->order_by('order', 'asc')
                                ->order_by('product_attribute_id', 'asc')
                                ->get()->result();
            endforeach;
        endif;
                        
        return $query;
    }
    
    public function insert($value) {
        $this->db->insert('product_attributes', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('product_attribute_id', $id)
                        ->update('product_attributes', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('product_attribute_id', $id)
                        ->update('product_attributes', $value);
        return $query;
    }    

}
