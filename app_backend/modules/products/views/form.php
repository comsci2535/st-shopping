<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="recommend"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> สินค้าอื่นๆ
                            <span></span>
                        </label>&nbsp&nbsp&nbsp
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->sale) &&  $info->sale=='1'){ echo "checked";}?> type="checkbox" name="sale" valus="1"> สินค้าขายดี
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ <span class="m--font-danger">*</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" placeholder="ระบุหัวข้อ" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug <span class="m--font-danger">*</span></label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" placeholder="ระบุ" required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="categorie_id">ประเภทสินค้า <span class="m--font-danger">*</span></label>
                    <div class="col-sm-7">
                        <select id="categorie_id" name="categorie_id[]" class="form-control m-input select2" required>
                            <option value="">เลือก</option>
                            <?php
                            if(!empty($infoCategorie)):
                                foreach($infoCategorie as $item):
                            ?>
                                <optgroup label="<?=$item->title;?>">
                            <?php
                                if(!empty($item->parent)):
                                    foreach($item->parent as $parent):
                                    $selected = '';
                                    if($categorie->categorie_id == $parent->categorie_id):
                                        $selected = 'selected';
                                    else:
                                        $selected = '';
                                    endif;
                            ?>
                                    <option value="<?=$parent->categorie_id;?>" <?=$selected?>><?=$parent->title;?></option>
                            <?php
                                 endforeach;
                                endif;
                            ?>
                                </optgroup>
                            <?php
                                endforeach;
                            endif;
                            ?>
                            
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt">รายละเอียดย่อ <span class="m--font-danger">*</span></label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="10" class="form-control" id="excerpt" placeholder="ระบุ"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control summernote" id="detail" placeholder="ระบุ"><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รูปหลัก</label>
                    <div class="col-sm-6">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>รูปภาพ</th>
                                    <th>เลือกรูป</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="tr-img-1">
                                    <td class="text-center">
                                        <img id="text-file" class="img-thumbnail" src="<?php echo !empty($info->file) ? $this->config->item('root_url').$info->file : '';?>" onerror="this.src='<?=$this->config->item('root_url').'images/no-image.png';?>'" style="width:150px">
                                    </td>
                                    <td>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="prodcut_file" data-img="text-file">
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                            <input type="hidden" name="outProductFile" value="<?php echo !empty($info->file) ? $info->file : '';?>">
                                            <small class="m--font-danger" style="padding: 5px;" >ขนาดรูปต้องกว้าง ยาว 800 x 800 px ไซต์ไม่เกิน 300 Kb นามสกุลไฟล์ jpg jpeg png เท่านั้น</small>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_qty">คุณสมบัติสินค้า</label>
                    <div class="col-sm-8">
                        <div id="display-attributes">
                            <?php
                            if(empty($stocks)):
                            ?>
                                <div id="display-bok-attributes-1" class="form-group m-form__group bok-attributes">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="attributes_loop[]" value="1">
                                            <div class="col-sm-12">
                                                <input value="" type="text" class="form-control m-input " data-type="currency" name="product_price_true[]" id="" placeholder="ระบุราคา(บาท)" required>
                                                <p>ราคา(บาท)</p>
                                            </div>
                                            <div class="col-sm-12">
                                                <input value="" type="text" class="form-control m-input " data-type="currency" name="product_price[]" id="" placeholder="ระบุราคาขาย(บาท)" required>
                                                <p>ราคาขาย(บาท)</p>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" 
                                                    data-row="display-bok-attributes-1"
                                                    class="form-control m-input check-product-unlimit" 
                                                    value="1" > ไม่จำกัดจำนวน
                                                    <span></span>
                                                </label>
                                                <input type="hidden" class="text-product-unlimit" name="product_unlimit[]" value="0">
                                            </div>
                                            <div class="col-sm-12">
                                                <input value="0" 
                                                type="text" 
                                                class="form-control m-input amount text-input-stock-qty" 
                                                name="product_qty[]" 
                                                id="" 
                                                placeholder="ระบุจำนวน"  
                                                required>
                                                <p class="text-input-stock-qty">จำนวน</p>
                                            </div>
                                            <div class="col-md-12">
                                                <img id="text-img-file-1" class="img-thumbnail" src="" onerror="this.src='<?=$this->config->item('root_url').'images/no-image.png';?>'" style="width:150px">
                                            </div>
                                            <div class="col-md-12" style="margin-top:10px">
                                                <div class="col-md-12">
                                                    <input type="file" class="custom-file-input" name="file_1" data-img="text-img-file-1">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                    <small class="m--font-danger" style="padding: 5px;" >ขนาดรูปต้องกว้าง ยาว 800 x 800 px ไซต์ไม่เกิน 300 Kb นามสกุลไฟล์ jpg jpeg png เท่านั้น</small>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <p style="margin-top:25px">
                                                    <button type="button" class="btn btn-success btn-sm btn-product-attributes-add" data-box="display-bok-attributes-1">
                                                        <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มสินค้า
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm btn-product-attributes-delete d-none" data-box="display-bok-attributes-1">
                                                        <i class="fa fa-times" aria-hidden="true"></i> ลบสินค้า
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            if(!empty($infoAttributes)):
                                                foreach($infoAttributes as $key => $h_item):
                                            ?>
                                            <div class="m-checkbox-inline" data-row="1">
                                                <p><i class="fa fa-chevron-circle-right"></i> <strong><?php echo !empty($h_item->title) ? $h_item->title : '';?></strong></p>
                                                <input type="hidden" class="text-attributes-checkbox" name="attributes_h[0][]" value="<?=$h_item->product_attribute_id?>">
                                                <?php
                                                    if(!empty($h_item->parent)):
                                                        foreach($h_item->parent as $l_item):
                                                    ?>
                                                        <label class="m-checkbox">
                                                            <input type="checkbox" 
                                                            data-id="<?=$h_item->product_attribute_id;?>" 
                                                            class="text-attributes-line-checkbox" 
                                                            name="attributes_line[0][]" 
                                                            data-row ="1"
                                                            value="<?php echo !empty($l_item->product_attribute_id) ? $l_item->product_attribute_id : '';?>"> 
                                                            <?php echo !empty($l_item->title) ? $l_item->title : '';?>
                                                            <span></span>
                                                        </label>
                                                        <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                    <hr>
                                            </div>
                                            <?php
                                                endforeach;
                                            endif;
                                            ?>
                                            
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            else:
                                $n = 1;
                                foreach($stocks as $key_h => $item):
                                   
                                    $attributes_option_arr = array();
                                    if(!empty($item->attributes_option)){
                                        $attributes_option_arr = explode(',', $item->attributes_option);
                                    }
                            ?>  
                           
                                <div id="display-bok-attributes-<?=$n?>" class="form-group m-form__group bok-attributes">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="attributes_loop[]" value="1">
                                            <div class="col-sm-12">
                                                <input value="<?php echo !empty($item->product_price_true) ? number_format($item->product_price_true) : 0;?>" type="text" class="form-control m-input " data-type="currency" name="product_price_true[]" id="" placeholder="ระบุราคา(บาท)" required>
                                                <p>ราคา(บาท)</p>
                                            </div>
                                            <div class="col-sm-12">
                                                <input value="<?php echo !empty($item->product_price) ? number_format($item->product_price) : 0;?>" type="text" class="form-control m-input " data-type="currency" name="product_price[]" id="" placeholder="ระบุราคาขาย(บาท)" required>
                                                <p>ราคาขาย(บาท)</p>
                                            </div>
                                            <div class="col-sm-12">
                                                <label class="m-checkbox">
                                                    <input type="checkbox" 
                                                    data-row="display-bok-attributes-<?=$n?>" 
                                                    <?php if(!empty($item->is_limit) &&  $item->is_limit=='1'){ echo "checked";}?> 
                                                    class="form-control m-input check-product-unlimit" 
                                                    value="1" > ไม่จำกัดจำนวน
                                                    <span></span>
                                                </label>
                                                <input type="hidden" class="text-product-unlimit" name="product_unlimit[]" value="<?php echo !empty($item->is_limit) ? $item->is_limit : 0;?>">
                                            </div>
                                            <div class="col-sm-12">
                                                <input value="<?php echo !empty($item->stock_qty) ? $item->stock_qty : 0;?>" 
                                                type="text" 
                                                class="form-control m-input amount text-input-stock-qty" 
                                                name="product_qty[]" 
                                                id="" 
                                                placeholder="ระบุจำนวน"
                                                style="display:
                                                <?php if(!empty($item->is_limit) &&  $item->is_limit=='1'){ echo "none";}?>
                                                " required>
                                                <p class="text-input-stock-qty" 
                                                style="display:
                                                <?php if(!empty($item->is_limit) &&  $item->is_limit=='1'){ echo "none";}?>
                                                "
                                                >จำนวน</p>
                                            </div>
                                            <div class="col-md-12">
                                                <img id="text-img-file-<?=$n?>" class="img-thumbnail" src="<?php echo !empty($item->file_path) ? $this->config->item('root_url').$item->file_path : '';?>" onerror="this.src='<?=$this->config->item('root_url').'images/no-image.png';?>'" style="width:150px">
                                            </div>
                                            <div class="col-md-12" style="margin-top:10px">
                                                <div class="col-md-12">
                                                    <input type="hidden" class="custom-outfile-input" name="outfile_<?=$n?>" value="<?php echo !empty($item->file_path) ? $item->file_path : '';?>">
                                                    <input type="file" class="custom-file-input" name="file_<?=$n?>" data-img="text-img-file-<?=$n?>">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                    <small class="m--font-danger" >ขนาดรูปต้องกว้าง ยาว 800 x 800 px ไซต์ไม่เกิน 300 Kb นามสกุลไฟล์ jpg jpeg png เท่านั้น</small>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <p style="margin-top:25px">
                                                    <button type="button" class="btn btn-success btn-sm btn-product-attributes-add" data-box="display-bok-attributes-<?=$n?>">
                                                        <i class="fa fa-plus" aria-hidden="true"></i> เพิ่มสินค้า
                                                    </button>
                                                    <button type="button" class="btn btn-danger btn-sm btn-product-attributes-delete" data-box="display-bok-attributes-<?=$n?>">
                                                        <i class="fa fa-times" aria-hidden="true"></i> ลบสินค้า
                                                    </button>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            if(!empty($infoAttributes)):
                                                foreach($infoAttributes as $key => $h_item):
                                            ?>
                                            
                                            <div class="m-checkbox-inline" data-row="<?=$n?>">
                                                <p><i class="fa fa-chevron-circle-right"></i> <strong><?php echo !empty($h_item->title) ? $h_item->title : '';?></strong></p>
                                                <input type="hidden" class="text-attributes-checkbox" name="attributes_h[<?=$key_h?>][]" value="<?=$h_item->product_attribute_id?>">
                                                <?php
                                                   
                                                    if(!empty($h_item->parent)):
                                                        foreach($h_item->parent as $keys => $l_item):
                                                            $filterAttribute = $l_item->product_attribute_id;
                                                            // $newAttribute = array_filter($attributes_option_arr, function ($var) use ($filterAttribute) {
                                                            //     return ($var == $filterAttribute);
                                                            // });
                                                            // arr($filterAttribute);
                                                            // arr(in_array($filterAttribute,$attributes_option_arr));
                                                    ?>
                                                        <label class="m-checkbox">
                                                            <input type="checkbox" 
                                                                data-id="<?=$h_item->product_attribute_id;?>" 
                                                                data-row ="<?=$n?>"
                                                                <?php if(!empty(in_array($filterAttribute,$attributes_option_arr))){ echo "checked";}?> 
                                                                <?php //if(!empty(in_array($filterAttribute,$attributes_option_arr)) && $newAttribute[$key] == $l_item->product_attribute_id){ echo "checked";}?> 
                                                                class="text-attributes-line-checkbox" 
                                                                name="attributes_line[<?=$key_h?>][]" 
                                                                value="<?php echo !empty($l_item->product_attribute_id) ? $l_item->product_attribute_id : '';?>"> 
                                                                <?php echo !empty($l_item->title) ? $l_item->title : '';?>
                                                            <span></span>
                                                        </label>
                                                        <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                    <hr>
                                            </div>
                                            <?php
                                                endforeach;
                                            endif;
                                            ?>
                                            
                                        </div>
                                        
                                        <div class="col-sm-12">
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            <?php     
                                $n++;
                                endforeach;
                            ?>
                            <?php
                             endif;
                            ?>
                        </div>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" > <h5 class="block">ข้อมูล SEO</h5></label>
                    <div class="col-sm-7">
                       
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อเรื่อง (Title)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->meta_title) ? $info->meta_title : NULL ?>" type="text" id="" class="form-control" placeholder="ระบุ" name="meta_title">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำอธิบาย (Description)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="5"  class="form-control" name="meta_description" placeholder="ระบุ"><?php echo isset($info->meta_description) ? $info->meta_description : NULL ?></textarea>
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำหลัก (Keyword)</label>
                    <div class="col-sm-7">
                        <textarea class="form-control" rows="3"  class="form-control" name="meta_keyword" placeholder="ระบุ"><?php echo isset($info->meta_keyword) ? $info->meta_keyword : NULL ?></textarea>
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                        </div>
                    </div>
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="products">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->product_id) ? encode_id($info->product_id) : 0 ?>">
        <?php echo form_close() ?>
        <!--end::Form-->
    </div>
</div>




