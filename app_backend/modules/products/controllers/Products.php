<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Products extends MX_Controller {

    private $_title             = "ข้อมูลสินค้า";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับข้อมูลสินค้า";
    private $_grpContent        = "products";
    private $_requiredExport    = true;
    private $_permission;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }

        $this->load->library('ckeditor');
        $this->load->library('uploadfile_library');
        $this->load->model("products_m");
        $this->load->model("categories/categories_m");
        $this->load->model("product_attributes/product_attributes_m");
        $this->load->model("stocks/stocks_m");
        $this->load->model("products_deliverys/products_deliverys_m");
        
    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf'   => site_url("{$this->router->class}/pdf"),
        );
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        //$action[1][]      = action_filter();
        $action[1][]        = action_add(site_url("{$this->router->class}/create"));
        //$action[2][]      = action_export_group($export);
        $action[3][]        = action_trash_multi("{$this->router->class}/destroy");
        $action[3][]        = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        $info               = $this->products_m->get_rows($input);
        $infoCount          = $this->products_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {

            $manage = action_custom(site_url("products_deliverys/index/{$rs->product_id}"),'btn-primary','add','ค่าจัดส่งสินค้า','fa-plus','','');
            $count_deliverys = $this->products_deliverys_m->count_product_by_id($rs->product_id);
            if($count_deliverys > 0):
                $manage.= '<p style="color: green;padding: 7px;"><i class="fa fa-check"></i><small> ตั้งค่าส่งแล้ว</small></p>';
            else:
                $manage.= '<p style="color: red;padding: 7px;"><i class="flaticon-refresh"></i><small> รอจัดการค่าส่ง</small></p>';
            endif;

            $id                         = encode_id($rs->product_id);
            $action                     = array();
            $action[1][]                = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $action[1][]  = table_print(site_url("reviews/index/{$rs->product_id}"), array(
                'title' => 'รีวิวสินค้า',
                'icon'  => 'plus'
                ));
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['deliverys']  = $manage;
            $column[$key]['active']     = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action']     = Modules::run('utils/build_button_group', $action);
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create()
    {
        $this->load->module('template');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("{$this->router->class}/create"));

        $input['active']        = 1;
        $input['recycle']       = 0;
        $data['infoCategorie']  = $this->categories_m->get_categorie_by_parent_id($input);

        $inputAttribute['active']   = 1;
        $inputAttribute['recycle']  = 0;
        $data['infoAttributes']     = $this->product_attributes_m->get_product_attributes_by_parent_id($inputAttribute);
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage()
    {
        $input  = $this->input->post(null, true);
        $value  = $this->_build_data($input);
        // add data product
        $result = $this->products_m->insert($value);
        if ( $result ) {
            // add data product categorie
            $value_map      = $this->_build_map_data($result, $input);
            $this->products_m->insert_product_categorie_map($value_map);
            // add data product stock
            $value_stock = $this->_build_data_stock($result ,$input);
            if(!empty($value_stock)):
                $this->stocks_m->insert_stock_batch($value_stock);
            endif;

            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id="")
    {
        $this->load->module('template');
        
        $id                     = decode_id($id);
        $input['product_id']    = $id;
        $info                   = $this->products_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        
        $info                   = $info->row();
        $data['info']           = $info;
        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}/edit"));


        $input['active']        = 1;
        $input['recycle']       = 0;
        $data['infoCategorie']  = $this->categories_m->get_categorie_by_parent_id($input);

        $data['categorie']      = $this->products_m->get_product_categorie_map($id)->row();

        $data['product_img']    = $this->products_m->get_product_img_map($id)->result();

        $inputAttribute['active']   = 1;
        $inputAttribute['recycle']  = 0;
        $data['infoAttributes']     = $this->product_attributes_m->get_product_attributes_by_parent_id($inputAttribute);
        
        $inputStock['product_id']  = $id;
        $infoStocks =  $this->stocks_m->get_stock_by_id($inputStock)->result();
        $data['stocks'] = $infoStocks;
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update()
    {
        $input      = $this->input->post(null, true);  
        $id         = decode_id($input['id']);
        $value      = $this->_build_data($input);
        $result     = $this->products_m->update($id, $value);
        if ( $result ) {

            if($this->products_m->delete_product_categorie_map($id)):
                $value_map  = $this->_build_map_data($id, $input);
                $this->products_m->insert_product_categorie_map($value_map);
            endif;

            if($this->stocks_m->delete_stocks($id)):
               // add data product stock
               $value_stock = $this->_build_data_stock($id ,$input);
                if(!empty($value_stock)):
                    $this->stocks_m->insert_stock_batch($value_stock);
                endif;
            endif;

            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input)
    {
        
        $value['title']      = $input['title'];
        $value['slug']       = $input['slug'];
        $value['excerpt']    = $input['excerpt'];
        $value['detail']     = $input['detail'];
        $value['recommend']  = !empty($input['recommend']) ? 1 : 0;
        $value['sale']       = !empty($input['sale']) ? 1 : 0;
        
        // Add file form product
        $path                = 'products';
        $upload              = $this->uploadfile_library->do_uploadSetWH('prodcut_file',TRUE,$path);
        $file                = '';
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];  
            $outfile = $input['outProductFile'];
            if(isset($outfile)){
                $this->load->helper("file");
                @unlink($outfile);
            }
            $value['file']      = $file;
        }

        if($input['meta_title']!=""){
            $value['meta_title'] = $input['meta_title'];
        }else{
             $value['meta_title'] = $input['title'];
        }
        if($input['meta_description']!=""){
            $value['meta_description'] = $input['meta_description'];
        }else{
            $value['meta_description'] = $input['excerpt'];
        }
        if($input['meta_keyword']!=""){
            $value['meta_keyword'] = $input['meta_keyword'];
        }else{
             $value['meta_keyword'] = $input['title'];
        }
        
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }

        return $value;
    }

    private function _build_map_data($id , $input)
    {
        $value_arr = array();
        if(!empty($input['categorie_id'])):
            foreach($input['categorie_id'] as $item):
                array_push($value_arr, array(
                    'categorie_id' => $item
                    ,'product_id'  => $id
                ));
            endforeach;
        endif;
        
        return $value_arr;
    }

    private function _build_data_stock($id , $input)
    {
        $value_arr = array();
        if(!empty($input['attributes_loop'])):
            foreach($input['attributes_loop'] as $key => $item):

                $product_price      = !empty($input['product_price'][$key]) ? $input['product_price'][$key] : 0;
                $product_price      = str_replace(",","",$product_price);
                $product_price_true = !empty($input['product_price_true'][$key]) ? $input['product_price_true'][$key] : 0;
                $product_price_true = str_replace(",","",$product_price_true);
                $product_unlimit    = !empty($input['product_unlimit'][$key]) ? $input['product_unlimit'][$key] : 0;
                $product_qty        = !empty($input['product_qty'][$key]) ? str_replace(",","", $input['product_qty'][$key]) : 0;
                if($product_unlimit > 0){
                    $product_qty    = 0;
                }
                list($attributes_group, $attributes_option) = $this->_build_data_attributes($input, $key);

                $path   = 'products';
                $upload = $this->uploadfile_library->do_uploadSetWH('file_'.($key+1),TRUE,$path);
                $file_path = '';
                if(isset($upload['index'])){
                    $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                    $outfile = $input['outfile_'.($key+1)];
                    if(isset($outfile)){
                        $this->load->helper("file");
                        @unlink($outfile);
                    }
                    $file_path = $file;
                }else{
                    $file_path = $input['outfile_'.($key+1)];
                }
                
                $value_arr[] = array(
                    'product_id'            => $id,
                    'product_price'         => $product_price,
                    'product_price_true'    => $product_price_true,
                    'stock_qty'             => $product_qty,
                    'attributes_group'      => $attributes_group,
                    'attributes_option'     => $attributes_option,
                    'file_path'             => $file_path,
                    'is_limit'              => $product_unlimit,
                    'created_at'            => db_datetime_now(),
                    'created_by'            => $this->session->users['user_id'],
                    'updated_at'            => db_datetime_now(),
                    'updated_by'            => $this->session->users['user_id']
                    
                );
               
            endforeach;
            
        endif;

        // delete file form
        if(!empty($input['file_delete'])):
            foreach($input['file_delete'] as $files):
                if(isset($files)){
                    $this->load->helper("file");
                    @unlink($files);
                }
            endforeach;
        endif;

        return $value_arr;
    }

    private function _build_data_attributes($input, $key)
    { 
        if(!empty($input['attributes_h'][$key])):
            $attributes_group   = '';
            $attributes_option  = '';
            foreach($input['attributes_h'][$key] as $keys => $attributes):

                if(!empty($input['attributes_line'][$key][$keys])):

                    $attributes = '';
                    if(!empty($input['attributes_line'][$key][$keys])):
                        $infoattributes = $this->product_attributes_m->get_product_attributes_by_in($input['attributes_line'][$key][$keys])->row();
                        $attributes     = !empty($infoattributes->parent_id) ? $infoattributes->parent_id :'';
                        unset($infoattributes);
                    endif;
                    if($keys > 0):
                        $attributes_group   .= ','.$attributes;
                        $attributes_option  .= ','.$input['attributes_line'][$key][$keys];
                    else:
                        $attributes_group   .= $attributes;
                        $attributes_option  .= $input['attributes_line'][$key][$keys];
                    endif;
                endif;
            endforeach;
            return array($attributes_group, $attributes_option);
        endif;
    }

    private function _build_map_file($id, $input)
    {
        $file_arr = array();

        if ( $input['mode'] == 'create' ) {
            if(!empty($input['loop_id'])):
                foreach ($input['loop_id'] as $key => $size):
                    $path   = 'products';
                    $upload = $this->uploadfile_library->do_upload('file_'.($key+1),TRUE,$path);
                    $file_path = '';
                    if(isset($upload['index'])){
                        $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                        $file_path = $file;
                    }
                    
                    array_push($file_arr, array(
                        'product_id'    => $id,
                        'file_path'     => $file_path,
                        'created_at'    => db_datetime_now(),
                        'created_by'    => $this->session->users['user_id'],
                        'updated_at'    => db_datetime_now(),
                        'updated_by'    => $this->session->users['user_id'],
                    ));
                endforeach;   
            endif;
        }else{
            if(!empty($input['loop_id'])):
                foreach ($input['loop_id'] as $key => $size):
                    $path   = 'products';
                    $upload = $this->uploadfile_library->do_upload('file_'.($key+1),TRUE,$path);
                    $file_path = '';
                    if(isset($upload['index'])){
                        $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
                        $outfile = $input['outfile'][$key];
                        if(isset($outfile)){
                            $this->load->helper("file");
                            @unlink($outfile);
                        }
                        $file_path = $file;
                    }else{
                        $file_path = $input['outfile'][$key];
                    }
                    
                    array_push($file_arr, array(
                        'product_id'    => $id,
                        'file_path'     => $file_path,
                        'created_at'    => db_datetime_now(),
                        'created_by'    => $this->session->users['user_id'],
                        'updated_at'    => db_datetime_now(),
                        'updated_by'    => $this->session->users['user_id'],
                    ));
                endforeach;   
            endif;
        }
       
        return $file_arr;
    }
    
    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->products_m->get_rows($input);
        $fileName       = "products";
        $sheetName      = "Sheet name";
        $sheetTitle     = "Sheet title";
        
        $spreadsheet = new Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->products_m->get_rows($input);
        $data['info']   = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
        $mpdf       = new \Mpdf\Mpdf($mpdfConfig);
        $content    = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "products.pdf";
        $pathFile = "uploads/pdf/products/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash()
    {
        $this->load->module('template');
        
        // toobar
        $action[1][]            = action_list_view(site_url("{$this->router->class}"));
        $action[2][]            = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction']      = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input              = $this->input->post();
        $input['recycle']   = 1;
        $info               = $this->products_m->get_rows($input);
        $infoCount          = $this->products_m->get_count($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->product_id);
            $action                     = array();
            $action[1][]                = table_restore("{$this->router->class}/restore");         
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action']     = Modules::run('utils/build_toolbar', $action);
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 1;
                $value['recycle_at']    = $dateTime;
                $value['recycle_by']    = $this->session->users['user_id'];
                $result                 = $this->products_m->update_in($input['id'], $value);
                
                if ( $result ) {
                    $toastr['type']     = 'success';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
                } else {
                    $toastr['type']     = 'error';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 0;
                $result                 = $this->products_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 2;
                $result                 = $this->products_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result = false;
                if ( $type == "active" ) {
                    $value['active']    = $input['status'] == "true" ? 1 : 0;
                    $result             = $this->products_m->update_in($input['id'], $value);
                }
                
                if ( $result ) {
                    $toastr['type']     = 'success';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
                } else {
                    $toastr['type']     = 'error';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                }

            $data['success']    = $result;
            $data['toastr']     = $toastr;

        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path   = 'content';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}
		echo $picture;
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}
    
}
