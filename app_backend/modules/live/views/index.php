<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal  frm-main', 'method' => 'post')) ?>
        <div class="m-portlet__body">
             <div class="form-group m-form__group row m-form__group row">
                <label class="col-sm-2 col-form-label" > <h4 class="block">Live สด</h4></label>
                <div class="col-sm-7"></div>
            </div>  
           
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="title"></label>
                <div class="col-sm-7">
                    <label class="m-checkbox m-checkbox--brand">
                        <input <?php if(!empty($info['active']) &&  $info['active'] =='on'){ echo "checked";}?> type="checkbox" name="active" valus="1"> เปิด/ปิด
                        <span></span>
                    </label>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">Url Live</label>
                <div class="col-sm-8">
                    <textarea type="text" rows="10" id="" class="form-control" name="url_live"><?php echo isset($info['url_live']) ? html_entity_decode($info['url_live']) : NULL ?></textarea>
                </div>
            </div>
           
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                <div class="col-sm-8">
                    <textarea name="detail" rows="5" class="form-control summernote" id="detail" placeholder="ระบุ"><?php echo isset($info['detail']) ? $info['detail'] : NULL ?></textarea>
                </div>
            </div>          
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-primary pullleft">บันทึก</button>
                <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php echo form_close() ?>

    </div>  
</div>


