<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">ประเภทค่าส่ง</label>
                    <div class="col-sm-7">
                        <select id="delivery_id" name="delivery_id" class="form-control m-input select2" required>
                            <option value="">เลือก</option>
                            <?php
                            if(isset($deliverys)):
                                $selected = '';
                                foreach($deliverys as $item):
                                    if($info->delivery_id == $item->delivery_id):
                                        $selected = 'selected';
                                    else:
                                        $selected = '';
                                    endif;
                                    
                                ?>
                                    <option value="<?=$item->delivery_id;?>" <?=$selected?>><?=$item->title;?></option>
                                <?php
                                endforeach;
                            
                            endif;
                            ?>
                            
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="start_qty">จำนวนสินค้า</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->start_qty) ? $info->start_qty : NULL ?>" type="text" class="form-control m-input " name="start_qty" id="start_qty" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="end_qty">ถึงจำนวนสินค้า</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->end_qty) ? $info->end_qty : NULL ?>" type="text" class="form-control m-input " name="end_qty" id="end_qty" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="price">ราคา</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->price) ? $info->price : NULL ?>" type="text" class="form-control m-input " name="price" id="price" placeholder="ระบุ" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->is_item) &&  $info->is_item=='1'){ echo "checked";}?> type="checkbox" name="is_item" valus="1"> คำนวณต่อชิ้น
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="products_deliverys">
             <input type="hidden" class="form-control" name="product_id" id="product_id" value="<?php echo isset($info->product_id) ? $info->product_id : $product_id ?>">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->products_delivery_id) ? encode_id($info->products_delivery_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>
</div>





