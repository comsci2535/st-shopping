
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <input type="hidden" id="product_id" value="<?php echo !empty($product_id) ? $product_id : 0;?>">
            <form  role="form">
                <table id="data-list" class="table table-hover dataTable" width="100%">
                    <thead>
                        <tr>
                            <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                            <th>รูปแบบค่าส่ง</th>
                            <th>ราคา</th>
                            <th>อัตราช่วงสินค้า</th>
                            <th>สร้าง</th>
                            <th>แก้ไข</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </form>

        </div>
    </div>
</div>