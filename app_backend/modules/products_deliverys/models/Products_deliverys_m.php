<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products_deliverys_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*, b.title')
                        ->from('products_deliverys a')
                        ->join('deliverys b', 'a.delivery_id = b.delivery_id', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('products_deliverys a')
                        ->join('deliverys b', 'a.delivery_id = b.delivery_id', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['keyword'])
                    ->or_like('a.end_qty', $param['keyword'])
                    ->or_like('a.end_qty', $param['keyword'])
                    ->or_like('a.price', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['keyword'])
                    ->or_like('a.start_qty', $param['search']['value'])
                    ->or_like('a.end_qty', $param['search']['value'])
                    ->or_like('a.price', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.delivery_id";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.price";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['products_delivery_id']) ) 
            $this->db->where('a.products_delivery_id', $param['products_delivery_id']);

        if ( isset($param['product_id']) ) 
            $this->db->where('a.product_id', $param['product_id']);
            
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }

    public function count_product_by_id($product_id) 
    {
        $this->db->where('a.product_id', $product_id);
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        return $this->db->count_all_results('products_deliverys a');;
    }
    
    public function insert($value) {
        $this->db->insert('products_deliverys', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('products_delivery_id', $id)
                        ->update('products_deliverys', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('products_delivery_id', $id)
                        ->update('products_deliverys', $value);
        return $query;
    }    

}
