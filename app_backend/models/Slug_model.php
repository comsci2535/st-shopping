<?php

/**
 *
 */
class Slug_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getNameCount($name, $id = null, $db) {

		if ($id === null) {
			$query = $this->db->where('title', $name)
							  ->where('recycle !=', 2)
							  ->get($db);
		} else {
			$query = $this->db->where('title', $name)
				->where('id != ', $id)
				->where('recycle !=', 2)
				->get($db);
		}

		return $query->num_rows();

	}

	public function getSlugCount($name, $id = null, $db) {

		if ($id === null) {
			$query = $this->db->where('slug', $name)
							  ->where('recycle !=', 2)
							  ->get($db);
		} else {
			$query = $this->db->where('slug', $name)
				->where('id != ', $id)
				->where('recycle !=', 2)
				->get($db);
		}

		return $query->num_rows();

	}
}

?>